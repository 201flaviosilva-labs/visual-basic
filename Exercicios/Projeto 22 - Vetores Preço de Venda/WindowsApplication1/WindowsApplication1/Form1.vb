﻿Public Class Form1
    Dim dados(5) As Double
    Dim n As Double = 1
    Dim margem As Double
    Dim final(5) As Double
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        dados(n) = TextBox1.Text
        ListBox1.Items.Add(dados(n))
        If n = 5 Then
            Button1.Enabled = False
            Button2.Enabled = True
        End If
        n += 1
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Dim count As Double
        margem = TextBox2.Text
        For count = 1 To 5
            final(count) = dados(count) * margem
            ListBox2.Items.Add(final(count))
        Next
        Button3.Enabled = True

    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Dim temp As Integer
        ListBox2.Items.Clear()

        For i = 0 To 9
            For j = i + 1 To 9
                If final(i) > final(j) Then
                    temp = final(i)
                    final(i) = final(j)
                    final(j) = temp
                End If
            Next
        Next

        For count = 0 To 9
            ListBox2.Items.Add(final(count))
        Next
    End Sub
End Class
