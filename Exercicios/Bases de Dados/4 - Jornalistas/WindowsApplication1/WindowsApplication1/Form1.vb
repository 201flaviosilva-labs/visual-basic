﻿Public Class Form1

    Private Sub Indentificacao_Dos_JornalistasBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Indentificacao_Dos_JornalistasBindingNavigatorSaveItem.Click
        Me.Validate()
        Me.Indentificacao_Dos_JornalistasBindingSource.EndEdit()
        Me.TableAdapterManager.UpdateAll(Me._MyDatabase_1DataSet)

    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the '_MyDatabase_1DataSet.Indentificacao_Dos_Jornalistas' table. You can move, or remove it, as needed.
        Me.Indentificacao_Dos_JornalistasTableAdapter.Fill(Me._MyDatabase_1DataSet.Indentificacao_Dos_Jornalistas)
        PictureBox_FotoJornalista.Image = Image.FromFile("F:\12º J\LP\4 - Jornalistas\Fotos\" & FotografiaTextBox.Text)
    End Sub

    Private Sub mostrarImg()
        On Error GoTo abc
        If Not IsNothing(FotografiaTextBox.Text) Then
            PictureBox_FotoJornalista.Image = Image.FromFile("F:\12º J\LP\4 - Jornalistas\Fotos\" & FotografiaTextBox.Text)
        End If
        Exit Sub
abc:
        Resume Next
    End Sub

    Private Sub BindingNavigatorMovePreviousItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorMovePreviousItem.Click
        Call mostrarImg()
    End Sub

    Private Sub BindingNavigatorMoveNextItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorMoveNextItem.Click
        Call mostrarImg()
    End Sub

    Private Sub FotografiaTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles FotografiaTextBox.TextChanged
        Call mostrarImg()
    End Sub
End Class
