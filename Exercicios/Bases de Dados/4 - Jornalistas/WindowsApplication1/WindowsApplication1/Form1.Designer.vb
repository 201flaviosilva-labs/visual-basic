﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim NomeLabel As System.Windows.Forms.Label
        Dim FotografiaLabel As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me._MyDatabase_1DataSet = New WindowsApplication1._MyDatabase_1DataSet()
        Me.Indentificacao_Dos_JornalistasBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Indentificacao_Dos_JornalistasTableAdapter = New WindowsApplication1._MyDatabase_1DataSetTableAdapters.Indentificacao_Dos_JornalistasTableAdapter()
        Me.TableAdapterManager = New WindowsApplication1._MyDatabase_1DataSetTableAdapters.TableAdapterManager()
        Me.Indentificacao_Dos_JornalistasBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorAddNewItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorCountItem = New System.Windows.Forms.ToolStripLabel()
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveFirstItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMovePreviousItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorPositionItem = New System.Windows.Forms.ToolStripTextBox()
        Me.BindingNavigatorSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorMoveNextItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveLastItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.Indentificacao_Dos_JornalistasBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        Me.NomeTextBox = New System.Windows.Forms.TextBox()
        Me.FotografiaTextBox = New System.Windows.Forms.TextBox()
        Me.PictureBox_FotoJornalista = New System.Windows.Forms.PictureBox()
        NomeLabel = New System.Windows.Forms.Label()
        FotografiaLabel = New System.Windows.Forms.Label()
        CType(Me._MyDatabase_1DataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Indentificacao_Dos_JornalistasBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Indentificacao_Dos_JornalistasBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Indentificacao_Dos_JornalistasBindingNavigator.SuspendLayout()
        CType(Me.PictureBox_FotoJornalista, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'NomeLabel
        '
        NomeLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        NomeLabel.Font = New System.Drawing.Font("Consolas", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        NomeLabel.Location = New System.Drawing.Point(12, 44)
        NomeLabel.Name = "NomeLabel"
        NomeLabel.Size = New System.Drawing.Size(128, 29)
        NomeLabel.TabIndex = 1
        NomeLabel.Text = "Nome:"
        '
        'FotografiaLabel
        '
        FotografiaLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        FotografiaLabel.Font = New System.Drawing.Font("Consolas", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        FotografiaLabel.Location = New System.Drawing.Point(12, 260)
        FotografiaLabel.Name = "FotografiaLabel"
        FotografiaLabel.Size = New System.Drawing.Size(128, 30)
        FotografiaLabel.TabIndex = 3
        FotografiaLabel.Text = "Fotografia:"
        '
        '_MyDatabase_1DataSet
        '
        Me._MyDatabase_1DataSet.DataSetName = "_MyDatabase_1DataSet"
        Me._MyDatabase_1DataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Indentificacao_Dos_JornalistasBindingSource
        '
        Me.Indentificacao_Dos_JornalistasBindingSource.DataMember = "Indentificacao_Dos_Jornalistas"
        Me.Indentificacao_Dos_JornalistasBindingSource.DataSource = Me._MyDatabase_1DataSet
        '
        'Indentificacao_Dos_JornalistasTableAdapter
        '
        Me.Indentificacao_Dos_JornalistasTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.Indentificacao_Dos_JornalistasTableAdapter = Me.Indentificacao_Dos_JornalistasTableAdapter
        Me.TableAdapterManager.UpdateOrder = WindowsApplication1._MyDatabase_1DataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'Indentificacao_Dos_JornalistasBindingNavigator
        '
        Me.Indentificacao_Dos_JornalistasBindingNavigator.AddNewItem = Me.BindingNavigatorAddNewItem
        Me.Indentificacao_Dos_JornalistasBindingNavigator.BindingSource = Me.Indentificacao_Dos_JornalistasBindingSource
        Me.Indentificacao_Dos_JornalistasBindingNavigator.CountItem = Me.BindingNavigatorCountItem
        Me.Indentificacao_Dos_JornalistasBindingNavigator.DeleteItem = Me.BindingNavigatorDeleteItem
        Me.Indentificacao_Dos_JornalistasBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorMoveFirstItem, Me.BindingNavigatorMovePreviousItem, Me.BindingNavigatorSeparator, Me.BindingNavigatorPositionItem, Me.BindingNavigatorCountItem, Me.BindingNavigatorSeparator1, Me.BindingNavigatorMoveNextItem, Me.BindingNavigatorMoveLastItem, Me.BindingNavigatorSeparator2, Me.BindingNavigatorAddNewItem, Me.BindingNavigatorDeleteItem, Me.Indentificacao_Dos_JornalistasBindingNavigatorSaveItem})
        Me.Indentificacao_Dos_JornalistasBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.Indentificacao_Dos_JornalistasBindingNavigator.MoveFirstItem = Me.BindingNavigatorMoveFirstItem
        Me.Indentificacao_Dos_JornalistasBindingNavigator.MoveLastItem = Me.BindingNavigatorMoveLastItem
        Me.Indentificacao_Dos_JornalistasBindingNavigator.MoveNextItem = Me.BindingNavigatorMoveNextItem
        Me.Indentificacao_Dos_JornalistasBindingNavigator.MovePreviousItem = Me.BindingNavigatorMovePreviousItem
        Me.Indentificacao_Dos_JornalistasBindingNavigator.Name = "Indentificacao_Dos_JornalistasBindingNavigator"
        Me.Indentificacao_Dos_JornalistasBindingNavigator.PositionItem = Me.BindingNavigatorPositionItem
        Me.Indentificacao_Dos_JornalistasBindingNavigator.Size = New System.Drawing.Size(480, 25)
        Me.Indentificacao_Dos_JornalistasBindingNavigator.TabIndex = 0
        Me.Indentificacao_Dos_JornalistasBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorAddNewItem
        '
        Me.BindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorAddNewItem.Image = CType(resources.GetObject("BindingNavigatorAddNewItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorAddNewItem.Name = "BindingNavigatorAddNewItem"
        Me.BindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorAddNewItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorAddNewItem.Text = "Add new"
        '
        'BindingNavigatorCountItem
        '
        Me.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem"
        Me.BindingNavigatorCountItem.Size = New System.Drawing.Size(35, 22)
        Me.BindingNavigatorCountItem.Text = "of {0}"
        Me.BindingNavigatorCountItem.ToolTipText = "Total number of items"
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorDeleteItem.Text = "Delete"
        '
        'BindingNavigatorMoveFirstItem
        '
        Me.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveFirstItem.Image = CType(resources.GetObject("BindingNavigatorMoveFirstItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem"
        Me.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveFirstItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveFirstItem.Text = "Move first"
        '
        'BindingNavigatorMovePreviousItem
        '
        Me.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMovePreviousItem.Image = CType(resources.GetObject("BindingNavigatorMovePreviousItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem"
        Me.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMovePreviousItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMovePreviousItem.Text = "Move previous"
        '
        'BindingNavigatorSeparator
        '
        Me.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator"
        Me.BindingNavigatorSeparator.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorPositionItem
        '
        Me.BindingNavigatorPositionItem.AccessibleName = "Position"
        Me.BindingNavigatorPositionItem.AutoSize = False
        Me.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem"
        Me.BindingNavigatorPositionItem.Size = New System.Drawing.Size(50, 23)
        Me.BindingNavigatorPositionItem.Text = "0"
        Me.BindingNavigatorPositionItem.ToolTipText = "Current position"
        '
        'BindingNavigatorSeparator1
        '
        Me.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1"
        Me.BindingNavigatorSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorMoveNextItem
        '
        Me.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveNextItem.Image = CType(resources.GetObject("BindingNavigatorMoveNextItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem"
        Me.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveNextItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveNextItem.Text = "Move next"
        '
        'BindingNavigatorMoveLastItem
        '
        Me.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveLastItem.Image = CType(resources.GetObject("BindingNavigatorMoveLastItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem"
        Me.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveLastItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveLastItem.Text = "Move last"
        '
        'BindingNavigatorSeparator2
        '
        Me.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2"
        Me.BindingNavigatorSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'Indentificacao_Dos_JornalistasBindingNavigatorSaveItem
        '
        Me.Indentificacao_Dos_JornalistasBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.Indentificacao_Dos_JornalistasBindingNavigatorSaveItem.Image = CType(resources.GetObject("Indentificacao_Dos_JornalistasBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.Indentificacao_Dos_JornalistasBindingNavigatorSaveItem.Name = "Indentificacao_Dos_JornalistasBindingNavigatorSaveItem"
        Me.Indentificacao_Dos_JornalistasBindingNavigatorSaveItem.Size = New System.Drawing.Size(23, 22)
        Me.Indentificacao_Dos_JornalistasBindingNavigatorSaveItem.Text = "Save Data"
        '
        'NomeTextBox
        '
        Me.NomeTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Indentificacao_Dos_JornalistasBindingSource, "Nome", True))
        Me.NomeTextBox.Font = New System.Drawing.Font("Consolas", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NomeTextBox.Location = New System.Drawing.Point(146, 42)
        Me.NomeTextBox.Name = "NomeTextBox"
        Me.NomeTextBox.Size = New System.Drawing.Size(322, 30)
        Me.NomeTextBox.TabIndex = 2
        '
        'FotografiaTextBox
        '
        Me.FotografiaTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Indentificacao_Dos_JornalistasBindingSource, "Fotografia", True))
        Me.FotografiaTextBox.Font = New System.Drawing.Font("Consolas", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FotografiaTextBox.Location = New System.Drawing.Point(146, 258)
        Me.FotografiaTextBox.Name = "FotografiaTextBox"
        Me.FotografiaTextBox.Size = New System.Drawing.Size(322, 30)
        Me.FotografiaTextBox.TabIndex = 4
        '
        'PictureBox_FotoJornalista
        '
        Me.PictureBox_FotoJornalista.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox_FotoJornalista.Location = New System.Drawing.Point(146, 78)
        Me.PictureBox_FotoJornalista.Name = "PictureBox_FotoJornalista"
        Me.PictureBox_FotoJornalista.Size = New System.Drawing.Size(322, 174)
        Me.PictureBox_FotoJornalista.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox_FotoJornalista.TabIndex = 5
        Me.PictureBox_FotoJornalista.TabStop = False
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(480, 409)
        Me.Controls.Add(Me.PictureBox_FotoJornalista)
        Me.Controls.Add(FotografiaLabel)
        Me.Controls.Add(Me.FotografiaTextBox)
        Me.Controls.Add(NomeLabel)
        Me.Controls.Add(Me.NomeTextBox)
        Me.Controls.Add(Me.Indentificacao_Dos_JornalistasBindingNavigator)
        Me.Name = "Form1"
        Me.Text = "Form1"
        CType(Me._MyDatabase_1DataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Indentificacao_Dos_JornalistasBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Indentificacao_Dos_JornalistasBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Indentificacao_Dos_JornalistasBindingNavigator.ResumeLayout(False)
        Me.Indentificacao_Dos_JornalistasBindingNavigator.PerformLayout()
        CType(Me.PictureBox_FotoJornalista, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents _MyDatabase_1DataSet As WindowsApplication1._MyDatabase_1DataSet
    Friend WithEvents Indentificacao_Dos_JornalistasBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Indentificacao_Dos_JornalistasTableAdapter As WindowsApplication1._MyDatabase_1DataSetTableAdapters.Indentificacao_Dos_JornalistasTableAdapter
    Friend WithEvents TableAdapterManager As WindowsApplication1._MyDatabase_1DataSetTableAdapters.TableAdapterManager
    Friend WithEvents Indentificacao_Dos_JornalistasBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorAddNewItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorCountItem As System.Windows.Forms.ToolStripLabel
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMoveFirstItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMovePreviousItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorPositionItem As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents BindingNavigatorSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorMoveNextItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMoveLastItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents Indentificacao_Dos_JornalistasBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents NomeTextBox As System.Windows.Forms.TextBox
    Friend WithEvents FotografiaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents PictureBox_FotoJornalista As System.Windows.Forms.PictureBox

End Class
