﻿Public Class Form2

    Private Sub FornecedoresBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles FornecedoresBindingNavigatorSaveItem.Click
        Me.Validate()
        Me.FornecedoresBindingSource.EndEdit()
        Me.TableAdapterManager.UpdateAll(Me.EmpresaDataSet)

    End Sub

    Private Sub Form2_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'EmpresaDataSet.Artigos' table. You can move, or remove it, as needed.
        Me.ArtigosTableAdapter.Fill(Me.EmpresaDataSet.Artigos)
        'TODO: This line of code loads data into the 'EmpresaDataSet.Fornecedores' table. You can move, or remove it, as needed.
        Me.FornecedoresTableAdapter.Fill(Me.EmpresaDataSet.Fornecedores)

    End Sub
End Class