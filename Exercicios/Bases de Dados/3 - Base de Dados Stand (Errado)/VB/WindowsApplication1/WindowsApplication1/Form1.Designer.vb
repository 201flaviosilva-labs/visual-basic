﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Dim CodigoLabel As System.Windows.Forms.Label
        Dim MarcaLabel As System.Windows.Forms.Label
        Dim ModeloLabel As System.Windows.Forms.Label
        Dim AnoLabel As System.Windows.Forms.Label
        Dim EstadoLabel As System.Windows.Forms.Label
        Dim CombustivelLabel As System.Windows.Forms.Label
        Dim ImportadoLabel As System.Windows.Forms.Label
        Dim VendidoLabel As System.Windows.Forms.Label
        Dim PrecoLabel As System.Windows.Forms.Label
        Me.StandeDataSet = New WindowsApplication1.StandeDataSet()
        Me.T_ViaturaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.T_ViaturaTableAdapter = New WindowsApplication1.StandeDataSetTableAdapters.T_ViaturaTableAdapter()
        Me.TableAdapterManager = New WindowsApplication1.StandeDataSetTableAdapters.TableAdapterManager()
        Me.T_ViaturaBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorMoveFirstItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMovePreviousItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorPositionItem = New System.Windows.Forms.ToolStripTextBox()
        Me.BindingNavigatorCountItem = New System.Windows.Forms.ToolStripLabel()
        Me.BindingNavigatorSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorMoveNextItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveLastItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorAddNewItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton()
        Me.T_ViaturaBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        Me.CodigoTextBox = New System.Windows.Forms.TextBox()
        Me.MarcaComboBox = New System.Windows.Forms.ComboBox()
        Me.ModeloTextBox = New System.Windows.Forms.TextBox()
        Me.AnoComboBox = New System.Windows.Forms.ComboBox()
        Me.EstadoComboBox = New System.Windows.Forms.ComboBox()
        Me.CombustivelComboBox = New System.Windows.Forms.ComboBox()
        Me.ImportadoCheckBox = New System.Windows.Forms.CheckBox()
        Me.VendidoCheckBox = New System.Windows.Forms.CheckBox()
        Me.PrecoTextBox = New System.Windows.Forms.TextBox()
        CodigoLabel = New System.Windows.Forms.Label()
        MarcaLabel = New System.Windows.Forms.Label()
        ModeloLabel = New System.Windows.Forms.Label()
        AnoLabel = New System.Windows.Forms.Label()
        EstadoLabel = New System.Windows.Forms.Label()
        CombustivelLabel = New System.Windows.Forms.Label()
        ImportadoLabel = New System.Windows.Forms.Label()
        VendidoLabel = New System.Windows.Forms.Label()
        PrecoLabel = New System.Windows.Forms.Label()
        CType(Me.StandeDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.T_ViaturaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.T_ViaturaBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.T_ViaturaBindingNavigator.SuspendLayout()
        Me.SuspendLayout()
        '
        'StandeDataSet
        '
        Me.StandeDataSet.DataSetName = "StandeDataSet"
        Me.StandeDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'T_ViaturaBindingSource
        '
        Me.T_ViaturaBindingSource.DataMember = "T_Viatura"
        Me.T_ViaturaBindingSource.DataSource = Me.StandeDataSet
        '
        'T_ViaturaTableAdapter
        '
        Me.T_ViaturaTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.T_ViaturaTableAdapter = Me.T_ViaturaTableAdapter
        Me.TableAdapterManager.UpdateOrder = WindowsApplication1.StandeDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'T_ViaturaBindingNavigator
        '
        Me.T_ViaturaBindingNavigator.AddNewItem = Me.BindingNavigatorAddNewItem
        Me.T_ViaturaBindingNavigator.BindingSource = Me.T_ViaturaBindingSource
        Me.T_ViaturaBindingNavigator.CountItem = Me.BindingNavigatorCountItem
        Me.T_ViaturaBindingNavigator.DeleteItem = Me.BindingNavigatorDeleteItem
        Me.T_ViaturaBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorMoveFirstItem, Me.BindingNavigatorMovePreviousItem, Me.BindingNavigatorSeparator, Me.BindingNavigatorPositionItem, Me.BindingNavigatorCountItem, Me.BindingNavigatorSeparator1, Me.BindingNavigatorMoveNextItem, Me.BindingNavigatorMoveLastItem, Me.BindingNavigatorSeparator2, Me.BindingNavigatorAddNewItem, Me.BindingNavigatorDeleteItem, Me.T_ViaturaBindingNavigatorSaveItem})
        Me.T_ViaturaBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.T_ViaturaBindingNavigator.MoveFirstItem = Me.BindingNavigatorMoveFirstItem
        Me.T_ViaturaBindingNavigator.MoveLastItem = Me.BindingNavigatorMoveLastItem
        Me.T_ViaturaBindingNavigator.MoveNextItem = Me.BindingNavigatorMoveNextItem
        Me.T_ViaturaBindingNavigator.MovePreviousItem = Me.BindingNavigatorMovePreviousItem
        Me.T_ViaturaBindingNavigator.Name = "T_ViaturaBindingNavigator"
        Me.T_ViaturaBindingNavigator.PositionItem = Me.BindingNavigatorPositionItem
        Me.T_ViaturaBindingNavigator.Size = New System.Drawing.Size(478, 25)
        Me.T_ViaturaBindingNavigator.TabIndex = 0
        Me.T_ViaturaBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorMoveFirstItem
        '
        Me.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveFirstItem.Image = CType(resources.GetObject("BindingNavigatorMoveFirstItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem"
        Me.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveFirstItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveFirstItem.Text = "Move first"
        '
        'BindingNavigatorMovePreviousItem
        '
        Me.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMovePreviousItem.Image = CType(resources.GetObject("BindingNavigatorMovePreviousItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem"
        Me.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMovePreviousItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMovePreviousItem.Text = "Move previous"
        '
        'BindingNavigatorSeparator
        '
        Me.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator"
        Me.BindingNavigatorSeparator.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorPositionItem
        '
        Me.BindingNavigatorPositionItem.AccessibleName = "Position"
        Me.BindingNavigatorPositionItem.AutoSize = False
        Me.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem"
        Me.BindingNavigatorPositionItem.Size = New System.Drawing.Size(50, 23)
        Me.BindingNavigatorPositionItem.Text = "0"
        Me.BindingNavigatorPositionItem.ToolTipText = "Current position"
        '
        'BindingNavigatorCountItem
        '
        Me.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem"
        Me.BindingNavigatorCountItem.Size = New System.Drawing.Size(35, 22)
        Me.BindingNavigatorCountItem.Text = "of {0}"
        Me.BindingNavigatorCountItem.ToolTipText = "Total number of items"
        '
        'BindingNavigatorSeparator1
        '
        Me.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator"
        Me.BindingNavigatorSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorMoveNextItem
        '
        Me.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveNextItem.Image = CType(resources.GetObject("BindingNavigatorMoveNextItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem"
        Me.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveNextItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveNextItem.Text = "Move next"
        '
        'BindingNavigatorMoveLastItem
        '
        Me.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveLastItem.Image = CType(resources.GetObject("BindingNavigatorMoveLastItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem"
        Me.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveLastItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveLastItem.Text = "Move last"
        '
        'BindingNavigatorSeparator2
        '
        Me.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator"
        Me.BindingNavigatorSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorAddNewItem
        '
        Me.BindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorAddNewItem.Image = CType(resources.GetObject("BindingNavigatorAddNewItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorAddNewItem.Name = "BindingNavigatorAddNewItem"
        Me.BindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorAddNewItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorAddNewItem.Text = "Add new"
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorDeleteItem.Text = "Delete"
        '
        'T_ViaturaBindingNavigatorSaveItem
        '
        Me.T_ViaturaBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.T_ViaturaBindingNavigatorSaveItem.Image = CType(resources.GetObject("T_ViaturaBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.T_ViaturaBindingNavigatorSaveItem.Name = "T_ViaturaBindingNavigatorSaveItem"
        Me.T_ViaturaBindingNavigatorSaveItem.Size = New System.Drawing.Size(23, 22)
        Me.T_ViaturaBindingNavigatorSaveItem.Text = "Save Data"
        '
        'CodigoLabel
        '
        CodigoLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        CodigoLabel.Font = New System.Drawing.Font("Consolas", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CodigoLabel.Location = New System.Drawing.Point(16, 45)
        CodigoLabel.Name = "CodigoLabel"
        CodigoLabel.Size = New System.Drawing.Size(131, 26)
        CodigoLabel.TabIndex = 1
        CodigoLabel.Text = "Codigo:"
        '
        'CodigoTextBox
        '
        Me.CodigoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.T_ViaturaBindingSource, "Codigo", True))
        Me.CodigoTextBox.Font = New System.Drawing.Font("Consolas", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CodigoTextBox.Location = New System.Drawing.Point(153, 45)
        Me.CodigoTextBox.Name = "CodigoTextBox"
        Me.CodigoTextBox.Size = New System.Drawing.Size(286, 26)
        Me.CodigoTextBox.TabIndex = 2
        '
        'MarcaLabel
        '
        MarcaLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        MarcaLabel.Font = New System.Drawing.Font("Consolas", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        MarcaLabel.Location = New System.Drawing.Point(16, 74)
        MarcaLabel.Name = "MarcaLabel"
        MarcaLabel.Size = New System.Drawing.Size(131, 24)
        MarcaLabel.TabIndex = 3
        MarcaLabel.Text = "Marca:"
        '
        'MarcaComboBox
        '
        Me.MarcaComboBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.T_ViaturaBindingSource, "Marca", True))
        Me.MarcaComboBox.Font = New System.Drawing.Font("Consolas", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MarcaComboBox.FormattingEnabled = True
        Me.MarcaComboBox.Items.AddRange(New Object() {"BMW", "Mercedes", "Ford", "Tesla", "Jeep‎", "Kia‎", "Jaguar‎ "})
        Me.MarcaComboBox.Location = New System.Drawing.Point(153, 71)
        Me.MarcaComboBox.Name = "MarcaComboBox"
        Me.MarcaComboBox.Size = New System.Drawing.Size(286, 27)
        Me.MarcaComboBox.TabIndex = 4
        '
        'ModeloLabel
        '
        ModeloLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        ModeloLabel.Font = New System.Drawing.Font("Consolas", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ModeloLabel.Location = New System.Drawing.Point(16, 101)
        ModeloLabel.Name = "ModeloLabel"
        ModeloLabel.Size = New System.Drawing.Size(131, 23)
        ModeloLabel.TabIndex = 5
        ModeloLabel.Text = "Modelo:"
        '
        'ModeloTextBox
        '
        Me.ModeloTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.T_ViaturaBindingSource, "Modelo", True))
        Me.ModeloTextBox.Font = New System.Drawing.Font("Consolas", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ModeloTextBox.Location = New System.Drawing.Point(153, 98)
        Me.ModeloTextBox.Name = "ModeloTextBox"
        Me.ModeloTextBox.Size = New System.Drawing.Size(286, 26)
        Me.ModeloTextBox.TabIndex = 6
        '
        'AnoLabel
        '
        AnoLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        AnoLabel.Font = New System.Drawing.Font("Consolas", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        AnoLabel.Location = New System.Drawing.Point(16, 127)
        AnoLabel.Name = "AnoLabel"
        AnoLabel.Size = New System.Drawing.Size(131, 24)
        AnoLabel.TabIndex = 7
        AnoLabel.Text = "Ano:"
        '
        'AnoComboBox
        '
        Me.AnoComboBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.T_ViaturaBindingSource, "Ano", True))
        Me.AnoComboBox.Font = New System.Drawing.Font("Consolas", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.AnoComboBox.FormattingEnabled = True
        Me.AnoComboBox.Location = New System.Drawing.Point(153, 124)
        Me.AnoComboBox.Name = "AnoComboBox"
        Me.AnoComboBox.Size = New System.Drawing.Size(286, 27)
        Me.AnoComboBox.TabIndex = 8
        '
        'EstadoLabel
        '
        EstadoLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        EstadoLabel.Font = New System.Drawing.Font("Consolas", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        EstadoLabel.Location = New System.Drawing.Point(16, 154)
        EstadoLabel.Name = "EstadoLabel"
        EstadoLabel.Size = New System.Drawing.Size(131, 24)
        EstadoLabel.TabIndex = 9
        EstadoLabel.Text = "Estado:"
        '
        'EstadoComboBox
        '
        Me.EstadoComboBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.T_ViaturaBindingSource, "Estado", True))
        Me.EstadoComboBox.Font = New System.Drawing.Font("Consolas", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EstadoComboBox.FormattingEnabled = True
        Me.EstadoComboBox.Location = New System.Drawing.Point(153, 151)
        Me.EstadoComboBox.Name = "EstadoComboBox"
        Me.EstadoComboBox.Size = New System.Drawing.Size(286, 27)
        Me.EstadoComboBox.TabIndex = 10
        '
        'CombustivelLabel
        '
        CombustivelLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        CombustivelLabel.Font = New System.Drawing.Font("Consolas", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CombustivelLabel.Location = New System.Drawing.Point(16, 181)
        CombustivelLabel.Name = "CombustivelLabel"
        CombustivelLabel.Size = New System.Drawing.Size(131, 24)
        CombustivelLabel.TabIndex = 11
        CombustivelLabel.Text = "Combustivel:"
        '
        'CombustivelComboBox
        '
        Me.CombustivelComboBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.T_ViaturaBindingSource, "Combustivel", True))
        Me.CombustivelComboBox.Font = New System.Drawing.Font("Consolas", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CombustivelComboBox.FormattingEnabled = True
        Me.CombustivelComboBox.Location = New System.Drawing.Point(153, 178)
        Me.CombustivelComboBox.Name = "CombustivelComboBox"
        Me.CombustivelComboBox.Size = New System.Drawing.Size(286, 27)
        Me.CombustivelComboBox.TabIndex = 12
        '
        'ImportadoLabel
        '
        ImportadoLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        ImportadoLabel.Font = New System.Drawing.Font("Consolas", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ImportadoLabel.Location = New System.Drawing.Point(16, 210)
        ImportadoLabel.Name = "ImportadoLabel"
        ImportadoLabel.Size = New System.Drawing.Size(131, 19)
        ImportadoLabel.TabIndex = 13
        ImportadoLabel.Text = "Importado:"
        '
        'ImportadoCheckBox
        '
        Me.ImportadoCheckBox.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.T_ViaturaBindingSource, "Importado", True))
        Me.ImportadoCheckBox.Font = New System.Drawing.Font("Consolas", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ImportadoCheckBox.Location = New System.Drawing.Point(153, 205)
        Me.ImportadoCheckBox.Name = "ImportadoCheckBox"
        Me.ImportadoCheckBox.Size = New System.Drawing.Size(286, 24)
        Me.ImportadoCheckBox.TabIndex = 14
        Me.ImportadoCheckBox.Text = "CheckBox1"
        Me.ImportadoCheckBox.UseVisualStyleBackColor = True
        '
        'VendidoLabel
        '
        VendidoLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        VendidoLabel.Font = New System.Drawing.Font("Consolas", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        VendidoLabel.Location = New System.Drawing.Point(16, 240)
        VendidoLabel.Name = "VendidoLabel"
        VendidoLabel.Size = New System.Drawing.Size(131, 19)
        VendidoLabel.TabIndex = 15
        VendidoLabel.Text = "Vendido:"
        '
        'VendidoCheckBox
        '
        Me.VendidoCheckBox.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.T_ViaturaBindingSource, "Vendido", True))
        Me.VendidoCheckBox.Font = New System.Drawing.Font("Consolas", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.VendidoCheckBox.Location = New System.Drawing.Point(153, 235)
        Me.VendidoCheckBox.Name = "VendidoCheckBox"
        Me.VendidoCheckBox.Size = New System.Drawing.Size(286, 24)
        Me.VendidoCheckBox.TabIndex = 16
        Me.VendidoCheckBox.Text = "CheckBox1"
        Me.VendidoCheckBox.UseVisualStyleBackColor = True
        '
        'PrecoLabel
        '
        PrecoLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        PrecoLabel.Font = New System.Drawing.Font("Consolas", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        PrecoLabel.Location = New System.Drawing.Point(16, 268)
        PrecoLabel.Name = "PrecoLabel"
        PrecoLabel.Size = New System.Drawing.Size(131, 23)
        PrecoLabel.TabIndex = 17
        PrecoLabel.Text = "Preco:"
        '
        'PrecoTextBox
        '
        Me.PrecoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.T_ViaturaBindingSource, "Preco", True))
        Me.PrecoTextBox.Font = New System.Drawing.Font("Consolas", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PrecoTextBox.Location = New System.Drawing.Point(153, 265)
        Me.PrecoTextBox.Name = "PrecoTextBox"
        Me.PrecoTextBox.Size = New System.Drawing.Size(286, 26)
        Me.PrecoTextBox.TabIndex = 18
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(478, 344)
        Me.Controls.Add(CodigoLabel)
        Me.Controls.Add(Me.CodigoTextBox)
        Me.Controls.Add(MarcaLabel)
        Me.Controls.Add(Me.MarcaComboBox)
        Me.Controls.Add(ModeloLabel)
        Me.Controls.Add(Me.ModeloTextBox)
        Me.Controls.Add(AnoLabel)
        Me.Controls.Add(Me.AnoComboBox)
        Me.Controls.Add(EstadoLabel)
        Me.Controls.Add(Me.EstadoComboBox)
        Me.Controls.Add(CombustivelLabel)
        Me.Controls.Add(Me.CombustivelComboBox)
        Me.Controls.Add(ImportadoLabel)
        Me.Controls.Add(Me.ImportadoCheckBox)
        Me.Controls.Add(VendidoLabel)
        Me.Controls.Add(Me.VendidoCheckBox)
        Me.Controls.Add(PrecoLabel)
        Me.Controls.Add(Me.PrecoTextBox)
        Me.Controls.Add(Me.T_ViaturaBindingNavigator)
        Me.Name = "Form1"
        Me.Text = "Form1"
        CType(Me.StandeDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.T_ViaturaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.T_ViaturaBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.T_ViaturaBindingNavigator.ResumeLayout(False)
        Me.T_ViaturaBindingNavigator.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents StandeDataSet As WindowsApplication1.StandeDataSet
    Friend WithEvents T_ViaturaBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents T_ViaturaTableAdapter As WindowsApplication1.StandeDataSetTableAdapters.T_ViaturaTableAdapter
    Friend WithEvents TableAdapterManager As WindowsApplication1.StandeDataSetTableAdapters.TableAdapterManager
    Friend WithEvents T_ViaturaBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorAddNewItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorCountItem As System.Windows.Forms.ToolStripLabel
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMoveFirstItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMovePreviousItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorPositionItem As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents BindingNavigatorSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorMoveNextItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMoveLastItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents T_ViaturaBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents CodigoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents MarcaComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents ModeloTextBox As System.Windows.Forms.TextBox
    Friend WithEvents AnoComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents EstadoComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents CombustivelComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents ImportadoCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents VendidoCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents PrecoTextBox As System.Windows.Forms.TextBox

End Class
