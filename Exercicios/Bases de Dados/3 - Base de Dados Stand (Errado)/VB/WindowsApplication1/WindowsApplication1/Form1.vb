﻿Public Class Form1

    Private Sub T_ViaturaBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles T_ViaturaBindingNavigatorSaveItem.Click
        Me.Validate()
        Me.T_ViaturaBindingSource.EndEdit()
        Me.TableAdapterManager.UpdateAll(Me.StandeDataSet)

    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'StandeDataSet.T_Viatura' table. You can move, or remove it, as needed.
        Me.T_ViaturaTableAdapter.Fill(Me.StandeDataSet.T_Viatura)

    End Sub
End Class
