﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Dim NumeroLabel As System.Windows.Forms.Label
        Dim DataAdmissaoLabel As System.Windows.Forms.Label
        Dim MoradaLabel As System.Windows.Forms.Label
        Dim NomeLabel As System.Windows.Forms.Label
        Dim CodigoPostalLabel As System.Windows.Forms.Label
        Dim LocalidadeLabel As System.Windows.Forms.Label
        Dim TelefoneLabel As System.Windows.Forms.Label
        Dim EmailLabel As System.Windows.Forms.Label
        Dim ObservacaoLabel As System.Windows.Forms.Label
        Me.Coletividade_DesportivaDataSet = New WindowsApplication1.Coletividade_DesportivaDataSet()
        Me.T_sociosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.T_sociosTableAdapter = New WindowsApplication1.Coletividade_DesportivaDataSetTableAdapters.t_sociosTableAdapter()
        Me.TableAdapterManager = New WindowsApplication1.Coletividade_DesportivaDataSetTableAdapters.TableAdapterManager()
        Me.T_sociosBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorMoveFirstItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMovePreviousItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorPositionItem = New System.Windows.Forms.ToolStripTextBox()
        Me.BindingNavigatorCountItem = New System.Windows.Forms.ToolStripLabel()
        Me.BindingNavigatorSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorMoveNextItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveLastItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorAddNewItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton()
        Me.T_sociosBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        Me.NumeroTextBox = New System.Windows.Forms.TextBox()
        Me.DataAdmissaoDateTimePicker = New System.Windows.Forms.DateTimePicker()
        Me.MoradaTextBox = New System.Windows.Forms.TextBox()
        Me.NomeTextBox = New System.Windows.Forms.TextBox()
        Me.CodigoPostalTextBox = New System.Windows.Forms.TextBox()
        Me.LocalidadeTextBox = New System.Windows.Forms.TextBox()
        Me.TelefoneTextBox = New System.Windows.Forms.TextBox()
        Me.EmailTextBox = New System.Windows.Forms.TextBox()
        Me.ObservacaoTextBox = New System.Windows.Forms.TextBox()
        Me.Button1 = New System.Windows.Forms.Button()
        NumeroLabel = New System.Windows.Forms.Label()
        DataAdmissaoLabel = New System.Windows.Forms.Label()
        MoradaLabel = New System.Windows.Forms.Label()
        NomeLabel = New System.Windows.Forms.Label()
        CodigoPostalLabel = New System.Windows.Forms.Label()
        LocalidadeLabel = New System.Windows.Forms.Label()
        TelefoneLabel = New System.Windows.Forms.Label()
        EmailLabel = New System.Windows.Forms.Label()
        ObservacaoLabel = New System.Windows.Forms.Label()
        CType(Me.Coletividade_DesportivaDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.T_sociosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.T_sociosBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.T_sociosBindingNavigator.SuspendLayout()
        Me.SuspendLayout()
        '
        'Coletividade_DesportivaDataSet
        '
        Me.Coletividade_DesportivaDataSet.DataSetName = "Coletividade_DesportivaDataSet"
        Me.Coletividade_DesportivaDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'T_sociosBindingSource
        '
        Me.T_sociosBindingSource.DataMember = "t_socios"
        Me.T_sociosBindingSource.DataSource = Me.Coletividade_DesportivaDataSet
        '
        'T_sociosTableAdapter
        '
        Me.T_sociosTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.t_sociosTableAdapter = Me.T_sociosTableAdapter
        Me.TableAdapterManager.UpdateOrder = WindowsApplication1.Coletividade_DesportivaDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'T_sociosBindingNavigator
        '
        Me.T_sociosBindingNavigator.AddNewItem = Me.BindingNavigatorAddNewItem
        Me.T_sociosBindingNavigator.BindingSource = Me.T_sociosBindingSource
        Me.T_sociosBindingNavigator.CountItem = Me.BindingNavigatorCountItem
        Me.T_sociosBindingNavigator.DeleteItem = Me.BindingNavigatorDeleteItem
        Me.T_sociosBindingNavigator.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.T_sociosBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorMoveFirstItem, Me.BindingNavigatorMovePreviousItem, Me.BindingNavigatorSeparator, Me.BindingNavigatorPositionItem, Me.BindingNavigatorCountItem, Me.BindingNavigatorSeparator1, Me.BindingNavigatorMoveNextItem, Me.BindingNavigatorMoveLastItem, Me.BindingNavigatorSeparator2, Me.BindingNavigatorAddNewItem, Me.BindingNavigatorDeleteItem, Me.T_sociosBindingNavigatorSaveItem})
        Me.T_sociosBindingNavigator.Location = New System.Drawing.Point(0, 393)
        Me.T_sociosBindingNavigator.MoveFirstItem = Me.BindingNavigatorMoveFirstItem
        Me.T_sociosBindingNavigator.MoveLastItem = Me.BindingNavigatorMoveLastItem
        Me.T_sociosBindingNavigator.MoveNextItem = Me.BindingNavigatorMoveNextItem
        Me.T_sociosBindingNavigator.MovePreviousItem = Me.BindingNavigatorMovePreviousItem
        Me.T_sociosBindingNavigator.Name = "T_sociosBindingNavigator"
        Me.T_sociosBindingNavigator.PositionItem = Me.BindingNavigatorPositionItem
        Me.T_sociosBindingNavigator.Size = New System.Drawing.Size(427, 25)
        Me.T_sociosBindingNavigator.TabIndex = 0
        Me.T_sociosBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorMoveFirstItem
        '
        Me.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveFirstItem.Image = CType(resources.GetObject("BindingNavigatorMoveFirstItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem"
        Me.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveFirstItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveFirstItem.Text = "Move first"
        '
        'BindingNavigatorMovePreviousItem
        '
        Me.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMovePreviousItem.Image = CType(resources.GetObject("BindingNavigatorMovePreviousItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem"
        Me.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMovePreviousItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMovePreviousItem.Text = "Move previous"
        '
        'BindingNavigatorSeparator
        '
        Me.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator"
        Me.BindingNavigatorSeparator.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorPositionItem
        '
        Me.BindingNavigatorPositionItem.AccessibleName = "Position"
        Me.BindingNavigatorPositionItem.AutoSize = False
        Me.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem"
        Me.BindingNavigatorPositionItem.Size = New System.Drawing.Size(50, 23)
        Me.BindingNavigatorPositionItem.Text = "0"
        Me.BindingNavigatorPositionItem.ToolTipText = "Current position"
        '
        'BindingNavigatorCountItem
        '
        Me.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem"
        Me.BindingNavigatorCountItem.Size = New System.Drawing.Size(35, 22)
        Me.BindingNavigatorCountItem.Text = "of {0}"
        Me.BindingNavigatorCountItem.ToolTipText = "Total number of items"
        '
        'BindingNavigatorSeparator1
        '
        Me.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator"
        Me.BindingNavigatorSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorMoveNextItem
        '
        Me.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveNextItem.Image = CType(resources.GetObject("BindingNavigatorMoveNextItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem"
        Me.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveNextItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveNextItem.Text = "Move next"
        '
        'BindingNavigatorMoveLastItem
        '
        Me.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveLastItem.Image = CType(resources.GetObject("BindingNavigatorMoveLastItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem"
        Me.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveLastItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveLastItem.Text = "Move last"
        '
        'BindingNavigatorSeparator2
        '
        Me.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator"
        Me.BindingNavigatorSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorAddNewItem
        '
        Me.BindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorAddNewItem.Image = CType(resources.GetObject("BindingNavigatorAddNewItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorAddNewItem.Name = "BindingNavigatorAddNewItem"
        Me.BindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorAddNewItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorAddNewItem.Text = "Add new"
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorDeleteItem.Text = "Delete"
        '
        'T_sociosBindingNavigatorSaveItem
        '
        Me.T_sociosBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.T_sociosBindingNavigatorSaveItem.Image = CType(resources.GetObject("T_sociosBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.T_sociosBindingNavigatorSaveItem.Name = "T_sociosBindingNavigatorSaveItem"
        Me.T_sociosBindingNavigatorSaveItem.Size = New System.Drawing.Size(23, 22)
        Me.T_sociosBindingNavigatorSaveItem.Text = "Save Data"
        '
        'NumeroLabel
        '
        NumeroLabel.AutoSize = True
        NumeroLabel.Location = New System.Drawing.Point(21, 83)
        NumeroLabel.Name = "NumeroLabel"
        NumeroLabel.Size = New System.Drawing.Size(47, 13)
        NumeroLabel.TabIndex = 1
        NumeroLabel.Text = "Numero:"
        '
        'NumeroTextBox
        '
        Me.NumeroTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.T_sociosBindingSource, "Numero", True))
        Me.NumeroTextBox.Location = New System.Drawing.Point(74, 80)
        Me.NumeroTextBox.Name = "NumeroTextBox"
        Me.NumeroTextBox.Size = New System.Drawing.Size(63, 20)
        Me.NumeroTextBox.TabIndex = 2
        '
        'DataAdmissaoLabel
        '
        DataAdmissaoLabel.AutoSize = True
        DataAdmissaoLabel.Location = New System.Drawing.Point(21, 48)
        DataAdmissaoLabel.Name = "DataAdmissaoLabel"
        DataAdmissaoLabel.Size = New System.Drawing.Size(81, 13)
        DataAdmissaoLabel.TabIndex = 3
        DataAdmissaoLabel.Text = "Data Admissao:"
        '
        'DataAdmissaoDateTimePicker
        '
        Me.DataAdmissaoDateTimePicker.DataBindings.Add(New System.Windows.Forms.Binding("Value", Me.T_sociosBindingSource, "DataAdmissao", True))
        Me.DataAdmissaoDateTimePicker.Location = New System.Drawing.Point(108, 44)
        Me.DataAdmissaoDateTimePicker.Name = "DataAdmissaoDateTimePicker"
        Me.DataAdmissaoDateTimePicker.Size = New System.Drawing.Size(200, 20)
        Me.DataAdmissaoDateTimePicker.TabIndex = 4
        '
        'MoradaLabel
        '
        MoradaLabel.AutoSize = True
        MoradaLabel.Location = New System.Drawing.Point(21, 151)
        MoradaLabel.Name = "MoradaLabel"
        MoradaLabel.Size = New System.Drawing.Size(46, 13)
        MoradaLabel.TabIndex = 5
        MoradaLabel.Text = "Morada:"
        '
        'MoradaTextBox
        '
        Me.MoradaTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.T_sociosBindingSource, "Morada", True))
        Me.MoradaTextBox.Location = New System.Drawing.Point(73, 148)
        Me.MoradaTextBox.Name = "MoradaTextBox"
        Me.MoradaTextBox.Size = New System.Drawing.Size(335, 20)
        Me.MoradaTextBox.TabIndex = 6
        '
        'NomeLabel
        '
        NomeLabel.AutoSize = True
        NomeLabel.Location = New System.Drawing.Point(21, 119)
        NomeLabel.Name = "NomeLabel"
        NomeLabel.Size = New System.Drawing.Size(38, 13)
        NomeLabel.TabIndex = 7
        NomeLabel.Text = "Nome:"
        '
        'NomeTextBox
        '
        Me.NomeTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.T_sociosBindingSource, "Nome", True))
        Me.NomeTextBox.Location = New System.Drawing.Point(74, 116)
        Me.NomeTextBox.Name = "NomeTextBox"
        Me.NomeTextBox.Size = New System.Drawing.Size(334, 20)
        Me.NomeTextBox.TabIndex = 8
        '
        'CodigoPostalLabel
        '
        CodigoPostalLabel.AutoSize = True
        CodigoPostalLabel.Location = New System.Drawing.Point(21, 187)
        CodigoPostalLabel.Name = "CodigoPostalLabel"
        CodigoPostalLabel.Size = New System.Drawing.Size(72, 13)
        CodigoPostalLabel.TabIndex = 9
        CodigoPostalLabel.Text = "Código Postal"
        '
        'CodigoPostalTextBox
        '
        Me.CodigoPostalTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.T_sociosBindingSource, "CodigoPostal", True))
        Me.CodigoPostalTextBox.Location = New System.Drawing.Point(102, 184)
        Me.CodigoPostalTextBox.Name = "CodigoPostalTextBox"
        Me.CodigoPostalTextBox.Size = New System.Drawing.Size(89, 20)
        Me.CodigoPostalTextBox.TabIndex = 10
        '
        'LocalidadeLabel
        '
        LocalidadeLabel.AutoSize = True
        LocalidadeLabel.Location = New System.Drawing.Point(21, 222)
        LocalidadeLabel.Name = "LocalidadeLabel"
        LocalidadeLabel.Size = New System.Drawing.Size(62, 13)
        LocalidadeLabel.TabIndex = 11
        LocalidadeLabel.Text = "Localidade:"
        '
        'LocalidadeTextBox
        '
        Me.LocalidadeTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.T_sociosBindingSource, "Localidade", True))
        Me.LocalidadeTextBox.Location = New System.Drawing.Point(89, 219)
        Me.LocalidadeTextBox.Name = "LocalidadeTextBox"
        Me.LocalidadeTextBox.Size = New System.Drawing.Size(100, 20)
        Me.LocalidadeTextBox.TabIndex = 12
        '
        'TelefoneLabel
        '
        TelefoneLabel.AutoSize = True
        TelefoneLabel.Location = New System.Drawing.Point(21, 257)
        TelefoneLabel.Name = "TelefoneLabel"
        TelefoneLabel.Size = New System.Drawing.Size(52, 13)
        TelefoneLabel.TabIndex = 13
        TelefoneLabel.Text = "Telefone:"
        '
        'TelefoneTextBox
        '
        Me.TelefoneTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.T_sociosBindingSource, "Telefone", True))
        Me.TelefoneTextBox.Location = New System.Drawing.Point(79, 254)
        Me.TelefoneTextBox.Name = "TelefoneTextBox"
        Me.TelefoneTextBox.Size = New System.Drawing.Size(100, 20)
        Me.TelefoneTextBox.TabIndex = 14
        '
        'EmailLabel
        '
        EmailLabel.AutoSize = True
        EmailLabel.Location = New System.Drawing.Point(21, 289)
        EmailLabel.Name = "EmailLabel"
        EmailLabel.Size = New System.Drawing.Size(35, 13)
        EmailLabel.TabIndex = 15
        EmailLabel.Text = "Email:"
        '
        'EmailTextBox
        '
        Me.EmailTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.T_sociosBindingSource, "Email", True))
        Me.EmailTextBox.Location = New System.Drawing.Point(62, 286)
        Me.EmailTextBox.Name = "EmailTextBox"
        Me.EmailTextBox.Size = New System.Drawing.Size(346, 20)
        Me.EmailTextBox.TabIndex = 16
        '
        'ObservacaoLabel
        '
        ObservacaoLabel.AutoSize = True
        ObservacaoLabel.Location = New System.Drawing.Point(21, 326)
        ObservacaoLabel.Name = "ObservacaoLabel"
        ObservacaoLabel.Size = New System.Drawing.Size(68, 13)
        ObservacaoLabel.TabIndex = 17
        ObservacaoLabel.Text = "Observacao:"
        '
        'ObservacaoTextBox
        '
        Me.ObservacaoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.T_sociosBindingSource, "Observacao", True))
        Me.ObservacaoTextBox.Location = New System.Drawing.Point(95, 323)
        Me.ObservacaoTextBox.Name = "ObservacaoTextBox"
        Me.ObservacaoTextBox.Size = New System.Drawing.Size(313, 20)
        Me.ObservacaoTextBox.TabIndex = 18
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(300, 396)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(108, 22)
        Me.Button1.TabIndex = 19
        Me.Button1.Text = "Lista de Sócios"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(427, 418)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(ObservacaoLabel)
        Me.Controls.Add(Me.ObservacaoTextBox)
        Me.Controls.Add(EmailLabel)
        Me.Controls.Add(Me.EmailTextBox)
        Me.Controls.Add(TelefoneLabel)
        Me.Controls.Add(Me.TelefoneTextBox)
        Me.Controls.Add(LocalidadeLabel)
        Me.Controls.Add(Me.LocalidadeTextBox)
        Me.Controls.Add(CodigoPostalLabel)
        Me.Controls.Add(Me.CodigoPostalTextBox)
        Me.Controls.Add(NomeLabel)
        Me.Controls.Add(Me.NomeTextBox)
        Me.Controls.Add(MoradaLabel)
        Me.Controls.Add(Me.MoradaTextBox)
        Me.Controls.Add(DataAdmissaoLabel)
        Me.Controls.Add(Me.DataAdmissaoDateTimePicker)
        Me.Controls.Add(NumeroLabel)
        Me.Controls.Add(Me.NumeroTextBox)
        Me.Controls.Add(Me.T_sociosBindingNavigator)
        Me.Name = "Form1"
        Me.Text = "Código Postal"
        CType(Me.Coletividade_DesportivaDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.T_sociosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.T_sociosBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.T_sociosBindingNavigator.ResumeLayout(False)
        Me.T_sociosBindingNavigator.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Coletividade_DesportivaDataSet As WindowsApplication1.Coletividade_DesportivaDataSet
    Friend WithEvents T_sociosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents T_sociosTableAdapter As WindowsApplication1.Coletividade_DesportivaDataSetTableAdapters.t_sociosTableAdapter
    Friend WithEvents TableAdapterManager As WindowsApplication1.Coletividade_DesportivaDataSetTableAdapters.TableAdapterManager
    Friend WithEvents T_sociosBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorAddNewItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorCountItem As System.Windows.Forms.ToolStripLabel
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMoveFirstItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMovePreviousItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorPositionItem As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents BindingNavigatorSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorMoveNextItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMoveLastItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents T_sociosBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents NumeroTextBox As System.Windows.Forms.TextBox
    Friend WithEvents DataAdmissaoDateTimePicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents MoradaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents NomeTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CodigoPostalTextBox As System.Windows.Forms.TextBox
    Friend WithEvents LocalidadeTextBox As System.Windows.Forms.TextBox
    Friend WithEvents TelefoneTextBox As System.Windows.Forms.TextBox
    Friend WithEvents EmailTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ObservacaoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Button1 As System.Windows.Forms.Button

End Class
