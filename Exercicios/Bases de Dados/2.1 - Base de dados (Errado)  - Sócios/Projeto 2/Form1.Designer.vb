﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim NúmeroLabel As System.Windows.Forms.Label
        Dim Data_AdmissaoLabel As System.Windows.Forms.Label
        Dim NomeLabel As System.Windows.Forms.Label
        Dim MoradaLabel As System.Windows.Forms.Label
        Dim Codigo_PostalLabel As System.Windows.Forms.Label
        Dim LocalidadeLabel As System.Windows.Forms.Label
        Dim TelefoneLabel As System.Windows.Forms.Label
        Dim EmailLabel As System.Windows.Forms.Label
        Dim ObservacaoLabel As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me.BdDataSet = New Projeto_2.bdDataSet()
        Me.T_SOCIOSBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.T_SOCIOSTableAdapter = New Projeto_2.bdDataSetTableAdapters.T_SOCIOSTableAdapter()
        Me.TableAdapterManager = New Projeto_2.bdDataSetTableAdapters.TableAdapterManager()
        Me.T_SOCIOSBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorAddNewItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorCountItem = New System.Windows.Forms.ToolStripLabel()
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveFirstItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMovePreviousItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorPositionItem = New System.Windows.Forms.ToolStripTextBox()
        Me.BindingNavigatorSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorMoveNextItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveLastItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.T_SOCIOSBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        Me.NúmeroTextBox = New System.Windows.Forms.TextBox()
        Me.Data_AdmissaoDateTimePicker = New System.Windows.Forms.DateTimePicker()
        Me.NomeTextBox = New System.Windows.Forms.TextBox()
        Me.MoradaTextBox = New System.Windows.Forms.TextBox()
        Me.Codigo_PostalTextBox = New System.Windows.Forms.TextBox()
        Me.LocalidadeTextBox = New System.Windows.Forms.TextBox()
        Me.TelefoneTextBox = New System.Windows.Forms.TextBox()
        Me.EmailTextBox = New System.Windows.Forms.TextBox()
        Me.ObservacaoTextBox = New System.Windows.Forms.TextBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        NúmeroLabel = New System.Windows.Forms.Label()
        Data_AdmissaoLabel = New System.Windows.Forms.Label()
        NomeLabel = New System.Windows.Forms.Label()
        MoradaLabel = New System.Windows.Forms.Label()
        Codigo_PostalLabel = New System.Windows.Forms.Label()
        LocalidadeLabel = New System.Windows.Forms.Label()
        TelefoneLabel = New System.Windows.Forms.Label()
        EmailLabel = New System.Windows.Forms.Label()
        ObservacaoLabel = New System.Windows.Forms.Label()
        CType(Me.BdDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.T_SOCIOSBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.T_SOCIOSBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.T_SOCIOSBindingNavigator.SuspendLayout()
        Me.SuspendLayout()
        '
        'NúmeroLabel
        '
        NúmeroLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        NúmeroLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        NúmeroLabel.Location = New System.Drawing.Point(12, 26)
        NúmeroLabel.Name = "NúmeroLabel"
        NúmeroLabel.Size = New System.Drawing.Size(160, 29)
        NúmeroLabel.TabIndex = 1
        NúmeroLabel.Text = "Número:"
        '
        'Data_AdmissaoLabel
        '
        Data_AdmissaoLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Data_AdmissaoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Data_AdmissaoLabel.Location = New System.Drawing.Point(12, 76)
        Data_AdmissaoLabel.Name = "Data_AdmissaoLabel"
        Data_AdmissaoLabel.Size = New System.Drawing.Size(160, 29)
        Data_AdmissaoLabel.TabIndex = 3
        Data_AdmissaoLabel.Text = "Data Admissao:"
        '
        'NomeLabel
        '
        NomeLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        NomeLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        NomeLabel.Location = New System.Drawing.Point(12, 126)
        NomeLabel.Name = "NomeLabel"
        NomeLabel.Size = New System.Drawing.Size(160, 29)
        NomeLabel.TabIndex = 5
        NomeLabel.Text = "Nome:"
        '
        'MoradaLabel
        '
        MoradaLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        MoradaLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        MoradaLabel.Location = New System.Drawing.Point(12, 176)
        MoradaLabel.Name = "MoradaLabel"
        MoradaLabel.Size = New System.Drawing.Size(160, 29)
        MoradaLabel.TabIndex = 7
        MoradaLabel.Text = "Morada:"
        '
        'Codigo_PostalLabel
        '
        Codigo_PostalLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Codigo_PostalLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Codigo_PostalLabel.Location = New System.Drawing.Point(12, 226)
        Codigo_PostalLabel.Name = "Codigo_PostalLabel"
        Codigo_PostalLabel.Size = New System.Drawing.Size(160, 29)
        Codigo_PostalLabel.TabIndex = 9
        Codigo_PostalLabel.Text = "Codigo Postal:"
        '
        'LocalidadeLabel
        '
        LocalidadeLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        LocalidadeLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        LocalidadeLabel.Location = New System.Drawing.Point(12, 276)
        LocalidadeLabel.Name = "LocalidadeLabel"
        LocalidadeLabel.Size = New System.Drawing.Size(160, 29)
        LocalidadeLabel.TabIndex = 11
        LocalidadeLabel.Text = "Localidade:"
        '
        'TelefoneLabel
        '
        TelefoneLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        TelefoneLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        TelefoneLabel.Location = New System.Drawing.Point(12, 326)
        TelefoneLabel.Name = "TelefoneLabel"
        TelefoneLabel.Size = New System.Drawing.Size(160, 29)
        TelefoneLabel.TabIndex = 13
        TelefoneLabel.Text = "Telefone:"
        '
        'EmailLabel
        '
        EmailLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        EmailLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        EmailLabel.Location = New System.Drawing.Point(12, 376)
        EmailLabel.Name = "EmailLabel"
        EmailLabel.Size = New System.Drawing.Size(160, 29)
        EmailLabel.TabIndex = 15
        EmailLabel.Text = "Email:"
        '
        'ObservacaoLabel
        '
        ObservacaoLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        ObservacaoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ObservacaoLabel.Location = New System.Drawing.Point(12, 427)
        ObservacaoLabel.Name = "ObservacaoLabel"
        ObservacaoLabel.Size = New System.Drawing.Size(160, 29)
        ObservacaoLabel.TabIndex = 17
        ObservacaoLabel.Text = "Observacao:"
        '
        'BdDataSet
        '
        Me.BdDataSet.DataSetName = "bdDataSet"
        Me.BdDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'T_SOCIOSBindingSource
        '
        Me.T_SOCIOSBindingSource.DataMember = "T_SOCIOS"
        Me.T_SOCIOSBindingSource.DataSource = Me.BdDataSet
        '
        'T_SOCIOSTableAdapter
        '
        Me.T_SOCIOSTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.T_SOCIOSTableAdapter = Me.T_SOCIOSTableAdapter
        Me.TableAdapterManager.UpdateOrder = Projeto_2.bdDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'T_SOCIOSBindingNavigator
        '
        Me.T_SOCIOSBindingNavigator.AddNewItem = Me.BindingNavigatorAddNewItem
        Me.T_SOCIOSBindingNavigator.BindingSource = Me.T_SOCIOSBindingSource
        Me.T_SOCIOSBindingNavigator.CountItem = Me.BindingNavigatorCountItem
        Me.T_SOCIOSBindingNavigator.DeleteItem = Me.BindingNavigatorDeleteItem
        Me.T_SOCIOSBindingNavigator.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.T_SOCIOSBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorMoveFirstItem, Me.BindingNavigatorMovePreviousItem, Me.BindingNavigatorSeparator, Me.BindingNavigatorPositionItem, Me.BindingNavigatorCountItem, Me.BindingNavigatorSeparator1, Me.BindingNavigatorMoveNextItem, Me.BindingNavigatorMoveLastItem, Me.BindingNavigatorSeparator2, Me.BindingNavigatorAddNewItem, Me.BindingNavigatorDeleteItem, Me.T_SOCIOSBindingNavigatorSaveItem})
        Me.T_SOCIOSBindingNavigator.Location = New System.Drawing.Point(0, 519)
        Me.T_SOCIOSBindingNavigator.MoveFirstItem = Me.BindingNavigatorMoveFirstItem
        Me.T_SOCIOSBindingNavigator.MoveLastItem = Me.BindingNavigatorMoveLastItem
        Me.T_SOCIOSBindingNavigator.MoveNextItem = Me.BindingNavigatorMoveNextItem
        Me.T_SOCIOSBindingNavigator.MovePreviousItem = Me.BindingNavigatorMovePreviousItem
        Me.T_SOCIOSBindingNavigator.Name = "T_SOCIOSBindingNavigator"
        Me.T_SOCIOSBindingNavigator.PositionItem = Me.BindingNavigatorPositionItem
        Me.T_SOCIOSBindingNavigator.Size = New System.Drawing.Size(621, 25)
        Me.T_SOCIOSBindingNavigator.TabIndex = 0
        Me.T_SOCIOSBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorAddNewItem
        '
        Me.BindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorAddNewItem.Image = CType(resources.GetObject("BindingNavigatorAddNewItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorAddNewItem.Name = "BindingNavigatorAddNewItem"
        Me.BindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorAddNewItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorAddNewItem.Text = "Add new"
        '
        'BindingNavigatorCountItem
        '
        Me.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem"
        Me.BindingNavigatorCountItem.Size = New System.Drawing.Size(35, 22)
        Me.BindingNavigatorCountItem.Text = "of {0}"
        Me.BindingNavigatorCountItem.ToolTipText = "Total number of items"
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorDeleteItem.Text = "Delete"
        '
        'BindingNavigatorMoveFirstItem
        '
        Me.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveFirstItem.Image = CType(resources.GetObject("BindingNavigatorMoveFirstItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem"
        Me.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveFirstItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveFirstItem.Text = "Move first"
        '
        'BindingNavigatorMovePreviousItem
        '
        Me.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMovePreviousItem.Image = CType(resources.GetObject("BindingNavigatorMovePreviousItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem"
        Me.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMovePreviousItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMovePreviousItem.Text = "Move previous"
        '
        'BindingNavigatorSeparator
        '
        Me.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator"
        Me.BindingNavigatorSeparator.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorPositionItem
        '
        Me.BindingNavigatorPositionItem.AccessibleName = "Position"
        Me.BindingNavigatorPositionItem.AutoSize = False
        Me.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem"
        Me.BindingNavigatorPositionItem.Size = New System.Drawing.Size(50, 23)
        Me.BindingNavigatorPositionItem.Text = "0"
        Me.BindingNavigatorPositionItem.ToolTipText = "Current position"
        '
        'BindingNavigatorSeparator1
        '
        Me.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1"
        Me.BindingNavigatorSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorMoveNextItem
        '
        Me.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveNextItem.Image = CType(resources.GetObject("BindingNavigatorMoveNextItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem"
        Me.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveNextItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveNextItem.Text = "Move next"
        '
        'BindingNavigatorMoveLastItem
        '
        Me.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveLastItem.Image = CType(resources.GetObject("BindingNavigatorMoveLastItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem"
        Me.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveLastItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveLastItem.Text = "Move last"
        '
        'BindingNavigatorSeparator2
        '
        Me.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2"
        Me.BindingNavigatorSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'T_SOCIOSBindingNavigatorSaveItem
        '
        Me.T_SOCIOSBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.T_SOCIOSBindingNavigatorSaveItem.Image = CType(resources.GetObject("T_SOCIOSBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.T_SOCIOSBindingNavigatorSaveItem.Name = "T_SOCIOSBindingNavigatorSaveItem"
        Me.T_SOCIOSBindingNavigatorSaveItem.Size = New System.Drawing.Size(23, 22)
        Me.T_SOCIOSBindingNavigatorSaveItem.Text = "Save Data"
        '
        'NúmeroTextBox
        '
        Me.NúmeroTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.T_SOCIOSBindingSource, "Número", True))
        Me.NúmeroTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NúmeroTextBox.Location = New System.Drawing.Point(178, 26)
        Me.NúmeroTextBox.Name = "NúmeroTextBox"
        Me.NúmeroTextBox.Size = New System.Drawing.Size(412, 29)
        Me.NúmeroTextBox.TabIndex = 2
        '
        'Data_AdmissaoDateTimePicker
        '
        Me.Data_AdmissaoDateTimePicker.DataBindings.Add(New System.Windows.Forms.Binding("Value", Me.T_SOCIOSBindingSource, "Data Admissao", True))
        Me.Data_AdmissaoDateTimePicker.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Data_AdmissaoDateTimePicker.Location = New System.Drawing.Point(178, 76)
        Me.Data_AdmissaoDateTimePicker.Name = "Data_AdmissaoDateTimePicker"
        Me.Data_AdmissaoDateTimePicker.Size = New System.Drawing.Size(412, 29)
        Me.Data_AdmissaoDateTimePicker.TabIndex = 4
        '
        'NomeTextBox
        '
        Me.NomeTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.T_SOCIOSBindingSource, "Nome", True))
        Me.NomeTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NomeTextBox.Location = New System.Drawing.Point(178, 126)
        Me.NomeTextBox.Name = "NomeTextBox"
        Me.NomeTextBox.Size = New System.Drawing.Size(412, 29)
        Me.NomeTextBox.TabIndex = 6
        '
        'MoradaTextBox
        '
        Me.MoradaTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.T_SOCIOSBindingSource, "Morada", True))
        Me.MoradaTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MoradaTextBox.Location = New System.Drawing.Point(178, 176)
        Me.MoradaTextBox.Name = "MoradaTextBox"
        Me.MoradaTextBox.Size = New System.Drawing.Size(412, 29)
        Me.MoradaTextBox.TabIndex = 8
        '
        'Codigo_PostalTextBox
        '
        Me.Codigo_PostalTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.T_SOCIOSBindingSource, "Codigo Postal", True))
        Me.Codigo_PostalTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Codigo_PostalTextBox.Location = New System.Drawing.Point(178, 226)
        Me.Codigo_PostalTextBox.Name = "Codigo_PostalTextBox"
        Me.Codigo_PostalTextBox.Size = New System.Drawing.Size(412, 29)
        Me.Codigo_PostalTextBox.TabIndex = 10
        '
        'LocalidadeTextBox
        '
        Me.LocalidadeTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.T_SOCIOSBindingSource, "Localidade", True))
        Me.LocalidadeTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LocalidadeTextBox.Location = New System.Drawing.Point(178, 276)
        Me.LocalidadeTextBox.Name = "LocalidadeTextBox"
        Me.LocalidadeTextBox.Size = New System.Drawing.Size(412, 29)
        Me.LocalidadeTextBox.TabIndex = 12
        '
        'TelefoneTextBox
        '
        Me.TelefoneTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.T_SOCIOSBindingSource, "Telefone", True))
        Me.TelefoneTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TelefoneTextBox.Location = New System.Drawing.Point(178, 326)
        Me.TelefoneTextBox.Name = "TelefoneTextBox"
        Me.TelefoneTextBox.Size = New System.Drawing.Size(412, 29)
        Me.TelefoneTextBox.TabIndex = 14
        '
        'EmailTextBox
        '
        Me.EmailTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.T_SOCIOSBindingSource, "Email", True))
        Me.EmailTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EmailTextBox.Location = New System.Drawing.Point(178, 376)
        Me.EmailTextBox.Name = "EmailTextBox"
        Me.EmailTextBox.Size = New System.Drawing.Size(412, 29)
        Me.EmailTextBox.TabIndex = 16
        '
        'ObservacaoTextBox
        '
        Me.ObservacaoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.T_SOCIOSBindingSource, "Observacao", True))
        Me.ObservacaoTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ObservacaoTextBox.Location = New System.Drawing.Point(178, 427)
        Me.ObservacaoTextBox.Name = "ObservacaoTextBox"
        Me.ObservacaoTextBox.Size = New System.Drawing.Size(412, 29)
        Me.ObservacaoTextBox.TabIndex = 18
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(50, 464)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(160, 40)
        Me.Button1.TabIndex = 19
        Me.Button1.Text = "Lista de Sócios"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(443, 462)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(148, 42)
        Me.Button2.TabIndex = 20
        Me.Button2.Text = "Sair"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(621, 544)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(ObservacaoLabel)
        Me.Controls.Add(Me.ObservacaoTextBox)
        Me.Controls.Add(EmailLabel)
        Me.Controls.Add(Me.EmailTextBox)
        Me.Controls.Add(TelefoneLabel)
        Me.Controls.Add(Me.TelefoneTextBox)
        Me.Controls.Add(LocalidadeLabel)
        Me.Controls.Add(Me.LocalidadeTextBox)
        Me.Controls.Add(Codigo_PostalLabel)
        Me.Controls.Add(Me.Codigo_PostalTextBox)
        Me.Controls.Add(MoradaLabel)
        Me.Controls.Add(Me.MoradaTextBox)
        Me.Controls.Add(NomeLabel)
        Me.Controls.Add(Me.NomeTextBox)
        Me.Controls.Add(Data_AdmissaoLabel)
        Me.Controls.Add(Me.Data_AdmissaoDateTimePicker)
        Me.Controls.Add(NúmeroLabel)
        Me.Controls.Add(Me.NúmeroTextBox)
        Me.Controls.Add(Me.T_SOCIOSBindingNavigator)
        Me.Name = "Form1"
        Me.Text = "Sócio"
        CType(Me.BdDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.T_SOCIOSBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.T_SOCIOSBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.T_SOCIOSBindingNavigator.ResumeLayout(False)
        Me.T_SOCIOSBindingNavigator.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents BdDataSet As Projeto_2.bdDataSet
    Friend WithEvents T_SOCIOSBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents T_SOCIOSTableAdapter As Projeto_2.bdDataSetTableAdapters.T_SOCIOSTableAdapter
    Friend WithEvents TableAdapterManager As Projeto_2.bdDataSetTableAdapters.TableAdapterManager
    Friend WithEvents T_SOCIOSBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorAddNewItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorCountItem As System.Windows.Forms.ToolStripLabel
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMoveFirstItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMovePreviousItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorPositionItem As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents BindingNavigatorSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorMoveNextItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMoveLastItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents T_SOCIOSBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents NúmeroTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Data_AdmissaoDateTimePicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents NomeTextBox As System.Windows.Forms.TextBox
    Friend WithEvents MoradaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Codigo_PostalTextBox As System.Windows.Forms.TextBox
    Friend WithEvents LocalidadeTextBox As System.Windows.Forms.TextBox
    Friend WithEvents TelefoneTextBox As System.Windows.Forms.TextBox
    Friend WithEvents EmailTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ObservacaoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button

End Class
