﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form2
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim NúmeroLabel As System.Windows.Forms.Label
        Me.BdDataSet = New Projeto_2.bdDataSet()
        Me.T_SOCIOSBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.T_SOCIOSTableAdapter = New Projeto_2.bdDataSetTableAdapters.T_SOCIOSTableAdapter()
        Me.TableAdapterManager = New Projeto_2.bdDataSetTableAdapters.TableAdapterManager()
        Me.T_SOCIOSDataGridView = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        NúmeroLabel = New System.Windows.Forms.Label()
        CType(Me.BdDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.T_SOCIOSBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.T_SOCIOSDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'BdDataSet
        '
        Me.BdDataSet.DataSetName = "bdDataSet"
        Me.BdDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'T_SOCIOSBindingSource
        '
        Me.T_SOCIOSBindingSource.DataMember = "T_SOCIOS"
        Me.T_SOCIOSBindingSource.DataSource = Me.BdDataSet
        '
        'T_SOCIOSTableAdapter
        '
        Me.T_SOCIOSTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.T_SOCIOSTableAdapter = Me.T_SOCIOSTableAdapter
        Me.TableAdapterManager.UpdateOrder = Projeto_2.bdDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'T_SOCIOSDataGridView
        '
        Me.T_SOCIOSDataGridView.AutoGenerateColumns = False
        Me.T_SOCIOSDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.T_SOCIOSDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2, Me.DataGridViewTextBoxColumn3, Me.DataGridViewTextBoxColumn4, Me.DataGridViewTextBoxColumn5, Me.DataGridViewTextBoxColumn6, Me.DataGridViewTextBoxColumn7, Me.DataGridViewTextBoxColumn8, Me.DataGridViewTextBoxColumn9})
        Me.T_SOCIOSDataGridView.DataSource = Me.T_SOCIOSBindingSource
        Me.T_SOCIOSDataGridView.Location = New System.Drawing.Point(34, 62)
        Me.T_SOCIOSDataGridView.Name = "T_SOCIOSDataGridView"
        Me.T_SOCIOSDataGridView.Size = New System.Drawing.Size(449, 220)
        Me.T_SOCIOSDataGridView.TabIndex = 1
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "Número"
        Me.DataGridViewTextBoxColumn1.HeaderText = "Número"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "Data Admissao"
        Me.DataGridViewTextBoxColumn2.HeaderText = "Data Admissao"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.DataPropertyName = "Nome"
        Me.DataGridViewTextBoxColumn3.HeaderText = "Nome"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.DataPropertyName = "Morada"
        Me.DataGridViewTextBoxColumn4.HeaderText = "Morada"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.DataPropertyName = "Codigo Postal"
        Me.DataGridViewTextBoxColumn5.HeaderText = "Codigo Postal"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.DataPropertyName = "Localidade"
        Me.DataGridViewTextBoxColumn6.HeaderText = "Localidade"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.DataPropertyName = "Telefone"
        Me.DataGridViewTextBoxColumn7.HeaderText = "Telefone"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.DataPropertyName = "Email"
        Me.DataGridViewTextBoxColumn8.HeaderText = "Email"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.DataPropertyName = "Observacao"
        Me.DataGridViewTextBoxColumn9.HeaderText = "Observacao"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        '
        'NúmeroLabel
        '
        NúmeroLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        NúmeroLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        NúmeroLabel.Location = New System.Drawing.Point(34, 9)
        NúmeroLabel.Name = "NúmeroLabel"
        NúmeroLabel.Size = New System.Drawing.Size(449, 50)
        NúmeroLabel.TabIndex = 2
        NúmeroLabel.Text = "Lista de Sócios"
        NúmeroLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Form2
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(525, 291)
        Me.Controls.Add(NúmeroLabel)
        Me.Controls.Add(Me.T_SOCIOSDataGridView)
        Me.Name = "Form2"
        Me.Text = "Lista de Sócios"
        CType(Me.BdDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.T_SOCIOSBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.T_SOCIOSDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents BdDataSet As Projeto_2.bdDataSet
    Friend WithEvents T_SOCIOSBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents T_SOCIOSTableAdapter As Projeto_2.bdDataSetTableAdapters.T_SOCIOSTableAdapter
    Friend WithEvents TableAdapterManager As Projeto_2.bdDataSetTableAdapters.TableAdapterManager
    Friend WithEvents T_SOCIOSDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
