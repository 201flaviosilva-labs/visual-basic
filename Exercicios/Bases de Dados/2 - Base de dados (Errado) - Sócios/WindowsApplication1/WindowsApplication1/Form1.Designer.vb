﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim NumeroLabel As System.Windows.Forms.Label
        Dim Data_AdmiçãoLabel As System.Windows.Forms.Label
        Dim NomeLabel As System.Windows.Forms.Label
        Dim MoradaLabel As System.Windows.Forms.Label
        Dim CodigoPostalLabel As System.Windows.Forms.Label
        Dim LocalidadeLabel As System.Windows.Forms.Label
        Dim TelefoneLabel As System.Windows.Forms.Label
        Dim EmailLabel As System.Windows.Forms.Label
        Dim ObservacaoLabel As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me._MyDatabase_1DataSet = New WindowsApplication1._MyDatabase_1DataSet()
        Me.T_SociosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.T_SociosTableAdapter = New WindowsApplication1._MyDatabase_1DataSetTableAdapters.T_SociosTableAdapter()
        Me.TableAdapterManager = New WindowsApplication1._MyDatabase_1DataSetTableAdapters.TableAdapterManager()
        Me.NumeroTextBox = New System.Windows.Forms.TextBox()
        Me.Data_AdmiçãoDateTimePicker = New System.Windows.Forms.DateTimePicker()
        Me.NomeTextBox = New System.Windows.Forms.TextBox()
        Me.MoradaTextBox = New System.Windows.Forms.TextBox()
        Me.CodigoPostalTextBox = New System.Windows.Forms.TextBox()
        Me.LocalidadeTextBox = New System.Windows.Forms.TextBox()
        Me.TelefoneTextBox = New System.Windows.Forms.TextBox()
        Me.EmailTextBox = New System.Windows.Forms.TextBox()
        Me.ObservacaoTextBox = New System.Windows.Forms.TextBox()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.T_SociosBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorAddNewItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorCountItem = New System.Windows.Forms.ToolStripLabel()
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveFirstItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMovePreviousItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorPositionItem = New System.Windows.Forms.ToolStripTextBox()
        Me.BindingNavigatorSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorMoveNextItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveLastItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.T_SociosBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        NumeroLabel = New System.Windows.Forms.Label()
        Data_AdmiçãoLabel = New System.Windows.Forms.Label()
        NomeLabel = New System.Windows.Forms.Label()
        MoradaLabel = New System.Windows.Forms.Label()
        CodigoPostalLabel = New System.Windows.Forms.Label()
        LocalidadeLabel = New System.Windows.Forms.Label()
        TelefoneLabel = New System.Windows.Forms.Label()
        EmailLabel = New System.Windows.Forms.Label()
        ObservacaoLabel = New System.Windows.Forms.Label()
        CType(Me._MyDatabase_1DataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.T_SociosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.T_SociosBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.T_SociosBindingNavigator.SuspendLayout()
        Me.SuspendLayout()
        '
        '_MyDatabase_1DataSet
        '
        Me._MyDatabase_1DataSet.DataSetName = "_MyDatabase_1DataSet"
        Me._MyDatabase_1DataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'T_SociosBindingSource
        '
        Me.T_SociosBindingSource.DataMember = "T_Socios"
        Me.T_SociosBindingSource.DataSource = Me._MyDatabase_1DataSet
        '
        'T_SociosTableAdapter
        '
        Me.T_SociosTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.T_SociosTableAdapter = Me.T_SociosTableAdapter
        Me.TableAdapterManager.UpdateOrder = WindowsApplication1._MyDatabase_1DataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'NumeroLabel
        '
        NumeroLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        NumeroLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        NumeroLabel.Location = New System.Drawing.Point(12, 9)
        NumeroLabel.Name = "NumeroLabel"
        NumeroLabel.Size = New System.Drawing.Size(130, 20)
        NumeroLabel.TabIndex = 0
        NumeroLabel.Text = "Numero:"
        '
        'NumeroTextBox
        '
        Me.NumeroTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.T_SociosBindingSource, "Numero", True))
        Me.NumeroTextBox.Location = New System.Drawing.Point(168, 9)
        Me.NumeroTextBox.Name = "NumeroTextBox"
        Me.NumeroTextBox.Size = New System.Drawing.Size(276, 20)
        Me.NumeroTextBox.TabIndex = 1
        '
        'Data_AdmiçãoLabel
        '
        Data_AdmiçãoLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Data_AdmiçãoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Data_AdmiçãoLabel.Location = New System.Drawing.Point(12, 32)
        Data_AdmiçãoLabel.Name = "Data_AdmiçãoLabel"
        Data_AdmiçãoLabel.Size = New System.Drawing.Size(130, 24)
        Data_AdmiçãoLabel.TabIndex = 2
        Data_AdmiçãoLabel.Text = "Data Admição:"
        '
        'Data_AdmiçãoDateTimePicker
        '
        Me.Data_AdmiçãoDateTimePicker.DataBindings.Add(New System.Windows.Forms.Binding("Value", Me.T_SociosBindingSource, "Data Admição", True))
        Me.Data_AdmiçãoDateTimePicker.Location = New System.Drawing.Point(168, 32)
        Me.Data_AdmiçãoDateTimePicker.Name = "Data_AdmiçãoDateTimePicker"
        Me.Data_AdmiçãoDateTimePicker.Size = New System.Drawing.Size(276, 20)
        Me.Data_AdmiçãoDateTimePicker.TabIndex = 3
        '
        'NomeLabel
        '
        NomeLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        NomeLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        NomeLabel.Location = New System.Drawing.Point(12, 62)
        NomeLabel.Name = "NomeLabel"
        NomeLabel.Size = New System.Drawing.Size(130, 24)
        NomeLabel.TabIndex = 4
        NomeLabel.Text = "Nome:"
        '
        'NomeTextBox
        '
        Me.NomeTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.T_SociosBindingSource, "Nome", True))
        Me.NomeTextBox.Location = New System.Drawing.Point(168, 62)
        Me.NomeTextBox.Name = "NomeTextBox"
        Me.NomeTextBox.Size = New System.Drawing.Size(276, 20)
        Me.NomeTextBox.TabIndex = 5
        '
        'MoradaLabel
        '
        MoradaLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        MoradaLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        MoradaLabel.Location = New System.Drawing.Point(12, 93)
        MoradaLabel.Name = "MoradaLabel"
        MoradaLabel.Size = New System.Drawing.Size(130, 25)
        MoradaLabel.TabIndex = 6
        MoradaLabel.Text = "Morada:"
        '
        'MoradaTextBox
        '
        Me.MoradaTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.T_SociosBindingSource, "Morada", True))
        Me.MoradaTextBox.Location = New System.Drawing.Point(168, 93)
        Me.MoradaTextBox.Name = "MoradaTextBox"
        Me.MoradaTextBox.Size = New System.Drawing.Size(276, 20)
        Me.MoradaTextBox.TabIndex = 7
        '
        'CodigoPostalLabel
        '
        CodigoPostalLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        CodigoPostalLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CodigoPostalLabel.Location = New System.Drawing.Point(12, 122)
        CodigoPostalLabel.Name = "CodigoPostalLabel"
        CodigoPostalLabel.Size = New System.Drawing.Size(130, 22)
        CodigoPostalLabel.TabIndex = 8
        CodigoPostalLabel.Text = "Codigo Postal:"
        '
        'CodigoPostalTextBox
        '
        Me.CodigoPostalTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.T_SociosBindingSource, "CodigoPostal", True))
        Me.CodigoPostalTextBox.Location = New System.Drawing.Point(168, 122)
        Me.CodigoPostalTextBox.Name = "CodigoPostalTextBox"
        Me.CodigoPostalTextBox.Size = New System.Drawing.Size(276, 20)
        Me.CodigoPostalTextBox.TabIndex = 9
        '
        'LocalidadeLabel
        '
        LocalidadeLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        LocalidadeLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        LocalidadeLabel.Location = New System.Drawing.Point(12, 148)
        LocalidadeLabel.Name = "LocalidadeLabel"
        LocalidadeLabel.Size = New System.Drawing.Size(130, 24)
        LocalidadeLabel.TabIndex = 10
        LocalidadeLabel.Text = "Localidade:"
        '
        'LocalidadeTextBox
        '
        Me.LocalidadeTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.T_SociosBindingSource, "Localidade", True))
        Me.LocalidadeTextBox.Location = New System.Drawing.Point(168, 148)
        Me.LocalidadeTextBox.Name = "LocalidadeTextBox"
        Me.LocalidadeTextBox.Size = New System.Drawing.Size(276, 20)
        Me.LocalidadeTextBox.TabIndex = 11
        '
        'TelefoneLabel
        '
        TelefoneLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        TelefoneLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        TelefoneLabel.Location = New System.Drawing.Point(12, 176)
        TelefoneLabel.Name = "TelefoneLabel"
        TelefoneLabel.Size = New System.Drawing.Size(130, 23)
        TelefoneLabel.TabIndex = 12
        TelefoneLabel.Text = "Telefone:"
        '
        'TelefoneTextBox
        '
        Me.TelefoneTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.T_SociosBindingSource, "Telefone", True))
        Me.TelefoneTextBox.Location = New System.Drawing.Point(168, 176)
        Me.TelefoneTextBox.Name = "TelefoneTextBox"
        Me.TelefoneTextBox.Size = New System.Drawing.Size(276, 20)
        Me.TelefoneTextBox.TabIndex = 13
        '
        'EmailLabel
        '
        EmailLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        EmailLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        EmailLabel.Location = New System.Drawing.Point(12, 203)
        EmailLabel.Name = "EmailLabel"
        EmailLabel.Size = New System.Drawing.Size(130, 22)
        EmailLabel.TabIndex = 14
        EmailLabel.Text = "Email:"
        '
        'EmailTextBox
        '
        Me.EmailTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.T_SociosBindingSource, "Email", True))
        Me.EmailTextBox.Location = New System.Drawing.Point(168, 203)
        Me.EmailTextBox.Name = "EmailTextBox"
        Me.EmailTextBox.Size = New System.Drawing.Size(276, 20)
        Me.EmailTextBox.TabIndex = 15
        '
        'ObservacaoLabel
        '
        ObservacaoLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        ObservacaoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ObservacaoLabel.Location = New System.Drawing.Point(12, 231)
        ObservacaoLabel.Name = "ObservacaoLabel"
        ObservacaoLabel.Size = New System.Drawing.Size(130, 24)
        ObservacaoLabel.TabIndex = 16
        ObservacaoLabel.Text = "Observacao:"
        '
        'ObservacaoTextBox
        '
        Me.ObservacaoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.T_SociosBindingSource, "Observacao", True))
        Me.ObservacaoTextBox.Location = New System.Drawing.Point(168, 231)
        Me.ObservacaoTextBox.Name = "ObservacaoTextBox"
        Me.ObservacaoTextBox.Size = New System.Drawing.Size(276, 20)
        Me.ObservacaoTextBox.TabIndex = 17
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(14, 262)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(128, 35)
        Me.Button2.TabIndex = 19
        Me.Button2.Text = "Lista de Socios"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(316, 262)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(128, 35)
        Me.Button3.TabIndex = 20
        Me.Button3.Text = "Sair"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'T_SociosBindingNavigator
        '
        Me.T_SociosBindingNavigator.AddNewItem = Me.BindingNavigatorAddNewItem
        Me.T_SociosBindingNavigator.BindingSource = Me.T_SociosBindingSource
        Me.T_SociosBindingNavigator.CountItem = Me.BindingNavigatorCountItem
        Me.T_SociosBindingNavigator.DeleteItem = Me.BindingNavigatorDeleteItem
        Me.T_SociosBindingNavigator.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.T_SociosBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorMoveFirstItem, Me.BindingNavigatorMovePreviousItem, Me.BindingNavigatorSeparator, Me.BindingNavigatorPositionItem, Me.BindingNavigatorCountItem, Me.BindingNavigatorSeparator1, Me.BindingNavigatorMoveNextItem, Me.BindingNavigatorMoveLastItem, Me.BindingNavigatorSeparator2, Me.BindingNavigatorAddNewItem, Me.BindingNavigatorDeleteItem, Me.T_SociosBindingNavigatorSaveItem})
        Me.T_SociosBindingNavigator.Location = New System.Drawing.Point(0, 308)
        Me.T_SociosBindingNavigator.MoveFirstItem = Me.BindingNavigatorMoveFirstItem
        Me.T_SociosBindingNavigator.MoveLastItem = Me.BindingNavigatorMoveLastItem
        Me.T_SociosBindingNavigator.MoveNextItem = Me.BindingNavigatorMoveNextItem
        Me.T_SociosBindingNavigator.MovePreviousItem = Me.BindingNavigatorMovePreviousItem
        Me.T_SociosBindingNavigator.Name = "T_SociosBindingNavigator"
        Me.T_SociosBindingNavigator.PositionItem = Me.BindingNavigatorPositionItem
        Me.T_SociosBindingNavigator.Size = New System.Drawing.Size(468, 25)
        Me.T_SociosBindingNavigator.TabIndex = 21
        Me.T_SociosBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorAddNewItem
        '
        Me.BindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorAddNewItem.Image = CType(resources.GetObject("BindingNavigatorAddNewItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorAddNewItem.Name = "BindingNavigatorAddNewItem"
        Me.BindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorAddNewItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorAddNewItem.Text = "Add new"
        '
        'BindingNavigatorCountItem
        '
        Me.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem"
        Me.BindingNavigatorCountItem.Size = New System.Drawing.Size(35, 22)
        Me.BindingNavigatorCountItem.Text = "of {0}"
        Me.BindingNavigatorCountItem.ToolTipText = "Total number of items"
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorDeleteItem.Text = "Delete"
        '
        'BindingNavigatorMoveFirstItem
        '
        Me.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveFirstItem.Image = CType(resources.GetObject("BindingNavigatorMoveFirstItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem"
        Me.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveFirstItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveFirstItem.Text = "Move first"
        '
        'BindingNavigatorMovePreviousItem
        '
        Me.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMovePreviousItem.Image = CType(resources.GetObject("BindingNavigatorMovePreviousItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem"
        Me.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMovePreviousItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMovePreviousItem.Text = "Move previous"
        '
        'BindingNavigatorSeparator
        '
        Me.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator"
        Me.BindingNavigatorSeparator.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorPositionItem
        '
        Me.BindingNavigatorPositionItem.AccessibleName = "Position"
        Me.BindingNavigatorPositionItem.AutoSize = False
        Me.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem"
        Me.BindingNavigatorPositionItem.Size = New System.Drawing.Size(50, 23)
        Me.BindingNavigatorPositionItem.Text = "0"
        Me.BindingNavigatorPositionItem.ToolTipText = "Current position"
        '
        'BindingNavigatorSeparator1
        '
        Me.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1"
        Me.BindingNavigatorSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorMoveNextItem
        '
        Me.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveNextItem.Image = CType(resources.GetObject("BindingNavigatorMoveNextItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem"
        Me.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveNextItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveNextItem.Text = "Move next"
        '
        'BindingNavigatorMoveLastItem
        '
        Me.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveLastItem.Image = CType(resources.GetObject("BindingNavigatorMoveLastItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem"
        Me.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveLastItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveLastItem.Text = "Move last"
        '
        'BindingNavigatorSeparator2
        '
        Me.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2"
        Me.BindingNavigatorSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'T_SociosBindingNavigatorSaveItem
        '
        Me.T_SociosBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.T_SociosBindingNavigatorSaveItem.Image = CType(resources.GetObject("T_SociosBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.T_SociosBindingNavigatorSaveItem.Name = "T_SociosBindingNavigatorSaveItem"
        Me.T_SociosBindingNavigatorSaveItem.Size = New System.Drawing.Size(23, 22)
        Me.T_SociosBindingNavigatorSaveItem.Text = "Save Data"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(468, 333)
        Me.Controls.Add(Me.T_SociosBindingNavigator)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(ObservacaoLabel)
        Me.Controls.Add(Me.ObservacaoTextBox)
        Me.Controls.Add(EmailLabel)
        Me.Controls.Add(Me.EmailTextBox)
        Me.Controls.Add(TelefoneLabel)
        Me.Controls.Add(Me.TelefoneTextBox)
        Me.Controls.Add(LocalidadeLabel)
        Me.Controls.Add(Me.LocalidadeTextBox)
        Me.Controls.Add(CodigoPostalLabel)
        Me.Controls.Add(Me.CodigoPostalTextBox)
        Me.Controls.Add(MoradaLabel)
        Me.Controls.Add(Me.MoradaTextBox)
        Me.Controls.Add(NomeLabel)
        Me.Controls.Add(Me.NomeTextBox)
        Me.Controls.Add(Data_AdmiçãoLabel)
        Me.Controls.Add(Me.Data_AdmiçãoDateTimePicker)
        Me.Controls.Add(NumeroLabel)
        Me.Controls.Add(Me.NumeroTextBox)
        Me.Name = "Form1"
        Me.Text = "Form1"
        CType(Me._MyDatabase_1DataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.T_SociosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.T_SociosBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.T_SociosBindingNavigator.ResumeLayout(False)
        Me.T_SociosBindingNavigator.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents _MyDatabase_1DataSet As WindowsApplication1._MyDatabase_1DataSet
    Friend WithEvents T_SociosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents T_SociosTableAdapter As WindowsApplication1._MyDatabase_1DataSetTableAdapters.T_SociosTableAdapter
    Friend WithEvents TableAdapterManager As WindowsApplication1._MyDatabase_1DataSetTableAdapters.TableAdapterManager
    Friend WithEvents NumeroTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Data_AdmiçãoDateTimePicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents NomeTextBox As System.Windows.Forms.TextBox
    Friend WithEvents MoradaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CodigoPostalTextBox As System.Windows.Forms.TextBox
    Friend WithEvents LocalidadeTextBox As System.Windows.Forms.TextBox
    Friend WithEvents TelefoneTextBox As System.Windows.Forms.TextBox
    Friend WithEvents EmailTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ObservacaoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents T_SociosBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorAddNewItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorCountItem As System.Windows.Forms.ToolStripLabel
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMoveFirstItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMovePreviousItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorPositionItem As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents BindingNavigatorSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorMoveNextItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMoveLastItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents T_SociosBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton

End Class
