﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Dim CodigoLabel As System.Windows.Forms.Label
        Dim ImportadoLabel As System.Windows.Forms.Label
        Dim VendidoLabel As System.Windows.Forms.Label
        Dim PrecoLabel As System.Windows.Forms.Label
        Dim MarcaLabel As System.Windows.Forms.Label
        Dim AnoLabel As System.Windows.Forms.Label
        Dim EstadoLabel As System.Windows.Forms.Label
        Dim ConbustivelLabel As System.Windows.Forms.Label
        Dim ModeloLabel As System.Windows.Forms.Label
        Me._MyDatabase_1DataSet1 = New WindowsApplication1._MyDatabase_1DataSet1()
        Me.CarrosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CarrosTableAdapter = New WindowsApplication1._MyDatabase_1DataSet1TableAdapters.CarrosTableAdapter()
        Me.TableAdapterManager = New WindowsApplication1._MyDatabase_1DataSet1TableAdapters.TableAdapterManager()
        Me.CarrosBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorMoveFirstItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMovePreviousItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorPositionItem = New System.Windows.Forms.ToolStripTextBox()
        Me.BindingNavigatorCountItem = New System.Windows.Forms.ToolStripLabel()
        Me.BindingNavigatorSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorMoveNextItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveLastItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorAddNewItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton()
        Me.CarrosBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        Me.CodigoTextBox = New System.Windows.Forms.TextBox()
        Me.ImportadoCheckBox = New System.Windows.Forms.CheckBox()
        Me.VendidoCheckBox = New System.Windows.Forms.CheckBox()
        Me.PrecoTextBox = New System.Windows.Forms.TextBox()
        Me.MarcaComboBox = New System.Windows.Forms.ComboBox()
        Me.AnoComboBox = New System.Windows.Forms.ComboBox()
        Me.EstadoComboBox = New System.Windows.Forms.ComboBox()
        Me.ConbustivelComboBox = New System.Windows.Forms.ComboBox()
        Me.ModeloTextBox = New System.Windows.Forms.TextBox()
        Me.Button1 = New System.Windows.Forms.Button()
        CodigoLabel = New System.Windows.Forms.Label()
        ImportadoLabel = New System.Windows.Forms.Label()
        VendidoLabel = New System.Windows.Forms.Label()
        PrecoLabel = New System.Windows.Forms.Label()
        MarcaLabel = New System.Windows.Forms.Label()
        AnoLabel = New System.Windows.Forms.Label()
        EstadoLabel = New System.Windows.Forms.Label()
        ConbustivelLabel = New System.Windows.Forms.Label()
        ModeloLabel = New System.Windows.Forms.Label()
        CType(Me._MyDatabase_1DataSet1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CarrosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CarrosBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CarrosBindingNavigator.SuspendLayout()
        Me.SuspendLayout()
        '
        '_MyDatabase_1DataSet1
        '
        Me._MyDatabase_1DataSet1.DataSetName = "_MyDatabase_1DataSet1"
        Me._MyDatabase_1DataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'CarrosBindingSource
        '
        Me.CarrosBindingSource.DataMember = "Carros"
        Me.CarrosBindingSource.DataSource = Me._MyDatabase_1DataSet1
        '
        'CarrosTableAdapter
        '
        Me.CarrosTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.CarrosTableAdapter = Me.CarrosTableAdapter
        Me.TableAdapterManager.UpdateOrder = WindowsApplication1._MyDatabase_1DataSet1TableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'CarrosBindingNavigator
        '
        Me.CarrosBindingNavigator.AddNewItem = Me.BindingNavigatorAddNewItem
        Me.CarrosBindingNavigator.BindingSource = Me.CarrosBindingSource
        Me.CarrosBindingNavigator.CountItem = Me.BindingNavigatorCountItem
        Me.CarrosBindingNavigator.DeleteItem = Me.BindingNavigatorDeleteItem
        Me.CarrosBindingNavigator.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.CarrosBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorMoveFirstItem, Me.BindingNavigatorMovePreviousItem, Me.BindingNavigatorSeparator, Me.BindingNavigatorPositionItem, Me.BindingNavigatorCountItem, Me.BindingNavigatorSeparator1, Me.BindingNavigatorMoveNextItem, Me.BindingNavigatorMoveLastItem, Me.BindingNavigatorSeparator2, Me.BindingNavigatorAddNewItem, Me.BindingNavigatorDeleteItem, Me.CarrosBindingNavigatorSaveItem})
        Me.CarrosBindingNavigator.Location = New System.Drawing.Point(0, 294)
        Me.CarrosBindingNavigator.MoveFirstItem = Me.BindingNavigatorMoveFirstItem
        Me.CarrosBindingNavigator.MoveLastItem = Me.BindingNavigatorMoveLastItem
        Me.CarrosBindingNavigator.MoveNextItem = Me.BindingNavigatorMoveNextItem
        Me.CarrosBindingNavigator.MovePreviousItem = Me.BindingNavigatorMovePreviousItem
        Me.CarrosBindingNavigator.Name = "CarrosBindingNavigator"
        Me.CarrosBindingNavigator.PositionItem = Me.BindingNavigatorPositionItem
        Me.CarrosBindingNavigator.Size = New System.Drawing.Size(331, 25)
        Me.CarrosBindingNavigator.TabIndex = 0
        Me.CarrosBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorMoveFirstItem
        '
        Me.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveFirstItem.Image = CType(resources.GetObject("BindingNavigatorMoveFirstItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem"
        Me.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveFirstItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveFirstItem.Text = "Move first"
        '
        'BindingNavigatorMovePreviousItem
        '
        Me.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMovePreviousItem.Image = CType(resources.GetObject("BindingNavigatorMovePreviousItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem"
        Me.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMovePreviousItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMovePreviousItem.Text = "Move previous"
        '
        'BindingNavigatorSeparator
        '
        Me.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator"
        Me.BindingNavigatorSeparator.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorPositionItem
        '
        Me.BindingNavigatorPositionItem.AccessibleName = "Position"
        Me.BindingNavigatorPositionItem.AutoSize = False
        Me.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem"
        Me.BindingNavigatorPositionItem.Size = New System.Drawing.Size(50, 23)
        Me.BindingNavigatorPositionItem.Text = "0"
        Me.BindingNavigatorPositionItem.ToolTipText = "Current position"
        '
        'BindingNavigatorCountItem
        '
        Me.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem"
        Me.BindingNavigatorCountItem.Size = New System.Drawing.Size(35, 15)
        Me.BindingNavigatorCountItem.Text = "of {0}"
        Me.BindingNavigatorCountItem.ToolTipText = "Total number of items"
        '
        'BindingNavigatorSeparator1
        '
        Me.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator"
        Me.BindingNavigatorSeparator1.Size = New System.Drawing.Size(6, 6)
        '
        'BindingNavigatorMoveNextItem
        '
        Me.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveNextItem.Image = CType(resources.GetObject("BindingNavigatorMoveNextItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem"
        Me.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveNextItem.Size = New System.Drawing.Size(23, 20)
        Me.BindingNavigatorMoveNextItem.Text = "Move next"
        '
        'BindingNavigatorMoveLastItem
        '
        Me.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveLastItem.Image = CType(resources.GetObject("BindingNavigatorMoveLastItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem"
        Me.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveLastItem.Size = New System.Drawing.Size(23, 20)
        Me.BindingNavigatorMoveLastItem.Text = "Move last"
        '
        'BindingNavigatorSeparator2
        '
        Me.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator"
        Me.BindingNavigatorSeparator2.Size = New System.Drawing.Size(6, 6)
        '
        'BindingNavigatorAddNewItem
        '
        Me.BindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorAddNewItem.Image = CType(resources.GetObject("BindingNavigatorAddNewItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorAddNewItem.Name = "BindingNavigatorAddNewItem"
        Me.BindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorAddNewItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorAddNewItem.Text = "Add new"
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(23, 20)
        Me.BindingNavigatorDeleteItem.Text = "Delete"
        '
        'CarrosBindingNavigatorSaveItem
        '
        Me.CarrosBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.CarrosBindingNavigatorSaveItem.Image = CType(resources.GetObject("CarrosBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.CarrosBindingNavigatorSaveItem.Name = "CarrosBindingNavigatorSaveItem"
        Me.CarrosBindingNavigatorSaveItem.Size = New System.Drawing.Size(23, 23)
        Me.CarrosBindingNavigatorSaveItem.Text = "Save Data"
        '
        'CodigoLabel
        '
        CodigoLabel.AutoSize = True
        CodigoLabel.Location = New System.Drawing.Point(12, 19)
        CodigoLabel.Name = "CodigoLabel"
        CodigoLabel.Size = New System.Drawing.Size(43, 13)
        CodigoLabel.TabIndex = 1
        CodigoLabel.Text = "Codigo:"
        '
        'CodigoTextBox
        '
        Me.CodigoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CarrosBindingSource, "Codigo", True))
        Me.CodigoTextBox.Location = New System.Drawing.Point(88, 16)
        Me.CodigoTextBox.Name = "CodigoTextBox"
        Me.CodigoTextBox.Size = New System.Drawing.Size(100, 20)
        Me.CodigoTextBox.TabIndex = 2
        '
        'ImportadoLabel
        '
        ImportadoLabel.AutoSize = True
        ImportadoLabel.Location = New System.Drawing.Point(15, 193)
        ImportadoLabel.Name = "ImportadoLabel"
        ImportadoLabel.Size = New System.Drawing.Size(57, 13)
        ImportadoLabel.TabIndex = 13
        ImportadoLabel.Text = "Importado:"
        '
        'ImportadoCheckBox
        '
        Me.ImportadoCheckBox.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.CarrosBindingSource, "Importado", True))
        Me.ImportadoCheckBox.Location = New System.Drawing.Point(88, 188)
        Me.ImportadoCheckBox.Name = "ImportadoCheckBox"
        Me.ImportadoCheckBox.Size = New System.Drawing.Size(104, 24)
        Me.ImportadoCheckBox.TabIndex = 14
        Me.ImportadoCheckBox.Text = "CheckBox1"
        Me.ImportadoCheckBox.UseVisualStyleBackColor = True
        '
        'VendidoLabel
        '
        VendidoLabel.AutoSize = True
        VendidoLabel.Location = New System.Drawing.Point(17, 223)
        VendidoLabel.Name = "VendidoLabel"
        VendidoLabel.Size = New System.Drawing.Size(49, 13)
        VendidoLabel.TabIndex = 15
        VendidoLabel.Text = "Vendido:"
        '
        'VendidoCheckBox
        '
        Me.VendidoCheckBox.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.CarrosBindingSource, "Vendido", True))
        Me.VendidoCheckBox.Location = New System.Drawing.Point(88, 218)
        Me.VendidoCheckBox.Name = "VendidoCheckBox"
        Me.VendidoCheckBox.Size = New System.Drawing.Size(104, 24)
        Me.VendidoCheckBox.TabIndex = 16
        Me.VendidoCheckBox.Text = "CheckBox1"
        Me.VendidoCheckBox.UseVisualStyleBackColor = True
        '
        'PrecoLabel
        '
        PrecoLabel.AutoSize = True
        PrecoLabel.Location = New System.Drawing.Point(17, 257)
        PrecoLabel.Name = "PrecoLabel"
        PrecoLabel.Size = New System.Drawing.Size(38, 13)
        PrecoLabel.TabIndex = 17
        PrecoLabel.Text = "Preco:"
        '
        'PrecoTextBox
        '
        Me.PrecoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CarrosBindingSource, "Preco", True))
        Me.PrecoTextBox.Location = New System.Drawing.Point(88, 254)
        Me.PrecoTextBox.Name = "PrecoTextBox"
        Me.PrecoTextBox.Size = New System.Drawing.Size(100, 20)
        Me.PrecoTextBox.TabIndex = 18
        '
        'MarcaLabel
        '
        MarcaLabel.AutoSize = True
        MarcaLabel.Location = New System.Drawing.Point(12, 43)
        MarcaLabel.Name = "MarcaLabel"
        MarcaLabel.Size = New System.Drawing.Size(40, 13)
        MarcaLabel.TabIndex = 18
        MarcaLabel.Text = "Marca:"
        '
        'MarcaComboBox
        '
        Me.MarcaComboBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CarrosBindingSource, "Marca", True))
        Me.MarcaComboBox.FormattingEnabled = True
        Me.MarcaComboBox.Items.AddRange(New Object() {"Audi", "BMW"})
        Me.MarcaComboBox.Location = New System.Drawing.Point(58, 40)
        Me.MarcaComboBox.Name = "MarcaComboBox"
        Me.MarcaComboBox.Size = New System.Drawing.Size(121, 21)
        Me.MarcaComboBox.TabIndex = 19
        '
        'AnoLabel
        '
        AnoLabel.AutoSize = True
        AnoLabel.Location = New System.Drawing.Point(10, 94)
        AnoLabel.Name = "AnoLabel"
        AnoLabel.Size = New System.Drawing.Size(29, 13)
        AnoLabel.TabIndex = 20
        AnoLabel.Text = "Ano:"
        '
        'AnoComboBox
        '
        Me.AnoComboBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CarrosBindingSource, "Ano", True))
        Me.AnoComboBox.FormattingEnabled = True
        Me.AnoComboBox.Items.AddRange(New Object() {"2000", "2001", "2002", "2003", "2004", "2005", "2006", "2007", "2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018"})
        Me.AnoComboBox.Location = New System.Drawing.Point(45, 91)
        Me.AnoComboBox.Name = "AnoComboBox"
        Me.AnoComboBox.Size = New System.Drawing.Size(121, 21)
        Me.AnoComboBox.TabIndex = 21
        '
        'EstadoLabel
        '
        EstadoLabel.AutoSize = True
        EstadoLabel.Location = New System.Drawing.Point(10, 115)
        EstadoLabel.Name = "EstadoLabel"
        EstadoLabel.Size = New System.Drawing.Size(43, 13)
        EstadoLabel.TabIndex = 21
        EstadoLabel.Text = "Estado:"
        '
        'EstadoComboBox
        '
        Me.EstadoComboBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CarrosBindingSource, "Estado", True))
        Me.EstadoComboBox.FormattingEnabled = True
        Me.EstadoComboBox.Items.AddRange(New Object() {"Novo", "Semi-Novo", "Usado"})
        Me.EstadoComboBox.Location = New System.Drawing.Point(59, 112)
        Me.EstadoComboBox.Name = "EstadoComboBox"
        Me.EstadoComboBox.Size = New System.Drawing.Size(121, 21)
        Me.EstadoComboBox.TabIndex = 22
        '
        'ConbustivelLabel
        '
        ConbustivelLabel.AutoSize = True
        ConbustivelLabel.Location = New System.Drawing.Point(10, 145)
        ConbustivelLabel.Name = "ConbustivelLabel"
        ConbustivelLabel.Size = New System.Drawing.Size(65, 13)
        ConbustivelLabel.TabIndex = 22
        ConbustivelLabel.Text = "Conbustivel:"
        '
        'ConbustivelComboBox
        '
        Me.ConbustivelComboBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CarrosBindingSource, "Conbustivel", True))
        Me.ConbustivelComboBox.FormattingEnabled = True
        Me.ConbustivelComboBox.Items.AddRange(New Object() {"Dísel", "Gasolina", "Gasólio"})
        Me.ConbustivelComboBox.Location = New System.Drawing.Point(81, 142)
        Me.ConbustivelComboBox.Name = "ConbustivelComboBox"
        Me.ConbustivelComboBox.Size = New System.Drawing.Size(121, 21)
        Me.ConbustivelComboBox.TabIndex = 23
        '
        'ModeloLabel
        '
        ModeloLabel.AutoSize = True
        ModeloLabel.Location = New System.Drawing.Point(39, 72)
        ModeloLabel.Name = "ModeloLabel"
        ModeloLabel.Size = New System.Drawing.Size(45, 13)
        ModeloLabel.TabIndex = 23
        ModeloLabel.Text = "Modelo:"
        '
        'ModeloTextBox
        '
        Me.ModeloTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CarrosBindingSource, "Modelo", True))
        Me.ModeloTextBox.Location = New System.Drawing.Point(90, 69)
        Me.ModeloTextBox.Name = "ModeloTextBox"
        Me.ModeloTextBox.Size = New System.Drawing.Size(100, 20)
        Me.ModeloTextBox.TabIndex = 24
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(275, 294)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(44, 23)
        Me.Button1.TabIndex = 25
        Me.Button1.Text = "F 2"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(331, 319)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(ModeloLabel)
        Me.Controls.Add(Me.ModeloTextBox)
        Me.Controls.Add(ConbustivelLabel)
        Me.Controls.Add(Me.ConbustivelComboBox)
        Me.Controls.Add(EstadoLabel)
        Me.Controls.Add(Me.EstadoComboBox)
        Me.Controls.Add(AnoLabel)
        Me.Controls.Add(Me.AnoComboBox)
        Me.Controls.Add(MarcaLabel)
        Me.Controls.Add(Me.MarcaComboBox)
        Me.Controls.Add(PrecoLabel)
        Me.Controls.Add(Me.PrecoTextBox)
        Me.Controls.Add(VendidoLabel)
        Me.Controls.Add(Me.VendidoCheckBox)
        Me.Controls.Add(ImportadoLabel)
        Me.Controls.Add(Me.ImportadoCheckBox)
        Me.Controls.Add(CodigoLabel)
        Me.Controls.Add(Me.CodigoTextBox)
        Me.Controls.Add(Me.CarrosBindingNavigator)
        Me.Name = "Form1"
        Me.Text = "Form1"
        CType(Me._MyDatabase_1DataSet1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CarrosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CarrosBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CarrosBindingNavigator.ResumeLayout(False)
        Me.CarrosBindingNavigator.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents _MyDatabase_1DataSet1 As WindowsApplication1._MyDatabase_1DataSet1
    Friend WithEvents CarrosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CarrosTableAdapter As WindowsApplication1._MyDatabase_1DataSet1TableAdapters.CarrosTableAdapter
    Friend WithEvents TableAdapterManager As WindowsApplication1._MyDatabase_1DataSet1TableAdapters.TableAdapterManager
    Friend WithEvents CarrosBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorAddNewItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorCountItem As System.Windows.Forms.ToolStripLabel
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMoveFirstItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMovePreviousItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorPositionItem As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents BindingNavigatorSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorMoveNextItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMoveLastItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents CarrosBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents CodigoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ImportadoCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents VendidoCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents PrecoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents MarcaComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents AnoComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents EstadoComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents ConbustivelComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents ModeloTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Button1 As System.Windows.Forms.Button

End Class
