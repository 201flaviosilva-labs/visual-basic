﻿Public Class exercicio_2
    Dim n1, tentativa, repeticoes, nt, minimo, maximo, maximotentativas As Integer
    Dim nr As New Random
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        GroupBox1.Enabled = False
        GroupBox2.Enabled = False
        Button1.Enabled = False
        NumericUpDown1.Enabled = False
        NumericUpDown2.Enabled = False


        Label1.Enabled = True
        Label2.Enabled = True
        TextBox1.Enabled = True
        Button2.Enabled = True
        ListBox1.Enabled = True

        n1 = 0
        tentativa = 0
        repeticoes = 0

        If RadioButton1.Checked Then
            Randomize()
            n1 = Int(100 * Rnd() + 1)
        End If


        If RadioButton2.Checked Then
            minimo = NumericUpDown1.Value
            maximo = NumericUpDown2.Value
            n1 = nr.Next(maximo, minimo + 1)
        End If

        ' Label3.Text = n1 (só ativar para testar) '
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        nt = nt + 1

        tentativa = TextBox1.Text
        If n1 < tentativa Then
            MsgBox(" Escolha um número Menor")
            ListBox1.Items.Add(nt & " " & tentativa & " > Número")
        End If


        If n1 > tentativa Then
            MsgBox("Escolha um número Maior")
            ListBox1.Items.Add(nt & " " & tentativa & " < Número")
        End If

        If n1 = tentativa Then
            MsgBox("Parabens conseguiste")
            ListBox1.Items.Add(nt & " " & tentativa & " Parabens")
        End If











        If CheckBox2.Checked Then
            maximotentativas = NumericUpDown3.Value

            If maximotentativas = nt Then
                MsgBox("Atiguio o limite")
                ListBox1.Items.Add(nt & " Atiguio o limite")
                Button2.Enabled = False
            End If


        End If


    End Sub

    Private Sub RadioButton1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton1.CheckedChanged
        NumericUpDown1.Enabled = False
        NumericUpDown2.Enabled = False
        CheckBox1.Enabled = False
    End Sub

    Private Sub RadioButton2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton2.CheckedChanged
        NumericUpDown1.Enabled = True
        NumericUpDown2.Enabled = True
        CheckBox1.Enabled = True
    End Sub

    Private Sub CheckBox2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox2.CheckedChanged
        NumericUpDown3.Enabled = True
    End Sub

    Private Sub Exercicio_2_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        NumericUpDown3.Enabled = False

        Label1.Enabled = False
        Label2.Enabled = False
        TextBox1.Enabled = False
        Button2.Enabled = False
        ListBox1.Enabled = False
    End Sub

End Class