﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.Opeção1ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.Opção11ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.Opção12ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.Opeção2ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.HoraEDataToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.FrasesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.BotãoDeCliquesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.InputMnsBoxToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AnosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ProjetosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.NomesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.MenuStrip1.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Opeção1ToolStripMenuItem, Me.Opeção2ToolStripMenuItem, Me.InputMnsBoxToolStripMenuItem, Me.ProjetosToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(993, 24)
        Me.MenuStrip1.TabIndex = 0
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'Opeção1ToolStripMenuItem
        '
        Me.Opeção1ToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Opção11ToolStripMenuItem, Me.Opção12ToolStripMenuItem})
        Me.Opeção1ToolStripMenuItem.Name = "Opeção1ToolStripMenuItem"
        Me.Opeção1ToolStripMenuItem.Size = New System.Drawing.Size(104, 20)
        Me.Opeção1ToolStripMenuItem.Text = "Formulário Web"
        '
        'Opção11ToolStripMenuItem
        '
        Me.Opção11ToolStripMenuItem.Name = "Opção11ToolStripMenuItem"
        Me.Opção11ToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.Opção11ToolStripMenuItem.Text = "Opção 1. 1"
        '
        'Opção12ToolStripMenuItem
        '
        Me.Opção12ToolStripMenuItem.Name = "Opção12ToolStripMenuItem"
        Me.Opção12ToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.Opção12ToolStripMenuItem.Text = "Opção 1.2"
        '
        'Opeção2ToolStripMenuItem
        '
        Me.Opeção2ToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.HoraEDataToolStripMenuItem, Me.FrasesToolStripMenuItem, Me.BotãoDeCliquesToolStripMenuItem})
        Me.Opeção2ToolStripMenuItem.Name = "Opeção2ToolStripMenuItem"
        Me.Opeção2ToolStripMenuItem.Size = New System.Drawing.Size(140, 20)
        Me.Opeção2ToolStripMenuItem.Text = "Label e Caixas de Texto"
        '
        'HoraEDataToolStripMenuItem
        '
        Me.HoraEDataToolStripMenuItem.Name = "HoraEDataToolStripMenuItem"
        Me.HoraEDataToolStripMenuItem.Size = New System.Drawing.Size(161, 22)
        Me.HoraEDataToolStripMenuItem.Text = "Hora e data"
        '
        'FrasesToolStripMenuItem
        '
        Me.FrasesToolStripMenuItem.Name = "FrasesToolStripMenuItem"
        Me.FrasesToolStripMenuItem.Size = New System.Drawing.Size(161, 22)
        Me.FrasesToolStripMenuItem.Text = "Frases"
        '
        'BotãoDeCliquesToolStripMenuItem
        '
        Me.BotãoDeCliquesToolStripMenuItem.Name = "BotãoDeCliquesToolStripMenuItem"
        Me.BotãoDeCliquesToolStripMenuItem.Size = New System.Drawing.Size(161, 22)
        Me.BotãoDeCliquesToolStripMenuItem.Text = "Botão de cliques"
        '
        'InputMnsBoxToolStripMenuItem
        '
        Me.InputMnsBoxToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AnosToolStripMenuItem})
        Me.InputMnsBoxToolStripMenuItem.Name = "InputMnsBoxToolStripMenuItem"
        Me.InputMnsBoxToolStripMenuItem.Size = New System.Drawing.Size(95, 20)
        Me.InputMnsBoxToolStripMenuItem.Text = "Input Mns Box"
        '
        'AnosToolStripMenuItem
        '
        Me.AnosToolStripMenuItem.Name = "AnosToolStripMenuItem"
        Me.AnosToolStripMenuItem.Size = New System.Drawing.Size(101, 22)
        Me.AnosToolStripMenuItem.Text = "Anos"
        '
        'ProjetosToolStripMenuItem
        '
        Me.ProjetosToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.NomesToolStripMenuItem})
        Me.ProjetosToolStripMenuItem.Name = "ProjetosToolStripMenuItem"
        Me.ProjetosToolStripMenuItem.Size = New System.Drawing.Size(62, 20)
        Me.ProjetosToolStripMenuItem.Text = "Projetos"
        '
        'NomesToolStripMenuItem
        '
        Me.NomesToolStripMenuItem.Name = "NomesToolStripMenuItem"
        Me.NomesToolStripMenuItem.Size = New System.Drawing.Size(112, 22)
        Me.NomesToolStripMenuItem.Text = "Nomes"
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(0, 27)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(993, 328)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 1
        Me.PictureBox1.TabStop = False
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(993, 353)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.MenuStrip1)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents Opeção1ToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Opção11ToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Opeção2ToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Opção12ToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents HoraEDataToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents FrasesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents InputMnsBoxToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AnosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents BotãoDeCliquesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ProjetosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents NomesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem

End Class
