﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class web2_clubes
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(web2_clubes))
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.ClubesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PrimeiraLigaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SegundaLigaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SportingToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.BenficaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.OlhanelasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.OlhanelesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PortugalToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        Me.PictureBox4 = New System.Windows.Forms.PictureBox()
        Me.PictureBox5 = New System.Windows.Forms.PictureBox()
        Me.PictureBox6 = New System.Windows.Forms.PictureBox()
        Me.MenuStrip1.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ClubesToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(459, 24)
        Me.MenuStrip1.TabIndex = 0
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'ClubesToolStripMenuItem
        '
        Me.ClubesToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.PrimeiraLigaToolStripMenuItem, Me.SegundaLigaToolStripMenuItem})
        Me.ClubesToolStripMenuItem.Name = "ClubesToolStripMenuItem"
        Me.ClubesToolStripMenuItem.Size = New System.Drawing.Size(53, 20)
        Me.ClubesToolStripMenuItem.Text = "clubes"
        '
        'PrimeiraLigaToolStripMenuItem
        '
        Me.PrimeiraLigaToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.SportingToolStripMenuItem, Me.BenficaToolStripMenuItem, Me.OlhanelasToolStripMenuItem, Me.OlhanelesToolStripMenuItem, Me.PortugalToolStripMenuItem})
        Me.PrimeiraLigaToolStripMenuItem.Name = "PrimeiraLigaToolStripMenuItem"
        Me.PrimeiraLigaToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.PrimeiraLigaToolStripMenuItem.Text = "Primeira liga"
        '
        'SegundaLigaToolStripMenuItem
        '
        Me.SegundaLigaToolStripMenuItem.Name = "SegundaLigaToolStripMenuItem"
        Me.SegundaLigaToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.SegundaLigaToolStripMenuItem.Text = "segunda liga"
        '
        'SportingToolStripMenuItem
        '
        Me.SportingToolStripMenuItem.Name = "SportingToolStripMenuItem"
        Me.SportingToolStripMenuItem.Size = New System.Drawing.Size(166, 22)
        Me.SportingToolStripMenuItem.Text = "sporting"
        '
        'BenficaToolStripMenuItem
        '
        Me.BenficaToolStripMenuItem.Name = "BenficaToolStripMenuItem"
        Me.BenficaToolStripMenuItem.Size = New System.Drawing.Size(166, 22)
        Me.BenficaToolStripMenuItem.Text = "benfica"
        '
        'OlhanelasToolStripMenuItem
        '
        Me.OlhanelasToolStripMenuItem.Name = "OlhanelasToolStripMenuItem"
        Me.OlhanelasToolStripMenuItem.Size = New System.Drawing.Size(166, 22)
        Me.OlhanelasToolStripMenuItem.Text = "Vitória de Setubal"
        '
        'OlhanelesToolStripMenuItem
        '
        Me.OlhanelesToolStripMenuItem.Name = "OlhanelesToolStripMenuItem"
        Me.OlhanelesToolStripMenuItem.Size = New System.Drawing.Size(166, 22)
        Me.OlhanelesToolStripMenuItem.Text = "Olhaneses"
        '
        'PortugalToolStripMenuItem
        '
        Me.PortugalToolStripMenuItem.Name = "PortugalToolStripMenuItem"
        Me.PortugalToolStripMenuItem.Size = New System.Drawing.Size(166, 22)
        Me.PortugalToolStripMenuItem.Text = "Portugal"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(80, 24)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(34, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Clube"
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(120, 59)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(327, 314)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 2
        Me.PictureBox1.TabStop = False
        '
        'PictureBox2
        '
        Me.PictureBox2.Image = CType(resources.GetObject("PictureBox2.Image"), System.Drawing.Image)
        Me.PictureBox2.Location = New System.Drawing.Point(0, 59)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(61, 63)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox2.TabIndex = 3
        Me.PictureBox2.TabStop = False
        Me.PictureBox2.Visible = False
        '
        'PictureBox3
        '
        Me.PictureBox3.Image = CType(resources.GetObject("PictureBox3.Image"), System.Drawing.Image)
        Me.PictureBox3.Location = New System.Drawing.Point(0, 128)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(61, 67)
        Me.PictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox3.TabIndex = 4
        Me.PictureBox3.TabStop = False
        Me.PictureBox3.Visible = False
        '
        'PictureBox4
        '
        Me.PictureBox4.Image = CType(resources.GetObject("PictureBox4.Image"), System.Drawing.Image)
        Me.PictureBox4.Location = New System.Drawing.Point(0, 201)
        Me.PictureBox4.Name = "PictureBox4"
        Me.PictureBox4.Size = New System.Drawing.Size(61, 61)
        Me.PictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox4.TabIndex = 5
        Me.PictureBox4.TabStop = False
        Me.PictureBox4.Visible = False
        '
        'PictureBox5
        '
        Me.PictureBox5.Image = CType(resources.GetObject("PictureBox5.Image"), System.Drawing.Image)
        Me.PictureBox5.Location = New System.Drawing.Point(0, 268)
        Me.PictureBox5.Name = "PictureBox5"
        Me.PictureBox5.Size = New System.Drawing.Size(33, 68)
        Me.PictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox5.TabIndex = 6
        Me.PictureBox5.TabStop = False
        Me.PictureBox5.Visible = False
        '
        'PictureBox6
        '
        Me.PictureBox6.Image = CType(resources.GetObject("PictureBox6.Image"), System.Drawing.Image)
        Me.PictureBox6.Location = New System.Drawing.Point(39, 268)
        Me.PictureBox6.Name = "PictureBox6"
        Me.PictureBox6.Size = New System.Drawing.Size(59, 78)
        Me.PictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox6.TabIndex = 7
        Me.PictureBox6.TabStop = False
        Me.PictureBox6.Visible = False
        '
        'web2_clubes
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(459, 368)
        Me.Controls.Add(Me.PictureBox6)
        Me.Controls.Add(Me.PictureBox5)
        Me.Controls.Add(Me.PictureBox4)
        Me.Controls.Add(Me.PictureBox3)
        Me.Controls.Add(Me.PictureBox2)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.MenuStrip1)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "web2_clubes"
        Me.Text = "web2_clubes"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents ClubesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PrimeiraLigaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SportingToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents BenficaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents OlhanelasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents OlhanelesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PortugalToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SegundaLigaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox3 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox4 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox5 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox6 As System.Windows.Forms.PictureBox
End Class
