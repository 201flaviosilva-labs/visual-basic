﻿Public Class Form1

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        pausa(750) 'Chama o Sub Pausa e diz quantos milisegundos é de pausa
        Label1.Visible = True 'Torna a label Visivel
    End Sub

    Sub pausa(ByVal tempopausa)
        'Tempo de Pausa'

        'Não fui eu que criei este código, por isso não sei como isto fonciona, mas tentei explicar o melhor que pude

        Dim tempoatual As Date 'Cria uma variabel do tipo data
        Dim tempoparado As Date 'Cria uma variabel do tipo data

        tempoatual = Date.Now() 'Recebe o tempo atual
        tempoparado = tempoatual.AddMilliseconds(tempopausa) 'O tempo que irá estar parado é igual ao tempo que foi defenido (tempopausa) que iria estar em pausa

        Do While Date.Now < tempoparado 'Faz isto enquanto agora(Tempo) for menor que o tempo parado
            Application.DoEvents() 'a aplicação pára o que está a aconter
        Loop ' Fim do ciclo
    End Sub
End Class
