﻿Public Class Form2
    Dim NErros, NivelErro As Double
    Dim tempo As Double = 10000
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        AnalizadorErros()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Form1.Show()
        Me.Close()
    End Sub

    Private Sub Form2_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        AnalizadorErros()
    End Sub
    Sub AnalizadorErros()
        NErros = 0
        Randomize()
        NErros = Int(10 * Rnd())

        NivelErro = Int(2 * Rnd() + 1)

        If NivelErro = 1 And NErros >= 1 Then
            Label16.BackColor = Color.Red
        ElseIf NivelErro = 2 And NErros >= 1 Then
            Label16.BackColor = Color.Orange
        Else
            Label16.BackColor = Color.White
        End If
        Label65.Text = NErros & "/10"
    End Sub

    Private Sub Atualizar_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Atualizar.Tick
        tempo = tempo - 1
        Label4.Text = tempo & "/1000"
        If tempo = 0 Then
            AnalizadorErros()
            tempo = 10000
        End If
    End Sub
End Class