﻿Public Class Form1
    Dim ErroLinha, ErroLinhaAleatorio, ErroCuluna, ErroCulunaAleatorio, NErros, NivelErro, NErrosCel(49) As Double
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim count2 As Double
        For count2 = 1 To 49
            NerrosCel(count2) = 0
        Next
        Label2.Visible = False
        Label3.Visible = False
        Label4.Visible = False
        Label5.Visible = False
        Label6.Visible = False
        Label7.Visible = False
        Label8.Visible = False
        Label9.Visible = False
        Label10.Visible = False
        Label11.Visible = False
        Label12.Visible = False
        Label13.Visible = False
        Label14.Visible = False
        Label15.Visible = False
        Label16.Visible = False
        Label17.Visible = False
        Label18.Visible = False
        Label19.Visible = False
        Label20.Visible = False
        Label21.Visible = False
        Label22.Visible = False
        Label23.Visible = False
        Label24.Visible = False
        Label25.Visible = False
        Label26.Visible = False
        Label27.Visible = False
        Label28.Visible = False
        Label29.Visible = False
        Label30.Visible = False
        Label31.Visible = False
        Label32.Visible = False
        Label33.Visible = False
        Label34.Visible = False
        Label35.Visible = False
        Label36.Visible = False
        Label37.Visible = False
        Label38.Visible = False
        Label39.Visible = False
        Label40.Visible = False
        Label41.Visible = False
        Label42.Visible = False
        Label43.Visible = False
        Label44.Visible = False
        Label45.Visible = False
        Label46.Visible = False
        Label47.Visible = False
        Label48.Visible = False
        Label49.Visible = False
        Label50.Visible = False
        Label51.Visible = False
        Label52.Visible = False
        Label53.Visible = False
        Label54.Visible = False
        Label55.Visible = False
        Label56.Visible = False
        Label57.Visible = False
        Label58.Visible = False
        Label59.Visible = False
        Label60.Visible = False
        Label61.Visible = False
        Label62.Visible = False
        Label63.Visible = False
        Label64.Visible = False

        NErros = 0
        Randomize()
        NErros = Int(10 * Rnd())

        For count = 1 To NErros

            Randomize()
            ErroLinhaAleatorio = Int(7 * Rnd() + 1)

            Randomize()
            ErroCulunaAleatorio = Int(7 * Rnd() + 1)

            NivelErro = Int(2 * Rnd() + 1)

            ErroLinha = ErroLinhaAleatorio
            ErroCuluna = ErroCulunaAleatorio

            If ErroCuluna = 1 Then
                Label2.Visible = True
                If ErroLinha = 1 Then
                    Label9.Visible = True
                    Label16.Visible = True
                    Label16.Enabled = True
                    If NivelErro = 1 Then
                        Label16.BackColor = Color.Red
                    Else
                        Label16.BackColor = Color.Orange
                    End If
                    NErrosCel(1) += 1
                    Label16.Text = NErrosCel(1)

                ElseIf ErroLinha = 2 Then
                    Label10.Visible = True
                    Label17.Visible = True
                    Label17.Enabled = True
                    If NivelErro = 1 Then
                        Label17.BackColor = Color.Red
                    Else
                        Label17.BackColor = Color.Orange
                    End If
                    NErrosCel(2) += 1
                    Label17.Text = NErrosCel(2)

                ElseIf ErroLinha = 3 Then
                    Label11.Visible = True
                    Label18.Visible = True
                    Label18.Enabled = True
                    If NivelErro = 1 Then
                        Label18.BackColor = Color.Red
                    Else
                        Label18.BackColor = Color.Orange
                    End If
                    NErrosCel(3) += 1
                    Label18.Text = NErrosCel(3)

                ElseIf ErroLinha = 4 Then
                    Label12.Visible = True
                    Label19.Visible = True
                    Label19.Enabled = True
                    If NivelErro = 1 Then
                        Label19.BackColor = Color.Red
                    Else
                        Label19.BackColor = Color.Orange
                    End If
                    NErrosCel(4) += 1
                    Label19.Text = NErrosCel(4)

                ElseIf ErroLinha = 5 Then
                    Label13.Visible = True
                    Label20.Visible = True
                    Label20.Enabled = True
                    If NivelErro = 1 Then
                        Label20.BackColor = Color.Red
                    Else
                        Label20.BackColor = Color.Orange
                    End If
                    NErrosCel(5) += 1
                    Label20.Text = NErrosCel(5)

                ElseIf ErroLinha = 6 Then
                    Label14.Visible = True
                    Label21.Visible = True
                    Label21.Enabled = True
                    If NivelErro = 1 Then
                        Label21.BackColor = Color.Red
                    Else
                        Label21.BackColor = Color.Orange
                    End If
                    NErrosCel(6) += 1
                    Label21.Text = NErrosCel(6)

                ElseIf ErroLinha = 7 Then
                    Label15.Visible = True
                    Label22.Visible = True
                    Label22.Enabled = True
                    If NivelErro = 1 Then
                        Label22.BackColor = Color.Red
                    Else
                        Label22.BackColor = Color.Orange
                    End If
                    NErrosCel(7) += 1
                    Label22.Text = NErrosCel(7)
                End If
                
            ElseIf ErroCuluna = 2 Then
                Label3.Visible = True
                If ErroLinha = 1 Then
                    Label9.Visible = True
                    Label23.Visible = True
                    Label23.Enabled = True
                    If NivelErro = 1 Then
                        Label23.BackColor = Color.Red
                    Else
                        Label23.BackColor = Color.Orange
                    End If
                    NErrosCel(ErroCuluna * ErroLinha) += 1
                    Label23.Text = NErrosCel(ErroCuluna * ErroLinha)

                ElseIf ErroLinha = 2 Then
                    Label10.Visible = True
                    Label24.Visible = True
                    Label24.Enabled = True
                    If NivelErro = 1 Then
                        Label24.BackColor = Color.Red
                    Else
                        Label24.BackColor = Color.Orange
                    End If
                    NErrosCel(ErroCuluna * ErroLinha) += 1
                    Label24.Text = NErrosCel(ErroCuluna * ErroLinha)

                ElseIf ErroLinha = 3 Then
                    Label11.Visible = True
                    Label25.Visible = True
                    Label25.Enabled = True
                    If NivelErro = 1 Then
                        Label25.BackColor = Color.Red
                    Else
                        Label25.BackColor = Color.Orange
                    End If
                    NErrosCel(ErroCuluna * ErroLinha) += 1
                    Label25.Text = NErrosCel(ErroCuluna * ErroLinha)

                ElseIf ErroLinha = 4 Then
                    Label12.Visible = True
                    Label26.Visible = True
                    Label26.Enabled = True
                    If NivelErro = 1 Then
                        Label26.BackColor = Color.Red
                    Else
                        Label26.BackColor = Color.Orange
                    End If
                    NErrosCel(ErroCuluna * ErroLinha) += 1
                    Label26.Text = NErrosCel(ErroCuluna * ErroLinha)

                ElseIf ErroLinha = 5 Then
                    Label13.Visible = True
                    Label27.Visible = True
                    Label27.Enabled = True
                    If NivelErro = 1 Then
                        Label27.BackColor = Color.Red
                    Else
                        Label27.BackColor = Color.Orange
                    End If
                    NErrosCel(ErroCuluna * ErroLinha) += 1
                    Label27.Text = NErrosCel(ErroCuluna * ErroLinha)

                ElseIf ErroLinha = 6 Then
                    Label14.Visible = True
                    Label28.Visible = True
                    Label28.Enabled = True
                    If NivelErro = 1 Then
                        Label28.BackColor = Color.Red
                    Else
                        Label28.BackColor = Color.Orange
                    End If
                    NErrosCel(ErroCuluna * ErroLinha) += 1
                    Label28.Text = NErrosCel(ErroCuluna * ErroLinha)

                ElseIf ErroLinha = 7 Then
                    Label15.Visible = True
                    Label29.Visible = True
                    Label29.Enabled = True
                    If NivelErro = 1 Then
                        Label29.BackColor = Color.Red
                    Else
                        Label29.BackColor = Color.Orange
                    End If
                    NErrosCel(ErroCuluna * ErroLinha) += 1
                    Label29.Text = NErrosCel(ErroCuluna * ErroLinha)
                End If

            ElseIf ErroCuluna = 3 Then
                Label4.Visible = True
                If ErroLinha = 1 Then
                    Label9.Visible = True
                    Label30.Visible = True
                    Label30.Enabled = True
                    If NivelErro = 1 Then
                        Label30.BackColor = Color.Red
                    Else
                        Label30.BackColor = Color.Orange
                    End If
                    NErrosCel(ErroCuluna * ErroLinha) += 1
                    Label30.Text = NErrosCel(ErroCuluna * ErroLinha)

                ElseIf ErroLinha = 2 Then
                    Label10.Visible = True
                    Label31.Visible = True
                    Label31.Enabled = True
                    If NivelErro = 1 Then
                        Label31.BackColor = Color.Red
                    Else
                        Label31.BackColor = Color.Orange
                    End If
                    NErrosCel(ErroCuluna * ErroLinha) += 1
                    Label31.Text = NErrosCel(ErroCuluna * ErroLinha)

                ElseIf ErroLinha = 3 Then
                    Label11.Visible = True
                    Label32.Visible = True
                    Label32.Enabled = True
                    If NivelErro = 1 Then
                        Label32.BackColor = Color.Red
                    Else
                        Label32.BackColor = Color.Orange
                    End If
                    NErrosCel(ErroCuluna * ErroLinha) += 1
                    Label32.Text = NErrosCel(ErroCuluna * ErroLinha)

                ElseIf ErroLinha = 4 Then
                    Label12.Visible = True
                    Label33.Visible = True
                    Label33.Enabled = True
                    If NivelErro = 1 Then
                        Label33.BackColor = Color.Red
                    Else
                        Label33.BackColor = Color.Orange
                    End If
                    NErrosCel(ErroCuluna * ErroLinha) += 1
                    Label33.Text = NErrosCel(ErroCuluna * ErroLinha)

                ElseIf ErroLinha = 5 Then
                    Label13.Visible = True
                    Label34.Visible = True
                    Label34.Enabled = True
                    If NivelErro = 1 Then
                        Label34.BackColor = Color.Red
                    Else
                        Label34.BackColor = Color.Orange
                    End If
                    NErrosCel(ErroCuluna * ErroLinha) += 1
                    Label34.Text = NErrosCel(ErroCuluna * ErroLinha)

                ElseIf ErroLinha = 6 Then
                    Label14.Visible = True
                    Label35.Visible = True
                    Label35.Enabled = True
                    If NivelErro = 1 Then
                        Label35.BackColor = Color.Red
                    Else
                        Label35.BackColor = Color.Orange
                    End If
                    NErrosCel(ErroCuluna * ErroLinha) += 1
                    Label35.Text = NErrosCel(ErroCuluna * ErroLinha)

                ElseIf ErroLinha = 7 Then
                    Label15.Visible = True
                    Label36.Visible = True
                    Label36.Enabled = True
                    If NivelErro = 1 Then
                        Label36.BackColor = Color.Red
                    Else
                        Label36.BackColor = Color.Orange
                    End If
                    NErrosCel(ErroCuluna * ErroLinha) += 1
                    Label36.Text = NErrosCel(ErroCuluna * ErroLinha)
                End If

            ElseIf ErroCuluna = 4 Then
                Label5.Visible = True
                If ErroLinha = 1 Then
                    Label9.Visible = True
                    Label37.Visible = True
                    Label37.Enabled = True
                    If NivelErro = 1 Then
                        Label37.BackColor = Color.Red
                    Else
                        Label37.BackColor = Color.Orange
                    End If
                    NErrosCel(ErroCuluna * ErroLinha) += 1
                    Label37.Text = NErrosCel(ErroCuluna * ErroLinha)

                ElseIf ErroLinha = 2 Then
                    Label10.Visible = True
                    Label38.Visible = True
                    Label38.Enabled = True
                    If NivelErro = 1 Then
                        Label38.BackColor = Color.Red
                    Else
                        Label38.BackColor = Color.Orange
                    End If
                    NErrosCel(ErroCuluna * ErroLinha) += 1
                    Label38.Text = NErrosCel(ErroCuluna * ErroLinha)

                ElseIf ErroLinha = 3 Then
                    Label11.Visible = True
                    Label39.Visible = True
                    Label39.Enabled = True
                    If NivelErro = 1 Then
                        Label39.BackColor = Color.Red
                    Else
                        Label39.BackColor = Color.Orange
                    End If
                    NErrosCel(ErroCuluna * ErroLinha) += 1
                    Label39.Text = NErrosCel(ErroCuluna * ErroLinha)

                ElseIf ErroLinha = 4 Then
                    Label12.Visible = True
                    Label40.Visible = True
                    Label40.Enabled = True
                    If NivelErro = 1 Then
                        Label40.BackColor = Color.Red
                    Else
                        Label40.BackColor = Color.Orange
                    End If
                    NErrosCel(ErroCuluna * ErroLinha) += 1
                    Label40.Text = NErrosCel(ErroCuluna * ErroLinha)

                ElseIf ErroLinha = 5 Then
                    Label13.Visible = True
                    Label41.Visible = True
                    Label41.Enabled = True
                    If NivelErro = 1 Then
                        Label41.BackColor = Color.Red
                    Else
                        Label41.BackColor = Color.Orange
                    End If
                    NErrosCel(ErroCuluna * ErroLinha) += 1
                    Label41.Text = NErrosCel(ErroCuluna * ErroLinha)

                ElseIf ErroLinha = 6 Then
                    Label14.Visible = True
                    Label42.Visible = True
                    Label42.Enabled = True
                    If NivelErro = 1 Then
                        Label42.BackColor = Color.Red
                    Else
                        Label42.BackColor = Color.Orange
                    End If
                    NErrosCel(ErroCuluna * ErroLinha) += 1
                    Label42.Text = NErrosCel(ErroCuluna * ErroLinha)

                ElseIf ErroLinha = 7 Then
                    Label15.Visible = True
                    Label43.Visible = True
                    Label43.Enabled = True
                    If NivelErro = 1 Then
                        Label43.BackColor = Color.Red
                    Else
                        Label43.BackColor = Color.Orange
                    End If
                    NErrosCel(ErroCuluna * ErroLinha) += 1
                    Label43.Text = NErrosCel(ErroCuluna * ErroLinha)
                End If

            ElseIf ErroCuluna = 5 Then
                Label6.Visible = True
                If ErroLinha = 1 Then
                    Label9.Visible = True
                    Label44.Visible = True
                    Label44.Enabled = True
                    If NivelErro = 1 Then
                        Label44.BackColor = Color.Red
                    Else
                        Label44.BackColor = Color.Orange
                    End If
                    NErrosCel(ErroCuluna * ErroLinha) += 1
                    Label44.Text = NErrosCel(ErroCuluna * ErroLinha)

                ElseIf ErroLinha = 2 Then
                    Label10.Visible = True
                    Label45.Visible = True
                    Label45.Enabled = True
                    If NivelErro = 1 Then
                        Label45.BackColor = Color.Red
                    Else
                        Label45.BackColor = Color.Orange
                    End If
                    NErrosCel(ErroCuluna * ErroLinha) += 1
                    Label45.Text = NErrosCel(ErroCuluna * ErroLinha)

                ElseIf ErroLinha = 3 Then
                    Label11.Visible = True
                    Label46.Visible = True
                    Label46.Enabled = True
                    If NivelErro = 1 Then
                        Label46.BackColor = Color.Red
                    Else
                        Label46.BackColor = Color.Orange
                    End If
                    NErrosCel(ErroCuluna * ErroLinha) += 1
                    Label46.Text = NErrosCel(ErroCuluna * ErroLinha)

                ElseIf ErroLinha = 4 Then
                    Label12.Visible = True
                    Label47.Visible = True
                    Label47.Enabled = True
                    If NivelErro = 1 Then
                        Label47.BackColor = Color.Red
                    Else
                        Label47.BackColor = Color.Orange
                    End If
                    NErrosCel(ErroCuluna * ErroLinha) += 1
                    Label47.Text = NErrosCel(ErroCuluna * ErroLinha)

                ElseIf ErroLinha = 5 Then
                    Label13.Visible = True
                    Label48.Visible = True
                    Label48.Enabled = True
                    If NivelErro = 1 Then
                        Label48.BackColor = Color.Red
                    Else
                        Label48.BackColor = Color.Orange
                    End If
                    NErrosCel(ErroCuluna * ErroLinha) += 1
                    Label48.Text = NErrosCel(ErroCuluna * ErroLinha)

                ElseIf ErroLinha = 6 Then
                    Label14.Visible = True
                    Label49.Visible = True
                    Label49.Enabled = True
                    If NivelErro = 1 Then
                        Label49.BackColor = Color.Red
                    Else
                        Label49.BackColor = Color.Orange
                    End If
                    NErrosCel(ErroCuluna * ErroLinha) += 1
                    Label49.Text = NErrosCel(ErroCuluna * ErroLinha)

                ElseIf ErroLinha = 7 Then
                    Label15.Visible = True
                    Label50.Visible = True
                    Label50.Enabled = True
                    If NivelErro = 1 Then
                        Label50.BackColor = Color.Red
                    Else
                        Label50.BackColor = Color.Orange
                    End If
                    NErrosCel(ErroCuluna * ErroLinha) += 1
                    Label50.Text = NErrosCel(ErroCuluna * ErroLinha)
                End If

            ElseIf ErroCuluna = 6 Then
                Label7.Visible = True
                If ErroLinha = 1 Then
                    Label9.Visible = True
                    Label51.Visible = True
                    Label51.Enabled = True
                    If NivelErro = 1 Then
                        Label51.BackColor = Color.Red
                    Else
                        Label51.BackColor = Color.Orange
                    End If
                    NErrosCel(ErroCuluna * ErroLinha) += 1
                    Label51.Text = NErrosCel(ErroCuluna * ErroLinha)

                ElseIf ErroLinha = 2 Then
                    Label10.Visible = True
                    Label52.Visible = True
                    Label52.Enabled = True
                    If NivelErro = 1 Then
                        Label52.BackColor = Color.Red
                    Else
                        Label52.BackColor = Color.Orange
                    End If
                    NErrosCel(ErroCuluna * ErroLinha) += 1
                    Label52.Text = NErrosCel(ErroCuluna * ErroLinha)

                ElseIf ErroLinha = 3 Then
                    Label11.Visible = True
                    Label53.Visible = True
                    Label53.Enabled = True
                    If NivelErro = 1 Then
                        Label53.BackColor = Color.Red
                    Else
                        Label53.BackColor = Color.Orange
                    End If
                    NErrosCel(ErroCuluna * ErroLinha) += 1
                    Label53.Text = NErrosCel(ErroCuluna * ErroLinha)

                ElseIf ErroLinha = 4 Then
                    Label12.Visible = True
                    Label54.Visible = True
                    Label54.Enabled = True
                    If NivelErro = 1 Then
                        Label54.BackColor = Color.Red
                    Else
                        Label54.BackColor = Color.Orange
                    End If
                    NErrosCel(ErroCuluna * ErroLinha) += 1
                    Label54.Text = NErrosCel(ErroCuluna * ErroLinha)

                ElseIf ErroLinha = 5 Then
                    Label13.Visible = True
                    Label55.Visible = True
                    Label55.Enabled = True
                    If NivelErro = 1 Then
                        Label55.BackColor = Color.Red
                    Else
                        Label55.BackColor = Color.Orange
                    End If
                    NErrosCel(ErroCuluna * ErroLinha) += 1
                    Label55.Text = NErrosCel(ErroCuluna * ErroLinha)

                ElseIf ErroLinha = 6 Then
                    Label14.Visible = True
                    Label56.Visible = True
                    Label56.Enabled = True
                    If NivelErro = 1 Then
                        Label56.BackColor = Color.Red
                    Else
                        Label56.BackColor = Color.Orange
                    End If
                    NErrosCel(ErroCuluna * ErroLinha) += 1
                    Label56.Text = NErrosCel(ErroCuluna * ErroLinha)

                ElseIf ErroLinha = 7 Then
                    Label15.Visible = True
                    Label57.Visible = True
                    Label57.Enabled = True
                    If NivelErro = 1 Then
                        Label57.BackColor = Color.Red
                    Else
                        Label57.BackColor = Color.Orange
                    End If
                    NErrosCel(ErroCuluna * ErroLinha) += 1
                    Label57.Text = NErrosCel(ErroCuluna * ErroLinha)
                End If

            ElseIf ErroCuluna = 7 Then
                Label8.Visible = True
                If ErroLinha = 1 Then
                    Label9.Visible = True
                    Label58.Visible = True
                    Label58.Enabled = True
                    If NivelErro = 1 Then
                        Label58.BackColor = Color.Red
                    Else
                        Label58.BackColor = Color.Orange
                    End If
                    NErrosCel(ErroCuluna * ErroLinha) += 1
                    Label58.Text = NErrosCel(ErroCuluna * ErroLinha)

                ElseIf ErroLinha = 2 Then
                    Label10.Visible = True
                    Label59.Visible = True
                    Label59.Enabled = True
                    If NivelErro = 1 Then
                        Label59.BackColor = Color.Red
                    Else
                        Label59.BackColor = Color.Orange
                    End If
                    NErrosCel(ErroCuluna * ErroLinha) += 1
                    Label59.Text = NErrosCel(ErroCuluna * ErroLinha)

                ElseIf ErroLinha = 3 Then
                    Label11.Visible = True
                    Label60.Visible = True
                    Label60.Enabled = True
                    If NivelErro = 1 Then
                        Label60.BackColor = Color.Red
                    Else
                        Label60.BackColor = Color.Orange
                    End If
                    NErrosCel(ErroCuluna * ErroLinha) += 1
                    Label60.Text = NErrosCel(ErroCuluna * ErroLinha)

                ElseIf ErroLinha = 4 Then
                    Label12.Visible = True
                    Label61.Visible = True
                    Label61.Enabled = True
                    If NivelErro = 1 Then
                        Label61.BackColor = Color.Red
                    Else
                        Label61.BackColor = Color.Orange
                    End If
                    NErrosCel(ErroCuluna * ErroLinha) += 1
                    Label61.Text = NErrosCel(ErroCuluna * ErroLinha)

                ElseIf ErroLinha = 5 Then
                    Label13.Visible = True
                    Label62.Visible = True
                    Label62.Enabled = True
                    If NivelErro = 1 Then
                        Label62.BackColor = Color.Red
                    Else
                        Label62.BackColor = Color.Orange
                    End If
                    NErrosCel(ErroCuluna * ErroLinha) += 1
                    Label62.Text = NErrosCel(ErroCuluna * ErroLinha)

                ElseIf ErroLinha = 6 Then
                    Label14.Visible = True
                    Label63.Visible = True
                    Label63.Enabled = True
                    If NivelErro = 1 Then
                        Label63.BackColor = Color.Red
                    Else
                        Label63.BackColor = Color.Orange
                    End If
                    NErrosCel(ErroCuluna * ErroLinha) += 1
                    Label63.Text = NErrosCel(ErroCuluna * ErroLinha)

                ElseIf ErroLinha = 7 Then
                    Label15.Visible = True
                    Label64.Visible = True
                    Label64.Enabled = True
                    If NivelErro = 1 Then
                        Label64.BackColor = Color.Red
                    Else
                        Label64.BackColor = Color.Orange
                    End If
                    NErrosCel(ErroCuluna * ErroLinha) += 1
                    Label64.Text = NErrosCel(ErroCuluna * ErroLinha)
                End If
            End If
        Next
        Label65.Text = NErros & "/10"
    End Sub

    Private Sub Label16_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label16.Click
        MsgBox("Servidor: 'X' está com " & NErrosCel(1) & " erros no serviço 'b'")
    End Sub

    Private Sub Label17_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label17.Click
        MsgBox("Servidor: 'X' está com " & NErrosCel(2) & " erros no serviço 'b'")
    End Sub

    Private Sub Label18_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label18.Click
        MsgBox("Servidor: 'X' está com " & NErrosCel(3) & " erros no serviço 'b'")
    End Sub

    Private Sub Label19_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label19.Click
        MsgBox("Servidor: 'X' está com " & NErrosCel(4) & " erros no serviço 'b'")
    End Sub

    Private Sub Label20_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label20.Click
        MsgBox("Servidor: 'X' está com " & NErrosCel(5) & " erros no serviço 'b'")
    End Sub

    Private Sub Label21_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label21.Click
        MsgBox("Servidor: 'X' está com " & NErrosCel(6) & " erros no serviço 'b'")
    End Sub

    Private Sub Label22_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label22.Click
        MsgBox("Servidor: 'X' está com " & NErrosCel(7) & " erros no serviço 'b'")
    End Sub

    Private Sub Label23_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label23.Click
        MsgBox("Servidor: 'X' está com " & NErrosCel(8) & " erros no serviço 'b'")
    End Sub

    Private Sub Label24_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label24.Click
        MsgBox("Servidor: 'X' está com " & NErrosCel(9) & " erros no serviço 'b'")
    End Sub

    Private Sub Label25_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label25.Click
        MsgBox("Servidor: 'X' está com " & NErrosCel(10) & " erros no serviço 'b'")
    End Sub

    Private Sub Label26_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label26.Click
        MsgBox("Servidor: 'X' está com " & NErrosCel(11) & " erros no serviço 'b'")
    End Sub

    Private Sub Label27_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label27.Click
        MsgBox("Servidor: 'X' está com " & NErrosCel(12) & " erros no serviço 'b'")
    End Sub

    Private Sub Label28_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label28.Click
        MsgBox("Servidor: 'X' está com " & NErrosCel(13) & " erros no serviço 'b'")
    End Sub

    Private Sub Label29_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label29.Click
        MsgBox("Servidor: 'X' está com " & NErrosCel(14) & " erros no serviço 'b'")
    End Sub

    Private Sub Label30_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label30.Click
        MsgBox("Servidor: 'X' está com " & NErrosCel(15) & " erros no serviço 'b'")
    End Sub

    Private Sub Label31_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label31.Click
        MsgBox("Servidor: 'X' está com " & NErrosCel(16) & " erros no serviço 'b'")
    End Sub

    Private Sub Label32_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label32.Click
        MsgBox("Servidor: 'X' está com " & NErrosCel(17) & " erros no serviço 'b'")
    End Sub

    Private Sub Label33_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label33.Click
        MsgBox("Servidor: 'X' está com " & NErrosCel(18) & " erros no serviço 'b'")
    End Sub

    Private Sub Label34_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label34.Click
        MsgBox("Servidor: 'X' está com " & NErrosCel(19) & " erros no serviço 'b'")
    End Sub

    Private Sub Label35_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label35.Click
        MsgBox("Servidor: 'X' está com " & NErrosCel(20) & " erros no serviço 'b'")
    End Sub

    Private Sub Label36_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label36.Click
        MsgBox("Servidor: 'X' está com " & NErrosCel(21) & " erros no serviço 'b'")
    End Sub

    Private Sub Label37_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label37.Click
        MsgBox("Servidor: 'X' está com " & NErrosCel(22) & " erros no serviço 'b'")
    End Sub

    Private Sub Label38_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label38.Click
        MsgBox("Servidor: 'X' está com " & NErrosCel(23) & " erros no serviço 'b'")
    End Sub

    Private Sub Label39_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label39.Click
        MsgBox("Servidor: 'X' está com " & NErrosCel(24) & " erros no serviço 'b'")
    End Sub

    Private Sub Label40_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label40.Click
        MsgBox("Servidor: 'X' está com " & NErrosCel(25) & " erros no serviço 'b'")
    End Sub

    Private Sub Label41_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label41.Click
        MsgBox("Servidor: 'X' está com " & NErrosCel(26) & " erros no serviço 'b'")
    End Sub

    Private Sub Label42_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label42.Click
        MsgBox("Servidor: 'X' está com " & NErrosCel(27) & " erros no serviço 'b'")
    End Sub

    Private Sub Label43_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label43.Click
        MsgBox("Servidor: 'X' está com " & NErrosCel(28) & " erros no serviço 'b'")
    End Sub

    Private Sub Label44_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label44.Click
        MsgBox("Servidor: 'X' está com " & NErrosCel(29) & " erros no serviço 'b'")
    End Sub

    Private Sub Label45_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label45.Click
        MsgBox("Servidor: 'X' está com " & NErrosCel(30) & " erros no serviço 'b'")
    End Sub

    Private Sub Label46_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label46.Click
        MsgBox("Servidor: 'X' está com " & NErrosCel(31) & " erros no serviço 'b'")
    End Sub

    Private Sub Label47_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label47.Click
        MsgBox("Servidor: 'X' está com " & NErrosCel(32) & " erros no serviço 'b'")
    End Sub

    Private Sub Label48_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label48.Click
        MsgBox("Servidor: 'X' está com " & NErrosCel(33) & " erros no serviço 'b'")
    End Sub

    Private Sub Label49_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label49.Click
        MsgBox("Servidor: 'X' está com " & NErrosCel(34) & " erros no serviço 'b'")
    End Sub

    Private Sub Label50_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label50.Click
        MsgBox("Servidor: 'X' está com " & NErrosCel(35) & " erros no serviço 'b'")
    End Sub

    Private Sub Label51_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label51.Click
        MsgBox("Servidor: 'X' está com " & NErrosCel(36) & " erros no serviço 'b'")
    End Sub

    Private Sub Label52_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label52.Click
        MsgBox("Servidor: 'X' está com " & NErrosCel(37) & " erros no serviço 'b'")
    End Sub

    Private Sub Label53_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label53.Click
        MsgBox("Servidor: 'X' está com " & NErrosCel(38) & " erros no serviço 'b'")
    End Sub

    Private Sub Label54_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label54.Click
        MsgBox("Servidor: 'X' está com " & NErrosCel(39) & " erros no serviço 'b'")
    End Sub

    Private Sub Label55_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label55.Click
        MsgBox("Servidor: 'X' está com " & NErrosCel(40) & " erros no serviço 'b'")
    End Sub

    Private Sub Label56_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label56.Click
        MsgBox("Servidor: 'X' está com " & NErrosCel(41) & " erros no serviço 'b'")
    End Sub

    Private Sub Label57_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label57.Click
        MsgBox("Servidor: 'X' está com " & NErrosCel(42) & " erros no serviço 'b'")
    End Sub

    Private Sub Label58_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label58.Click
        MsgBox("Servidor: 'X' está com " & NErrosCel(43) & " erros no serviço 'b'")
    End Sub

    Private Sub Label59_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label59.Click
        MsgBox("Servidor: 'X' está com " & NErrosCel(44) & " erros no serviço 'b'")
    End Sub

    Private Sub Label60_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label60.Click
        MsgBox("Servidor: 'X' está com " & NErrosCel(45) & " erros no serviço 'b'")
    End Sub

    Private Sub Label61_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label61.Click
        MsgBox("Servidor: 'X' está com " & NErrosCel(46) & " erros no serviço 'b'")
    End Sub

    Private Sub Label62_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label62.Click
        MsgBox("Servidor: 'X' está com " & NErrosCel(47) & " erros no serviço 'b'")
    End Sub

    Private Sub Label63_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label63.Click
        MsgBox("Servidor: 'X' está com " & NErrosCel(48) & " erros no serviço 'b'")
    End Sub

    Private Sub Label64_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label64.Click
        MsgBox("Servidor: 'X' está com " & NErrosCel(49) & " erros no serviço 'b'")
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Form2.Show()
    End Sub
End Class
