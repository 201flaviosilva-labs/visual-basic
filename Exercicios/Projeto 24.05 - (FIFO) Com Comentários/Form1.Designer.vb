﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.ListBox1 = New System.Windows.Forms.ListBox()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.Inserir = New System.Windows.Forms.Button()
        Me.Remover = New System.Windows.Forms.Button()
        Me.Nome = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'ListBox1
        '
        Me.ListBox1.FormattingEnabled = True
        Me.ListBox1.Location = New System.Drawing.Point(19, 95)
        Me.ListBox1.Name = "ListBox1"
        Me.ListBox1.Size = New System.Drawing.Size(175, 212)
        Me.ListBox1.TabIndex = 6
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(82, 26)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(112, 20)
        Me.TextBox1.TabIndex = 5
        '
        'Inserir
        '
        Me.Inserir.Location = New System.Drawing.Point(19, 57)
        Me.Inserir.Name = "Inserir"
        Me.Inserir.Size = New System.Drawing.Size(76, 32)
        Me.Inserir.TabIndex = 4
        Me.Inserir.Text = "Inserir"
        Me.Inserir.UseVisualStyleBackColor = True
        '
        'Remover
        '
        Me.Remover.Location = New System.Drawing.Point(118, 57)
        Me.Remover.Name = "Remover"
        Me.Remover.Size = New System.Drawing.Size(76, 32)
        Me.Remover.TabIndex = 7
        Me.Remover.Text = "Remover"
        Me.Remover.UseVisualStyleBackColor = True
        '
        'Nome
        '
        Me.Nome.AutoSize = True
        Me.Nome.Location = New System.Drawing.Point(28, 29)
        Me.Nome.Name = "Nome"
        Me.Nome.Size = New System.Drawing.Size(35, 13)
        Me.Nome.TabIndex = 8
        Me.Nome.Text = "Nome"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(227, 331)
        Me.Controls.Add(Me.Nome)
        Me.Controls.Add(Me.Remover)
        Me.Controls.Add(Me.ListBox1)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.Inserir)
        Me.Name = "Form1"
        Me.Text = "FIFO"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ListBox1 As System.Windows.Forms.ListBox
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Inserir As System.Windows.Forms.Button
    Friend WithEvents Remover As System.Windows.Forms.Button
    Friend WithEvents Nome As System.Windows.Forms.Label

End Class
