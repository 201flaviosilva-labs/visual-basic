﻿Public Class Form1
    Dim registos() As String
    Dim elementos As Integer

    Private Sub Inserir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Inserir.Click

        ReDim Preserve registos(elementos)
        'redefine o tamanho do vetor sem alterar dados'

        registos(elementos) = TextBox1.Text
        'atribuição de um valor à nova posição'

        ListBox1.Items.Clear()
        For count = 0 To elementos
            ListBox1.Items.Add(registos(count))
        Next
        'O conteudo da lista é apagado e inserido novamente com o antigo + o novo'

        elementos += 1
        MsgBox("Número de elementos: " & elementos)
        'atualizar a variável elementos'

        TextBox1.Clear()
        TextBox1.Select()
        'limpar a text box'    
    End Sub

    Private Sub Remover_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Remover.Click
        elementos -= 1
        MsgBox("Número de elementos: " & elementos)
        'atualizar a variável elementos'

        For count = 0 To elementos - 1
            registos(count) = registos(count + 1)
        Next
        ListBox1.Items.Clear()
        For count = 0 To elementos - 1
            ListBox1.Items.Add(registos(count))
        Next
        'Os elementos sobem uma posição e apaga o primeiro'
    End Sub

End Class
