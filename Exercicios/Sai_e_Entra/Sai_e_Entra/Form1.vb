﻿Public Class Form1

    Private Sub Label1_MouseHover(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label1.MouseHover
        Label1.Text = "Sai"
        Label1.BackColor = Color.Yellow
        Label1.BorderStyle = BorderStyle.Fixed3D
        Label1.FlatStyle = FlatStyle.Flat
        Label1.ForeColor = Color.Blue
        Label1.TextAlign = ContentAlignment.TopLeft

    End Sub

    Private Sub Label1_MouseLeave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label1.MouseLeave
        Label1.Text = "Entra"
        Label1.BackColor = Color.Green
        Label1.BorderStyle = BorderStyle.FixedSingle
        Label1.FlatStyle = FlatStyle.System
        Label1.ForeColor = Color.Pink
        Label1.TextAlign = ContentAlignment.MiddleCenter
    End Sub

    Private Sub Label2_MouseDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Label2.MouseDown
        Label2.Text = "Desclica"
        Label2.BackColor = Color.Red
        Label2.BorderStyle = BorderStyle.FixedSingle
        Label2.FlatStyle = FlatStyle.Standard
        Label2.ForeColor = Color.Wheat
        Label2.TextAlign = ContentAlignment.BottomLeft
    End Sub

    Private Sub Label2_MouseUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Label2.MouseUp
        Label2.Text = "Clica"
        Label2.BackColor = Color.Yellow
        Label2.BorderStyle = BorderStyle.Fixed3D
        Label2.FlatStyle = FlatStyle.Popup
        Label2.ForeColor = Color.Brown
        Label2.TextAlign = ContentAlignment.MiddleCenter
    End Sub

    Private Sub Label3_MouseMove(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Label3.MouseMove
        Label3.Text = "Move"
        Label3.BackColor = Color.Firebrick
        Label3.BorderStyle = BorderStyle.Fixed3D
        Label3.FlatStyle = FlatStyle.Standard
        Label3.ForeColor = Color.Honeydew
        Label3.TextAlign = ContentAlignment.TopCenter
    End Sub

    Private Sub Label4_MouseEnter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label4.MouseEnter
        Label4.Text = "Entra"
        Label4.BackColor = Color.Teal
        Label4.BorderStyle = BorderStyle.None
        Label4.FlatStyle = FlatStyle.Flat
        Label4.ForeColor = Color.Ivory
        Label4.TextAlign = ContentAlignment.BottomRight
    End Sub
End Class
