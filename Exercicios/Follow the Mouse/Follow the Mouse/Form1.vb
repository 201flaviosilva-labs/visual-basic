﻿Public Class Form1

    Dim RatoX As Integer
    Dim RatoY As Integer

    Private Sub Form1_MouseMove(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles MyBase.MouseMove
        RatoX = e.X
        RatoY = e.Y
    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        Dim PicX As Integer = PictureBox1.Location.X
        Dim PicY As Integer = PictureBox1.Location.Y

        If PicX > RatoX Then
            PicX -= 2
            PictureBox1.Location = New Point(PicX, PicY)
        Else
            PicX += 2
            PictureBox1.Location = New Point(PicX, PicY)
        End If

        If PicY > RatoY Then
            PicY -= 2
            PictureBox1.Location = New Point(PicX, PicY)
        Else
            PicY += 2
            PictureBox1.Location = New Point(PicX, PicY)
        End If
    End Sub
End Class
