﻿Public Class Form1
    Dim mes, bsx, mes2, dia As Double

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        mes = TextBox1.Text
        If mes = 1 Or mes = 3 Or mes = 5 Or mes = 7 Or mes = 8 Or mes = 10 Or mes = 12 Then
            Label2.Text = 31
        End If
        If mes = 4 Or mes = 6 Or mes = 9 Or mes = 11 Then
            Label2.Text = 30
        ElseIf mes > 12 Then
            Label2.Text = "Mês Ainda Inexistente"
        ElseIf mes = 2 Then
            bsx = MsgBox("Ano Bissexto", vbQuestion + vbYesNo)
            If bsx = vbNo Then Label2.Text = "28" Else Label2.Text = "29"
        End If
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        mes2 = TextBox1.Text
        Select Case mes2
            Case 1, 3, 5, 7, 8, 10, 12
                dia = 31
                Label3.Text = dia
            Case 4, 6, 9, 11
                dia = 30
                Label3.Text = dia
            Case 2
                mes2 = MsgBox("Ano Bissexto", vbQuestion + vbYesNo)
                If mes2 = vbYes Then
                    dia = 29
                    Label3.Text = dia
                Else : dia = 28
                    Label3.Text = dia
                End If
        End Select
    End Sub
End Class
