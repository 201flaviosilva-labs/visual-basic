﻿Public Class Form1
    Dim MoverDireita As Boolean
    Dim MoverCima As Boolean
    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        If MoverDireita = True Then
            OvalShape1.Left += 5
        Else
            OvalShape1.Left -= 5
        End If

        If MoverCima = True Then
            OvalShape1.Top -= 5
        Else
            OvalShape1.Top += 5
        End If

        If OvalShape1.Left <= Me.ClientRectangle.Left Then
            MoverDireita = True
        End If

        If OvalShape1.Left + OvalShape1.Width >= Me.ClientRectangle.Right Then
            MoverDireita = False
        End If

        If OvalShape1.Top <= Me.ClientRectangle.Top Then
            MoverCima = False
        End If

        If OvalShape1.Top + OvalShape1.Height >= Me.ClientRectangle.Bottom Then
            MoverCima = True
        End If

    End Sub
End Class
