# Visual Basic


## PT
Neste repositório estão alguns projetos/exercícios/jogos/etc... que fiz durante o meu secundário nas aulas de informática, quando estava a aprender programação (peço desculpa pela falta de oganização) :,)

Alguns projetos podem estão incompletos/bugados/ou simplesmente não funcionam :)

Peço desculpa a todas as pessoas/grupos/empresas a quem uso as fotos/conteúdo para os jogos/projetos/etc... e não dou créditos, mas como já fiz alguns destes projetos há muito tempo não sei de onde descarreguei o conteúdo.

Caso algum se sinta incomodado, basta entrar em contacto comigo :)

(Isto é apenas um repositório open source com os meus antigos projetos no tempo de escola (2016/2019), não tenho nenhum objetivo de ter benefícios com isto) :)

## EN
In this repository are some projects/exercises/games/etc... that I did during my high school in computer classes, when i was learning programing (I apologize for the miss of organization) :,)

Some projects may be incomplete/buggy/or just don't work :)

I apologize to all the people/groups/companies I use the photos/content for the games/projects/etc... and I don't give credit, but as I've done some of these projects a long time ago, I don't know where I downloaded the content from.

If anyone feels uncomfortable, just get in touch with me :)

(This is just an open source repository with my old projects back in school (2016/2019), I have no goal of benefiting from this) :)
