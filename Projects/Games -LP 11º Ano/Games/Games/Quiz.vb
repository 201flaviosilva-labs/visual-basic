﻿Public Class Quiz
    Dim tempo1, Perguntas As Double
    Sub errada()

        MsgBox("Erraste na resposta!")

        Timer1.Enabled = True

        Label1.Visible = True
        Button1.Visible = True
        Button2.Visible = True
        Button3.Visible = True
        Button4.Visible = True


        PictureBox1.Visible = False
        PictureBox2.Visible = False



        Perguntas = 1
        tempo1 = 15
        Timer1.Enabled = True


        Label1.Text = Perguntas & " Quem escreveu Frei Luís de Sousa?"

        Button1.Text = "A) Frei Luís de Sousa"
        Button2.Text = "B) Luís de Camões"
        Button3.Text = "C) Almeida Garret" 'Correrto'
        Button4.Text = "D) Vasco da Gama"


    End Sub
    Private Sub Quiz_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If programador = 1 Then
            Label2.Enabled = True
            Label2.Text = "1C-2B-3D-4B-5B-6A-7D-8A-9B-10D-11B-12A-13D-14D-15B-16B-17B-18C-19C-20D-21D"
            Label2.Visible = True
        End If
        Timer1.Enabled = True

        Label1.Visible = True
        Button1.Visible = True
        Button2.Visible = True
        Button3.Visible = True
        Button4.Visible = True


        PictureBox1.Visible = False
        PictureBox2.Visible = False


        Perguntas = 1
        tempo1 = 15
        Label3.Text = tempo1
        Timer1.Enabled = True

        Label1.Text = Perguntas & " Quem escreveu Frei Luís de Sousa?"

        Button1.Text = "A) Frei Luís de Sousa"
        Button2.Text = "B) Luís de Camões"
        Button3.Text = "C) Almeida Garret" 'Correrto'
        Button4.Text = "D) Vasco da Gama"

    End Sub
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Timer1.Enabled = False

        If Perguntas = 6 Then
            MsgBox("Parabens, próxima pergunta!")
            Perguntas = Perguntas + 1
            Timer1.Enabled = True
            'Pergunta 7'
            Label1.Text = Perguntas & " Qual o símbolo quimico da agua?"

            Button1.Text = "A) F "
            Button2.Text = "B) O3"
            Button3.Text = "C) CO2"
            Button4.Text = "D) H2O" 'Correrto'



        ElseIf Perguntas = 8 Then
            MsgBox("Parabens, próxima pergunta!")
            Perguntas = Perguntas + 1
            Timer1.Enabled = True

            'Pergunta 9'
            Label1.Text = Perguntas & " Economia é..."

            Button1.Text = "A) Modo de estudo da económia "
            Button2.Text = "B) Ciência Económica" 'Correrto'
            Button3.Text = "C) Ciência Financeira"
            Button4.Text = "D) Nenhuma destas"



        ElseIf Perguntas = 12 Then
            MsgBox("Parabens, próxima pergunta!")
            Perguntas = Perguntas + 1
            Timer1.Enabled = True
            'Pergunta 13'
            Label1.Text = Perguntas & " Quem é Luke Skywalker?"

            Button1.Text = "A) Personagem de novelas"
            Button2.Text = "B) Um cão que foi á lua"
            Button3.Text = "C) Uma marca de lenços de papel"
            Button4.Text = "D) Personagem fictício de filmes" 'Correrto'




        Else

            errada()
        End If
        tempo1 = 15
        Label3.Text = tempo1
    End Sub


    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Timer1.Enabled = False
        If Perguntas = 2 Then
            MsgBox("Parabens, próxima pergunta!")
            Perguntas = Perguntas + 1
            Timer1.Enabled = True

            'Pergunta 3'
            Label1.Text = Perguntas & " Quem é o Zé Povinho?"

            Button1.Text = "A) Cantor"
            Button2.Text = "B) Personagem de Banda Desenhada"
            Button3.Text = "C) Youtuber"
            Button4.Text = "D) Personagem satírica de crítica social" 'Correrto'

       else If Perguntas = 4 Then
        MsgBox("Parabens, próxima pergunta!")
            Perguntas = Perguntas + 1
            Timer1.Enabled = True
            'Pergunta 5'
            Label1.Text = Perguntas & " Quem foi o primeiro rei de Portugal?"

            Button1.Text = "A) D. João I"
            Button2.Text = "B) D. Afonso Henriques" 'Correrto'
            Button3.Text = "C) Viriato"
            Button4.Text = "D) António Cruz de Pau"


        ElseIf Perguntas = 5 Then
            MsgBox("Parabens, próxima pergunta!")
            Perguntas = Perguntas + 1
            Timer1.Enabled = True
            'Pergunta 6'
            Label1.Text = Perguntas & " Quantas cordas tem uma guitarra classica?"

            Button1.Text = "A) 6 " 'Correrto'
            Button2.Text = "B) 5"
            Button3.Text = "C) 4"
            Button4.Text = "D) 8"


        ElseIf Perguntas = 9 Then
            MsgBox("Parabens, próxima pergunta!")
            Perguntas = Perguntas + 1
            Timer1.Enabled = True
            'Pergunta 10'
            Label1.Text = Perguntas & " Em que local caiu o meteorito que matou todos os dinoçauros?"

            Button1.Text = "A) Deserto Alasca"
            Button2.Text = "B) Deserto Nevada"
            Button3.Text = "C) Deserto Saara"
            Button4.Text = "D) Golfo do Mexico " 'Correrto'


        ElseIf Perguntas = 11 Then
            MsgBox("Parabens, próxima pergunta!")
            Perguntas = Perguntas + 1
            Timer1.Enabled = True
            'Pergunta 12'
            Label1.Text = Perguntas & " Quanto tempo demora a Lua a dar a volta à Terra (período orbital)?"

            Button1.Text = "A) 27 dias e 8 horas" 'Correrto'
            Button2.Text = "B) 25 dias"
            Button3.Text = "C) 30 dias"
            Button4.Text = "D) 29 dias e 12 horas"


        ElseIf Perguntas = 15 Then
            MsgBox("Parabens, próxima pergunta!")
            Perguntas = Perguntas + 1
            Timer1.Enabled = True
            'Pergunta 16'
            PictureBox1.Visible = True
            Label1.Text = Perguntas & " Qual é a figura representada na imagem?"

            Button1.Text = "A) Cristiano Ronaldo"
            Button2.Text = "B) Hitler" 'Correrto'
            Button3.Text = "C) Papa Francisco"
            Button4.Text = "D) Donald Trump"

        ElseIf Perguntas = 16 Then
            MsgBox("Parabens, próxima pergunta!")
            Perguntas = Perguntas + 1

            PictureBox1.Visible = False
            Timer1.Enabled = True
            'Pergunta 17'
            Label1.Text = Perguntas & " Edgar Allan Poe foi...?"

            Button1.Text = "A) um jogador de futebol"
            Button2.Text = "B) um autor, poeta, editor e crítico" 'Correrto'
            Button3.Text = "C) um pintor, poeta, editor e crítico"
            Button4.Text = "D) Não se sabe quem ele foi por isso ainda é um misterio"



        ElseIf Perguntas = 17 Then
            MsgBox("Parabens, próxima pergunta!")
            Perguntas = Perguntas + 1

            PictureBox1.Visible = False
            Timer1.Enabled = True
            'Pergunta 18'
            Label1.Text = Perguntas & " Quantos jogador estão em campo durante um jogo de Basquete?"

            Button1.Text = "A) 3"
            Button2.Text = "B) 5"
            Button3.Text = "C) 10" 'Correrto'
            Button4.Text = "D) 12"

        Else

            errada()
        End If
        tempo1 = 15
        Label3.Text = tempo1
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Timer1.Enabled = False
        If Perguntas = 1 Then
            MsgBox("Parabens, próxima pergunta!")
            Perguntas = Perguntas + 1
            Timer1.Enabled = True
            'Pergunta 2'
            Label1.Text = Perguntas & " Quem foi o inventor da eletricidade?"

            Button1.Text = "A) Nikola Tesla"
            Button2.Text = "B) Benjamin Franklin" 'Correrto'
            Button3.Text = "C) Isaac Newton"
            Button4.Text = "D) Thomas Edison"


        ElseIf Perguntas = 18 Then
            MsgBox("Parabens, próxima pergunta!")
            Perguntas = Perguntas + 1

            PictureBox1.Visible = False
            Timer1.Enabled = True
            'Pergunta 19'
            PictureBox2.Visible = True
            Label1.Text = Perguntas & " Quem é o grupo musical na fotografia??"

            Button1.Text = "A) Kiss"
            Button2.Text = "B) Red Hot Chili Peppers"
            Button3.Text = "C) AD/DC" 'Correrto'
            Button4.Text = "D) Quim Barreiros"

        ElseIf Perguntas = 19 Then

            MsgBox("Parabens, próxima pergunta!")
            Perguntas = Perguntas + 1
            Timer1.Enabled = True
            'Pergunta 20'
            Label1.Text = Perguntas & " Na informatica o que significa html?"

            Button1.Text = "A) Hyper-Phrase Markup Language"
            Button2.Text = "B) Home Tool Markup Language"
            Button3.Text = "C) Hyperlinks And Text Markup Language"
            Button4.Text = "D) Hyper Text Markup Language" 'Correrto'



        Else
            errada()
        End If
        tempo1 = 15
        Label3.Text = tempo1
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Timer1.Enabled = False

        If Perguntas = 3 Then
            MsgBox("Parabens, próxima pergunta!")
            Perguntas = Perguntas + 1
            Timer1.Enabled = True
            'Pergunta 4'
            Label1.Text = Perguntas & " Quem disse 'Penso, logo existo'?"

            Button1.Text = "A) Sócrates"
            Button2.Text = "B) René Descartes" 'Correrto'
            Button3.Text = "C) George Washington"
            Button4.Text = "D) Pai Natal"


        ElseIf Perguntas = 7 Then
            MsgBox("Parabens, próxima pergunta!")
            Perguntas = Perguntas + 1
            Timer1.Enabled = True
            'Pergunta 8'
            Label1.Text = Perguntas & " Portugal é um pais de que Mundo?"

            Button1.Text = "A) Primeiro Mundo " 'Correrto'
            Button2.Text = "B) Segundo Mundo"
            Button3.Text = "C) Terceiro Mundo"
            Button4.Text = "D) Não está registado"

        ElseIf Perguntas = 10 Then
            MsgBox("Parabens, próxima pergunta!")
            Perguntas = Perguntas + 1
            Timer1.Enabled = True
            'Pergunta 11'
            Label1.Text = Perguntas & " Qual é o mais famoso detetive criado por Sir Arthur Conan Doyle?"

            Button1.Text = "A) Hercule Poirot"
            Button2.Text = "B) Sherlock Holmes" 'Correrto'
            Button3.Text = "C) Philip Marlowe"
            Button4.Text = "D) Agatha Christie"



        ElseIf Perguntas = 13 Then
            MsgBox("Parabens, próxima pergunta!")
            Perguntas = Perguntas + 1
            Timer1.Enabled = True
            'Pergunta 14'
            Label1.Text = Perguntas & " Quantos vertices tem um cubo?"

            Button1.Text = "A) 12"
            Button2.Text = "B) 4"
            Button3.Text = "C) 2"
            Button4.Text = "D) 8" 'Correrto'


        ElseIf Perguntas = 14 Then
            MsgBox("Parabens, próxima pergunta!")
            Perguntas = Perguntas + 1
            Timer1.Enabled = True
            'Pergunta 15'
            Label1.Text = Perguntas & " Quantos trabalhos/hobbies Leonardo da Vinci teve?"

            Button1.Text = "A) 1"
            Button2.Text = "B) 17" 'Correrto'
            Button3.Text = "C) 20"
            Button4.Text = "D) 5"

        ElseIf Perguntas = 20 Then
            MsgBox("Parabens, próxima pergunta!")
            Perguntas = Perguntas + 1

            PictureBox1.Visible = False
            Timer1.Enabled = True
            'Pergunta 21'
            PictureBox2.Visible = False
            Label1.Text = Perguntas & " Badminton é jogado com..."

            Button1.Text = "A) as mãos"
            Button2.Text = "B) os dedos dos pés"
            Button3.Text = "C) uma vassoura"
            Button4.Text = "D) uma raquete" 'Correrto'



        ElseIf Perguntas = 21 Then
            Timer1.Enabled = False
            MsgBox("Muito bem acertaste todas as perguntas! Com esse feitos recebes 21 pontos!")

            Timer1.Enabled = False
            Button1.Enabled = False
            Button2.Enabled = False
            Button3.Enabled = False
            Button4.Enabled = False

            PontosTotais = PontosTotais + 21
            experiencia = experiencia + 15

        Else
            errada()
        End If
        tempo1 = 15
        Label3.Text = tempo1
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        TInicialQuiz.Show()
        Me.Close()
    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        tempo1 = tempo1 - 1
        Label3.Text = tempo1
        If tempo1 = 0 Then
            MsgBox("Acabou o tempo!")
            errada()
        End If
    End Sub
End Class