﻿Public Class TelaInicial
    Dim jogo, tempo, sair As Double

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        'Jogo do Galo'
        TInicialJogoGalo.Show()
    End Sub
    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        'Sair'
        sair = MsgBox("Quer mesmo sair do programa?", vbQuestion + vbYesNo)
        If sair = vbNo Then MsgBox("Então aproveita o resto tempo") Else End
    End Sub
    Private Sub Button3_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        'Céditos'
        Creditos.Show()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        'Pedra Papel Tesoura'
        TInicialPPT.Show()
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        'Jogo aleatório'
        Randomize()
        jogo = Int(4 * Rnd() + 1)

        If jogo = 1 Then
            TInicialJogoGalo.Show()
        ElseIf jogo = 2 Then
            TInicialPPT.Show()
        ElseIf jogo = 3 Then
            TInicialQuiz.Show()
        ElseIf jogo = 4 Then
            TInicialkick.Show()
        End If
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        'Quiz'
        TInicialQuiz.Show()
    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        'Timer1'
        tempo = tempo + 1
        Label2.Text = PontosTotais
        Label3.Text = nivel
        Label5.Text = experiencia

        If experiencia >= 100 Then
            nivel = nivel + Math.Round(experiencia * 0.01)
            experiencia = experiencia * 0.01
        End If

        If tempo = 8 Then
            tempo = 0
        End If
        If tempo = 4 Then
            Label2.BackColor = Color.Black
            Label2.ForeColor = Color.White
            Label1.BackColor = Color.Black
            Label1.ForeColor = Color.White
            Label1.BorderStyle = BorderStyle.None
        End If
        If tempo = 0 Then
            Label2.BackColor = Color.White
            Label2.ForeColor = Color.Black
            Label1.BackColor = Color.White
            Label1.ForeColor = Color.Black
            Label1.BorderStyle = BorderStyle.Fixed3D
        End If



        If tempo = 4 Then
            Label3.BackColor = Color.Black
            Label3.ForeColor = Color.White
            Label4.BackColor = Color.Black
            Label4.ForeColor = Color.White
            Label4.BorderStyle = BorderStyle.None
        End If
        If tempo = 0 Then
            Label3.BackColor = Color.White
            Label3.ForeColor = Color.Black
            Label4.BackColor = Color.White
            Label4.ForeColor = Color.Black
            Label4.BorderStyle = BorderStyle.Fixed3D
        End If

        If tempo = 4 Then
            Label5.BackColor = Color.Black
            Label5.ForeColor = Color.White
            Label6.BackColor = Color.Black
            Label6.ForeColor = Color.White
            Label6.BorderStyle = BorderStyle.None
        End If
        If tempo = 0 Then
            Label5.BackColor = Color.White
            Label5.ForeColor = Color.Black
            Label6.BackColor = Color.White
            Label6.ForeColor = Color.Black
            Label6.BorderStyle = BorderStyle.Fixed3D
        End If



        If tempo = 4 Then
            Label7.BackColor = Color.Black
            Label7.ForeColor = Color.White
        End If
        If tempo = 0 Then
            Label7.BackColor = Color.White
            Label7.ForeColor = Color.Black
        End If


    End Sub

    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click
        'kickBox fighter'

        TInicialkick.Show()
    End Sub

    Private Sub TelaInicial_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Tela Inicial'
        treinos = 24
        vida = 20
        ataque = 0
        defesa = 0
        estamina = 0

        experiencia = 0
        nivel = 0


    End Sub

    Private Sub Button10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button10.Click
        '007'
        If PontosTotais >= 10 Then
            Button10.Enabled = True
            PontosTotais = PontosTotais - 10
        Else
            MsgBox("Não tem pontos suficientes")
        End If
    End Sub

    Private Sub Button9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button9.Click
        TInicialAS.Show()
    End Sub

    Private Sub Button12_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button12.Click
        TInicialEspaceInvaders.Show()
    End Sub

    Private Sub Button13_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button13.Click
        TISPInvaders2.Show()
    End Sub
End Class