﻿Public Class Space_Inavders2
    Dim direita As Boolean
    Dim esquerda As Boolean
    Dim velocidade As Integer = 2

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        movernave()
    End Sub

    Private Sub Space_Inavders2_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        If e.KeyValue = Keys.D Then
            direita = True
        End If

        If e.KeyValue = Keys.A Then
            esquerda = True
        End If
    End Sub

    Private Sub movernave()
        If direita = True And SPNave.Left + SPNave.Width < Me.ClientRectangle.Width Then
            SPNave.Left = SPNave.Left + velocidade
        End If

        If esquerda = True And SPNave.Left + SPNave.Width > Me.ClientRectangle.Width Then
            SPNave.Left = SPNave.Left - velocidade
        End If
    End Sub

    Private Sub Space_Inavders2_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        If e.KeyValue = Keys.D Then
            direita = False
        End If

        If e.KeyValue = Keys.A Then
            esquerda = False
        End If
    End Sub
End Class