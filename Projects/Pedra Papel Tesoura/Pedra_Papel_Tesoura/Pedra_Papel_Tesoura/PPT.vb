﻿Public Class PPT
     Dim cpu, vitoria, derrota, partidas, jpartidas, Continuar As Double
    Dim contador As Integer
    Dim jogador1 As String
    Dim programador As Double = 2
    'Continuar - desempatar ou não'
    'Contador - contador de partidas'
    'Npartidas - Maximo de partidas (Vindas das opções)'
    'jpartidas - partidas jogadas'
    'vitoria - Número de Vitórias'
    'Derrotas - Número de Derrotas'
    'Cpu - Número random escolhido pelo Pc para escolher entre a Tesoura, o Papel e a Pedra'
    'Jogador1 - Nome do Jogador'

    Sub jogadas()
        If Npartidas = jpartidas Then
            MessageBox.Show("O Jogo Acabou")
            Button10.Enabled = False
            If derrota = vitoria Then

                Continuar = MsgBox("Queres desempatar o jogo?", vbQuestion + vbYesNo)
                If Continuar = vbNo Then
                    Button10.Enabled = False
                Else
                    Button10.Enabled = True
                End If
            End If
        ElseIf Npartidas < jpartidas Then
            If derrota = vitoria Then
                Continuar = MsgBox("Queres desempatar o jogo?", vbQuestion + vbYesNo)
                If Continuar = vbNo Then
                    Button10.Enabled = False
                Else
                    Button10.Enabled = True
                End If
            Else
                Button10.Enabled = False
            End If
        End If
    End Sub

    Private Sub Button10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button10.Click
        'Novo Tabuleiro'

        PictureBox1.Visible = True
        PictureBox2.Visible = True
        PictureBox3.Visible = True
        PictureBox4.Visible = True
        PictureBox5.Visible = True
        PictureBox6.Visible = True


        PictureBox1.Enabled = True
        PictureBox2.Enabled = True
        PictureBox3.Enabled = True
        Me.BackColor = Color.LimeGreen
    End Sub

    Private Sub PictureBox2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox2.Click
        'Papel'

        PictureBox1.Visible = False
        PictureBox3.Visible = False


        PictureBox1.Enabled = False
        PictureBox2.Enabled = False
        PictureBox3.Enabled = False
        cpu = 0

        Randomize()
        cpu = Int(3 * Rnd() + 1)

        If programador = 1 Then
            cpu = 1
        End If

        If cpu = 1 Then
            '1 é para o PAPEL = picturebox6'
            'esconder a tesoura e a pedra'
            PictureBox4.Visible = False
            PictureBox5.Visible = False
            'Mensagem de Empate'
            MessageBox.Show("Empate")

        ElseIf cpu = 2 Then
            '2 é para o PEDRA = picturebox5'
            'esconder a tesoura e o papel'
            PictureBox4.Visible = False
            PictureBox6.Visible = False
            'Mensagem de Vitória'
            MessageBox.Show("O Jogador " & jogador1 & " Venceu")
            vitoria = vitoria + 1
            Label6.Text = vitoria
            Me.BackColor = Color.DarkCyan


        ElseIf cpu = 3 Then
            '3 é para o TESOURA = picturebox4'
            'esconder a pedra e o papel'
            PictureBox5.Visible = False
            PictureBox6.Visible = False
            'Mensagem de derrota'
            MessageBox.Show("O Jogador " & jogador1 & " Perdeu")
            derrota = derrota + 1
            Label7.Text = derrota
            Me.BackColor = Color.Orange
        End If
        jpartidas = jpartidas + 1
        Label9.Text = jpartidas

        If Npartidas >= 1 Then
            jogadas()
        End If
    End Sub

    Private Sub PictureBox1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox1.Click
        'Pedra'


        PictureBox2.Visible = False
        PictureBox3.Visible = False

        PictureBox1.Enabled = False
        PictureBox2.Enabled = False
        PictureBox3.Enabled = False


        cpu = 0


        Randomize()
        cpu = Int(3 * Rnd() + 1)

        If programador = 1 Then
            cpu = 1
        End If

        If cpu = 1 Then
            '1 é para o PAPEL = picturebox6'
            'esconder a tesoura e a pedra'
            PictureBox4.Visible = False
            PictureBox5.Visible = False
            'Mensagem de Derrota'
            MessageBox.Show("O Jogador " & jogador1 & " Perdeu")
            derrota = derrota + 1
            Label7.Text = derrota
            Me.BackColor = Color.Orange

        ElseIf cpu = 2 Then
            '2 é para o PEDRA = picturebox5'
            'esconder a tesoura e o papel'
            PictureBox4.Visible = False
            PictureBox6.Visible = False
            'Mensagem de Empate'
            MessageBox.Show("Empate")

        ElseIf cpu = 3 Then
            '3 é para o TESOURA = picturebox4'
            'esconder a pedra e o papel'
            PictureBox5.Visible = False
            PictureBox6.Visible = False
            'Mensagem de Vitória'
            MessageBox.Show("O Jogador " & jogador1 & " Venceu")
            vitoria = vitoria + 1
            Label6.Text = vitoria
            Me.BackColor = Color.DarkCyan
        End If
        jpartidas = jpartidas + 1
        Label9.Text = jpartidas

        If Npartidas >= 1 Then
            jogadas()
        End If
    End Sub

    Private Sub PictureBox3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox3.Click
        'Tesoura'

        PictureBox1.Visible = False
        PictureBox2.Visible = False

        PictureBox1.Enabled = False
        PictureBox2.Enabled = False
        PictureBox3.Enabled = False


        cpu = 0

        Randomize()
        cpu = Int(3 * Rnd() + 1)

        If programador = 1 Then
            cpu = 1
        End If

        If cpu = 1 Then
            '1 é para o PAPEL = picturebox6'
            'esconder a tesoura e a pedra'
            PictureBox4.Visible = False
            PictureBox5.Visible = False
            'Mensagem de Vitória'
            MessageBox.Show("O jogador " & jogador1 & " Venceu")
            vitoria = vitoria + 1
            Label6.Text = vitoria
            Me.BackColor = Color.DarkCyan

        ElseIf cpu = 2 Then
            '2 é para o PEDRA = picturebox5'
            'esconder a tesoura e o papel'
            PictureBox4.Visible = False
            PictureBox6.Visible = False
            'Mensagem de Derrota'
            MessageBox.Show("O Jogador " & jogador1 & " Perdeu")
            derrota = derrota + 1
            Label7.Text = derrota
            Me.BackColor = Color.Orange

        ElseIf cpu = 3 Then
            '3 é para o TESOURA = picturebox4'
            'esconder a pedra e o papel'
            PictureBox5.Visible = False
            PictureBox6.Visible = False
            'Mensagem de Empate'
            MessageBox.Show("Empate")
        End If
        jpartidas = jpartidas + 1
        Label9.Text = jpartidas

        If Npartidas >= 1 Then
            jogadas()
        End If
    End Sub

    Private Sub PPT_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Inicio do jogo, (from)'
        PictureBox4.Enabled = False
        PictureBox5.Enabled = False
        PictureBox6.Enabled = False
        'Deixar indiponivel o jogador selecionar os do cpu'
        PictureBox1.Visible = True
        PictureBox2.Visible = True
        PictureBox3.Visible = True
        PictureBox4.Visible = True
        PictureBox5.Visible = True
        PictureBox6.Visible = True
        PictureBox1.Enabled = True
        PictureBox2.Enabled = True
        PictureBox3.Enabled = True
        'Deixar tudo visivel'
        derrota = 0
        vitoria = 0
        contador = 0
        jpartidas = 0
        'Limpar as variaveis'
        Label6.Text = vitoria
        Label7.Text = derrota
        Label9.Text = jpartidas
        'Mostar as variaveis'
        Me.BackColor = Color.LimeGreen
        'Mudar a cor do form'

        If Npartidas >= 1 Then
            Label11.Visible = True
            Label12.Visible = True

            Label12.Enabled = True
            Label12.Text = Npartidas
        End If
    End Sub

    Private Sub Button14_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button14.Click
        'Sair'
        TelaInicial.Show()
        Me.Close()
    End Sub

    Private Sub Button13_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button13.Click
        'Novo Jogo'
        PictureBox1.Visible = True
        PictureBox2.Visible = True
        PictureBox3.Visible = True
        PictureBox4.Visible = True
        PictureBox5.Visible = True
        PictureBox6.Visible = True
        PictureBox1.Enabled = True
        PictureBox2.Enabled = True
        PictureBox3.Enabled = True
        PictureBox4.Enabled = False
        PictureBox5.Enabled = False
        PictureBox6.Enabled = False
        Button10.Enabled = True
        cpu = 0
        contador = 0
        derrota = 0
        vitoria = 0
        jpartidas = 0
        Label9.Text = jpartidas
        Label6.Text = vitoria
        Label7.Text = derrota
        Me.BackColor = Color.LimeGreen
    End Sub

    Private Sub Button11_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button11.Click
        'Nome do jogador'
        jogador1 = InputBox("Qual é o nome do jogador?")
        Label3.Text = jogador1
        Button11.Enabled = False
    End Sub
End Class