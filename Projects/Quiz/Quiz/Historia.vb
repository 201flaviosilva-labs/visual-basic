﻿Public Class Historia

    Dim CodigoPergunta As Byte = 0
    Dim somaPerguntas As Byte = 0
    Dim RespostasCorretas As Byte = 0
    Dim RespostasErradas As Byte = 0
    Dim Ajudas As Byte = 3
    Dim ProgCodPerg As Integer = 0 ' Escolher a pergunta como programadro
    Dim ClickCodPergunta As Boolean = False ' Afirmar que foi ativo a  escolha da pergunta

    'CodigoPergunta = Codigo Pergunta, Gera número aleatório
    'SomaPergunta = Soma de sequancia de perguntas respondidas

    Sub pausa(ByVal tempopausa)
        'Tempo de Pausa'
        Dim tempoatual As Date
        Dim tempoparado As Date

        tempoatual = Date.Now()
        tempoparado = tempoatual.AddMilliseconds(tempopausa)

        Do While Date.Now < tempoparado
            Application.DoEvents()
        Loop
    End Sub


    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        'Voltar
        TelaInicial.Show()
        Me.Close()
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        'Começar'
        Button1.Enabled = True
        Button2.Enabled = True
        Button3.Enabled = True
        Button4.Enabled = True
        Button7.Enabled = True
        Button8.Enabled = True
        Perguntas()
    End Sub

    Sub Correto()
        Label9.BackColor = Color.Green
        RespostasCorretas += 1
        Label5.Text = RespostasCorretas
        Label10.BackColor = Color.Green

        'Pintar Butoes de coretos
        Button1.BackColor = Color.Green
        Button2.BackColor = Color.Green
        Button3.BackColor = Color.Green
        Button4.BackColor = Color.Green
    End Sub

    Sub Errado()
        Label9.BackColor = Color.Red
        RespostasErradas += 1
        Label6.Text = RespostasErradas
        Label10.BackColor = Color.Red

        'Pintar Butoes de errado
        Button1.BackColor = Color.Red
        Button2.BackColor = Color.Red
        Button3.BackColor = Color.Red
        Button4.BackColor = Color.Red

    End Sub

    Sub Perguntas()
        'Somar Perguntas
        somaPerguntas += 1
        Randomize()
        CodigoPergunta = Int(232 * Rnd() + 1)


        If CodigoPergunta = 1 Then
            Label10.Text = somaPerguntas & " - Em que ano Portugal aderiu a União Europeia?"

            Button1.Text = "A) 599"
            Button2.Text = "B) 1790"
            Button3.Text = "C) 1986" 'Correrto'
            Button4.Text = "D) 2032"

        ElseIf CodigoPergunta = 2 Then
            Label10.Text = somaPerguntas & " - Salazar era professor em que Universidade?"

            Button1.Text = "A) Madride"
            Button2.Text = "B) Londres"
            Button3.Text = "C) Marte"
            Button4.Text = "D) Coimbra" 'Correrto'

        ElseIf CodigoPergunta = 3 Then
            Label10.Text = somaPerguntas & " - Qual era a lei fundamental de Salazar?"

            Button1.Text = "A) Constituição de 1933" 'Correrto'
            Button2.Text = "B) Nada"
            Button3.Text = "C) O tratado de tordesilhas"
            Button4.Text = "D) 10 Mandamento de Deus"

        ElseIf CodigoPergunta = 4 Then
            Label10.Text = somaPerguntas & " - Qual é o cognome de D. Afonso Henriques?"

            Button1.Text = "A) O Gordo"
            Button2.Text = "B) O Conquistador" 'Correrto'
            Button3.Text = "C) O Velho"
            Button4.Text = "D) O Povo"

        ElseIf CodigoPergunta = 5 Then
            Label10.Text = somaPerguntas & " - Que rei português, segunda a lenda, voltará numa manhã de nevoeiro?"

            Button1.Text = "A) D. Sebastião" 'Correrto'
            Button2.Text = "B) Zé Povinho"
            Button3.Text = "C) Jesus"
            Button4.Text = "D) Luís Váz de Camões"

        ElseIf CodigoPergunta = 6 Then
            Label10.Text = somaPerguntas & " - Em que ano foi descoberto o caminho marítimo para a Índia?"

            Button1.Text = "A) Ainda não foi"
            Button2.Text = "B) 1498" 'Correrto'
            Button3.Text = "C) Este ano"
            Button4.Text = "D) 2012"

        ElseIf CodigoPergunta = 7 Then
            Label10.Text = somaPerguntas & " - Qual é a cidade berço de Portugal?"

            Button1.Text = "A) Vila Real"
            Button2.Text = "B) Minho"
            Button3.Text = "C) Guimarães" 'Correrto'
            Button4.Text = "D) Alentejo"

        ElseIf CodigoPergunta = 8 Then
            Label10.Text = somaPerguntas & " - Quando foi implementada a Primeira Republica?"

            Button1.Text = "A) Potugal é uma Ditadura"
            Button2.Text = "B) 1909"
            Button3.Text = "C) 1911"
            Button4.Text = "D) 1910" 'Correrto'

        ElseIf CodigoPergunta = 9 Then
            Label10.Text = somaPerguntas & " - Quem foi responsável pelo Fontismo?"

            Button1.Text = "A) FONTES PEREIRA DE MELO" 'Correrto'
            Button2.Text = "B) Luis Vás de Camões"
            Button3.Text = "C) Hitler"
            Button4.Text = "D) Vasco Da Gama"

        ElseIf CodigoPergunta = 10 Then
            Label10.Text = somaPerguntas & " - Em que ano se deu o terramoto que destruiu maior parte da capital?"

            Button1.Text = "A) 2012"
            Button2.Text = "B) 1755" 'Correrto'
            Button3.Text = "C) 1900"
            Button4.Text = "D) 1700"

        ElseIf CodigoPergunta = 11 Then
            Label10.Text = somaPerguntas & " - O século 19 em Portugal ficou conhecido pelo século da...?"

            Button1.Text = "A) Morte"
            Button2.Text = "B) Luxo"
            Button3.Text = "C) Burguesia" 'Correrto'
            Button4.Text = "D) Não é conhecido"

        ElseIf CodigoPergunta = 12 Then
            Label10.Text = somaPerguntas & " - Em que ano ocorreu a transferência de poderes em Macau de Portugal para a China?"

            Button1.Text = "A) 1000"
            Button2.Text = "B) 1500"
            Button3.Text = "C) 1755"
            Button4.Text = "D) 1999" 'Correrto'

        ElseIf CodigoPergunta = 13 Then
            Label10.Text = somaPerguntas & " - Como ficou conhecida a Revolução de 1974 em Portugal?"

            Button1.Text = "A) Revolução dos Cravos" 'Correrto'
            Button2.Text = "B) Revolução dos Corvos"
            Button3.Text = "C) Revolução dos Mendigos"
            Button4.Text = "D) Revolução dos bons Samaritanos"

        ElseIf CodigoPergunta = 14 Then
            Label10.Text = somaPerguntas & " - Qual o nome dado ao regime regente em Portugal até 1974?"

            Button1.Text = "A) Estado Velho"
            Button2.Text = "B) Estado Novo" 'Correrto'
            Button3.Text = "C) Estado Antigo"
            Button4.Text = "D) Falta de Estado"

        ElseIf CodigoPergunta = 15 Then
            Label10.Text = somaPerguntas & " - Quem foi o Primeiro Ministro Português após a queda do Regime Salazarista?"

            Button1.Text = "A) Hitler"
            Button2.Text = "B) Mário Fortado"
            Button3.Text = "C) Mário Soares" 'Correrto'
            Button4.Text = "D) Mário Fonteno"

        ElseIf CodigoPergunta = 16 Then
            Label10.Text = somaPerguntas & " - Em que ano se socedeu a revolução dos cravos de 1974?"

            Button1.Text = "A) 1974" 'Correrto'
            Button2.Text = "B) 1974" 'Correrto'
            Button3.Text = "C) 1974" 'Correrto'
            Button4.Text = "D) 1974" 'Correrto'

        ElseIf CodigoPergunta = 17 Then
            Label10.Text = somaPerguntas & " - Quem foi Viriato?"

            Button1.Text = "A) Líderes da tribo Lusitana" 'Correrto'
            Button2.Text = "B) Um Famoso de Hollywood"
            Button3.Text = "C) Um Rei Português"
            Button4.Text = "D) Militar de Abril"

        ElseIf CodigoPergunta = 18 Then
            Label10.Text = somaPerguntas & " - Quando morreu Viriato?"

            Button1.Text = "A) 2015"
            Button2.Text = "B) 139 A. C." 'Correrto'
            Button3.Text = "C) 1 D. C."
            Button4.Text = "D) 500 A. C."

        ElseIf CodigoPergunta = 19 Then
            Label10.Text = somaPerguntas & " - Como morreu Viriato?"

            Button1.Text = "A) A nadar"
            Button2.Text = "B) Arma de Fogo"
            Button3.Text = "C) Traido Enquanto Dormia" 'Correrto'
            Button4.Text = "D) Não Morreu"

        ElseIf CodigoPergunta = 20 Then
            Label10.Text = somaPerguntas & " - Quando Nasceu Viriato?"

            Button1.Text = "A) 2015"
            Button2.Text = "B) 139 A. C."
            Button3.Text = "C) 500 A. C."
            Button4.Text = "D) Não se Sabe" 'Correrto'

        ElseIf CodigoPergunta = 21 Then
            Label10.Text = somaPerguntas & " - Quem é o responsável pelo Estado Novo?"

            Button1.Text = "A) António Oliveira Salazar" 'Correrto'
            Button2.Text = "B) Hittler"
            Button3.Text = "C) As Tartarugas Ninja"
            Button4.Text = "D) O Jerónimo Stilton"

        ElseIf CodigoPergunta = 22 Then
            Label10.Text = somaPerguntas & " - Em que ano se deu a Batalha de Aljubarrota?"

            Button1.Text = "A) 13 de agosto de 1385"
            Button2.Text = "B) 14 de Agosto de 1385" 'Correrto'
            Button3.Text = "C) 15 de agosto de 1385"
            Button4.Text = "D) 16 de agosto de 1385"

        ElseIf CodigoPergunta = 23 Then
            Label10.Text = somaPerguntas & " - Quem dobrou o cabo das Tormentas em 1488?"

            Button1.Text = "A) José Socrates"
            Button2.Text = "B) Bartolomeu Noites"
            Button3.Text = "C) Bartolomeu Dias" 'Correrto'
            Button4.Text = "D) Mussolini"

        ElseIf CodigoPergunta = 24 Then
            Label10.Text = somaPerguntas & " - Qual o Mosteiro que foi feito para celebrar a Vitória dos Portugueses sobre os Castelhanos em Aljubarrota?"

            Button1.Text = "A) Torre dos Clérigos"
            Button2.Text = "B) Torre Pisa"
            Button3.Text = "C) Torre Eiffel"
            Button4.Text = "D) Mosteiro da Batalha" 'Correrto'

        ElseIf CodigoPergunta = 25 Then
            Label10.Text = somaPerguntas & " - Quem foi o grande impulsionador dos descobrimentos portugueses?"

            Button1.Text = "A) Infante D. Henrique" 'Correrto'
            Button2.Text = "B) São Pedro"
            Button3.Text = "C) São João"
            Button4.Text = "D) Santo António"

        ElseIf CodigoPergunta = 26 Then
            Label10.Text = somaPerguntas & " - Qual dos navegadores seguintes não esteve ao serviço do Rei de Portugal?"

            Button1.Text = "A) Vasco da Gama"
            Button2.Text = "B) Cristóvao Colombo" 'Correrto'
            Button3.Text = "C) Bartolomeu Dias"
            Button4.Text = "D) Pedro Alvares Cabral"

        ElseIf CodigoPergunta = 27 Then
            Label10.Text = somaPerguntas & " - Quais produtos os portugueses traziam de Africa e Ásia?"

            Button1.Text = "A) Cigarros"
            Button2.Text = "B) Escravos"
            Button3.Text = "C) Ouro, especiarias e pedras preciosas" 'Correrto'
            Button4.Text = "D) Naves Espaciais"

        ElseIf CodigoPergunta = 28 Then
            Label10.Text = somaPerguntas & " - Como se chamava a policia politica no tempo de Salazar?"

            Button1.Text = "A) Capitaes de Abril"
            Button2.Text = "B) Militares"
            Button3.Text = "C) Policia"
            Button4.Text = "D) PIDE" 'Correrto'

        ElseIf CodigoPergunta = 29 Then
            Label10.Text = somaPerguntas & " - Quantos governos provisórios existiram após o 25 de abril?"

            Button1.Text = "A) 6" 'Correrto'
            Button2.Text = "B) 3"
            Button3.Text = "C) 2"
            Button4.Text = "D) 2O"

        ElseIf CodigoPergunta = 30 Then
            Label10.Text = somaPerguntas & " - Como se chama o período em que Portugal esteve perto de uma guerra civil em 1975?"

            Button1.Text = "A) Guerra Civil Do Calor"
            Button2.Text = "B) Verão Quente" 'Correrto'
            Button3.Text = "C) Quente"
            Button4.Text = "D) Guerra Fria"

        ElseIf CodigoPergunta = 31 Then
            Label10.Text = somaPerguntas & " - Que nome era dado á organização Portuguesa que integrava todos os jovens obrigatoriamente?"

            Button1.Text = "A) Mocidade Japonês"
            Button2.Text = "B) Mocidade Inglesa"
            Button3.Text = "C) Mocidade Portuguesa" 'Correrto'
            Button4.Text = "D) Mocidade Françesa"

        ElseIf CodigoPergunta = 32 Then
            Label10.Text = somaPerguntas & " - Qual a capital de Portugal?"

            Button1.Text = "A) Guimaraes"
            Button2.Text = "B) Coimbra"
            Button3.Text = "C) Porto"
            Button4.Text = "D) Lisboa" 'Correrto'

        ElseIf CodigoPergunta = 33 Then
            Label10.Text = somaPerguntas & " - Quais são as duas regiões autónomas do pais?"

            Button1.Text = "A) Açores e Madeira" 'Correrto'
            Button2.Text = "B) Açores e Algarve"
            Button3.Text = "C) Algarve e Madeira"
            Button4.Text = "D) Lisboa e Porto"

        ElseIf CodigoPergunta = 34 Then
            Label10.Text = somaPerguntas & " - Qual a data da conquista de Ceuta?"

            Button1.Text = "A) 1300"
            Button2.Text = "B) 1415" 'Correrto'
            Button3.Text = "C) 1200"
            Button4.Text = "D) 1500"

        ElseIf CodigoPergunta = 35 Then
            Label10.Text = somaPerguntas & " - Quantas Dinastias teve Portugal?"

            Button1.Text = "A) 2"
            Button2.Text = "B) 3"
            Button3.Text = "C) 4" 'Correrto'
            Button4.Text = "D) 2O"

        ElseIf CodigoPergunta = 36 Then
            Label10.Text = somaPerguntas & " - O nazismo ocorreu durante que período?"

            Button1.Text = "A) 1900 a 1945"
            Button2.Text = "B) 1944 a 1945"
            Button3.Text = "C) 1939 a 1945"
            Button4.Text = "D) 1933 a 1945" 'Correrto'

        ElseIf CodigoPergunta = 37 Then
            Label10.Text = somaPerguntas & " - Em que pais foi aplicado o Nazismo?"

            Button1.Text = "A) Alemanha" 'Correrto'
            Button2.Text = "B) Russia"
            Button3.Text = "C) Inglaterra"
            Button4.Text = "D) Londres"

        ElseIf CodigoPergunta = 38 Then
            Label10.Text = somaPerguntas & " - Quem liderou o nazismo?"

            Button1.Text = "A) Mussolini"
            Button2.Text = "B) Adolf Hitler" 'Correrto'
            Button3.Text = "C) Salazar"
            Button4.Text = "D) Castelo Branco"

        ElseIf CodigoPergunta = 39 Then
            Label10.Text = somaPerguntas & " - Em que ano nasceu Hitler?"

            Button1.Text = "A) 1901"
            Button2.Text = "B) 1900"
            Button3.Text = "C) 1889" 'Correrto'
            Button4.Text = "D) 1939"

        ElseIf CodigoPergunta = 40 Then
            Label10.Text = somaPerguntas & " - Qual o nome do símbolo nazista?"

            Button1.Text = "A) Dominar o Mundo"
            Button2.Text = "B) Bigode"
            Button3.Text = "C) Judeu"
            Button4.Text = "D) Suástica" 'Correrto'

        ElseIf CodigoPergunta = 41 Then
            Label10.Text = somaPerguntas & " - O que significa 'Fuhrer'?"

            Button1.Text = "A) Líder" 'Correrto'
            Button2.Text = "B) Hittler"
            Button3.Text = "C) Zé Povilho"
            Button4.Text = "D) Bicicleta"

        ElseIf CodigoPergunta = 42 Then
            Label10.Text = somaPerguntas & " - Durante o Nazismo onde é que os opositores eram exterminados?"

            Button1.Text = "A) Na lua"
            Button2.Text = "B) Campos de concentração" 'Correrto'
            Button3.Text = "C) No chão"
            Button4.Text = "D) No tribunal"

        ElseIf CodigoPergunta = 43 Then
            Label10.Text = somaPerguntas & " - Em que ano é que Hitler se suicidou?"

            Button1.Text = "A) 2000"
            Button2.Text = "B) 1993"
            Button3.Text = "C) 1945" 'Correrto'
            Button4.Text = "D) 1939"

        ElseIf CodigoPergunta = 44 Then
            Label10.Text = somaPerguntas & " - Que pais derrotou a Alemanha na Segunda Guerra Mundial?"

            Button1.Text = "A) Alemanha"
            Button2.Text = "B) China"
            Button3.Text = "C) Portugal"
            Button4.Text = "D) União Soviética" 'Correrto'

        ElseIf CodigoPergunta = 45 Then
            Label10.Text = somaPerguntas & " - O que significa ""Pax Romana""?"

            Button1.Text = "A) Paz Romana" 'Correrto'
            Button2.Text = "B) Morte a Roma"
            Button3.Text = "C) Roma no Poder"
            Button4.Text = "D) Roma em Guerra"

        ElseIf CodigoPergunta = 46 Then
            Label10.Text = somaPerguntas & " - Os romanos eram considerados grandes engenheiros pois construíam..."

            Button1.Text = "A) Terrem feito grandes conquistas"
            Button2.Text = "B) Aquedutos, Pontes e Termas" 'Correrto'
            Button3.Text = "C) Dominaram o mundo"
            Button4.Text = "D) Criaram o primeiro telemóvel"

        ElseIf CodigoPergunta = 47 Then
            Label10.Text = somaPerguntas & " - Em que ano foi assinado o Edito de Caracala?"

            Button1.Text = "A) 40 d.c."
            Button2.Text = "B) 3 d.c."
            Button3.Text = "C) 212 a.c" 'Correrto'
            Button4.Text = "D) 2O d.c."

        ElseIf CodigoPergunta = 48 Then
            Label10.Text = somaPerguntas & " - O Edito de Caracala consistia em que?"

            Button1.Text = "A) Derrota aos Romanos"
            Button2.Text = "B) Vitória aos Romanos"
            Button3.Text = "C) Morte aos Romanos"
            Button4.Text = "D) Cidadania aos Romanos" 'Correrto'

        ElseIf CodigoPergunta = 49 Then
            Label10.Text = somaPerguntas & " - A arte romana era inspirada em que arte?"

            Button1.Text = "A) Helenísticas, Etruscas e Gregas" 'Correrto'
            Button2.Text = "B) Nos Romanos"
            Button3.Text = "C) Nos Deuses"
            Button4.Text = "D) Nos Portugueses"

        ElseIf CodigoPergunta = 50 Then
            Label10.Text = somaPerguntas & " - Quais as duas características do comercio romano?"

            Button1.Text = "A) Monopolista"
            Button2.Text = "B) Urbanístico e esclavagista" 'Correrto'
            Button3.Text = "C) Urbanístico"
            Button4.Text = "D) Sempre em saldos"

        ElseIf CodigoPergunta = 51 Then
            Label10.Text = somaPerguntas & " - Quantos reis teve a primeira dinastia?"

            Button1.Text = "A) 1"
            Button2.Text = "B) 3"
            Button3.Text = "C) 9" 'Correrto'
            Button4.Text = "D) 2O"

        ElseIf CodigoPergunta = 52 Then
            Label10.Text = somaPerguntas & " - Quantos reis teve a segunda dinastia?"

            Button1.Text = "A) 1"
            Button2.Text = "B) 3"
            Button3.Text = "C) 2"
            Button4.Text = "D) 8" 'Correrto'

        ElseIf CodigoPergunta = 53 Then
            Label10.Text = somaPerguntas & " - Quantos reis teve a terceira  dinastia?"

            Button1.Text = "A) 3" 'Correrto'
            Button2.Text = "B) 30"
            Button3.Text = "C) 2"
            Button4.Text = "D) 2O"

        ElseIf CodigoPergunta = 54 Then
            Label10.Text = somaPerguntas & " - Quantos reis teve a quarta dinastia?"

            Button1.Text = "A) 20"
            Button2.Text = "B) 14" 'Correrto'
            Button3.Text = "C) 8"
            Button4.Text = "D) 6"

        ElseIf CodigoPergunta = 55 Then
            Label10.Text = somaPerguntas & " - Quantos reis teve Portugal?"

            Button1.Text = "A) 36"
            Button2.Text = "B) 35"
            Button3.Text = "C) 34" 'Correrto'
            Button4.Text = "D) 33"

        ElseIf CodigoPergunta = 56 Then
            Label10.Text = somaPerguntas & " - Quem foi o ultimo reis de Portugal?"

            Button1.Text = "A) D. Afonso Henriques"
            Button2.Text = "B) D. João II"
            Button3.Text = "C) D. Manuel I"
            Button4.Text = "D) D. Manuel II" 'Correrto'

        ElseIf CodigoPergunta = 57 Then
            Label10.Text = somaPerguntas & " - Quem foi o primeiro rei da quarta dinastia?"

            Button1.Text = "A) D. João IV" 'Correrto'
            Button2.Text = "B) D. João XX"
            Button3.Text = "C) D. João XXI"
            Button4.Text = "D) D. Afonso Henriques"

        ElseIf CodigoPergunta = 58 Then
            Label10.Text = somaPerguntas & " - Quem foi o primeiro rei da terceira dinastia?"

            Button1.Text = "A) Pai Natal"
            Button2.Text = "B) D. Felipe I" 'Correrto'
            Button3.Text = "C) D. Felipe III"
            Button4.Text = "D) D. Afonso Henriques"

        ElseIf CodigoPergunta = 59 Then
            Label10.Text = somaPerguntas & " - Quem foi o primeiro rei da segunda dinastia?"

            Button1.Text = "A) D. Afonso Henriques"
            Button2.Text = "B) D. João III"
            Button3.Text = "C) D. João I" 'Correrto'
            Button4.Text = "D) D. Afonso Henriques II"

        ElseIf CodigoPergunta = 60 Then
            Label10.Text = somaPerguntas & " - Quem foi o Primeiro Rei de Portugal?"

            Button1.Text = "A) D. Fernado "
            Button2.Text = "B) D. João III"
            Button3.Text = "C) D. Afonso Henriques II"
            Button4.Text = "D) D. Afonso Henriques" 'Correrto'

        ElseIf CodigoPergunta = 61 Then
            Label10.Text = somaPerguntas & " - A primeira dinastia ficou conhecida como?"

            Button1.Text = "A) Dinastia de Borgonha" 'Correrto'
            Button2.Text = "B) Dinastia de Avis"
            Button3.Text = "C) Dinastia Filipina"
            Button4.Text = "D) Dinastia de Bragança"

        ElseIf CodigoPergunta = 62 Then
            Label10.Text = somaPerguntas & " - A segunda dinastia ficou conhecida como?"

            Button1.Text = "A) Dinastia de Borgonha"
            Button2.Text = "B) Dinastia de Avis" 'Correrto'
            Button3.Text = "C) Dinastia Filipina"
            Button4.Text = "D) Dinastia de Bragança"

        ElseIf CodigoPergunta = 63 Then
            Label10.Text = somaPerguntas & " - A terceira dinastia ficou conhecida como?"

            Button1.Text = "A) Dinastia de Borgonha"
            Button2.Text = "B) Dinastia de Avis"
            Button3.Text = "C) Dinastia Filipina" 'Correrto'
            Button4.Text = "D) Dinastia de Bragança"

        ElseIf CodigoPergunta = 64 Then
            Label10.Text = somaPerguntas & " - A quarta dinastia ficou conhecida como?"

            Button1.Text = "A) Dinastia de Borgonha"
            Button2.Text = "B) Dinastia de Avis"
            Button3.Text = "C) Dinastia Filipina"
            Button4.Text = "D) Dinastia de Bragança" 'Correrto'

        ElseIf CodigoPergunta = 65 Then
            Label10.Text = somaPerguntas & " - Quem foi o pai de D. Afonso Henriques?"

            Button1.Text = "A) D. Henrique de Borgonha" 'Correrto'
            Button2.Text = "B) Pai Natal"
            Button3.Text = "C) Jesus"
            Button4.Text = "D) Barba Negra"

        ElseIf CodigoPergunta = 66 Then
            Label10.Text = somaPerguntas & " - Como se chama a batalha entre D. Afonso Henriques e D. Teresa?"

            Button1.Text = "A) Batalha da Morte"
            Button2.Text = "B) Batalha de S. Mamede" 'Correrto'
            Button3.Text = "C) Batalha de mãe contra filho"
            Button4.Text = "D) Batalha sem destino"

        ElseIf CodigoPergunta = 67 Then
            Label10.Text = somaPerguntas & " - Quem foi o filho de D. Afonso Henriques?"

            Button1.Text = "A) D. Sancho III"
            Button2.Text = "B) D. Sancho II"
            Button3.Text = "C) D. Sancho" 'Correrto'
            Button4.Text = "D) D. Afonso II"

        ElseIf CodigoPergunta = 68 Then
            Label10.Text = somaPerguntas & " - Em que ano foi assinado o Tratado de Zamora?"

            Button1.Text = "A) 2000"
            Button2.Text = "B) 1990"
            Button3.Text = "C) 1111"
            Button4.Text = "D) 1143" 'Correrto'

        ElseIf CodigoPergunta = 69 Then
            Label10.Text = somaPerguntas & " - Que rei Portugues tem o cognome de ""O Desejado""?"

            Button1.Text = "A) D. Sebastião" 'Correrto'
            Button2.Text = "B) D. João II"
            Button3.Text = "C) D. Manuel"
            Button4.Text = "D) D. Infante Henrrique"

        ElseIf CodigoPergunta = 70 Then
            Label10.Text = somaPerguntas & " - Em que ano D. Afonso Henriques morreu?"

            Button1.Text = "A) 1111"
            Button2.Text = "B) 1185" 'Correrto'
            Button3.Text = "C) 2012"
            Button4.Text = "D) 1990"

        ElseIf CodigoPergunta = 71 Then
            Label10.Text = somaPerguntas & " - Quais países representavam a RDA e RFA?"

            Button1.Text = "A) EUA/Alemanha"
            Button2.Text = "B) Alemanha/URRSS"
            Button3.Text = "C) URSS/EUA" 'Correrto'
            Button4.Text = "D) Portugal/Alemana"

        ElseIf CodigoPergunta = 72 Then
            Label10.Text = somaPerguntas & " - A NATO é uma organização..."

            Button1.Text = "A) Comunista"
            Button2.Text = "B) Economica"
            Button3.Text = "C) De preservação do Ambiente"
            Button4.Text = "D) Capitalista " 'Correrto'

        ElseIf CodigoPergunta = 73 Then
            Label10.Text = somaPerguntas & " - Qual é o principal representante da NATO?"

            Button1.Text = "A) EUA" 'Correrto'
            Button2.Text = "B) Portugal"
            Button3.Text = "C) Russia"
            Button4.Text = "D) Japão"

        ElseIf CodigoPergunta = 74 Then
            Label10.Text = somaPerguntas & " - O que foi a COMIC CON?"

            Button1.Text = "A) Camisola"
            Button2.Text = "B) Evenco de Cultura POP" 'Correrto'
            Button3.Text = "C) Ajuda económica da URSS"
            Button4.Text = "D) Uma Economia Estável"

        ElseIf CodigoPergunta = 75 Then
            Label10.Text = somaPerguntas & " - A Guerra Fria aconteceu durante quais anos?"

            Button1.Text = "A) 1933-1945"
            Button2.Text = "B) 1939-1945"
            Button3.Text = "C) 1947-1991" 'Correrto'
            Button4.Text = "D) 1939-2020"

        ElseIf CodigoPergunta = 76 Then
            Label10.Text = somaPerguntas & " - A Doutrina Truman foi criada por qual pais e em que ano?"

            Button1.Text = "A) Pai-Natalolandia em 1947"
            Button2.Text = "B) Bruxolandia em 2022"
            Button3.Text = "C) Zombieland em 1999"
            Button4.Text = "D) EUA em 1947" 'Correrto'

        ElseIf CodigoPergunta = 77 Then
            Label10.Text = somaPerguntas & " - O que foi a Guerra Fria?"

            Button1.Text = "A) Conflito não armado" 'Correrto'
            Button2.Text = "B) Uma Guerra no Gelo"
            Button3.Text = "C) Uma guerra com bolas de neve"
            Button4.Text = "D) Uma Guerra na Antartida"

        ElseIf CodigoPergunta = 78 Then
            Label10.Text = somaPerguntas & " - Qual era o pais que dominava o bloco capitalista Durante a Segunda Grande Guerra?"

            Button1.Text = "A) França"
            Button2.Text = "B) EUA <-" 'Correrto'
            Button3.Text = "C) Portugal"
            Button4.Text = "D) Grecia"

        ElseIf CodigoPergunta = 79 Then
            Label10.Text = somaPerguntas & " - Qual era o pais que dominava o bloco comunista Segunda Guerra Mundial?"

            Button1.Text = "A) URZZ"
            Button2.Text = "B) UR2S"
            Button3.Text = "C) URSS <-" 'Correrto'
            Button4.Text = "D) ORSS"

        ElseIf CodigoPergunta = 80 Then
            Label10.Text = somaPerguntas & " - Qual foi o desfecho da Guerra Fria?"

            Button1.Text = "A) Uma nova ordem Mundial"
            Button2.Text = "B) Criação de um novo desporto"
            Button3.Text = "C) Internet Livre para todos"
            Button4.Text = "D) Derrota da Alemanha" 'Correrto'

        ElseIf CodigoPergunta = 81 Then
            Label10.Text = somaPerguntas & " - Em que ao foi construído o muro de Berlim?"

            Button1.Text = "A) 1961" 'Correrto'
            Button2.Text = "B) 1993"
            Button3.Text = "C) 1914"
            Button4.Text = "D) 1918"

        ElseIf CodigoPergunta = 82 Then
            Label10.Text = somaPerguntas & " - Que líder supremo jacobino liderou a chamada 'Era do Terror'?"

            Button1.Text = "A) Fernão Lopes"
            Button2.Text = "B) Maximilien Robespierre" 'Correrto'
            Button3.Text = "C) D. Afonso Henriques"
            Button4.Text = "D) João Ferreira Luis de Sousa"

        ElseIf CodigoPergunta = 83 Then
            Label10.Text = somaPerguntas & " - Como Maria Antonieta foi morta?"

            Button1.Text = "A) Com uma maça"
            Button2.Text = "B) Com uma pá"
            Button3.Text = "C) Guilhotina" 'Correrto'
            Button4.Text = "D) Forca"

        ElseIf CodigoPergunta = 84 Then
            Label10.Text = somaPerguntas & " - Quando foi a invasão a Bastilha?"

            Button1.Text = "A)  25 de Desembro de 1504"
            Button2.Text = "B)  31 de Outubro de 1789"
            Button3.Text = "C)  14 de julho de 2001"
            Button4.Text = "D)  14 de Julho de 1789" 'Correrto'

        ElseIf CodigoPergunta = 85 Then
            Label10.Text = somaPerguntas & " - A famosa frase atribuída a Luis XIV: 'O Estado sou eu', define:"

            Button1.Text = "A) O absolutismo" 'Correrto'
            Button2.Text = "B) Luis XIV = Estado"
            Button3.Text = "C) America First, França Second"
            Button4.Text = "D) Luis XIV é depois de Luis XIII"

        ElseIf CodigoPergunta = 86 Then
            Label10.Text = somaPerguntas & " - Quais foram os principais motivos da Revolução Francesa?"

            Button1.Text = "A) Falta de Fome"
            Button2.Text = "B) Políticas, Económicas e Sociais" 'Correrto'
            Button3.Text = "C) Ervas daninhas a mais"
            Button4.Text = "D) Dias consecutivos de chuva"


        ElseIf CodigoPergunta = 87 Then
            Label10.Text = somaPerguntas & " - Como era chamada a prisão símbolo do absolutismo na França?"

            Button1.Text = "A) Fazeda D. Luis XIV"
            Button2.Text = "B) Terreiro do Paço"
            Button3.Text = "C) Bastilha" 'Correrto'
            Button4.Text = "D) Catedral"

        ElseIf CodigoPergunta = 88 Then
            Label10.Text = somaPerguntas & " - No reinado do chamado 'Terror', qual era o artefato responsável pela morte dos condenados?"

            Button1.Text = "A) Satisfação"
            Button2.Text = "B) Afogamento"
            Button3.Text = "C) Forca"
            Button4.Text = "D) Guilhotina" 'Correrto'

        ElseIf CodigoPergunta = 89 Then
            Label10.Text = somaPerguntas & " - Qual foi o principal ativista da era do terror?"

            Button1.Text = "A) Marat" 'Correrto'
            Button2.Text = "B) Papa"
            Button3.Text = "C) António Costa"
            Button4.Text = "D) António Oliveira de Salasar"

        ElseIf CodigoPergunta = 90 Then
            Label10.Text = somaPerguntas & " - Qual era ordem e os 3 estamentos da sociedade francesa pré-revolucionaria?"

            Button1.Text = "A) Inglêses, Portugueses e Franceses"
            Button2.Text = "B) Clero, Nobreza e Povo" 'Correrto'
            Button3.Text = "C) Bruxos, Feiticeiros e Ferreiros"
            Button4.Text = "D) Mecanicos, Recepcionistas e Ferreiros"

        ElseIf CodigoPergunta = 91 Then
            Label10.Text = somaPerguntas & " - Em que ano teve início a Revolução Francesa?"

            Button1.Text = "A) 25 de Abril de 2017"
            Button2.Text = "B) 5 de Maio de 1789"
            Button3.Text = "C) 45 de Maio de 1755" 'Correrto'
            Button4.Text = "D) 15 de Maio de 2650"

        ElseIf CodigoPergunta = 92 Then
            Label10.Text = somaPerguntas & " - Quem criou a guilhotina?"

            Button1.Text = "A) Quim Roscas"
            Button2.Text = "B) José Castelo Branco"
            Button3.Text = "C) Ludwig van Beethoven"
            Button4.Text = "D) Joseph-Ignace Guillotin" 'Correrto'

        ElseIf CodigoPergunta = 93 Then
            Label10.Text = somaPerguntas & " - Maria Antonieta de França, foi apelidada de quê?"

            Button1.Text = "A) L'Autre-chienne" 'Correrto'
            Button2.Text = "B) Menina do Povo"
            Button3.Text = "C) A Dadora de Sangue"
            Button4.Text = "D) A Sonhadora"

        ElseIf CodigoPergunta = 94 Then
            Label10.Text = somaPerguntas & " - Napoleão defendia que ideia revolucionária?"

            Button1.Text = "A) Pobreza"
            Button2.Text = "B) Liberalismo" 'Correrto'
            Button3.Text = "C) Assédio"
            Button4.Text = "D) Informática no Poder"

        ElseIf CodigoPergunta = 95 Then
            Label10.Text = somaPerguntas & " - Napoleão Bonaparte foi um grande Imperador de que país?"

            Button1.Text = "A) Roma"
            Button2.Text = "B) Portugal"
            Button3.Text = "C) França" 'Correrto'
            Button4.Text = "D) Estados Unidos"

        ElseIf CodigoPergunta = 96 Then
            Label10.Text = somaPerguntas & " - Qual era o nome da rainha da França que morreu na Revolução Francesa?"

            Button1.Text = "A) José Castelo Branco"
            Button2.Text = "B) D. Maria II"
            Button3.Text = "C) Ana Malhoa"
            Button4.Text = "D) Maria Antonieta" 'Correrto'

        ElseIf CodigoPergunta = 97 Then
            Label10.Text = somaPerguntas & " - Em que dia o rei Luís XVI foi morto?"

            Button1.Text = "A) 21 de janeiro de 1793" 'Correrto'
            Button2.Text = "B) 25 de junho de 2000"
            Button3.Text = "C) 13 de janeiro de 2001"
            Button4.Text = "D) 17 de Agosto de 1500"

        ElseIf CodigoPergunta = 98 Then
            Label10.Text = somaPerguntas & " - Que cidade Portuguesa mais sofreu com o terramoto de 1755 em Portugal?"

            Button1.Text = "A) França"
            Button2.Text = "B) Lisboa" 'Correrto'
            Button3.Text = "C) Coimbra"
            Button4.Text = "D) Algarve"

        ElseIf CodigoPergunta = 99 Then
            Label10.Text = somaPerguntas & " - A República foi implantada em Portugal em:"

            Button1.Text = "A) 25 de Dezembro de 2010"
            Button2.Text = "B) 5 de Outubro de 1500"
            Button3.Text = "C) 5 de Outubro de 1910" 'Correrto'
            Button4.Text = "D) 10 de Agosto de 2015"

        ElseIf CodigoPergunta = 100 Then
            Label10.Text = somaPerguntas & " - Qual	é a 'cidade-berço' de Portugal é?"

            Button1.Text = "A) Sintra"
            Button2.Text = "B) São Mamede de Infesta"
            Button3.Text = "C) Paços de Ferreira"
            Button4.Text = "D) Guimarães" 'Correrto'

        ElseIf CodigoPergunta = 101 Then
            Label10.Text = somaPerguntas & " - Qual foi o tema da expo 98 em Portugal?"

            Button1.Text = "A) Os oceanos: um património para o futuro" 'Correrto'
            Button2.Text = "B) O passado de Portugal"
            Button3.Text = "C) Portugal como uma Nação"
            Button4.Text = "D) As novas tecnologias em Portugal"

        ElseIf CodigoPergunta = 102 Then
            Label10.Text = somaPerguntas & " - Em que ano se deu em Lisboa um terrível terramoto, provocando a destruição de grande parte da capital?"

            Button1.Text = "A) 1999"
            Button2.Text = "B) 1755" 'Correrto'
            Button3.Text = "C) 1775"
            Button4.Text = "D) 2000"

        ElseIf CodigoPergunta = 103 Then
            Label10.Text = somaPerguntas & " - Segundo a lenda, que rei português, desaparecido na batalha de Alcácer-Quibir, voltará um dia, numa manhã de nevoeiro?"

            Button1.Text = "A) D. Fernando"
            Button2.Text = "B) D. Afonço Henriques"
            Button3.Text = "C) D. Sebastião" 'Correrto'
            Button4.Text = "D) D. João"

        ElseIf CodigoPergunta = 104 Then
            Label10.Text = somaPerguntas & " - Que navegador Português descobriu, em 1498, o caminho marítimo para a Índia?"

            Button1.Text = "A) Fernado de Albatraz"
            Button2.Text = "B) Toy"
            Button3.Text = "C) Camilo Castelo Branco"
            Button4.Text = "D) Vasco da Gama" 'Correrto'

        ElseIf CodigoPergunta = 105 Then
            Label10.Text = somaPerguntas & " - Em que século Portugal se tornou um país independente?"

            Button1.Text = "A) XII" 'Correrto'
            Button2.Text = "B) XXI"
            Button3.Text = "C) I"
            Button4.Text = "D) V"

        ElseIf CodigoPergunta = 106 Then
            Label10.Text = somaPerguntas & " - Quem descobriu o caminho marítimo para a América?"

            Button1.Text = "A) Fernando"
            Button2.Text = "B) Cristóvão Colombo" 'Correrto'
            Button3.Text = "C) Toy"
            Button4.Text = "D) Zé Povinho"

        ElseIf CodigoPergunta = 107 Then
            Label10.Text = somaPerguntas & " - Quem descobriu o caminho marítimo para o Brasil?"

            Button1.Text = "A) Brasileiros"
            Button2.Text = "B) Tony Carreira"
            Button3.Text = "C) Pedro Alvares Cabral" 'Correrto'
            Button4.Text = "D) Jonny Deep"

        ElseIf CodigoPergunta = 108 Then
            Label10.Text = somaPerguntas & " - Quem descobriu o caminho marítimo para a Madeira?"

            Button1.Text = "A) António Fontes Pereira de Melo"
            Button2.Text = "B) António Pereira Silva Madeira"
            Button3.Text = "C) Hitler"
            Button4.Text = "D) João Gonsalves Zarco " 'Correrto'

        ElseIf CodigoPergunta = 109 Then
            Label10.Text = somaPerguntas & " - Quem descobriu o caminho marítimo para os Açores?"

            Button1.Text = "A) Diogo de Silves" 'Correrto'
            Button2.Text = "B) Estaline"
            Button3.Text = "C) Jonh Cooner"
            Button4.Text = "D) Pedro Açores Silva"

        ElseIf CodigoPergunta = 110 Then
            Label10.Text = somaPerguntas & " - Em que ano foi descoberto o caminho marítimo para a Madeira?"

            Button1.Text = "A) 2018"
            Button2.Text = "B) 1419" 'Correrto'
            Button3.Text = "C) 1300"
            Button4.Text = "D) 1200"

        ElseIf CodigoPergunta = 111 Then
            Label10.Text = somaPerguntas & " - Em que ano foi descoberto o caminho marítimo para os Açores?"

            Button1.Text = "A) 2018"
            Button2.Text = "B) 13"
            Button3.Text = "C) 1427" 'Correrto'
            Button4.Text = "D) 1700"

        ElseIf CodigoPergunta = 112 Then
            Label10.Text = somaPerguntas & " - Em que ano foi descoberto o caminho marítimo para o Brasil?"

            Button1.Text = "A) 2001"
            Button2.Text = "B) 1999"
            Button3.Text = "C) 2000"
            Button4.Text = "D) 1500" 'Correrto'

        ElseIf CodigoPergunta = 113 Then
            Label10.Text = somaPerguntas & " - Em que ano foi descoberto o caminho marítimo para a América?"

            Button1.Text = "A) 1492" 'Correrto'
            Button2.Text = "B) 1999"
            Button3.Text = "C) 2000"
            Button4.Text = "D) 2001"

        ElseIf CodigoPergunta = 114 Then
            Label10.Text = somaPerguntas & " - Em que ano foi descoberto o caminho marítimo para a India?"

            Button1.Text = "A) 2000"
            Button2.Text = "B) 1498" 'Correrto'
            Button3.Text = "C) 1999"
            Button4.Text = "D) 2001"

        ElseIf CodigoPergunta = 115 Then
            Label10.Text = somaPerguntas & " - Quanto tempo durou o Império Romano unido?"

            Button1.Text = "A) 19 anos"
            Button2.Text = "B) 10 anos"
            Button3.Text = "C) 27 a.C. - 395 d.C. (422 anos)" 'Correrto'
            Button4.Text = "D) 700 anos"

        ElseIf CodigoPergunta = 116 Then
            Label10.Text = somaPerguntas & " - Quanto tempo durou a Republica + todo o Império Romano?"

            Button1.Text = "A) 100 anos"
            Button2.Text = "B) 500 anos"
            Button3.Text = "C) 2000 anos"
            Button4.Text = "D) 753 a.C. - 476 d.C. (1229 anos) " 'Correrto'

        ElseIf CodigoPergunta = 117 Then
            Label10.Text = somaPerguntas & " - Qual era 'Tropa' mais famosa e mais usada pelos romanos?"

            Button1.Text = "A) Legionários" 'Correrto'
            Button2.Text = "B) Soldados de Chumbo"
            Button3.Text = "C) Guardas de Buckingham"
            Button4.Text = "D) Vickings"

        ElseIf CodigoPergunta = 118 Then
            Label10.Text = somaPerguntas & " - Imperador mais famoso e mais importante de Roma?"

            Button1.Text = "A) Flavio Augusto"
            Button2.Text = "B) Julio Cesar" 'Correrto'
            Button3.Text = "C) Spartacos"
            Button4.Text = "D) Hitler"

        ElseIf CodigoPergunta = 119 Then
            Label10.Text = somaPerguntas & " - Quem foi o fundador de Roma?"

            Button1.Text = "A) Fernado"
            Button2.Text = "B) Remo"
            Button3.Text = "C) Rômulo" 'Correrto'
            Button4.Text = "D) António Damásio"

        ElseIf CodigoPergunta = 120 Then
            Label10.Text = somaPerguntas & " - Como Remo e Rômulo cresceram e foram criados?"

            Button1.Text = "A) Por uma Loira"
            Button2.Text = "B) Por um Avião"
            Button3.Text = "C) Por um Pássaro"
            Button4.Text = "D) Por uma Loba" 'Correrto'

        ElseIf CodigoPergunta = 121 Then
            Label10.Text = somaPerguntas & " - Que imperador romano conquistou a Galia a favor da antiga Roma?"

            Button1.Text = "A) Júlio César" 'Correrto'
            Button2.Text = "B) Arnold Schwarzenegger"
            Button3.Text = "C) Mark Zuckerberg"
            Button4.Text = "D) Bill Gates"

        ElseIf CodigoPergunta = 122 Then
            Label10.Text = somaPerguntas & " - A maior figura do renascimento?"

            Button1.Text = "A) Leonardo Da Cá Vinte"
            Button2.Text = "B) Leonardo da Vinci" 'Correrto'
            Button3.Text = "C) Leonardo Dicaprio"
            Button4.Text = "D) Leonardo Coimbra"

        ElseIf CodigoPergunta = 123 Then
            Label10.Text = somaPerguntas & " - Como eram chamados os protetores das artes e das ciências durante o Renascimento?"

            Button1.Text = "A) Terroristas"
            Button2.Text = "B) Androids"
            Button3.Text = "C) Mecenas" 'Correrto'
            Button4.Text = "D) Anónimos"

        ElseIf CodigoPergunta = 124 Then
            Label10.Text = somaPerguntas & " - Qual pais é considerado o berço do Renascimento?"

            Button1.Text = "A) Portugal"
            Button2.Text = "B) Polo Norte"
            Button3.Text = "C) Estás Unido na América"
            Button4.Text = "D) Itália" 'Correrto'

        ElseIf CodigoPergunta = 125 Then
            Label10.Text = somaPerguntas & " - Quando começou o Renascimento Italiano?"

            Button1.Text = "A) Meados do século XIV" 'Correrto'
            Button2.Text = "B) 2010"
            Button3.Text = "C) Meados do século V"
            Button4.Text = "D) Meados do século X"

        ElseIf CodigoPergunta = 126 Then
            Label10.Text = somaPerguntas & " - Qual escritor renascentista escreveu a obra 'Romeu e Julieta'?"

            Button1.Text = "A) Bartolomeu Dias"
            Button2.Text = "B) William Shakespeare" 'Correrto'
            Button3.Text = "C) Isaac Newton"
            Button4.Text = "D) Maomé"

        ElseIf CodigoPergunta = 127 Then
            Label10.Text = somaPerguntas & " - Quando acabou o Renascimento Italiano?"

            Button1.Text = "A) Meados do século X"
            Button2.Text = "B) Meados do século V"
            Button3.Text = "C) Finais do século XVI" 'Correrto'
            Button4.Text = "D) 2010"

        ElseIf CodigoPergunta = 128 Then
            Label10.Text = somaPerguntas & " - Quem pintou a Mona Liza?"

            Button1.Text = "A) Aristóteles"
            Button2.Text = "B) Albert Einstein"
            Button3.Text = "C) Cristovão Colombo"
            Button4.Text = "D) Leonardo da Vinci" 'Correrto'

        ElseIf CodigoPergunta = 129 Then
            Label10.Text = somaPerguntas & " - Em que ano foi pintada a Mona Liza de Leonardo da Vinci?"

            Button1.Text = "A) Entre 1503 e 1506" 'Correrto'
            Button2.Text = "B) 1300"
            Button3.Text = "C) 2000"
            Button4.Text = "D) 1000"

        ElseIf CodigoPergunta = 130 Then
            Label10.Text = somaPerguntas & " - O que foi renascimento?"

            Button1.Text = "A) Um golpe de estado"
            Button2.Text = "B) Um movimento artistico, intelectual e Ciêntifico" 'Correrto'
            Button3.Text = "C) Uma revolução"
            Button4.Text = "D) Uma engenhoca"

        ElseIf CodigoPergunta = 131 Then
            Label10.Text = somaPerguntas & " - A crítica dos humanistas do Renascimento era dirigida a qual sociedade?"

            Button1.Text = "A) Pobres"
            Button2.Text = "B) Musulmanos"
            Button3.Text = "C) Sociedade Feodal" 'Correrto'
            Button4.Text = "D) Paulo Portas"

        ElseIf CodigoPergunta = 132 Then
            Label10.Text = somaPerguntas & " - Qual artista renascentista italiano representou a virgem Maria com o menino Jesus? "

            Button1.Text = "A) Alibal Cavaco Silva"
            Button2.Text = "B) Pedro Passos Coelho"
            Button3.Text = "C) Vitor Hugo Cardinali"
            Button4.Text = "D) Rafael Sanzio" 'Correrto'


        ElseIf CodigoPergunta = 133 Then
            Label10.Text = somaPerguntas & " - O renascimento comercial iniciou durante qual período?"

            Button1.Text = "A) Baixa idade média" 'Correrto'
            Button2.Text = "B) Baixa idade Alta"
            Button3.Text = "C) Alta idade Baixa"
            Button4.Text = "D) Seculo XX"

        ElseIf CodigoPergunta = 134 Then
            Label10.Text = somaPerguntas & " - As principais características do Renascimento foram?"

            Button1.Text = "A) Renascimento"
            Button2.Text = "B) Humanismo, Racionalismo, Individualismo" 'Correrto'
            Button3.Text = "C) Segurança nas Nuvens"
            Button4.Text = "D) Simpatia"

        ElseIf CodigoPergunta = 135 Then
            Label10.Text = somaPerguntas & " - Como os renascentistas chamavam a Idade Média?"

            Button1.Text = "A) Idade de Outro Ser"
            Button2.Text = "B) Ilha das Cores"
            Button3.Text = "C) Idade das Trevas" 'Correrto'
            Button4.Text = "D) Idade Média"

        ElseIf CodigoPergunta = 136 Then
            Label10.Text = somaPerguntas & " - Como eram chamados os protetores das artes e das ciências?"

            Button1.Text = "A) Fadas Madrinhas"
            Button2.Text = "B) Vampiros"
            Button3.Text = "C) Guardîões dos Dragões"
            Button4.Text = "D) Mecenas" 'Correrto'


        ElseIf CodigoPergunta = 137 Then
            Label10.Text = somaPerguntas & " - Qual escritor renascentistas escreveu a obra 'Os Lusíadas'?"

            Button1.Text = "A) Luís de Camões" 'Correrto'
            Button2.Text = "B) Mark Zuckerberg"
            Button3.Text = "C) Thomas Alva Edison"
            Button4.Text = "D) Jesus de Nazaré"

        ElseIf CodigoPergunta = 138 Then
            Label10.Text = somaPerguntas & " - Que cientista teve que desmentir a sua teoria para fuguir á fugueira?"

            Button1.Text = "A) Sabrina"
            Button2.Text = "B) Galileu Galilei" 'Correrto'
            Button3.Text = "C) Jair Bolsonaro"
            Button4.Text = "D) Steve Jobs"

        ElseIf CodigoPergunta = 139 Then
            Label10.Text = somaPerguntas & " - O que foi a teoria geocentrica?"

            Button1.Text = "A) Poder de Deus"
            Button2.Text = "B) Abolir o Facebook"
            Button3.Text = "C) Terra no Cento do Universo" 'Correrto'
            Button4.Text = "D) Abraçar o WhatsApp"

        ElseIf CodigoPergunta = 140 Then
            Label10.Text = somaPerguntas & " - Quando começou a Guerra dos Sete Anos?"

            Button1.Text = "A) 1000"
            Button2.Text = "B) 2000"
            Button3.Text = "C) 1500"
            Button4.Text = "D) 1756" 'Correrto'

        ElseIf CodigoPergunta = 141 Then
            Label10.Text = somaPerguntas & " - Quando acabou a Guerra dos Sete Anos?"

            Button1.Text = "A) 1763" 'Correrto'
            Button2.Text = "B) 1000"
            Button3.Text = "C) 2000"
            Button4.Text = "D) 1500"

        ElseIf CodigoPergunta = 142 Then
            Label10.Text = somaPerguntas & " - Quanto tempo durou a Guerra dos Sete Anos?"

            Button1.Text = "A) 8 anos"
            Button2.Text = "B) 7 anos" 'Correrto'
            Button3.Text = "C) 6 anos"
            Button4.Text = "D) 5 anos"

        ElseIf CodigoPergunta = 143 Then
            Label10.Text = somaPerguntas & " - O que foi a Ordem dos Templários?"

            Button1.Text = "A) Seleção de Futebol"
            Button2.Text = "B) Uma marca de camisolas"
            Button3.Text = "C) Ordem militar" 'Correrto'
            Button4.Text = "D) Uma Empresa Comunista"

        ElseIf CodigoPergunta = 144 Then
            Label10.Text = somaPerguntas & " - Quando foi criada a Ordem dos Cavaleiros Templários?"

            Button1.Text = "A) 1000"
            Button2.Text = "B) 2000"
            Button3.Text = "C) 1500"
            Button4.Text = "D) 1118" 'Correrto'

        ElseIf CodigoPergunta = 145 Then
            Label10.Text = somaPerguntas & " - Quando foi extinta a Ordem dos Cavaleiros Templários?"

            Button1.Text = "A) 1312" 'Correrto'
            Button2.Text = "B) 1000"
            Button3.Text = "C) 2000"
            Button4.Text = "D) 1500"

        ElseIf CodigoPergunta = 146 Then
            Label10.Text = somaPerguntas & " - Quanto tempo durou a Ordem dos Templários?"

            Button1.Text = "A) 1000-2000 "
            Button2.Text = "B) 1118-1312 " 'Correrto'
            Button3.Text = "C) 1000-1500 "
            Button4.Text = "D) 1500-2000 "

        ElseIf CodigoPergunta = 147 Then
            Label10.Text = somaPerguntas & " - Qual era o principal objeto da Ordem dos Cavaleiros Templários? "

            Button1.Text = "A) Criar revoluções"
            Button2.Text = "B) Criar um canil"
            Button3.Text = "C) Proteger os Cristãos" 'Correrto'
            Button4.Text = "D) Chegar á Lua"

        ElseIf CodigoPergunta = 148 Then
            Label10.Text = somaPerguntas & " - Qual era a maior carateristica das vestes dos Cavaleiros Templários? "

            Button1.Text = "A) Caveiras Mexicanas "
            Button2.Text = "B) Bigodes Grandes Rosas"
            Button3.Text = "C) Laço Azul no Cabelo "
            Button4.Text = "D) Cruz Vermelha" 'Correrto'


        ElseIf CodigoPergunta = 149 Then
            Label10.Text = somaPerguntas & " - Onde era a sede dos Cavaleiros Templários?"

            Button1.Text = "A) Jerusalém" 'Correrto'
            Button2.Text = "B) Belém"
            Button3.Text = "C) Tomar"
            Button4.Text = "D) Lisboa"

        ElseIf CodigoPergunta = 150 Then
            Label10.Text = somaPerguntas & " - De quem era subordinados a Ordem dos Cavaleiros Templários? "

            Button1.Text = "A) El Rey D. João II "
            Button2.Text = "B) Papa" 'Correrto'
            Button3.Text = "C) Fernando Pessoa "
            Button4.Text = "D) Cesário Verde "

        ElseIf CodigoPergunta = 151 Then
            Label10.Text = somaPerguntas & " - Quando começou a Revoloção Russa? "

            Button1.Text = "A) 1500 "
            Button2.Text = "B) 2000 "
            Button3.Text = "C) 1917 " 'Correrto'
            Button4.Text = "D) 1939 "

        ElseIf CodigoPergunta = 152 Then
            Label10.Text = somaPerguntas & " - Quando começou a Revolução Americana? "

            Button1.Text = "A) 1945 "
            Button2.Text = "B) 2000 "
            Button3.Text = "C) 1550 "
            Button4.Text = "D) 1775 " 'Correrto'

        ElseIf CodigoPergunta = 153 Then
            Label10.Text = somaPerguntas & " - Quando acabou a Revolução Americana?"

            Button1.Text = "A) 1783 " 'Correrto'
            Button2.Text = "B) 2000 "
            Button3.Text = "C) 1755 "
            Button4.Text = "D) 1012 "

        ElseIf CodigoPergunta = 154 Then
            Label10.Text = somaPerguntas & " - Quanto tempo durou a Revolução Americana?"

            Button1.Text = "A) 10 anos "
            Button2.Text = "B) 8 anos " 'Correrto'
            Button3.Text = "C) 5 anos "
            Button4.Text = "D) 100 anos "

        ElseIf CodigoPergunta = 155 Then
            Label10.Text = somaPerguntas & " - Quando começou e acabou a Revolução Americana? "

            Button1.Text = "A) 1700 – 1755 "
            Button2.Text = "B) 1775 – 1799 "
            Button3.Text = "C) 1775 – 1783 " 'Correrto'
            Button4.Text = "D) 1500 – 2000 "

        ElseIf CodigoPergunta = 156 Then
            Label10.Text = somaPerguntas & " - Quantas colónias se destacaram no inicia da Revolução Americana?"

            Button1.Text = "A) 5 "
            Button2.Text = "B) 80 "
            Button3.Text = "C) 20 "
            Button4.Text = "D) 13 " 'Correrto'

        ElseIf CodigoPergunta = 157 Then
            Label10.Text = somaPerguntas & " - O que originou a Revolução Americana?"

            Button1.Text = "A) Independência " 'Correrto'
            Button2.Text = "B) Riqueza a mais "
            Button3.Text = "C) Poluição "
            Button4.Text = "D) Superpopulação "

        ElseIf CodigoPergunta = 158 Then
            Label10.Text = somaPerguntas & " - Personagem Caracteristica da Revolução Americana?"

            Button1.Text = "A) Zé Povinho "
            Button2.Text = "B) Benjamin Franklin " 'Correrto'
            Button3.Text = "C) Ruca "
            Button4.Text = "D) Pocoyo "

        ElseIf CodigoPergunta = 159 Then
            Label10.Text = somaPerguntas & " - Qual foi o primeiro resindeto dos Estados Unidos? "

            Button1.Text = "A) António Salazar "
            Button2.Text = "B) Haytham Kenway "
            Button3.Text = "C) George Washington " 'Correrto'
            Button4.Text = "D) Connor Kenway"

        ElseIf CodigoPergunta = 160 Then
            Label10.Text = somaPerguntas & " - ----------- "

            Button1.Text = "A) ----------- "
            Button2.Text = "B) ----------- "
            Button3.Text = "C) ----------- "
            Button4.Text = "D) ----------- " 'Correrto'

        ElseIf CodigoPergunta = 161 Then
            Label10.Text = somaPerguntas & " -  "

            Button1.Text = "A) ----------- " 'Correrto'
            Button2.Text = "B) ----------- "
            Button3.Text = "C) ----------- "
            Button4.Text = "D) ----------- "

        ElseIf CodigoPergunta = 162 Then
            Label10.Text = somaPerguntas & " -  "

            Button1.Text = "A) ----------- "
            Button2.Text = "B) ----------- " 'Correrto'
            Button3.Text = "C) ----------- "
            Button4.Text = "D) ----------- "

        ElseIf CodigoPergunta = 163 Then
            Label10.Text = somaPerguntas & " - ----------- "

            Button1.Text = "A) ----------- "
            Button2.Text = "B) ----------- "
            Button3.Text = "C) ----------- " 'Correrto'
            Button4.Text = "D) ----------- "

        ElseIf CodigoPergunta = 164 Then
            Label10.Text = somaPerguntas & " - ----------- "

            Button1.Text = "A) ----------- "
            Button2.Text = "B) ----------- "
            Button3.Text = "C) ----------- "
            Button4.Text = "D) ----------- " 'Correrto'

        ElseIf CodigoPergunta = 165 Then
            Label10.Text = somaPerguntas & " -  "

            Button1.Text = "A) ----------- " 'Correrto'
            Button2.Text = "B) ----------- "
            Button3.Text = "C) ----------- "
            Button4.Text = "D) ----------- "

        ElseIf CodigoPergunta = 166 Then
            Label10.Text = somaPerguntas & " -  "

            Button1.Text = "A) ----------- "
            Button2.Text = "B) ----------- " 'Correrto'
            Button3.Text = "C) ----------- "
            Button4.Text = "D) ----------- "

        ElseIf CodigoPergunta = 167 Then
            Label10.Text = somaPerguntas & " - ----------- "

            Button1.Text = "A) ----------- "
            Button2.Text = "B) ----------- "
            Button3.Text = "C) ----------- " 'Correrto'
            Button4.Text = "D) ----------- "

        ElseIf CodigoPergunta = 168 Then
            Label10.Text = somaPerguntas & " - ----------- "

            Button1.Text = "A) ----------- "
            Button2.Text = "B) ----------- "
            Button3.Text = "C) ----------- "
            Button4.Text = "D) ----------- " 'Correrto'

        ElseIf CodigoPergunta = 169 Then
            Label10.Text = somaPerguntas & " -  "

            Button1.Text = "A) ----------- " 'Correrto'
            Button2.Text = "B) ----------- "
            Button3.Text = "C) ----------- "
            Button4.Text = "D) ----------- "

        ElseIf CodigoPergunta = 170 Then
            Label10.Text = somaPerguntas & " -  "

            Button1.Text = "A) ----------- "
            Button2.Text = "B) ----------- " 'Correrto'
            Button3.Text = "C) ----------- "
            Button4.Text = "D) ----------- "

        ElseIf CodigoPergunta = 171 Then
            Label10.Text = somaPerguntas & " - ----------- "

            Button1.Text = "A) ----------- "
            Button2.Text = "B) ----------- "
            Button3.Text = "C) ----------- " 'Correrto'
            Button4.Text = "D) ----------- "

        ElseIf CodigoPergunta = 172 Then
            Label10.Text = somaPerguntas & " - ----------- "

            Button1.Text = "A) ----------- "
            Button2.Text = "B) ----------- "
            Button3.Text = "C) ----------- "
            Button4.Text = "D) ----------- " 'Correrto'

        ElseIf CodigoPergunta = 173 Then
            Label10.Text = somaPerguntas & " -  "

            Button1.Text = "A) ----------- " 'Correrto'
            Button2.Text = "B) ----------- "
            Button3.Text = "C) ----------- "
            Button4.Text = "D) ----------- "

        ElseIf CodigoPergunta = 174 Then
            Label10.Text = somaPerguntas & " -  "

            Button1.Text = "A) ----------- "
            Button2.Text = "B) ----------- " 'Correrto'
            Button3.Text = "C) ----------- "
            Button4.Text = "D) ----------- "

        ElseIf CodigoPergunta = 175 Then
            Label10.Text = somaPerguntas & " - ----------- "

            Button1.Text = "A) ----------- "
            Button2.Text = "B) ----------- "
            Button3.Text = "C) ----------- " 'Correrto'
            Button4.Text = "D) ----------- "

        ElseIf CodigoPergunta = 176 Then
            Label10.Text = somaPerguntas & " - ----------- "

            Button1.Text = "A) ----------- "
            Button2.Text = "B) ----------- "
            Button3.Text = "C) ----------- "
            Button4.Text = "D) ----------- " 'Correrto'

        ElseIf CodigoPergunta = 177 Then
            Label10.Text = somaPerguntas & " -  "

            Button1.Text = "A) ----------- " 'Correrto'
            Button2.Text = "B) ----------- "
            Button3.Text = "C) ----------- "
            Button4.Text = "D) ----------- "

        ElseIf CodigoPergunta = 178 Then
            Label10.Text = somaPerguntas & " -  "

            Button1.Text = "A) ----------- "
            Button2.Text = "B) ----------- " 'Correrto'
            Button3.Text = "C) ----------- "
            Button4.Text = "D) ----------- "

        ElseIf CodigoPergunta = 179 Then
            Label10.Text = somaPerguntas & " - ----------- "

            Button1.Text = "A) ----------- "
            Button2.Text = "B) ----------- "
            Button3.Text = "C) ----------- " 'Correrto'
            Button4.Text = "D) ----------- "

        ElseIf CodigoPergunta = 180 Then
            Label10.Text = somaPerguntas & " - ----------- "

            Button1.Text = "A) ----------- "
            Button2.Text = "B) ----------- "
            Button3.Text = "C) ----------- "
            Button4.Text = "D) ----------- " 'Correrto'

        ElseIf CodigoPergunta = 181 Then
            Label10.Text = somaPerguntas & " -  "

            Button1.Text = "A) ----------- " 'Correrto'
            Button2.Text = "B) ----------- "
            Button3.Text = "C) ----------- "
            Button4.Text = "D) ----------- "

        ElseIf CodigoPergunta = 182 Then
            Label10.Text = somaPerguntas & " -  "

            Button1.Text = "A) ----------- "
            Button2.Text = "B) ----------- " 'Correrto'
            Button3.Text = "C) ----------- "
            Button4.Text = "D) ----------- "

        ElseIf CodigoPergunta = 183 Then
            Label10.Text = somaPerguntas & " - ----------- "

            Button1.Text = "A) ----------- "
            Button2.Text = "B) ----------- "
            Button3.Text = "C) ----------- " 'Correrto'
            Button4.Text = "D) ----------- "

        ElseIf CodigoPergunta = 184 Then
            Label10.Text = somaPerguntas & " - ----------- "

            Button1.Text = "A) ----------- "
            Button2.Text = "B) ----------- "
            Button3.Text = "C) ----------- "
            Button4.Text = "D) ----------- " 'Correrto'

        ElseIf CodigoPergunta = 185 Then
            Label10.Text = somaPerguntas & " -  "

            Button1.Text = "A) ----------- " 'Correrto'
            Button2.Text = "B) ----------- "
            Button3.Text = "C) ----------- "
            Button4.Text = "D) ----------- "

        ElseIf CodigoPergunta = 186 Then
            Label10.Text = somaPerguntas & " -  "

            Button1.Text = "A) ----------- "
            Button2.Text = "B) ----------- " 'Correrto'
            Button3.Text = "C) ----------- "
            Button4.Text = "D) ----------- "

        ElseIf CodigoPergunta = 187 Then
            Label10.Text = somaPerguntas & " - ----------- "

            Button1.Text = "A) ----------- "
            Button2.Text = "B) ----------- "
            Button3.Text = "C) ----------- " 'Correrto'
            Button4.Text = "D) ----------- "

        ElseIf CodigoPergunta = 188 Then
            Label10.Text = somaPerguntas & " - ----------- "

            Button1.Text = "A) ----------- "
            Button2.Text = "B) ----------- "
            Button3.Text = "C) ----------- "
            Button4.Text = "D) ----------- " 'Correrto'

        ElseIf CodigoPergunta = 189 Then
            Label10.Text = somaPerguntas & " -  "

            Button1.Text = "A) ----------- " 'Correrto'
            Button2.Text = "B) ----------- "
            Button3.Text = "C) ----------- "
            Button4.Text = "D) ----------- "

        ElseIf CodigoPergunta = 190 Then
            Label10.Text = somaPerguntas & " -  "

            Button1.Text = "A) ----------- "
            Button2.Text = "B) ----------- " 'Correrto'
            Button3.Text = "C) ----------- "
            Button4.Text = "D) ----------- "

        ElseIf CodigoPergunta = 191 Then
            Label10.Text = somaPerguntas & " - ----------- "

            Button1.Text = "A) ----------- "
            Button2.Text = "B) ----------- "
            Button3.Text = "C) ----------- " 'Correrto'
            Button4.Text = "D) ----------- "

        ElseIf CodigoPergunta = 192 Then
            Label10.Text = somaPerguntas & " - ----------- "

            Button1.Text = "A) ----------- "
            Button2.Text = "B) ----------- "
            Button3.Text = "C) ----------- "
            Button4.Text = "D) ----------- " 'Correrto'

        ElseIf CodigoPergunta = 193 Then
            Label10.Text = somaPerguntas & " -  "

            Button1.Text = "A) ----------- " 'Correrto'
            Button2.Text = "B) ----------- "
            Button3.Text = "C) ----------- "
            Button4.Text = "D) ----------- "

        ElseIf CodigoPergunta = 194 Then
            Label10.Text = somaPerguntas & " -  "

            Button1.Text = "A) ----------- "
            Button2.Text = "B) ----------- " 'Correrto'
            Button3.Text = "C) ----------- "
            Button4.Text = "D) ----------- "

        ElseIf CodigoPergunta = 195 Then
            Label10.Text = somaPerguntas & " - ----------- "

            Button1.Text = "A) ----------- "
            Button2.Text = "B) ----------- "
            Button3.Text = "C) ----------- " 'Correrto'
            Button4.Text = "D) ----------- "

        ElseIf CodigoPergunta = 196 Then
            Label10.Text = somaPerguntas & " - ----------- "

            Button1.Text = "A) ----------- "
            Button2.Text = "B) ----------- "
            Button3.Text = "C) ----------- "
            Button4.Text = "D) ----------- " 'Correrto'

        ElseIf CodigoPergunta = 197 Then
            Label10.Text = somaPerguntas & " -  "

            Button1.Text = "A) ----------- " 'Correrto'
            Button2.Text = "B) ----------- "
            Button3.Text = "C) ----------- "
            Button4.Text = "D) ----------- "

        ElseIf CodigoPergunta = 198 Then
            Label10.Text = somaPerguntas & " -  "

            Button1.Text = "A) ----------- "
            Button2.Text = "B) ----------- " 'Correrto'
            Button3.Text = "C) ----------- "
            Button4.Text = "D) ----------- "

        ElseIf CodigoPergunta = 199 Then
            Label10.Text = somaPerguntas & " - ----------- "

            Button1.Text = "A) ----------- "
            Button2.Text = "B) ----------- "
            Button3.Text = "C) ----------- " 'Correrto'
            Button4.Text = "D) ----------- "

        ElseIf CodigoPergunta = 200 Then
            Label10.Text = somaPerguntas & " - ----------- "

            Button1.Text = "A) ----------- "
            Button2.Text = "B) ----------- "
            Button3.Text = "C) ----------- "
            Button4.Text = "D) ----------- " 'Correrto'

        ElseIf CodigoPergunta = 201 Then
            Label10.Text = somaPerguntas & " -  "

            Button1.Text = "A) ----------- " 'Correrto'
            Button2.Text = "B) ----------- "
            Button3.Text = "C) ----------- "
            Button4.Text = "D) ----------- "

        ElseIf CodigoPergunta = 202 Then
            Label10.Text = somaPerguntas & " -  "

            Button1.Text = "A) ----------- "
            Button2.Text = "B) ----------- " 'Correrto'
            Button3.Text = "C) ----------- "
            Button4.Text = "D) ----------- "

        ElseIf CodigoPergunta = 203 Then
            Label10.Text = somaPerguntas & " - ----------- "

            Button1.Text = "A) ----------- "
            Button2.Text = "B) ----------- "
            Button3.Text = "C) ----------- " 'Correrto'
            Button4.Text = "D) ----------- "

        ElseIf CodigoPergunta = 204 Then
            Label10.Text = somaPerguntas & " - ----------- "

            Button1.Text = "A) ----------- "
            Button2.Text = "B) ----------- "
            Button3.Text = "C) ----------- "
            Button4.Text = "D) ----------- " 'Correrto'

        ElseIf CodigoPergunta = 205 Then
            Label10.Text = somaPerguntas & " -  "

            Button1.Text = "A) ----------- " 'Correrto'
            Button2.Text = "B) ----------- "
            Button3.Text = "C) ----------- "
            Button4.Text = "D) ----------- "

        ElseIf CodigoPergunta = 206 Then
            Label10.Text = somaPerguntas & " -  "

            Button1.Text = "A) ----------- "
            Button2.Text = "B) ----------- " 'Correrto'
            Button3.Text = "C) ----------- "
            Button4.Text = "D) ----------- "

        ElseIf CodigoPergunta = 207 Then
            Label10.Text = somaPerguntas & " - ----------- "

            Button1.Text = "A) ----------- "
            Button2.Text = "B) ----------- "
            Button3.Text = "C) ----------- " 'Correrto'
            Button4.Text = "D) ----------- "

        ElseIf CodigoPergunta = 208 Then
            Label10.Text = somaPerguntas & " - ----------- "

            Button1.Text = "A) ----------- "
            Button2.Text = "B) ----------- "
            Button3.Text = "C) ----------- "
            Button4.Text = "D) ----------- " 'Correrto'

        ElseIf CodigoPergunta = 209 Then
            Label10.Text = somaPerguntas & " -  "

            Button1.Text = "A) ----------- " 'Correrto'
            Button2.Text = "B) ----------- "
            Button3.Text = "C) ----------- "
            Button4.Text = "D) ----------- "

        ElseIf CodigoPergunta = 210 Then
            Label10.Text = somaPerguntas & " -  "

            Button1.Text = "A) ----------- "
            Button2.Text = "B) ----------- " 'Correrto'
            Button3.Text = "C) ----------- "
            Button4.Text = "D) ----------- "

        ElseIf CodigoPergunta = 211 Then
            Label10.Text = somaPerguntas & " - ----------- "

            Button1.Text = "A) ----------- "
            Button2.Text = "B) ----------- "
            Button3.Text = "C) ----------- " 'Correrto'
            Button4.Text = "D) ----------- "

        ElseIf CodigoPergunta = 212 Then
            Label10.Text = somaPerguntas & " - ----------- "

            Button1.Text = "A) ----------- "
            Button2.Text = "B) ----------- "
            Button3.Text = "C) ----------- "
            Button4.Text = "D) ----------- " 'Correrto'

        ElseIf CodigoPergunta = 213 Then
            Label10.Text = somaPerguntas & " -  "

            Button1.Text = "A) ----------- " 'Correrto'
            Button2.Text = "B) ----------- "
            Button3.Text = "C) ----------- "
            Button4.Text = "D) ----------- "

        ElseIf CodigoPergunta = 214 Then
            Label10.Text = somaPerguntas & " -  "

            Button1.Text = "A) ----------- "
            Button2.Text = "B) ----------- " 'Correrto'
            Button3.Text = "C) ----------- "
            Button4.Text = "D) ----------- "

        ElseIf CodigoPergunta = 215 Then
            Label10.Text = somaPerguntas & " - ----------- "

            Button1.Text = "A) ----------- "
            Button2.Text = "B) ----------- "
            Button3.Text = "C) ----------- " 'Correrto'
            Button4.Text = "D) ----------- "

        ElseIf CodigoPergunta = 216 Then
            Label10.Text = somaPerguntas & " - ----------- "

            Button1.Text = "A) ----------- "
            Button2.Text = "B) ----------- "
            Button3.Text = "C) ----------- "
            Button4.Text = "D) ----------- " 'Correrto'

        ElseIf CodigoPergunta = 217 Then
            Label10.Text = somaPerguntas & " -  "

            Button1.Text = "A) ----------- " 'Correrto'
            Button2.Text = "B) ----------- "
            Button3.Text = "C) ----------- "
            Button4.Text = "D) ----------- "

        ElseIf CodigoPergunta = 218 Then
            Label10.Text = somaPerguntas & " -  "

            Button1.Text = "A) ----------- "
            Button2.Text = "B) ----------- " 'Correrto'
            Button3.Text = "C) ----------- "
            Button4.Text = "D) ----------- "

        ElseIf CodigoPergunta = 219 Then
            Label10.Text = somaPerguntas & " - ----------- "

            Button1.Text = "A) ----------- "
            Button2.Text = "B) ----------- "
            Button3.Text = "C) ----------- " 'Correrto'
            Button4.Text = "D) ----------- "

        ElseIf CodigoPergunta = 220 Then
            Label10.Text = somaPerguntas & " - ----------- "

            Button1.Text = "A) ----------- "
            Button2.Text = "B) ----------- "
            Button3.Text = "C) ----------- "
            Button4.Text = "D) ----------- " 'Correrto'

        ElseIf CodigoPergunta = 221 Then
            Label10.Text = somaPerguntas & " -  "

            Button1.Text = "A) ----------- " 'Correrto'
            Button2.Text = "B) ----------- "
            Button3.Text = "C) ----------- "
            Button4.Text = "D) ----------- "

        ElseIf CodigoPergunta = 222 Then
            Label10.Text = somaPerguntas & " -  "

            Button1.Text = "A) ----------- "
            Button2.Text = "B) ----------- " 'Correrto'
            Button3.Text = "C) ----------- "
            Button4.Text = "D) ----------- "

        ElseIf CodigoPergunta = 223 Then
            Label10.Text = somaPerguntas & " - ----------- "

            Button1.Text = "A) ----------- "
            Button2.Text = "B) ----------- "
            Button3.Text = "C) ----------- " 'Correrto'
            Button4.Text = "D) ----------- "

        ElseIf CodigoPergunta = 224 Then
            Label10.Text = somaPerguntas & " - ----------- "

            Button1.Text = "A) ----------- "
            Button2.Text = "B) ----------- "
            Button3.Text = "C) ----------- "
            Button4.Text = "D) ----------- " 'Correrto'

        ElseIf CodigoPergunta = 225 Then
            Label10.Text = somaPerguntas & " -  "

            Button1.Text = "A) ----------- " 'Correrto'
            Button2.Text = "B) ----------- "
            Button3.Text = "C) ----------- "
            Button4.Text = "D) ----------- "

        ElseIf CodigoPergunta = 226 Then
            Label10.Text = somaPerguntas & " -  "

            Button1.Text = "A) ----------- "
            Button2.Text = "B) ----------- " 'Correrto'
            Button3.Text = "C) ----------- "
            Button4.Text = "D) ----------- "

        ElseIf CodigoPergunta = 227 Then
            Label10.Text = somaPerguntas & " - ----------- "

            Button1.Text = "A) ----------- "
            Button2.Text = "B) ----------- "
            Button3.Text = "C) ----------- " 'Correrto'
            Button4.Text = "D) ----------- "

        ElseIf CodigoPergunta = 228 Then
            Label10.Text = somaPerguntas & " - ----------- "

            Button1.Text = "A) ----------- "
            Button2.Text = "B) ----------- "
            Button3.Text = "C) ----------- "
            Button4.Text = "D) ----------- " 'Correrto'

        ElseIf CodigoPergunta = 229 Then
            Label10.Text = somaPerguntas & " -  "

            Button1.Text = "A) ----------- " 'Correrto'
            Button2.Text = "B) ----------- "
            Button3.Text = "C) ----------- "
            Button4.Text = "D) ----------- "

        ElseIf CodigoPergunta = 230 Then
            Label10.Text = somaPerguntas & " -  "

            Button1.Text = "A) ----------- "
            Button2.Text = "B) ----------- " 'Correrto'
            Button3.Text = "C) ----------- "
            Button4.Text = "D) ----------- "

        ElseIf CodigoPergunta = 231 Then
            Label10.Text = somaPerguntas & " - ----------- "

            Button1.Text = "A) ----------- "
            Button2.Text = "B) ----------- "
            Button3.Text = "C) ----------- " 'Correrto'
            Button4.Text = "D) ----------- "

        ElseIf CodigoPergunta = 232 Then
            Label10.Text = somaPerguntas & " - ----------- "

            Button1.Text = "A) ----------- "
            Button2.Text = "B) ----------- "
            Button3.Text = "C) ----------- "
            Button4.Text = "D) ----------- " 'Correrto'

            'Fim do Código da Pergunta' 
        End If

        ClickCodPergunta = False
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        'Resposta 1/A'

        'Chamar o Pisca Botoes
        PiscarButao()

        If CodigoPergunta = 3 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 5 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 9 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 13 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 16 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 17 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 21 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 25 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 29 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 33 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 37 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 41 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 45 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 49 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 53 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 57 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 61 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 65 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 69 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 73 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 77 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 81 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 85 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 89 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 93 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 97 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 101 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 105 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 109 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 113 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 117 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 121 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 125 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 129 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 133 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 137 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 141 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 145 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 149 Then
            Correto()
            Perguntas()
        Else
            Errado()
            Perguntas()
        End If
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        'Resposta 2/B'

        'Chamar o pisca Butoes
        PiscarButao()

        If CodigoPergunta = 4 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 6 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 10 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 14 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 16 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 18 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 22 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 26 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 30 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 34 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 38 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 42 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 46 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 50 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 54 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 58 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 62 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 66 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 70 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 74 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 78 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 82 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 86 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 90 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 94 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 98 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 102 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 106 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 110 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 114 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 118 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 122 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 126 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 130 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 134 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 138 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 142 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 146 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 150 Then
            Correto()
            Perguntas()
        Else
            Errado()
            Perguntas()
        End If
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        'Resposta 3/C'

        'Chamar o pisca Butoes
        PiscarButao()

        If CodigoPergunta = 1 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 7 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 11 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 15 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 16 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 19 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 23 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 27 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 31 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 35 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 39 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 43 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 47 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 51 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 55 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 59 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 63 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 67 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 71 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 75 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 79 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 83 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 87 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 91 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 95 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 99 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 103 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 107 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 111 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 115 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 119 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 123 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 127 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 131 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 135 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 139 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 143 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 147 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 151 Then
            Correto()
            Perguntas()
        Else
            Errado()
            Perguntas()
        End If
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        'Resposta 4/D'

        'Chamar o pisca Butoes
        PiscarButao()

        If CodigoPergunta = 2 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 8 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 12 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 16 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 20 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 24 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 28 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 32 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 36 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 40 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 44 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 48 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 52 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 56 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 60 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 64 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 68 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 72 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 76 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 80 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 84 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 88 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 92 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 96 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 100 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 104 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 108 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 112 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 116 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 120 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 124 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 128 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 132 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 136 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 140 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 144 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 148 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 152 Then
            Correto()
            Perguntas()
        Else
            Errado()
            Perguntas()
        End If
    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        'Ajuda'
        If Ajudas > 0 Then
            Ajudas -= 1
            RespostaPergunta()
        Else
            Button7.Enabled = True
            MsgBox("Não têns mais ajudas disponiveis")
        End If
        Label7.Text = Ajudas
    End Sub

    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click
        'Recomeçar'
        Label9.BackColor = Color.White
        RespostasCorretas = 0
        RespostasErradas = 0
        somaPerguntas = 0
        Label5.Text = RespostasCorretas
        Label6.Text = RespostasErradas
        Perguntas()
    End Sub

    Sub RespostaPergunta()
        If CodigoPergunta = 1 Then
            MsgBox("C")
        ElseIf CodigoPergunta = 2 Then
            MsgBox("D")
        ElseIf CodigoPergunta = 3 Then
            MsgBox("A")
        ElseIf CodigoPergunta = 4 Then
            MsgBox("B")
        ElseIf CodigoPergunta = 5 Then
            MsgBox("A")
        ElseIf CodigoPergunta = 6 Then
            MsgBox("B")
        ElseIf CodigoPergunta = 7 Then
            MsgBox("C")
        ElseIf CodigoPergunta = 8 Then
            MsgBox("D")
        ElseIf CodigoPergunta = 9 Then
            MsgBox("A")
        ElseIf CodigoPergunta = 10 Then
            MsgBox("B")
        ElseIf CodigoPergunta = 11 Then
            MsgBox("C")
        ElseIf CodigoPergunta = 12 Then
            MsgBox("D")
        ElseIf CodigoPergunta = 13 Then
            MsgBox("A")
        ElseIf CodigoPergunta = 14 Then
            MsgBox("B")
        ElseIf CodigoPergunta = 15 Then
            MsgBox("C")
        ElseIf CodigoPergunta = 16 Then
            MsgBox("A,B,C,D")
        ElseIf CodigoPergunta = 17 Then
            MsgBox("A")
        ElseIf CodigoPergunta = 18 Then
            MsgBox("B")
        ElseIf CodigoPergunta = 19 Then
            MsgBox("C")
        ElseIf CodigoPergunta = 20 Then
            MsgBox("D")

        ElseIf CodigoPergunta = 21 Then
            MsgBox("A")
        ElseIf CodigoPergunta = 22 Then
            MsgBox("B")
        ElseIf CodigoPergunta = 23 Then
            MsgBox("C")
        ElseIf CodigoPergunta = 24 Then
            MsgBox("D")
        ElseIf CodigoPergunta = 25 Then
            MsgBox("A")
        ElseIf CodigoPergunta = 26 Then
            MsgBox("B")
        ElseIf CodigoPergunta = 27 Then
            MsgBox("C")
        ElseIf CodigoPergunta = 28 Then
            MsgBox("D")
        ElseIf CodigoPergunta = 29 Then
            MsgBox("A")
        ElseIf CodigoPergunta = 30 Then
            MsgBox("B")
        ElseIf CodigoPergunta = 31 Then
            MsgBox("C")
        ElseIf CodigoPergunta = 32 Then
            MsgBox("D")
        ElseIf CodigoPergunta = 33 Then
            MsgBox("A")
        ElseIf CodigoPergunta = 34 Then
            MsgBox("B")
        ElseIf CodigoPergunta = 35 Then
            MsgBox("C")
        ElseIf CodigoPergunta = 36 Then
            MsgBox("D")
        ElseIf CodigoPergunta = 37 Then
            MsgBox("A")
        ElseIf CodigoPergunta = 38 Then
            MsgBox("B")
        ElseIf CodigoPergunta = 39 Then
            MsgBox("C")
        ElseIf CodigoPergunta = 40 Then
            MsgBox("D")
        ElseIf CodigoPergunta = 41 Then
            MsgBox("A")
        ElseIf CodigoPergunta = 42 Then
            MsgBox("B")
        ElseIf CodigoPergunta = 43 Then
            MsgBox("C")
        ElseIf CodigoPergunta = 44 Then
            MsgBox("D")
        ElseIf CodigoPergunta = 45 Then
            MsgBox("A")
        ElseIf CodigoPergunta = 46 Then
            MsgBox("B")
        ElseIf CodigoPergunta = 47 Then
            MsgBox("C")
        ElseIf CodigoPergunta = 48 Then
            MsgBox("D")
        ElseIf CodigoPergunta = 49 Then
            MsgBox("A")
        ElseIf CodigoPergunta = 50 Then
            MsgBox("B")
        ElseIf CodigoPergunta = 51 Then
            MsgBox("C")
        ElseIf CodigoPergunta = 52 Then
            MsgBox("D")
        ElseIf CodigoPergunta = 53 Then
            MsgBox("A")
        ElseIf CodigoPergunta = 54 Then
            MsgBox("B")
        ElseIf CodigoPergunta = 55 Then
            MsgBox("C")
        ElseIf CodigoPergunta = 56 Then
            MsgBox("D")
        ElseIf CodigoPergunta = 57 Then
            MsgBox("A")
        ElseIf CodigoPergunta = 58 Then
            MsgBox("B")
        ElseIf CodigoPergunta = 59 Then
            MsgBox("C")
        ElseIf CodigoPergunta = 60 Then
            MsgBox("D")
        ElseIf CodigoPergunta = 61 Then
            MsgBox("A")
        ElseIf CodigoPergunta = 62 Then
            MsgBox("B")
        ElseIf CodigoPergunta = 63 Then
            MsgBox("C")
        ElseIf CodigoPergunta = 64 Then
            MsgBox("D")
        ElseIf CodigoPergunta = 65 Then
            MsgBox("A")
        ElseIf CodigoPergunta = 66 Then
            MsgBox("B")
        ElseIf CodigoPergunta = 67 Then
            MsgBox("C")
        ElseIf CodigoPergunta = 68 Then
            MsgBox("D")
        ElseIf CodigoPergunta = 69 Then
            MsgBox("A")
        ElseIf CodigoPergunta = 70 Then
            MsgBox("B")
        ElseIf CodigoPergunta = 71 Then
            MsgBox("C")
        ElseIf CodigoPergunta = 72 Then
            MsgBox("D")
        ElseIf CodigoPergunta = 73 Then
            MsgBox("A")
        ElseIf CodigoPergunta = 74 Then
            MsgBox("B")
        ElseIf CodigoPergunta = 75 Then
            MsgBox("C")
        ElseIf CodigoPergunta = 76 Then
            MsgBox("D")
        ElseIf CodigoPergunta = 77 Then
            MsgBox("A")
        ElseIf CodigoPergunta = 78 Then
            MsgBox("B")
        ElseIf CodigoPergunta = 79 Then
            MsgBox("C")
        ElseIf CodigoPergunta = 80 Then
            MsgBox("D")
        ElseIf CodigoPergunta = 81 Then
            MsgBox("A")
        ElseIf CodigoPergunta = 82 Then
            MsgBox("B")
        ElseIf CodigoPergunta = 83 Then
            MsgBox("C")
        ElseIf CodigoPergunta = 84 Then
            MsgBox("D")
        ElseIf CodigoPergunta = 85 Then
            MsgBox("A")
        ElseIf CodigoPergunta = 86 Then
            MsgBox("B")
        ElseIf CodigoPergunta = 87 Then
            MsgBox("C")
        ElseIf CodigoPergunta = 88 Then
            MsgBox("D")
        ElseIf CodigoPergunta = 89 Then
            MsgBox("A")
        ElseIf CodigoPergunta = 90 Then
            MsgBox("B")
        ElseIf CodigoPergunta = 91 Then
            MsgBox("C")
        ElseIf CodigoPergunta = 92 Then
            MsgBox("D")
        ElseIf CodigoPergunta = 93 Then
            MsgBox("A")
        ElseIf CodigoPergunta = 94 Then
            MsgBox("B")
        ElseIf CodigoPergunta = 91 Then
            MsgBox("C")
        ElseIf CodigoPergunta = 92 Then
            MsgBox("D")
        ElseIf CodigoPergunta = 93 Then
            MsgBox("A")
        ElseIf CodigoPergunta = 94 Then
            MsgBox("B")
        ElseIf CodigoPergunta = 95 Then
            MsgBox("C")
        ElseIf CodigoPergunta = 96 Then
            MsgBox("D")
        ElseIf CodigoPergunta = 97 Then
            MsgBox("A")
        ElseIf CodigoPergunta = 98 Then
            MsgBox("B")
        ElseIf CodigoPergunta = 99 Then
            MsgBox("C")
        ElseIf CodigoPergunta = 100 Then
            MsgBox("D")
        ElseIf CodigoPergunta = 101 Then
            MsgBox("A")
        ElseIf CodigoPergunta = 102 Then
            MsgBox("B")
        ElseIf CodigoPergunta = 103 Then
            MsgBox("C")
        ElseIf CodigoPergunta = 104 Then
            MsgBox("D")
        ElseIf CodigoPergunta = 105 Then
            MsgBox("A")
        ElseIf CodigoPergunta = 106 Then
            MsgBox("B")
        ElseIf CodigoPergunta = 107 Then
            MsgBox("C")
        ElseIf CodigoPergunta = 108 Then
            MsgBox("D")
        ElseIf CodigoPergunta = 109 Then
            MsgBox("A")
        ElseIf CodigoPergunta = 110 Then
            MsgBox("B")
        ElseIf CodigoPergunta = 111 Then
            MsgBox("C")
        ElseIf CodigoPergunta = 112 Then
            MsgBox("D")
        ElseIf CodigoPergunta = 113 Then
            MsgBox("A")
        ElseIf CodigoPergunta = 114 Then
            MsgBox("B")
        ElseIf CodigoPergunta = 115 Then
            MsgBox("C")
        ElseIf CodigoPergunta = 116 Then
            MsgBox("D")
        ElseIf CodigoPergunta = 117 Then
            MsgBox("A")
        ElseIf CodigoPergunta = 118 Then
            MsgBox("B")
        ElseIf CodigoPergunta = 119 Then
            MsgBox("C")
        ElseIf CodigoPergunta = 120 Then
            MsgBox("D")
        ElseIf CodigoPergunta = 121 Then
            MsgBox("A")
        ElseIf CodigoPergunta = 122 Then
            MsgBox("B")
        ElseIf CodigoPergunta = 123 Then
            MsgBox("C")
        ElseIf CodigoPergunta = 124 Then
            MsgBox("D")
        ElseIf CodigoPergunta = 125 Then
            MsgBox("A")
        ElseIf CodigoPergunta = 126 Then
            MsgBox("B")
        ElseIf CodigoPergunta = 127 Then
            MsgBox("C")
        ElseIf CodigoPergunta = 128 Then
            MsgBox("D")
        ElseIf CodigoPergunta = 129 Then
            MsgBox("A")
        ElseIf CodigoPergunta = 130 Then
            MsgBox("B")
        ElseIf CodigoPergunta = 131 Then
            MsgBox("C")
        ElseIf CodigoPergunta = 132 Then
            MsgBox("D")
        ElseIf CodigoPergunta = 133 Then
            MsgBox("A")
        ElseIf CodigoPergunta = 134 Then
            MsgBox("B")
        ElseIf CodigoPergunta = 135 Then
            MsgBox("C")
        ElseIf CodigoPergunta = 136 Then
            MsgBox("D")
        ElseIf CodigoPergunta = 137 Then
            MsgBox("A")
        ElseIf CodigoPergunta = 138 Then
            MsgBox("B")
        ElseIf CodigoPergunta = 139 Then
            MsgBox("C")
        ElseIf CodigoPergunta = 140 Then
            MsgBox("D")
        ElseIf CodigoPergunta = 141 Then
            MsgBox("A")
        ElseIf CodigoPergunta = 142 Then
            MsgBox("B")
        ElseIf CodigoPergunta = 143 Then
            MsgBox("C")
        ElseIf CodigoPergunta = 144 Then
            MsgBox("D")
        ElseIf CodigoPergunta = 145 Then
            MsgBox("A")
        ElseIf CodigoPergunta = 146 Then
            MsgBox("B")
        ElseIf CodigoPergunta = 147 Then
            MsgBox("C")
        ElseIf CodigoPergunta = 148 Then
            MsgBox("D")
        ElseIf CodigoPergunta = 149 Then
            MsgBox("A")
        ElseIf CodigoPergunta = 150 Then
            MsgBox("B")
        ElseIf CodigoPergunta = 151 Then
            MsgBox("C")
        ElseIf CodigoPergunta = 152 Then
            MsgBox("D")
        ElseIf CodigoPergunta = 153 Then
            MsgBox("A")
        ElseIf CodigoPergunta = 154 Then
            MsgBox("B")
        ElseIf CodigoPergunta = 155 Then
            MsgBox("C")
        ElseIf CodigoPergunta = 156 Then
            MsgBox("D")
        ElseIf CodigoPergunta = 157 Then
            MsgBox("A")
        ElseIf CodigoPergunta = 158 Then
            MsgBox("B")
        ElseIf CodigoPergunta = 159 Then
            MsgBox("C")
        ElseIf CodigoPergunta = 160 Then
            MsgBox("D")
        ElseIf CodigoPergunta = 161 Then
            MsgBox("A")
        ElseIf CodigoPergunta = 162 Then
            MsgBox("B")
        ElseIf CodigoPergunta = 163 Then
            MsgBox("C")
        ElseIf CodigoPergunta = 164 Then
            MsgBox("D")
        ElseIf CodigoPergunta = 165 Then
            MsgBox("A")
        ElseIf CodigoPergunta = 166 Then
            MsgBox("B")
        ElseIf CodigoPergunta = 167 Then
            MsgBox("C")
        ElseIf CodigoPergunta = 168 Then
            MsgBox("D")
        ElseIf CodigoPergunta = 169 Then
            MsgBox("A")
        ElseIf CodigoPergunta = 170 Then
            MsgBox("B")
        ElseIf CodigoPergunta = 171 Then
            MsgBox("C")
        ElseIf CodigoPergunta = 172 Then
            MsgBox("D")
        ElseIf CodigoPergunta = 173 Then
            MsgBox("A")
        ElseIf CodigoPergunta = 174 Then
            MsgBox("B")
        ElseIf CodigoPergunta = 175 Then
            MsgBox("C")
        ElseIf CodigoPergunta = 176 Then
            MsgBox("D")
        ElseIf CodigoPergunta = 177 Then
            MsgBox("A")
        ElseIf CodigoPergunta = 178 Then
            MsgBox("B")
        ElseIf CodigoPergunta = 179 Then
            MsgBox("C")
        ElseIf CodigoPergunta = 180 Then
            MsgBox("D")
        ElseIf CodigoPergunta = 181 Then
            MsgBox("A")
        ElseIf CodigoPergunta = 182 Then
            MsgBox("B")
        ElseIf CodigoPergunta = 183 Then
            MsgBox("C")
        ElseIf CodigoPergunta = 184 Then
            MsgBox("D")
        ElseIf CodigoPergunta = 185 Then
            MsgBox("A")
        ElseIf CodigoPergunta = 187 Then
            MsgBox("B")
        ElseIf CodigoPergunta = 188 Then
            MsgBox("C")
        ElseIf CodigoPergunta = 189 Then
            MsgBox("D")
        ElseIf CodigoPergunta = 190 Then
            MsgBox("A")
        ElseIf CodigoPergunta = 191 Then
            MsgBox("B")
        ElseIf CodigoPergunta = 192 Then
            MsgBox("C")
        ElseIf CodigoPergunta = 193 Then
            MsgBox("D")
        ElseIf CodigoPergunta = 194 Then
            MsgBox("A")
        ElseIf CodigoPergunta = 195 Then
            MsgBox("B")
        ElseIf CodigoPergunta = 196 Then
            MsgBox("C")
        ElseIf CodigoPergunta = 197 Then
            MsgBox("D")
        ElseIf CodigoPergunta = 198 Then
            MsgBox("A")
        ElseIf CodigoPergunta = 199 Then
            MsgBox("B")
        ElseIf CodigoPergunta = 200 Then
            MsgBox("C")
        ElseIf CodigoPergunta = 201 Then
            MsgBox("D")
        ElseIf CodigoPergunta = 202 Then
            MsgBox("A")
        ElseIf CodigoPergunta = 203 Then
            MsgBox("B")
        ElseIf CodigoPergunta = 204 Then
            MsgBox("C")
        ElseIf CodigoPergunta = 205 Then
            MsgBox("D")
        ElseIf CodigoPergunta = 206 Then
            MsgBox("A")
        ElseIf CodigoPergunta = 207 Then
            MsgBox("D")
        ElseIf CodigoPergunta = 208 Then
            MsgBox("A")
        ElseIf CodigoPergunta = 209 Then
            MsgBox("B")
        ElseIf CodigoPergunta = 210 Then
            MsgBox("C")
        ElseIf CodigoPergunta = 211 Then
            MsgBox("D")
        ElseIf CodigoPergunta = 212 Then
            MsgBox("A")
        ElseIf CodigoPergunta = 213 Then
            MsgBox("B")
        ElseIf CodigoPergunta = 214 Then
            MsgBox("C")
        ElseIf CodigoPergunta = 215 Then
            MsgBox("D")
        ElseIf CodigoPergunta = 216 Then
            MsgBox("A")
        ElseIf CodigoPergunta = 217 Then
            MsgBox("B")
        ElseIf CodigoPergunta = 218 Then
            MsgBox("C")
        ElseIf CodigoPergunta = 219 Then
            MsgBox("D")
        ElseIf CodigoPergunta = 220 Then
            MsgBox("A")
        End If
    End Sub

    Sub PiscarButao()
        'Piscar Butão'
        pausa(750)
        Button1.BackColor = Color.Red
        Button2.BackColor = Color.Red
        Button3.BackColor = Color.Red
        Button4.BackColor = Color.Red

        pausa(750)
        Button1.BackColor = Color.Green
        Button2.BackColor = Color.Green
        Button3.BackColor = Color.Green
        Button4.BackColor = Color.Green

        pausa(750)
        Button1.BackColor = Color.Red
        Button2.BackColor = Color.Red
        Button3.BackColor = Color.Red
        Button4.BackColor = Color.Red

        pausa(750)
        Button1.BackColor = Color.Green
        Button2.BackColor = Color.Green
        Button3.BackColor = Color.Green
        Button4.BackColor = Color.Green
    End Sub

End Class