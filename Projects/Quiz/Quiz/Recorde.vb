﻿Public Class Recorde

    Dim CodigoPergunta As Byte = 0
    Dim somaPerguntas As Byte = 0
    Dim RespostasCorretas As Byte = 0
    Dim RespostasErradas As Byte = 0
    Dim Ajudas As Byte = 3

    'CodigoPergunta = Codigo Pergunta, Gera número aleatório
    'SomaPergunta = Soma de sequancia de perguntas respondidas

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        'Voltar
        TelaInicial.Show()
        Me.Close()
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        'Começar'
        Button1.Enabled = True
        Button2.Enabled = True
        Button3.Enabled = True
        Button4.Enabled = True
        Button7.Enabled = True
        Button8.Enabled = True
        Perguntas()
    End Sub

    Sub Correto()
        Label9.BackColor = Color.Green
        RespostasCorretas += 1
        Label5.Text = RespostasCorretas
        Label10.BackColor = Color.Green
    End Sub

    Sub Errado()
        Label9.BackColor = Color.Red
        RespostasErradas += 1
        Label6.Text = RespostasErradas
        Label10.BackColor = Color.Red
    End Sub

    Sub Perguntas()
        'Somar Perguntas
        somaPerguntas += 1
        Randomize()
        CodigoPergunta = Int(81 * Rnd() + 1)

        If CodigoPergunta = 1 Then
            Label10.Text = somaPerguntas & " - Em que ano Portugal aderiu a União Europeia?"

            Button1.Text = "A) 599"
            Button2.Text = "B) 1790"
            Button3.Text = "C) 1986" 'Correrto'
            Button4.Text = "D) 2032"

        ElseIf CodigoPergunta = 2 Then
            Label10.Text = somaPerguntas & " - Salazar era professor em que Universidade?"

            Button1.Text = "A) Madride"
            Button2.Text = "B) Londres"
            Button3.Text = "C) Marte"
            Button4.Text = "D) Coimbra" 'Correrto'

        ElseIf CodigoPergunta = 3 Then
            Label10.Text = somaPerguntas & " - Qual era a lei fundamental de Salazar?"

            Button1.Text = "A) Constituição de 1933" 'Correrto'
            Button2.Text = "B) Nada"
            Button3.Text = "C) O tratado de tordesilhas"
            Button4.Text = "D) 10 Mandamento de Deus"

        ElseIf CodigoPergunta = 4 Then
            Label10.Text = somaPerguntas & " - Qual é o cognome de D. Afonso Henriques?"

            Button1.Text = "A) O Gordo"
            Button2.Text = "B) O Conquistador" 'Correrto'
            Button3.Text = "C) O Velho"
            Button4.Text = "D) O Povo"

        ElseIf CodigoPergunta = 5 Then
            Label10.Text = somaPerguntas & " - Que rei português, segunda a lenda, voltará numa manhã de nevoeiro?"

            Button1.Text = "A) D. Sebastião" 'Correrto'
            Button2.Text = "B) Zé Povinho"
            Button3.Text = "C) Jesus"
            Button4.Text = "D) Luís Váz de Camões"

        ElseIf CodigoPergunta = 6 Then
            Label10.Text = somaPerguntas & " - Em que ano foi descoberto o caminho marítimo para a Índia?"

            Button1.Text = "A) Ainda não foi"
            Button2.Text = "B) 1498" 'Correrto'
            Button3.Text = "C) Este ano"
            Button4.Text = "D) 2012"

        ElseIf CodigoPergunta = 7 Then
            Label10.Text = somaPerguntas & " - Qual é a cidade berço de Portugal?"

            Button1.Text = "A) Vila Real"
            Button2.Text = "B) Minho"
            Button3.Text = "C) Guimarães" 'Correrto'
            Button4.Text = "D) Alentejo"

        ElseIf CodigoPergunta = 8 Then
            Label10.Text = somaPerguntas & " - Quando foi implementada a Primeira Republica?"

            Button1.Text = "A) Potugal é uma Ditadura"
            Button2.Text = "B) 1909"
            Button3.Text = "C) 1911"
            Button4.Text = "D) 1910" 'Correrto'

        ElseIf CodigoPergunta = 9 Then
            Label10.Text = somaPerguntas & " - Quem foi responsável pelo Fontismo?"

            Button1.Text = "A) FONTES PEREIRA DE MELO" 'Correrto'
            Button2.Text = "B) Luis Vás de Camões"
            Button3.Text = "C) Hitler"
            Button4.Text = "D) Vasco Da Gama"

        ElseIf CodigoPergunta = 10 Then
            Label10.Text = somaPerguntas & " - Em que ano se deu o terramoto que destruiu maior parte da capital?"

            Button1.Text = "A) 2012"
            Button2.Text = "B) 1755" 'Correrto'
            Button3.Text = "C) 1900"
            Button4.Text = "D) 1700"

        ElseIf CodigoPergunta = 11 Then
            Label10.Text = somaPerguntas & " - O século 19 em Portugal ficou conhecido pelo século da...?"

            Button1.Text = "A) Morte"
            Button2.Text = "B) Luxo"
            Button3.Text = "C) Burguesia" 'Correrto'
            Button4.Text = "D) Não é conhecido"

        ElseIf CodigoPergunta = 12 Then
            Label10.Text = somaPerguntas & " - Em que ano ocorreu a transferência de poderes em Macau de Portugal para a China?"

            Button1.Text = "A) 1000"
            Button2.Text = "B) 1500"
            Button3.Text = "C) 1755"
            Button4.Text = "D) 1999" 'Correrto'

        ElseIf CodigoPergunta = 13 Then
            Label10.Text = somaPerguntas & " - Como ficou conhecida a Revolução de 1974 em Portugal?"

            Button1.Text = "A) Revolução dos Cravos" 'Correrto'
            Button2.Text = "B) Revolução dos Corvos"
            Button3.Text = "C) Revolução dos Mendigos"
            Button4.Text = "D) Revolução dos bons Samaritanos"

        ElseIf CodigoPergunta = 14 Then
            Label10.Text = somaPerguntas & " - Qual o nome dado ao regime regente em Portugal até 1974?"

            Button1.Text = "A) Estado Velho"
            Button2.Text = "B) Estado Novo" 'Correrto'
            Button3.Text = "C) Estado Antigo"
            Button4.Text = "D) Falta de Estado"

        ElseIf CodigoPergunta = 15 Then
            Label10.Text = somaPerguntas & " - Quem foi o Primeiro Ministro Português após a queda do Regime Salazarista?"

            Button1.Text = "A) Hitler"
            Button2.Text = "B) Mário Fortado"
            Button3.Text = "C) Mário Soares" 'Correrto'
            Button4.Text = "D) Mário Fonteno"

        ElseIf CodigoPergunta = 16 Then
            Label10.Text = somaPerguntas & " - Em que ano se socedeu a revolução dos cravos de 1974?"

            Button1.Text = "A) 1974" 'Correrto'
            Button2.Text = "B) 1974" 'Correrto'
            Button3.Text = "C) 1974" 'Correrto'
            Button4.Text = "D) 1974" 'Correrto'

        ElseIf CodigoPergunta = 17 Then
            Label10.Text = somaPerguntas & " - Quem foi Viriato?"

            Button1.Text = "A) Líderes da tribo Lusitana" 'Correrto'
            Button2.Text = "B) Um Famoso de Hollywood"
            Button3.Text = "C) Um Rei Português"
            Button4.Text = "D) Militar de Abril"

        ElseIf CodigoPergunta = 18 Then
            Label10.Text = somaPerguntas & " - Quando morreu Viriato?"

            Button1.Text = "A) 2015"
            Button2.Text = "B) 139 A. C." 'Correrto'
            Button3.Text = "C) 1 D. C."
            Button4.Text = "D) 500 A. C."

        ElseIf CodigoPergunta = 19 Then
            Label10.Text = somaPerguntas & " - Como morreu Viriato?"

            Button1.Text = "A) A nadar"
            Button2.Text = "B) Arma de Fogo"
            Button3.Text = "C) Traido Enquanto Dormia" 'Correrto'
            Button4.Text = "D) Não Morreu"

        ElseIf CodigoPergunta = 20 Then
            Label10.Text = somaPerguntas & " - Quando Nasceu Viriato?"

            Button1.Text = "A) 2015"
            Button2.Text = "B) 139 A. C."
            Button3.Text = "C) 500 A. C."
            Button4.Text = "D) Não se Sabe" 'Correrto'

        ElseIf CodigoPergunta = 21 Then
            Label10.Text = somaPerguntas & " - Quem é o responsável pelo Estado Novo?"

            Button1.Text = "A) António Oliveira Salazar" 'Correrto'
            Button2.Text = "B) Hittler"
            Button3.Text = "C) As Tartarugas Ninja"
            Button4.Text = "D) O Jerónimo Stilton"

        ElseIf CodigoPergunta = 22 Then
            Label10.Text = somaPerguntas & " - Em que ano se deu a Batalha de Aljubarrota?"

            Button1.Text = "A) 13 de agosto de 1385"
            Button2.Text = "B) 14 de Agosto de 1385" 'Correrto'
            Button3.Text = "C) 15 de agosto de 1385"
            Button4.Text = "D) 16 de agosto de 1385"

        ElseIf CodigoPergunta = 23 Then
            Label10.Text = somaPerguntas & " - Quem dobrou o cabo das Tormentas em 1488?"

            Button1.Text = "A) José Socrates"
            Button2.Text = "B) Bartolomeu Noites"
            Button3.Text = "C) Bartolomeu Dias" 'Correrto'
            Button4.Text = "D) Mussolini"

        ElseIf CodigoPergunta = 24 Then
            Label10.Text = somaPerguntas & " - Qual o Mosteiro que foi feito para celebrar a Vitória dos Portugueses sobre os Castelhanos em Aljubarrota?"

            Button1.Text = "A) Torre dos Clérigos"
            Button2.Text = "B) Torre Pisa"
            Button3.Text = "C) Torre Eiffel"
            Button4.Text = "D) Mosteiro da Batalha" 'Correrto'

        ElseIf CodigoPergunta = 25 Then
            Label10.Text = somaPerguntas & " - Quem foi o grande impulsionador dos descobrimentos portugueses?"

            Button1.Text = "A) Infante D. Henrique" 'Correrto'
            Button2.Text = "B) São Pedro"
            Button3.Text = "C) São João"
            Button4.Text = "D) Santo António"

        ElseIf CodigoPergunta = 26 Then
            Label10.Text = somaPerguntas & " - Qual dos navegadores seguintes não esteve ao serviço do Rei de Portugal?"

            Button1.Text = "A) Vasco da Gama"
            Button2.Text = "B) Cristóvao Colombo" 'Correrto'
            Button3.Text = "C) Bartolomeu Dias"
            Button4.Text = "D) Pedro Alvares Cabral"

        ElseIf CodigoPergunta = 27 Then
            Label10.Text = somaPerguntas & " - Quais produtos os portugueses traziam de Africa e Ásia?"

            Button1.Text = "A) Cigarros"
            Button2.Text = "B) Escravos"
            Button3.Text = "C) Ouro, especiarias e pedras preciosas" 'Correrto'
            Button4.Text = "D) Naves Espaciais"

        ElseIf CodigoPergunta = 28 Then
            Label10.Text = somaPerguntas & " - Como se chamava a policia politica no tempo de Salazar?"

            Button1.Text = "A) Capitaes de Abril"
            Button2.Text = "B) Militares"
            Button3.Text = "C) Policia"
            Button4.Text = "D) PIDE" 'Correrto'

        ElseIf CodigoPergunta = 29 Then
            Label10.Text = somaPerguntas & " - Quantos governos provisórios existiram após o 25 de abril?"

            Button1.Text = "A) 6" 'Correrto'
            Button2.Text = "B) 3"
            Button3.Text = "C) 2"
            Button4.Text = "D) 2O"

        ElseIf CodigoPergunta = 30 Then
            Label10.Text = somaPerguntas & " - Como se chama o período em que Portugal esteve perto de uma guerra civil em 1975?"

            Button1.Text = "A) Guerra Civil Do Calor"
            Button2.Text = "B) Verão Quente" 'Correrto'
            Button3.Text = "C) Quente"
            Button4.Text = "D) Guerra Fria"

        ElseIf CodigoPergunta = 31 Then
            Label10.Text = somaPerguntas & " - Que nome era dado á organização Portuguesa que integrava todos os jovens obrigatoriamente?"

            Button1.Text = "A) Mocidade Japonês"
            Button2.Text = "B) Mocidade Inglesa"
            Button3.Text = "C) Mocidade Portuguesa" 'Correrto'
            Button4.Text = "D) Mocidade Françesa"

        ElseIf CodigoPergunta = 32 Then
            Label10.Text = somaPerguntas & " - Qual a capital de Portugal?"

            Button1.Text = "A) Guimaraes"
            Button2.Text = "B) Coimbra"
            Button3.Text = "C) Porto"
            Button4.Text = "D) Lisboa" 'Correrto'

        ElseIf CodigoPergunta = 33 Then
            Label10.Text = somaPerguntas & " - Quais são as duas regiões autónomas do pais?"

            Button1.Text = "A) Açores e Madeira" 'Correrto'
            Button2.Text = "B) Açores e Algarve"
            Button3.Text = "C) Algarve e Madeira"
            Button4.Text = "D) Lisboa e Porto"

        ElseIf CodigoPergunta = 34 Then
            Label10.Text = somaPerguntas & " - Qual a data da conquista de Ceuta?"

            Button1.Text = "A) 1300"
            Button2.Text = "B) 1415" 'Correrto'
            Button3.Text = "C) 1200"
            Button4.Text = "D) 1500"

        ElseIf CodigoPergunta = 35 Then
            Label10.Text = somaPerguntas & " - Quantas Dinastias teve Portugal?"

            Button1.Text = "A) 2"
            Button2.Text = "B) 3"
            Button3.Text = "C) 4" 'Correrto'
            Button4.Text = "D) 2O"

        ElseIf CodigoPergunta = 36 Then
            Label10.Text = somaPerguntas & " - O nazismo ocorreu durante que período?"

            Button1.Text = "A) 1900 a 1945"
            Button2.Text = "B) 1944 a 1945"
            Button3.Text = "C) 1939 a 1945"
            Button4.Text = "D) 1933 a 1945" 'Correrto'

        ElseIf CodigoPergunta = 37 Then
            Label10.Text = somaPerguntas & " - Em que pais foi aplicado o Nazismo?"

            Button1.Text = "A) Alemanha" 'Correrto'
            Button2.Text = "B) Russia"
            Button3.Text = "C) Inglaterra"
            Button4.Text = "D) Londres"

        ElseIf CodigoPergunta = 38 Then
            Label10.Text = somaPerguntas & " - Quem liderou o nazismo?"

            Button1.Text = "A) Mussolini"
            Button2.Text = "B) Adolf Hitler" 'Correrto'
            Button3.Text = "C) Salazar"
            Button4.Text = "D) Castelo Branco"

        ElseIf CodigoPergunta = 39 Then
            Label10.Text = somaPerguntas & " - Em que ano nasceu Hitler?"

            Button1.Text = "A) 1901"
            Button2.Text = "B) 1900"
            Button3.Text = "C) 1889" 'Correrto'
            Button4.Text = "D) 1939"

        ElseIf CodigoPergunta = 40 Then
            Label10.Text = somaPerguntas & " - Qual o nome do símbolo nazista?"

            Button1.Text = "A) Dominar o Mundo"
            Button2.Text = "B) Bigode"
            Button3.Text = "C) Judeu"
            Button4.Text = "D) Suástica" 'Correrto'

        ElseIf CodigoPergunta = 41 Then
            Label10.Text = somaPerguntas & " - O que significa ""Fuhrer""?"

            Button1.Text = "A) Líder" 'Correrto'
            Button2.Text = "B) Hittler"
            Button3.Text = "C) Zé Povilho"
            Button4.Text = "D) Bicicleta"

        ElseIf CodigoPergunta = 42 Then
            Label10.Text = somaPerguntas & " - Durante o Nazismo onde é que os opositores eram exterminados?"

            Button1.Text = "A) Na lua"
            Button2.Text = "B) Campos de concentração" 'Correrto'
            Button3.Text = "C) No chão"
            Button4.Text = "D) No tribunal"

        ElseIf CodigoPergunta = 43 Then
            Label10.Text = somaPerguntas & " - Em que ano é que Hitler se suicidou?"

            Button1.Text = "A) 2000"
            Button2.Text = "B) 1993"
            Button3.Text = "C) 1945" 'Correrto'
            Button4.Text = "D) 1939"

        ElseIf CodigoPergunta = 44 Then
            Label10.Text = somaPerguntas & " - Que pais derrotou a Alemanha na Segunda Guerra Mundial?"

            Button1.Text = "A) Alemanha"
            Button2.Text = "B) China"
            Button3.Text = "C) Portugal"
            Button4.Text = "D) União Soviética" 'Correrto'

        ElseIf CodigoPergunta = 45 Then
            Label10.Text = somaPerguntas & " - O que significa ""Pax Romana""?"

            Button1.Text = "A) Paz Romana" 'Correrto'
            Button2.Text = "B) Morte a Roma"
            Button3.Text = "C) Roma no Poder"
            Button4.Text = "D) Roma em Guerra"

        ElseIf CodigoPergunta = 46 Then
            Label10.Text = somaPerguntas & " - Os romanos eram considerados grandes engenheiros pois construíam..."

            Button1.Text = "A) Terrem feito grandes conquistas"
            Button2.Text = "B) Aquedutos, Pontes e Termas" 'Correrto'
            Button3.Text = "C) Dominaram o mundo"
            Button4.Text = "D) Criaram o primeiro telemóvel"

        ElseIf CodigoPergunta = 47 Then
            Label10.Text = somaPerguntas & " - Em que ano foi assinado o Edito de Caracala?"

            Button1.Text = "A) 40 d.c."
            Button2.Text = "B) 3 d.c."
            Button3.Text = "C) 212 a.c" 'Correrto'
            Button4.Text = "D) 2O d.c."

        ElseIf CodigoPergunta = 48 Then
            Label10.Text = somaPerguntas & " - O Edito de Caracala consistia em que?"

            Button1.Text = "A) Derrota aos Romanos"
            Button2.Text = "B) Vitória aos Romanos"
            Button3.Text = "C) Morte aos Romanos"
            Button4.Text = "D) Cidadania aos Romanos" 'Correrto'

        ElseIf CodigoPergunta = 49 Then
            Label10.Text = somaPerguntas & " - A arte romana era inspirada em que arte?"

            Button1.Text = "A) Helenísticas, Etruscas e Gregas" 'Correrto'
            Button2.Text = "B) Nos Romanos"
            Button3.Text = "C) Nos Deuses"
            Button4.Text = "D) Nos Portugueses"

        ElseIf CodigoPergunta = 50 Then
            Label10.Text = somaPerguntas & " - Quais as duas características do comercio romano?"

            Button1.Text = "A) Monopolista"
            Button2.Text = "B) Urbanístico e esclavagista" 'Correrto'
            Button3.Text = "C) Urbanístico"
            Button4.Text = "D) Sempre em saldos"

        ElseIf CodigoPergunta = 51 Then
            Label10.Text = somaPerguntas & " - Quantos reis teve a primeira dinastia?"

            Button1.Text = "A) 1"
            Button2.Text = "B) 3"
            Button3.Text = "C) 9" 'Correrto'
            Button4.Text = "D) 2O"

        ElseIf CodigoPergunta = 52 Then
            Label10.Text = somaPerguntas & " - Quantos reis teve a segunda dinastia?"

            Button1.Text = "A) 1"
            Button2.Text = "B) 3"
            Button3.Text = "C) 2"
            Button4.Text = "D) 8" 'Correrto'

        ElseIf CodigoPergunta = 53 Then
            Label10.Text = somaPerguntas & " - Quantos reis teve a terceira  dinastia?"

            Button1.Text = "A) 3" 'Correrto'
            Button2.Text = "B) 30"
            Button3.Text = "C) 2"
            Button4.Text = "D) 2O"

        ElseIf CodigoPergunta = 54 Then
            Label10.Text = somaPerguntas & " - Quantos reis teve a quarta dinastia?"

            Button1.Text = "A) 20"
            Button2.Text = "B) 14" 'Correrto'
            Button3.Text = "C) 8"
            Button4.Text = "D) 6"

        ElseIf CodigoPergunta = 55 Then
            Label10.Text = somaPerguntas & " - Quantos reis teve Portugal?"

            Button1.Text = "A) 36"
            Button2.Text = "B) 35"
            Button3.Text = "C) 34" 'Correrto'
            Button4.Text = "D) 33"

        ElseIf CodigoPergunta = 56 Then
            Label10.Text = somaPerguntas & " - Quem foi o ultimo reis de Portugal?"

            Button1.Text = "A) D. Afonso Henriques"
            Button2.Text = "B) D. João II"
            Button3.Text = "C) D. Manuel I"
            Button4.Text = "D) D. Manuel II" 'Correrto'

        ElseIf CodigoPergunta = 57 Then
            Label10.Text = somaPerguntas & " - Quem foi o primeiro rei da quarta dinastia?"

            Button1.Text = "A) D. João IV" 'Correrto'
            Button2.Text = "B) D. João XX"
            Button3.Text = "C) D. João XXI"
            Button4.Text = "D) D. Afonso Henriques"

        ElseIf CodigoPergunta = 58 Then
            Label10.Text = somaPerguntas & " - Quem foi o primeiro rei da terceira dinastia?"

            Button1.Text = "A) Pai Natal"
            Button2.Text = "B) D. Felipe I" 'Correrto'
            Button3.Text = "C) D. Felipe III"
            Button4.Text = "D) D. Afonso Henriques"

        ElseIf CodigoPergunta = 59 Then
            Label10.Text = somaPerguntas & " - Quem foi o primeiro rei da segunda dinastia?"

            Button1.Text = "A) D. Afonso Henriques"
            Button2.Text = "B) D. João III"
            Button3.Text = "C) D. João I" 'Correrto'
            Button4.Text = "D) D. Afonso Henriques II"

        ElseIf CodigoPergunta = 60 Then
            Label10.Text = somaPerguntas & " - Quem foi o Primeiro Rei de Portugal?"

            Button1.Text = "A) D. Fernado "
            Button2.Text = "B) D. João III"
            Button3.Text = "C) D. Afonso Henriques II"
            Button4.Text = "D) D. Afonso Henriques" 'Correrto'

        ElseIf CodigoPergunta = 61 Then
            Label10.Text = somaPerguntas & " - A primeira dinastia ficou conhecida como?"

            Button1.Text = "A) Dinastia de Borgonha" 'Correrto'
            Button2.Text = "B) Dinastia de Avis"
            Button3.Text = "C) Dinastia Filipina"
            Button4.Text = "D) Dinastia de Bragança"

        ElseIf CodigoPergunta = 62 Then
            Label10.Text = somaPerguntas & " - A segunda dinastia ficou conhecida como?"

            Button1.Text = "A) Dinastia de Borgonha"
            Button2.Text = "B) Dinastia de Avis" 'Correrto'
            Button3.Text = "C) Dinastia Filipina"
            Button4.Text = "D) Dinastia de Bragança"

        ElseIf CodigoPergunta = 63 Then
            Label10.Text = somaPerguntas & " - A terceira dinastia ficou conhecida como?"

            Button1.Text = "A) Dinastia de Borgonha"
            Button2.Text = "B) Dinastia de Avis"
            Button3.Text = "C) Dinastia Filipina" 'Correrto'
            Button4.Text = "D) Dinastia de Bragança"

        ElseIf CodigoPergunta = 64 Then
            Label10.Text = somaPerguntas & " - A quarta dinastia ficou conhecida como?"

            Button1.Text = "A) Dinastia de Borgonha"
            Button2.Text = "B) Dinastia de Avis"
            Button3.Text = "C) Dinastia Filipina"
            Button4.Text = "D) Dinastia de Bragança" 'Correrto'

        ElseIf CodigoPergunta = 65 Then
            Label10.Text = somaPerguntas & " - Quem foi o pai de D. Afonso Henriques?"

            Button1.Text = "A) D. Henrique de Borgonha" 'Correrto'
            Button2.Text = "B) Pai Natal"
            Button3.Text = "C) Jesus"
            Button4.Text = "D) Barba Negra"

        ElseIf CodigoPergunta = 66 Then
            Label10.Text = somaPerguntas & " - Como se chama a batalha entre D. Afonso Henriques e D. Teresa?"

            Button1.Text = "A) Batalha da Morte"
            Button2.Text = "B) Batalha de S. Mamede" 'Correrto'
            Button3.Text = "C) Batalha de mãe contra filho"
            Button4.Text = "D) Batalha sem destino"

        ElseIf CodigoPergunta = 67 Then
            Label10.Text = somaPerguntas & " - Quem foi o filho de D. Afonso Henriques?"

            Button1.Text = "A) D. Sancho III"
            Button2.Text = "B) D. Sancho II"
            Button3.Text = "C) D. Sancho" 'Correrto'
            Button4.Text = "D) D. Afonso II"

        ElseIf CodigoPergunta = 68 Then
            Label10.Text = somaPerguntas & " - Em que ano foi assinado o Tratado de Zamora?"

            Button1.Text = "A) 2000"
            Button2.Text = "B) 1990"
            Button3.Text = "C) 1111"
            Button4.Text = "D) 1143" 'Correrto'

        ElseIf CodigoPergunta = 69 Then
            Label10.Text = somaPerguntas & " - Que rei Portugues tem o cognome de ""O Desejado""?"

            Button1.Text = "A) D. Sebastião" 'Correrto'
            Button2.Text = "B) D. João II"
            Button3.Text = "C) D. Manuel"
            Button4.Text = "D) D. Infante Henrrique"

        ElseIf CodigoPergunta = 70 Then
            Label10.Text = somaPerguntas & " - Em que ano D. Afonso Henriques morreu?"

            Button1.Text = "A) 1111"
            Button2.Text = "B) 1185" 'Correrto'
            Button3.Text = "C) 2012"
            Button4.Text = "D) 1990"

        ElseIf CodigoPergunta = 71 Then
            Label10.Text = somaPerguntas & " - Quais países representavam a RDA e RFA?"

            Button1.Text = "A) EUA/Alemanha"
            Button2.Text = "B) Alemanha/URRSS"
            Button3.Text = "C) URSS/EUA" 'Correrto'
            Button4.Text = "D) <----"

        ElseIf CodigoPergunta = 72 Then
            Label10.Text = somaPerguntas & " - A NATO é uma organização..."

            Button1.Text = "A) Comunista"
            Button2.Text = "B) Economica"
            Button3.Text = "C) De preservação do Ambiente"
            Button4.Text = "D) Capitalista " 'Correrto'

        ElseIf CodigoPergunta = 73 Then
            Label10.Text = somaPerguntas & " - Qual é o principal representante da NATO?"

            Button1.Text = "A) EUA" 'Correrto'
            Button2.Text = "B) Portugal"
            Button3.Text = "C) Russia"
            Button4.Text = "D) Japão"

        ElseIf CodigoPergunta = 74 Then
            Label10.Text = somaPerguntas & " - O que foi a COMECON?"

            Button1.Text = "A) Camisola"
            Button2.Text = "B) Evenco de Cultura POP" 'Correrto'
            Button3.Text = "C) Ajuda económica da URSS"
            Button4.Text = "D) Uma Economia Estável"

        ElseIf CodigoPergunta = 75 Then
            Label10.Text = somaPerguntas & " - A Guerra Fria aconteceu durante quais anos?"

            Button1.Text = "A) 1933-1945"
            Button2.Text = "B) 1939-1945"
            Button3.Text = "C) 1947-1991" 'Correrto'
            Button4.Text = "D) 1939-2020"

        ElseIf CodigoPergunta = 76 Then
            Label10.Text = somaPerguntas & " - A Doutrina Truman foi criada por qual pais e em que ano?"

            Button1.Text = "A) Pai-Natalolandia em 1947"
            Button2.Text = "B) Bruxolandia em 2022"
            Button3.Text = "C) Zombieland em 1999"
            Button4.Text = "D) EUA em 1947" 'Correrto'

        ElseIf CodigoPergunta = 77 Then
            Label10.Text = somaPerguntas & " - O que foi a Guerra Fria?"

            Button1.Text = "A) Conflito não armado" 'Correrto'
            Button2.Text = "B) Uma Guerra no Gelo"
            Button3.Text = "C) Uma guerra com bolas de neve"
            Button4.Text = "D) Uma Guerra na Antartida"

        ElseIf CodigoPergunta = 78 Then
            Label10.Text = somaPerguntas & " - Qual era o pais que dominava o bloco capitalista?"

            Button1.Text = "A) França"
            Button2.Text = "B) EUA" 'Correrto'
            Button3.Text = "C) Portugal"
            Button4.Text = "D) Grecia"

        ElseIf CodigoPergunta = 79 Then
            Label10.Text = somaPerguntas & " - Qual era o pais que dominava o bloco comunista?"

            Button1.Text = "A) URZZ"
            Button2.Text = "B) UR2S"
            Button3.Text = "C) URSS" 'Correrto'
            Button4.Text = "D) ORSS"

        ElseIf CodigoPergunta = 80 Then
            Label10.Text = somaPerguntas & " - Qual foi o desfecho da Guerra Fria?"

            Button1.Text = "A) Uma nova ordem Mundial"
            Button2.Text = "B) Criação de um novo desporto"
            Button3.Text = "C) Internet Livre para todos"
            Button4.Text = "D) Derrota da Alemanha" 'Correrto'

        ElseIf CodigoPergunta = 81 Then
            Label10.Text = somaPerguntas & " - Em que ao foi construído o muro de Berlim?"

            Button1.Text = "A) 1961" 'Correrto'
            Button2.Text = "B) 1993"
            Button3.Text = "C) 1914"
            Button4.Text = "D) 1918"

            '-----------------------------------------------------------------------------------------------------------'

        ElseIf CodigoPergunta = 82 Then
            Label10.Text = somaPerguntas & " - Qual o símbolo quimico da agua?"

            Button1.Text = "A) F "
            Button2.Text = "B) O3" 'Correrto'
            Button3.Text = "C) CO2"
            Button4.Text = "D) H2O"

        ElseIf CodigoPergunta = 83 Then
            Label10.Text = somaPerguntas & " - Qual o símbolo quimico da agua?"

            Button1.Text = "A) F "
            Button2.Text = "B) O3"
            Button3.Text = "C) CO2" 'Correrto'
            Button4.Text = "D) H2O"

        ElseIf CodigoPergunta = 84 Then
            Label10.Text = somaPerguntas & " - Qual o símbolo quimico da agua?"

            Button1.Text = "A) F "
            Button2.Text = "B) O3"
            Button3.Text = "C) CO2"
            Button4.Text = "D) H2O" 'Correrto'

        ElseIf CodigoPergunta = 85 Then
            Label10.Text = somaPerguntas & " - Qual o símbolo quimico da agua?"

            Button1.Text = "A) F " 'Correrto'
            Button2.Text = "B) O3"
            Button3.Text = "C) CO2"
            Button4.Text = "D) H2O"

        ElseIf CodigoPergunta = 86 Then
            Label10.Text = somaPerguntas & " - Qual o símbolo quimico da agua?"

            Button1.Text = "A) F "
            Button2.Text = "B) O3" 'Correrto'
            Button3.Text = "C) CO2"
            Button4.Text = "D) H2O"

        ElseIf CodigoPergunta = 87 Then
            Label10.Text = somaPerguntas & " - Qual o símbolo quimico da agua?"

            Button1.Text = "A) F "
            Button2.Text = "B) O3"
            Button3.Text = "C) CO2" 'Correrto'
            Button4.Text = "D) H2O"

        ElseIf CodigoPergunta = 88 Then
            Label10.Text = somaPerguntas & " - Qual o símbolo quimico da agua?"

            Button1.Text = "A) F "
            Button2.Text = "B) O3"
            Button3.Text = "C) CO2"
            Button4.Text = "D) H2O" 'Correrto'

        ElseIf CodigoPergunta = 89 Then
            Label10.Text = somaPerguntas & " - Qual o símbolo quimico da agua?"

            Button1.Text = "A) F " 'Correrto'
            Button2.Text = "B) O3"
            Button3.Text = "C) CO2"
            Button4.Text = "D) H2O"

        ElseIf CodigoPergunta = 90 Then
            Label10.Text = somaPerguntas & " - Qual o símbolo quimico da agua?"

            Button1.Text = "A) F "
            Button2.Text = "B) O3" 'Correrto'
            Button3.Text = "C) CO2"
            Button4.Text = "D) H2O"

        ElseIf CodigoPergunta = 91 Then
            Label10.Text = somaPerguntas & " - Qual o símbolo quimico da agua?"

            Button1.Text = "A) F "
            Button2.Text = "B) O3"
            Button3.Text = "C) CO2" 'Correrto'
            Button4.Text = "D) H2O"

        ElseIf CodigoPergunta = 92 Then
            Label10.Text = somaPerguntas & " - Qual o símbolo quimico da agua?"

            Button1.Text = "A) F "
            Button2.Text = "B) O3"
            Button3.Text = "C) CO2"
            Button4.Text = "D) H2O" 'Correrto'

        ElseIf CodigoPergunta = 93 Then
            Label10.Text = somaPerguntas & " - Qual o símbolo quimico da agua?"

            Button1.Text = "A) F " 'Correrto'
            Button2.Text = "B) O3"
            Button3.Text = "C) CO2"
            Button4.Text = "D) H2O"

        ElseIf CodigoPergunta = 94 Then
            Label10.Text = somaPerguntas & " - Qual o símbolo quimico da agua?"

            Button1.Text = "A) F "
            Button2.Text = "B) O3" 'Correrto'
            Button3.Text = "C) CO2"
            Button4.Text = "D) H2O"

        ElseIf CodigoPergunta = 95 Then
            Label10.Text = somaPerguntas & " - Qual o símbolo quimico da agua?"

            Button1.Text = "A) F "
            Button2.Text = "B) O3"
            Button3.Text = "C) CO2" 'Correrto'
            Button4.Text = "D) H2O"

        ElseIf CodigoPergunta = 96 Then
            Label10.Text = somaPerguntas & " - Qual o símbolo quimico da agua?"

            Button1.Text = "A) F "
            Button2.Text = "B) O3"
            Button3.Text = "C) CO2"
            Button4.Text = "D) H2O" 'Correrto'

        ElseIf CodigoPergunta = 97 Then
            Label10.Text = somaPerguntas & " - Qual o símbolo quimico da agua?"

            Button1.Text = "A) F " 'Correrto'
            Button2.Text = "B) O3"
            Button3.Text = "C) CO2"
            Button4.Text = "D) H2O"

        ElseIf CodigoPergunta = 98 Then
            Label10.Text = somaPerguntas & " - Qual o símbolo quimico da agua?"

            Button1.Text = "A) F "
            Button2.Text = "B) O3" 'Correrto'
            Button3.Text = "C) CO2"
            Button4.Text = "D) H2O"

        ElseIf CodigoPergunta = 99 Then
            Label10.Text = somaPerguntas & " - Qual o símbolo quimico da agua?"

            Button1.Text = "A) F "
            Button2.Text = "B) O3"
            Button3.Text = "C) CO2" 'Correrto'
            Button4.Text = "D) H2O"

        ElseIf CodigoPergunta = 100 Then
            Label10.Text = somaPerguntas & " - Qual o símbolo quimico da agua?"

            Button1.Text = "A) F "
            Button2.Text = "B) O3"
            Button3.Text = "C) CO2"
            Button4.Text = "D) H2O" 'Correrto'

        ElseIf CodigoPergunta = 101 Then
            Label10.Text = somaPerguntas & " - Qual o símbolo quimico da agua?"

            Button1.Text = "A) F " 'Correrto'
            Button2.Text = "B) O3"
            Button3.Text = "C) CO2"
            Button4.Text = "D) H2O"

        ElseIf CodigoPergunta = 102 Then
            Label10.Text = somaPerguntas & " - Qual o símbolo quimico da agua?"

            Button1.Text = "A) F "
            Button2.Text = "B) O3" 'Correrto'
            Button3.Text = "C) CO2"
            Button4.Text = "D) H2O"

        ElseIf CodigoPergunta = 103 Then
            Label10.Text = somaPerguntas & " - Qual o símbolo quimico da agua?"

            Button1.Text = "A) F "
            Button2.Text = "B) O3"
            Button3.Text = "C) CO2" 'Correrto'
            Button4.Text = "D) H2O"

        ElseIf CodigoPergunta = 104 Then
            Label10.Text = somaPerguntas & " - Qual o símbolo quimico da agua?"

            Button1.Text = "A) F "
            Button2.Text = "B) O3"
            Button3.Text = "C) CO2"
            Button4.Text = "D) H2O" 'Correrto'

        ElseIf CodigoPergunta = 105 Then
            Label10.Text = somaPerguntas & " - Qual o símbolo quimico da agua?"

            Button1.Text = "A) F " 'Correrto'
            Button2.Text = "B) O3"
            Button3.Text = "C) CO2"
            Button4.Text = "D) H2O"

        ElseIf CodigoPergunta = 106 Then
            Label10.Text = somaPerguntas & " - Qual o símbolo quimico da agua?"

            Button1.Text = "A) F "
            Button2.Text = "B) O3" 'Correrto'
            Button3.Text = "C) CO2"
            Button4.Text = "D) H2O"

        ElseIf CodigoPergunta = 107 Then
            Label10.Text = somaPerguntas & " - Qual o símbolo quimico da agua?"

            Button1.Text = "A) F "
            Button2.Text = "B) O3"
            Button3.Text = "C) CO2" 'Correrto'
            Button4.Text = "D) H2O"

        ElseIf CodigoPergunta = 108 Then
            Label10.Text = somaPerguntas & " - Qual o símbolo quimico da agua?"

            Button1.Text = "A) F "
            Button2.Text = "B) O3"
            Button3.Text = "C) CO2"
            Button4.Text = "D) H2O" 'Correrto'

        ElseIf CodigoPergunta = 109 Then
            Label10.Text = somaPerguntas & " - Qual o símbolo quimico da agua?"

            Button1.Text = "A) F " 'Correrto'
            Button2.Text = "B) O3"
            Button3.Text = "C) CO2"
            Button4.Text = "D) H2O"

        ElseIf CodigoPergunta = 110 Then
            Label10.Text = somaPerguntas & " - Qual o símbolo quimico da agua?"

            Button1.Text = "A) F "
            Button2.Text = "B) O3" 'Correrto'
            Button3.Text = "C) CO2"
            Button4.Text = "D) H2O"

        ElseIf CodigoPergunta = 111 Then
            Label10.Text = somaPerguntas & " - Qual o símbolo quimico da agua?"

            Button1.Text = "A) F "
            Button2.Text = "B) O3"
            Button3.Text = "C) CO2" 'Correrto'
            Button4.Text = "D) H2O"

        ElseIf CodigoPergunta = 112 Then
            Label10.Text = somaPerguntas & " - Qual o símbolo quimico da agua?"

            Button1.Text = "A) F "
            Button2.Text = "B) O3"
            Button3.Text = "C) CO2"
            Button4.Text = "D) H2O" 'Correrto'

        ElseIf CodigoPergunta = 113 Then
            Label10.Text = somaPerguntas & " - Qual o símbolo quimico da agua?"

            Button1.Text = "A) F " 'Correrto'
            Button2.Text = "B) O3"
            Button3.Text = "C) CO2"
            Button4.Text = "D) H2O"

        ElseIf CodigoPergunta = 114 Then
            Label10.Text = somaPerguntas & " - Qual o símbolo quimico da agua?"

            Button1.Text = "A) F "
            Button2.Text = "B) O3" 'Correrto'
            Button3.Text = "C) CO2"
            Button4.Text = "D) H2O"

        ElseIf CodigoPergunta = 115 Then
            Label10.Text = somaPerguntas & " - Qual o símbolo quimico da agua?"

            Button1.Text = "A) F "
            Button2.Text = "B) O3"
            Button3.Text = "C) CO2" 'Correrto'
            Button4.Text = "D) H2O"

        ElseIf CodigoPergunta = 116 Then
            Label10.Text = somaPerguntas & " - Qual o símbolo quimico da agua?"

            Button1.Text = "A) F "
            Button2.Text = "B) O3"
            Button3.Text = "C) CO2"
            Button4.Text = "D) H2O" 'Correrto'

        ElseIf CodigoPergunta = 117 Then
            Label10.Text = somaPerguntas & " - Qual o símbolo quimico da agua?"

            Button1.Text = "A) F " 'Correrto'
            Button2.Text = "B) O3"
            Button3.Text = "C) CO2"
            Button4.Text = "D) H2O"

        ElseIf CodigoPergunta = 118 Then
            Label10.Text = somaPerguntas & " - Qual o símbolo quimico da agua?"

            Button1.Text = "A) F "
            Button2.Text = "B) O3" 'Correrto'
            Button3.Text = "C) CO2"
            Button4.Text = "D) H2O"

        ElseIf CodigoPergunta = 119 Then
            Label10.Text = somaPerguntas & " - Qual o símbolo quimico da agua?"

            Button1.Text = "A) F "
            Button2.Text = "B) O3"
            Button3.Text = "C) CO2" 'Correrto'
            Button4.Text = "D) H2O"

        ElseIf CodigoPergunta = 120 Then
            Label10.Text = somaPerguntas & " - Qual o símbolo quimico da agua?"

            Button1.Text = "A) F "
            Button2.Text = "B) O3"
            Button3.Text = "C) CO2"
            Button4.Text = "D) H2O" 'Correrto'

        End If
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        'Resposta 1/A'
        If CodigoPergunta = 3 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 5 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 9 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 13 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 16 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 17 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 21 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 25 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 29 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 33 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 37 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 41 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 45 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 49 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 53 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 57 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 61 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 65 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 69 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 73 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 77 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 81 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 85 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 89 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 93 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 97 Then
            Correto()
            Perguntas()
        Else
            Errado()
            Perguntas()
        End If
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        'Resposta 2/B'
        If CodigoPergunta = 4 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 6 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 10 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 14 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 16 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 18 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 22 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 26 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 30 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 34 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 38 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 42 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 46 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 50 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 54 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 58 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 62 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 66 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 70 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 74 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 78 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 82 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 86 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 90 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 94 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 98 Then
            Correto()
            Perguntas()
        Else
            Errado()
            Perguntas()
        End If
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        'Resposta 3/C'
        If CodigoPergunta = 1 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 7 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 11 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 15 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 16 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 19 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 23 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 27 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 31 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 35 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 39 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 43 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 47 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 51 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 55 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 59 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 63 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 67 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 71 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 75 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 79 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 83 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 87 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 91 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 95 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 99 Then
            Correto()
            Perguntas()
        Else
            Errado()
            Perguntas()
        End If
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        'Resposta 4/D'
        If CodigoPergunta = 2 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 8 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 12 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 16 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 20 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 24 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 28 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 32 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 36 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 40 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 44 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 48 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 52 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 56 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 60 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 64 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 68 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 72 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 76 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 80 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 84 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 88 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 92 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 96 Then
            Correto()
            Perguntas()
        ElseIf CodigoPergunta = 100 Then
            Correto()
            Perguntas()
        Else
            Errado()
            Perguntas()
        End If
    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        'Ajuda'
        Ajudas -= 1
        Label7.Text = Ajudas
        MsgBox("Ajuda")
        If Ajudas = 0 Then
            Button7.Enabled = True
            MsgBox("Não tens mais ajudas disponiveis")
        End If
    End Sub

    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click
        'Recomeçar'
        Label9.BackColor = Color.White
        RespostasCorretas = 0
        RespostasErradas = 0
        Label5.Text = RespostasCorretas
        Label6.Text = RespostasErradas
        Perguntas()
    End Sub
End Class