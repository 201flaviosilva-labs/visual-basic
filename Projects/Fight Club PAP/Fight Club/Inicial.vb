﻿Public Class Inicial

    Private Sub Inicial_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Loading Inicial

        ''Exemplo ---------------------------------------
        ' NomeCpu(1) = "Exemplo"
        ' VidaCpu(1) = 1 000 Max.
        'EscudoCpu(1) = 1 000 Max.
        ' ForcaCpu(1) = 100 Max.
        ' ResistenciaCpu(1) = 100 Max.
        ' AgilidadeCpu(1) = 100 Max.
        'InteligenciaCpu(1) = 100 Max.
        'PercepcaoCpu(1) = 100 Max.


        '-----------------------------------------------------------------------------------------------------------
        '-------------------------------------------- DC Comics ----------------------------------------------------
        '-----------------------------------------------------------------------------------------------------------
        For x As Integer = 1 To 9
            VidaCpu(x) = 1000
            EscudoCpu(x) = 1000
        Next

        'Mulher Maravilha ---------------------------------------
        NomeCpu(1) = "Tony"

        ForcaCpu(1) = 65
        ResistenciaCpu(1) = 60
        AgilidadeCpu(1) = 75
        InteligenciaCpu(1) = 60
        PercepcaoCpu(1) = 80
        VelocidadeCpu(1) = 65


        'Super-Homem ---------------------------------------
        NomeCpu(2) = "Pedro"

        ForcaCpu(2) = 100
        ResistenciaCpu(2) = 75
        AgilidadeCpu(2) = 60
        InteligenciaCpu(2) = 60
        PercepcaoCpu(2) = 70
        VelocidadeCpu(2) = 85


        'Super-Girl ---------------------------------------
        NomeCpu(3) = "Helder"

        ForcaCpu(3) = 95
        ResistenciaCpu(3) = 95
        AgilidadeCpu(3) = 95
        InteligenciaCpu(3) = 95
        PercepcaoCpu(3) = 95
        VelocidadeCpu(3) = 95


        'Batman ---------------------------------------
        NomeCpu(4) = "António"

        ForcaCpu(4) = 60
        ResistenciaCpu(4) = 60
        AgilidadeCpu(4) = 90
        InteligenciaCpu(4) = 50
        PercepcaoCpu(4) = 50
        VelocidadeCpu(4) = 75


        'Flash ---------------------------------------
        NomeCpu(5) = "Henrique"

        ForcaCpu(5) = 50
        ResistenciaCpu(5) = 90
        AgilidadeCpu(5) = 100
        InteligenciaCpu(5) = 50
        PercepcaoCpu(5) = 75
        VelocidadeCpu(5) = 90


        'Lanterna Verde ---------------------------------------
        NomeCpu(6) = "João"

        ForcaCpu(6) = 60
        ResistenciaCpu(6) = 50
        AgilidadeCpu(6) = 55
        InteligenciaCpu(6) = 40
        PercepcaoCpu(6) = 50
        VelocidadeCpu(6) = 50


        'Aqua-Men ---------------------------------------
        NomeCpu(7) = "Paulo"

        ForcaCpu(7) = 30
        ResistenciaCpu(7) = 30
        AgilidadeCpu(7) = 30
        InteligenciaCpu(7) = 30
        PercepcaoCpu(7) = 30
        VelocidadeCpu(7) = 30


        'Ciborgue  ---------------------------------------
        NomeCpu(8) = "Afonso "

        ForcaCpu(8) = 95
        ResistenciaCpu(8) = 35
        AgilidadeCpu(8) = 70
        InteligenciaCpu(8) = 10
        PercepcaoCpu(8) = 55
        VelocidadeCpu(8) = 35


        'Senhor Destino ---------------------------------------
        NomeCpu(9) = "Zé"

        ForcaCpu(9) = 45
        ResistenciaCpu(9) = 40
        AgilidadeCpu(9) = 40
        InteligenciaCpu(9) = 55
        PercepcaoCpu(9) = 60
        VelocidadeCpu(9) = 45


    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        EscolherHeroi.Show()
        CombateRapido = True
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        EscolherHeroi.Show()
        CombateRapido = False
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        End
    End Sub
End Class