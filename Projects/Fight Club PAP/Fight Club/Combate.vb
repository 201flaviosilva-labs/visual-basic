﻿Public Class Combate
    Dim LutadorCpu As Integer
    Dim SomaHabilidade, SomaHabilidadecpu, fasecombate As Integer
    Dim Ataquecpu, AtaqueJog, metadepodercpu, metadepoderjog As Integer
    Dim vidacombatejogador, escudocombatejogador, vidacombatecpu, escudocombatecpu As Integer
    Dim vitoria, empate As Boolean

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        LutadorCpu = 8
        Apresentarjogador()
        If CombateRapido = True Then
            Button1.Text = "Mudar Adversário"
            Label27.Visible = False
        Else
            Button1.Text = "Avançar fase"
            Label27.Visible = True
        End If
        CpuLutadorRotina()
        Button1.Enabled = False
        vitoria = False
        fasecombate = 1
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If CombateRapido = True Then
            Randomize()
            LutadorCpu = Int(9 * Rnd() + 1) 'Cria um número inteiro de 0 + 1 = 1 a 9
        Else
            AvancarFase()
        End If
        CpuLutadorRotina()
        Apresentarjogador()
        Button2.Enabled = True

    End Sub

    Sub CpuLutadorRotina()

        Label2.Text = NomeCpu(LutadorCpu)

        Label13.Text = VidaCpu(LutadorCpu)
        vidacombatecpu = EscudoCpu(LutadorCpu)
        Label14.Text = EscudoCpu(LutadorCpu)
        escudocombatecpu = EscudoCpu(LutadorCpu)

        Label15.Text = ForcaCpu(LutadorCpu)
        Label16.Text = ResistenciaCpu(LutadorCpu)
        Label17.Text = AgilidadeCpu(LutadorCpu)
        Label20.Text = InteligenciaCpu(LutadorCpu)
        Label22.Text = PercepcaoCpu(LutadorCpu)
        Label25.Text = VelocidadeCpu(LutadorCpu)
        SomaHabilidadecpu = ForcaCpu(LutadorCpu) + ResistenciaCpu(LutadorCpu) + AgilidadeCpu(LutadorCpu) + InteligenciaCpu(LutadorCpu) + PercepcaoCpu(LutadorCpu) + VelocidadeCpu(LutadorCpu)


        If LutadorCpu = 8 Then
            PictureBox2.Image = My.Resources.Afonso
        ElseIf LutadorCpu = 4 Then
            PictureBox2.Image = My.Resources.Antonio
        ElseIf LutadorCpu = 3 Then
            PictureBox2.Image = My.Resources.Helder
        ElseIf LutadorCpu = 5 Then
            PictureBox2.Image = My.Resources.Henrique
        ElseIf LutadorCpu = 6 Then
            PictureBox2.Image = My.Resources.Joao
        ElseIf LutadorCpu = 7 Then
            PictureBox2.Image = My.Resources.Paulo
        ElseIf LutadorCpu = 2 Then
            PictureBox2.Image = My.Resources.Pedro
        ElseIf LutadorCpu = 1 Then
            PictureBox2.Image = My.Resources.Tony
        Else
            PictureBox2.Image = My.Resources.Ze
        End If
    End Sub

    Sub AvancarFase()
        If vitoria = True Then
            fasecombate += 1
            If fasecombate < 9 Then
                MsgBox("Parabéns passaste para a proxima fase!")
            Else
                MsgBox("Venceste o torneio! Ganhaste mais uma personagem")
                NTorneioVencido += 1
                Button2.Enabled = False
            End If
        ElseIf vitoria = False And empate = True Then
            MsgBox("Ainda não podes avaçar pois ainda não ganhaste!")
        Else
            fasecombate = 1
            MsgBox("Voltaste para o inicio")
        End If

        If fasecombate = 1 Then
            LutadorCpu = 7
        ElseIf fasecombate = 2 Then
            LutadorCpu = 9
        ElseIf fasecombate = 3 Then
            LutadorCpu = 8
        ElseIf fasecombate = 4 Then
            LutadorCpu = 6
        ElseIf fasecombate = 5 Then
            LutadorCpu = 4
        ElseIf fasecombate = 6 Then
            LutadorCpu = 1
        ElseIf fasecombate = 7 Then
            LutadorCpu = 2
        ElseIf fasecombate = 8 Then
            LutadorCpu = 5
        Else
            LutadorCpu = 3
        End If
        Label27.Text = "Combate " & fasecombate
        Button1.Enabled = False
        Button2.Enabled = True
    End Sub

    Sub Apresentarjogador()
        Label1.Text = NomeCpu(jogadorescolhido)

        Label8.Text = VidaCpu(jogadorescolhido)
        vidacombatejogador = VidaCpu(jogadorescolhido)
        Label9.Text = EscudoCpu(jogadorescolhido)
        Escudocombatejogador = EscudoCpu(jogadorescolhido)

        Label10.Text = ForcaCpu(jogadorescolhido)
        Label11.Text = ResistenciaCpu(jogadorescolhido)
        Label12.Text = AgilidadeCpu(jogadorescolhido)
        Label19.Text = InteligenciaCpu(jogadorescolhido)
        Label23.Text = PercepcaoCpu(jogadorescolhido)
        Label24.Text = VelocidadeCpu(jogadorescolhido)
        SomaHabilidade = ForcaCpu(jogadorescolhido) + ResistenciaCpu(jogadorescolhido) + AgilidadeCpu(jogadorescolhido) + InteligenciaCpu(jogadorescolhido) + PercepcaoCpu(jogadorescolhido) + VelocidadeCpu(jogadorescolhido)

        If jogadorescolhido = 8 Then
            PictureBox1.Image = My.Resources.Afonso
        ElseIf jogadorescolhido = 4 Then
            PictureBox1.Image = My.Resources.Antonio
        ElseIf jogadorescolhido = 3 Then
            PictureBox1.Image = My.Resources.Helder
        ElseIf jogadorescolhido = 5 Then
            PictureBox1.Image = My.Resources.Henrique
        ElseIf jogadorescolhido = 6 Then
            PictureBox1.Image = My.Resources.Joao
        ElseIf jogadorescolhido = 7 Then
            PictureBox1.Image = My.Resources.Paulo
        ElseIf jogadorescolhido = 2 Then
            PictureBox1.Image = My.Resources.Pedro
        ElseIf jogadorescolhido = 1 Then
            PictureBox1.Image = My.Resources.Tony
        Else
            PictureBox1.Image = My.Resources.Ze
        End If
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Button1.Enabled = False
        Button2.Enabled = False
        Do
            metadepodercpu = SomaHabilidadecpu / 2
            metadepoderjog = SomaHabilidade / 2
            Ataquecpu = Int(metadepodercpu * Rnd() + metadepodercpu)
            AtaqueJog = Int(metadepoderjog * Rnd() + metadepoderjog)


            If escudocombatecpu <= 0 Then
                vidacombatecpu -= AtaqueJog
            Else
                escudocombatecpu -= AtaqueJog
                If escudocombatecpu < 0 Then
                    escudocombatecpu = 0
                End If
            End If

            If escudocombatejogador <= 0 Then
                vidacombatejogador -= Ataquecpu
            Else
                escudocombatejogador -= Ataquecpu
                If escudocombatejogador < 0 Then
                    escudocombatejogador = 0
                End If
            End If


            Label8.Text = vidacombatejogador
            Label9.Text = escudocombatejogador

            Label13.Text = vidacombatecpu
            Label14.Text = escudocombatecpu

            empate = False
            vitoria = False
            If vidacombatejogador <= 0 And vidacombatecpu > 0 Then
                MsgBox("Perdeste")
                vitoria = False
            ElseIf vidacombatejogador > 0 And vidacombatecpu <= 0 Then
                MsgBox("Venceste")
                vitoria = True
            ElseIf vidacombatejogador <= 0 And vidacombatecpu <= 0 Then
                MsgBox("Empate")
                empate = True
            End If

            pausa(500) 'Chama o Sub Pausa e diz quantos milisegundos é de pausa
            Label1.Visible = True 'Torna a label Visivel

        Loop Until ((vidacombatejogador <= 0) Or (vidacombatecpu <= 0))
        Button1.Enabled = True
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Me.Close()
    End Sub

    Sub pausa(ByVal tempopausa)
        'Tempo de Pausa'

        'Não fui eu que criei este código, por isso não sei como isto fonciona, mas tentei explicar o melhor que pude

        Dim tempoatual As Date 'Cria uma variabel do tipo data
        Dim tempoparado As Date 'Cria uma variabel do tipo data

        tempoatual = Date.Now() 'Recebe o tempo atual
        tempoparado = tempoatual.AddMilliseconds(tempopausa) 'O tempo que irá estar parado é igual ao tempo que foi defenido (tempopausa) que iria estar em pausa

        Do While Date.Now < tempoparado 'Faz isto enquanto agora(Tempo) for menor que o tempo parado
            Application.DoEvents() 'a aplicação pára o que está a aconter
        Loop ' Fim do ciclo
    End Sub
End Class
