﻿Public Class EscolherHeroi

    Sub DeixarDetalhes()
        Label15.Text = 0
        Label16.Text = 0
        Label17.Text = 0
        Label20.Text = 0
        Label22.Text = 0
        Label25.Text = 0
    End Sub

    Private Sub PictureBox1_MouseEnter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox1.MouseEnter
        Label15.Text = ForcaCpu(8)
        Label16.Text = ResistenciaCpu(8)
        Label17.Text = AgilidadeCpu(8)
        Label20.Text = InteligenciaCpu(8)
        Label22.Text = PercepcaoCpu(8)
        Label25.Text = VelocidadeCpu(8)
    End Sub

    Private Sub PictureBox2_MouseEnter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox2.MouseEnter
        Label15.Text = ForcaCpu(4)
        Label16.Text = ResistenciaCpu(4)
        Label17.Text = AgilidadeCpu(4)
        Label20.Text = InteligenciaCpu(4)
        Label22.Text = PercepcaoCpu(4)
        Label25.Text = VelocidadeCpu(4)
    End Sub

    Private Sub PictureBox3_MouseEnter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox3.MouseEnter
        Label15.Text = ForcaCpu(3)
        Label16.Text = ResistenciaCpu(3)
        Label17.Text = AgilidadeCpu(3)
        Label20.Text = InteligenciaCpu(3)
        Label22.Text = PercepcaoCpu(3)
        Label25.Text = VelocidadeCpu(3)
    End Sub

    Private Sub PictureBox4_MouseEnter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox4.MouseEnter
        Label15.Text = ForcaCpu(5)
        Label16.Text = ResistenciaCpu(5)
        Label17.Text = AgilidadeCpu(5)
        Label20.Text = InteligenciaCpu(5)
        Label22.Text = PercepcaoCpu(5)
        Label25.Text = VelocidadeCpu(5)
    End Sub

    Private Sub PictureBox9_MouseEnter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox9.MouseEnter
        Label15.Text = ForcaCpu(9)
        Label16.Text = ResistenciaCpu(9)
        Label17.Text = AgilidadeCpu(9)
        Label20.Text = InteligenciaCpu(9)
        Label22.Text = PercepcaoCpu(9)
        Label25.Text = VelocidadeCpu(9)
    End Sub

    Private Sub PictureBox5_MouseEnter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox5.MouseEnter
        Label15.Text = ForcaCpu(6)
        Label16.Text = ResistenciaCpu(6)
        Label17.Text = AgilidadeCpu(6)
        Label20.Text = InteligenciaCpu(6)
        Label22.Text = PercepcaoCpu(6)
        Label25.Text = VelocidadeCpu(6)
    End Sub

    Private Sub PictureBox6_MouseEnter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox6.MouseEnter
        Label15.Text = ForcaCpu(7)
        Label16.Text = ResistenciaCpu(7)
        Label17.Text = AgilidadeCpu(7)
        Label20.Text = InteligenciaCpu(7)
        Label22.Text = PercepcaoCpu(7)
        Label25.Text = VelocidadeCpu(7)
    End Sub

    Private Sub PictureBox7_MouseEnter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox7.MouseEnter
        Label15.Text = ForcaCpu(2)
        Label16.Text = ResistenciaCpu(2)
        Label17.Text = AgilidadeCpu(2)
        Label20.Text = InteligenciaCpu(2)
        Label22.Text = PercepcaoCpu(2)
        Label25.Text = VelocidadeCpu(2)
    End Sub

    Private Sub PictureBox8_MouseEnter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox8.MouseEnter
        Label15.Text = ForcaCpu(1)
        Label16.Text = ResistenciaCpu(1)
        Label17.Text = AgilidadeCpu(1)
        Label20.Text = InteligenciaCpu(1)
        Label22.Text = PercepcaoCpu(1)
        Label25.Text = VelocidadeCpu(1)
    End Sub

    Private Sub Button10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button10.Click
        Me.Close()
    End Sub

    Private Sub PictureBox1_MouseLeave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox1.MouseLeave
        DeixarDetalhes()
    End Sub

    Private Sub PictureBox2_MouseLeave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox2.MouseLeave
        DeixarDetalhes()
    End Sub

    Private Sub PictureBox3_MouseLeave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox3.MouseLeave
        DeixarDetalhes()
    End Sub

    Private Sub PictureBox4_MouseLeave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox4.MouseLeave
        DeixarDetalhes()
    End Sub

    Private Sub PictureBox9_MouseLeave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox9.MouseLeave
        DeixarDetalhes()
    End Sub

    Private Sub PictureBox5_MouseLeave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox5.MouseLeave
        DeixarDetalhes()
    End Sub

    Private Sub PictureBox6_MouseLeave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox6.MouseLeave
        DeixarDetalhes()
    End Sub

    Private Sub PictureBox7_MouseLeave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox7.MouseLeave
        DeixarDetalhes()
    End Sub

    Private Sub PictureBox8_MouseLeave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox8.MouseLeave
        DeixarDetalhes()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        jogadorescolhido = 8
        irCombateForm()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        jogadorescolhido = 4
        irCombateForm()
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        jogadorescolhido = 3
        irCombateForm()
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        jogadorescolhido = 5
        irCombateForm()
    End Sub

    Private Sub Button9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button9.Click
        jogadorescolhido = 9
        irCombateForm()
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        jogadorescolhido = 6
        irCombateForm()
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        jogadorescolhido = 7
        irCombateForm()
    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        jogadorescolhido = 2
        irCombateForm()
    End Sub

    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click
        jogadorescolhido = 1
        irCombateForm()
    End Sub

    Sub irCombateForm()
        Combate.Show()
        Me.Close()
    End Sub

    Private Sub EscolherHeroi_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If NTorneioVencido >= 1 Then
            Button6.Enabled = True
        End If
        If NTorneioVencido >= 2 Then
            Button1.Enabled = True
        End If
        If NTorneioVencido >= 3 Then
            Button4.Enabled = True
        End If
        If NTorneioVencido >= 4 Then
            Button7.Enabled = True
        End If
        If NTorneioVencido >= 5 Then
            Button3.Enabled = True
        End If
    End Sub
End Class