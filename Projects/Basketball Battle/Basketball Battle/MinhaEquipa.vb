﻿Public Class MinhaEquipa
    Dim troca As Byte = 0
    Dim soma As Byte = 0
    Dim TrocaInicial As Byte
    Dim somaPoderes As Double
    Dim NJSuplenteVazio As String
    Dim par As Boolean

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Cartas.Show()
        Me.Close()
        Button2.Enabled = True
    End Sub

    Private Sub MinhaEquipa_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Mostra o nome, poder e fotografia dos jogadores suplentes e o dinheiro'
        Label8.Text = NJSuplente(1)
        Label10.Text = NJSuplente(2)
        Label12.Text = NJSuplente(3)
        Label21.Text = NJSuplente(4)
        Label23.Text = NJSuplente(5)

        Label7.Text = PJSuplente(1) & "/100"
        Label9.Text = PJSuplente(2) & "/100"
        Label11.Text = PJSuplente(3) & "/100"
        Label20.Text = PJSuplente(4) & "/100"
        Label22.Text = PJSuplente(5) & "/100"

        Label27.Text = DinheiroAcumolado & "$"
        'Mostra o nome, poder e fotografia dos jogadores suplentes e o dinheiro'


        'Mostra o nome, poder e fotografia dos jogadores Titulares'
        Label2.Text = NJogadorTitular(1)
        Label4.Text = NJogadorTitular(2)
        Label6.Text = NJogadorTitular(3)
        Label5.Text = NJogadorTitular(4)
        Label3.Text = NJogadorTitular(5)

        Label13.Text = PJTitular(1) & "/100"
        Label14.Text = PJTitular(2) & "/100"
        Label15.Text = PJTitular(3) & "/100"
        Label16.Text = PJTitular(4) & "/100"
        Label17.Text = PJTitular(5) & "/100"
        'Mostra o nome, poder e fotografia dos jogadores suplentes'

        'Nome da Equipa e média, Vitórias e derrotas'
        Label1.Text = NomeEquipa
        Label24.Text = MediaUTL
        Label18.Text = "N. Vitórias: " & NVitórias
        Label19.Text = "N. Derrotas: " & NDerrotas
        'Nome da Equipa e média, Vitórias e derrotas'
        NJSuplenteVazio = "N. Jogador"
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        'Voltar'
        If JogoModoTreinadorTF = True Then
            JogoModoTreinadorTF = False
            JogoModoTreinador.Show()
            Me.Close()
        ElseIf TorneioTF = True Then
            TorneioTF = False
            Torneio.Show()
            Me.Close()
        ElseIf treinoTF = True Then
            treinoTF = False
            Treino.Show()
            Me.Close()
        Else
            TelaInicial.Show()
            Me.Close()
        End If
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        'Substituições'

        soma += 1
        'Mod = "%" = Resto de uma conta, exemplo (3/2=1) ou (8/4=0)'
        If soma Mod 2 = 0 Then
            par = True
        Else
            par = False
        End If
        'Assim não terá limite de vezes a querer substituir'
        If par = False Then
            TrocaInicial = 1 ' Pode ser feita a troca'
            Button3.BackColor = Color.Green
            Label2.BackColor = Color.Green
            Label3.BackColor = Color.Green
            Label4.BackColor = Color.Green
            Label5.BackColor = Color.Green
            Label6.BackColor = Color.Green
            Label8.BackColor = Color.Green
            Label10.BackColor = Color.Green
            Label12.BackColor = Color.Green
            Label21.BackColor = Color.Green
            Label23.BackColor = Color.Green
        Else
            TrocaInicial = 0 ' Não pode ser feita a troca'
            Button3.BackColor = Color.Red
            Label2.BackColor = Color.White
            Label3.BackColor = Color.White
            Label4.BackColor = Color.White
            Label5.BackColor = Color.White
            Label6.BackColor = Color.White
            Label8.BackColor = Color.White
            Label10.BackColor = Color.White
            Label12.BackColor = Color.White
            Label21.BackColor = Color.White
            Label23.BackColor = Color.White
        End If
    End Sub

    Private Sub Label8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label8.Click
        'Jogador Substituto 1'
        If TrocaInicial = 1 Then
            troca = 11
        Else
            MsgBox("Ativa as Substituições")
            troca = 0
        End If
    End Sub

    Private Sub Label10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label10.Click
        'Jogador Substituto 2'
        If TrocaInicial = 1 Then
            troca = 12
        Else
            MsgBox("Ativa as Substituições")
            troca = 0
        End If
    End Sub

    Private Sub Label12_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label12.Click
        'Jogador Substituto 3'
        If TrocaInicial = 1 Then
            troca = 13
        Else
            MsgBox("Ativa as Substituições")
            troca = 0
        End If
    End Sub

    Private Sub Label21_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label21.Click
        'Jogador Substituto 4'
        If TrocaInicial = 1 Then
            troca = 14
        Else
            MsgBox("Ativa as Substituições")
            troca = 0
        End If
    End Sub

    Private Sub Label23_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label23.Click
        'Jogador Substituto 5'
        If TrocaInicial = 1 Then
            troca = 15
        Else
            MsgBox("Ativa as Substituições")
            troca = 0
        End If
    End Sub

    Private Sub Label2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label2.Click
        'Jogador Titular 1'
        If troca = 11 Then
            NJogadorTitular(1) = NJSuplente(1)
            PJTitular(1) = PJSuplente(1)
            Label2.Text = NJogadorTitular(1)
            Label13.Text = PJTitular(1) & "/100"
            Label8.Text = "N. Jogador"
            Label7.Text = "0/100"

            'Retitar o nome e o poder dos Suplentes'
            NJSuplente(1) = NJSuplenteVazio
            PJSuplente(1) = 0
            'Retitar o nome e o poder dos Suplentes'

        ElseIf troca = 12 Then
            NJogadorTitular(1) = NJSuplente(2)
            PJTitular(1) = PJSuplente(2)
            Label2.Text = NJogadorTitular(1)
            Label13.Text = PJTitular(1) & "/100"
            Label10.Text = "N. Jogador"
            Label9.Text = "0/100"

            'Retitar o nome e o poder dos Suplentes'
            NJSuplente(2) = NJSuplenteVazio
            PJSuplente(2) = 0
            'Retitar o nome e o poder dos Suplentes'

        ElseIf troca = 13 Then
            NJogadorTitular(1) = NJSuplente(3)
            PJTitular(1) = PJSuplente(3)
            Label2.Text = NJogadorTitular(1)
            Label13.Text = PJTitular(1) & "/100"
            Label12.Text = "N. Jogador"
            Label11.Text = "0/100"

            'Retitar o nome e o poder dos Suplentes'
            NJSuplente(3) = NJSuplenteVazio
            PJSuplente(3) = 0
            'Retitar o nome e o poder dos Suplentes'

        ElseIf troca = 14 Then
            NJogadorTitular(1) = NJSuplente(4)
            PJTitular(1) = PJSuplente(4)
            Label2.Text = NJogadorTitular(1)
            Label13.Text = PJTitular(1) & "/100"
            Label21.Text = "N. Jogador"
            Label20.Text = "0/100"

            'Retitar o nome e o poder dos Suplentes'
            NJSuplente(4) = NJSuplenteVazio
            PJSuplente(4) = 0
            'Retitar o nome e o poder dos Suplentes'

        ElseIf troca = 15 Then
            NJogadorTitular(1) = NJSuplente(5)
            PJTitular(1) = PJSuplente(5)
            Label2.Text = NJogadorTitular(1)
            Label13.Text = PJTitular(1) & "/100"
            Label23.Text = "N. Jogador"
            Label22.Text = "0/100"

            'Retitar o nome e o poder dos Suplentes'
            NJSuplente(5) = NJSuplenteVazio
            PJSuplente(5) = 0
            'Retitar o nome e o poder dos Suplentes'

        Else
            MsgBox("Primeiro tens de escolher um suplente para trocar")
        End If

        mediarotina()
    End Sub

    Private Sub Label4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label4.Click
        'Jogador Titular 2'
        If troca = 11 Then
            NJogadorTitular(2) = NJSuplente(1)
            PJTitular(2) = PJSuplente(1)
            Label4.Text = NJogadorTitular(2)
            Label14.Text = PJTitular(2) & "/100"
            Label8.Text = "N. Jogador"
            Label7.Text = "0/100"

            'Retitar o nome e o poder dos Suplentes'
            NJSuplente(1) = NJSuplenteVazio
            PJSuplente(1) = 0
            'Retitar o nome e o poder dos Suplentes'

        ElseIf troca = 12 Then
            NJogadorTitular(2) = NJSuplente(2)
            PJTitular(2) = PJSuplente(2)
            Label4.Text = NJogadorTitular(2)
            Label14.Text = PJTitular(2) & "/100"
            Label10.Text = "N. Jogador"
            Label9.Text = "0/100"

            'Retitar o nome e o poder dos Suplentes'
            NJSuplente(2) = NJSuplenteVazio
            PJSuplente(2) = 0
            'Retitar o nome e o poder dos Suplentes'

        ElseIf troca = 13 Then
            NJogadorTitular(2) = NJSuplente(3)
            PJTitular(2) = PJSuplente(3)
            Label4.Text = NJogadorTitular(2)
            Label14.Text = PJTitular(2) & "/100"
            Label12.Text = "N. Jogador"
            Label11.Text = "0/100"

            'Retitar o nome e o poder dos Suplentes'
            NJSuplente(3) = NJSuplenteVazio
            PJSuplente(3) = 0
            'Retitar o nome e o poder dos Suplentes'

        ElseIf troca = 14 Then
            NJogadorTitular(2) = NJSuplente(4)
            PJTitular(2) = PJSuplente(4)
            Label4.Text = NJogadorTitular(2)
            Label14.Text = PJTitular(2) & "/100"
            Label21.Text = "N. Jogador"
            Label20.Text = "0/100"

            'Retitar o nome e o poder dos Suplentes'
            NJSuplente(4) = NJSuplenteVazio
            PJSuplente(4) = 0
            'Retitar o nome e o poder dos Suplentes'

        ElseIf troca = 15 Then
            NJogadorTitular(2) = NJSuplente(5)
            PJTitular(2) = PJSuplente(5)
            Label4.Text = NJogadorTitular(2)
            Label14.Text = PJTitular(2) & "/100"
            Label23.Text = "N. Jogador"
            Label22.Text = "0/100"

            'Retitar o nome e o poder dos Suplentes'
            NJSuplente(5) = NJSuplenteVazio
            PJSuplente(5) = 0
            'Retitar o nome e o poder dos Suplentes'

        Else
            MsgBox("Primeiro tens de escolher um suplente para trocar")
        End If

        mediarotina()
    End Sub

    Private Sub Label6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label6.Click
        'Jogador Titular 3'
        If troca = 11 Then
            NJogadorTitular(3) = NJSuplente(1)
            PJTitular(3) = PJSuplente(1)
            Label6.Text = NJogadorTitular(3)
            Label15.Text = PJTitular(3) & "/100"
            Label8.Text = "N. Jogador"
            Label7.Text = "0/100"

            'Retitar o nome e o poder dos Suplentes'
            NJSuplente(1) = NJSuplenteVazio
            PJSuplente(1) = 0
            'Retitar o nome e o poder dos Suplentes'

        ElseIf troca = 12 Then
            NJogadorTitular(3) = NJSuplente(2)
            PJTitular(3) = PJSuplente(2)
            Label6.Text = NJogadorTitular(3)
            Label15.Text = PJTitular(3) & "/100"
            Label10.Text = "N. Jogador"
            Label9.Text = "0/100"

            'Retitar o nome e o poder dos Suplentes'
            NJSuplente(2) = NJSuplenteVazio
            PJSuplente(2) = 0
            'Retitar o nome e o poder dos Suplentes'

        ElseIf troca = 13 Then
            NJogadorTitular(3) = NJSuplente(3)
            PJTitular(3) = PJSuplente(3)
            Label6.Text = NJogadorTitular(3)
            Label15.Text = PJTitular(3) & "/100"
            Label12.Text = "N. Jogador"
            Label11.Text = "0/100"

            'Retitar o nome e o poder dos Suplentes'
            NJSuplente(3) = NJSuplenteVazio
            PJSuplente(3) = 0
            'Retitar o nome e o poder dos Suplentes'

        ElseIf troca = 14 Then
            NJogadorTitular(3) = NJSuplente(4)
            PJTitular(3) = PJSuplente(4)
            Label6.Text = NJogadorTitular(3)
            Label15.Text = PJTitular(3) & "/100"
            Label21.Text = "N. Jogador"
            Label20.Text = "0/100"

            'Retitar o nome e o poder dos Suplentes'
            NJSuplente(4) = NJSuplenteVazio
            PJSuplente(4) = 0
            'Retitar o nome e o poder dos Suplentes'

        ElseIf troca = 15 Then
            NJogadorTitular(3) = NJSuplente(5)
            PJTitular(3) = PJSuplente(5)
            Label6.Text = NJogadorTitular(3)
            Label15.Text = PJTitular(3) & "/100"
            Label23.Text = "N. Jogador"
            Label22.Text = "0/100"

            'Retitar o nome e o poder dos Suplentes'
            NJSuplente(5) = NJSuplenteVazio
            PJSuplente(5) = 0
            'Retitar o nome e o poder dos Suplentes'

        Else
            MsgBox("Primeiro tens de escolher um suplente para trocar")
        End If

        mediarotina()
    End Sub

    Private Sub Label5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label5.Click
        'Jogador Titular 4'
        If troca = 11 Then
            NJogadorTitular(4) = NJSuplente(1)
            PJTitular(4) = PJSuplente(1)
            Label5.Text = NJogadorTitular(4)
            Label16.Text = PJTitular(4) & "/100"
            Label8.Text = "N. Jogador"
            Label7.Text = "0/100"

            'Retitar o nome e o poder dos Suplentes'
            NJSuplente(1) = NJSuplenteVazio
            PJSuplente(1) = 0
            'Retitar o nome e o poder dos Suplentes'

        ElseIf troca = 12 Then
            NJogadorTitular(4) = NJSuplente(2)
            PJTitular(4) = PJSuplente(2)
            Label5.Text = NJogadorTitular(4)
            Label16.Text = PJTitular(4) & "/100"
            Label10.Text = "N. Jogador"
            Label9.Text = "0/100"

            'Retitar o nome e o poder dos Suplentes'
            NJSuplente(2) = NJSuplenteVazio
            PJSuplente(2) = 0
            'Retitar o nome e o poder dos Suplentes'

        ElseIf troca = 13 Then
            NJogadorTitular(4) = NJSuplente(3)
            PJTitular(4) = PJSuplente(3)
            Label5.Text = NJogadorTitular(4)
            Label16.Text = PJTitular(4) & "/100"
            Label12.Text = "N. Jogador"
            Label11.Text = "0/100"

            'Retitar o nome e o poder dos Suplentes'
            NJSuplente(3) = NJSuplenteVazio
            PJSuplente(3) = 0
            'Retitar o nome e o poder dos Suplentes'

        ElseIf troca = 14 Then
            NJogadorTitular(4) = NJSuplente(4)
            PJTitular(4) = PJSuplente(4)
            Label5.Text = NJogadorTitular(4)
            Label16.Text = PJTitular(4) & "/100"
            Label21.Text = "N. Jogador"
            Label20.Text = "0/100"

            'Retitar o nome e o poder dos Suplentes'
            NJSuplente(4) = NJSuplenteVazio
            PJSuplente(4) = 0
            'Retitar o nome e o poder dos Suplentes'

        ElseIf troca = 15 Then
            NJogadorTitular(4) = NJSuplente(5)
            PJTitular(4) = PJSuplente(5)
            Label5.Text = NJogadorTitular(4)
            Label16.Text = PJTitular(4) & "/100"
            Label23.Text = "N. Jogador"
            Label22.Text = "0/100"

            'Retitar o nome e o poder dos Suplentes'
            NJSuplente(5) = NJSuplenteVazio
            PJSuplente(5) = 0
            'Retitar o nome e o poder dos Suplentes'

        Else
            MsgBox("Primeiro tens de escolher um suplente para trocar")
        End If

        mediarotina()
    End Sub

    Private Sub Label3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label3.Click
        'Jogador Titular 5'
        If troca = 11 Then
            NJogadorTitular(5) = NJSuplente(1)
            PJTitular(5) = PJSuplente(1)
            Label3.Text = NJogadorTitular(5)
            Label17.Text = PJTitular(5) & "/100"
            Label8.Text = "N. Jogador"
            Label7.Text = "0/100"

            'Retitar o nome e o poder dos Suplentes'
            NJSuplente(1) = NJSuplenteVazio
            PJSuplente(1) = 0
            'Retitar o nome e o poder dos Suplentes'

        ElseIf troca = 12 Then
            NJogadorTitular(5) = NJSuplente(2)
            PJTitular(5) = PJSuplente(2)
            Label3.Text = NJogadorTitular(5)
            Label17.Text = PJTitular(5) & "/100"
            Label10.Text = "N. Jogador"
            Label9.Text = "0/100"

            'Retitar o nome e o poder dos Suplentes'
            NJSuplente(2) = NJSuplenteVazio
            PJSuplente(2) = 0
            'Retitar o nome e o poder dos Suplentes'

        ElseIf troca = 13 Then
            NJogadorTitular(5) = NJSuplente(3)
            PJTitular(5) = PJSuplente(3)
            Label3.Text = NJogadorTitular(5)
            Label17.Text = PJTitular(5) & "/100"
            Label12.Text = "N. Jogador"
            Label11.Text = "0/100"

            'Retitar o nome e o poder dos Suplentes'
            NJSuplente(3) = NJSuplenteVazio
            PJSuplente(3) = 0
            'Retitar o nome e o poder dos Suplentes'

        ElseIf troca = 14 Then
            NJogadorTitular(5) = NJSuplente(4)
            PJTitular(5) = PJSuplente(4)
            Label3.Text = NJogadorTitular(5)
            Label17.Text = PJTitular(5) & "/100"
            Label21.Text = "N. Jogador"
            Label20.Text = "0/100"

            'Retitar o nome e o poder dos Suplentes'
            NJSuplente(4) = NJSuplenteVazio
            PJSuplente(4) = 0
            'Retitar o nome e o poder dos Suplentes'

        ElseIf troca = 15 Then
            NJogadorTitular(5) = NJSuplente(5)
            PJTitular(5) = PJSuplente(5)
            Label3.Text = NJogadorTitular(5)
            Label17.Text = PJTitular(5) & "/100"
            Label23.Text = "N. Jogador"
            Label22.Text = "0/100"

            'Retitar o nome e o poder dos Suplentes'
            NJSuplente(5) = NJSuplenteVazio
            PJSuplente(5) = 0
            'Retitar o nome e o poder dos Suplentes'

        Else
            MsgBox("Primeiro tens de escolher um suplente para trocar")
        End If

        mediarotina()
    End Sub

    Sub mediarotina()
        'Soma da Média da equipa'
        somaPoderes = PJTitular(1) + PJTitular(2) + PJTitular(3) + PJTitular(4) + PJTitular(5)
        MediaUTL = somaPoderes / 5
        Label24.Text = MediaUTL & "/100"
        'Soma da Média da equipa'
    End Sub

    Private Sub Label1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label1.Click
        NomeEquipa = InputBox("Qual é o nome que queres dar á tua equipa?")
        Label1.Text = NomeEquipa
    End Sub

    Private Sub Button10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button10.Click
        'Ajuda'
        MsgBox("Eis a tua maravilhosa, se caso ainda não jogadores, clica em 'Cartas'!")
        MsgBox("Aqui podes Escolher quem vai jogar no próximo jogo ou não. Escolhe com sabdoria e é bom que venhas aqui regularmente.")
        MsgBox("Aqui tambem podes mudar o nome da tua equipa, clicando no espaço ao lado de: 'N. Equipa:'! Podes alterar quantas vezes quiseres.")
        MsgBox("Tambem podes vir aqui confirar o teu ´número de vitórias e derrotas.")
    End Sub
End Class