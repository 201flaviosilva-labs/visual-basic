﻿Public Class TelaInicial
    Dim Cheats, Programador As String
    Dim PRandomCheats As Integer
    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        'Sair'
        End
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        'Partida Rápida
        Jogo.Show()
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        'Minha Equipa'
        MinhaEquipa.Show()
        Button1.Enabled = True
        Button2.Enabled = True
        Button3.Enabled = True
        Button4.Enabled = True
        Button5.Enabled = True
        Button9.Enabled = True
    End Sub

    Private Sub TelaInicial_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Tela Inicial'
        CriarEquipaTorn()
        MsgBox("Olá! Agora começa a tua jornada no Basketball Batllle. Aqui podes criar a tua equipa, e podes jogar contra outras. Agora vamos conhecer a tua equipa, por isso clica em 'Minha Equipa'!")
        MsgBox("Sempre que precisares de ajuda, podes clicar no butão que diz 'Ajuda?'!")
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        'Programador'
        Programador = InputBox("Qual é a palavra chave?", "Programador", "20")
        If Programador = "1" Then
            DinheiroAcumolado = 10000000

            NJogadorTitular(1) = "Programador"
            NJogadorTitular(2) = "Programador"
            NJogadorTitular(3) = "Programador"
            NJogadorTitular(4) = "Programador"
            NJogadorTitular(5) = "Programador"

            PJTitular(1) = 100
            PJTitular(2) = 100
            PJTitular(3) = 100
            PJTitular(4) = 100
            PJTitular(5) = 100
            MediaUTL = 100
        End If
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        'Campanha'
        Button7.Visible = True
        Button8.Visible = True
    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        'Modo Treinador
        ModoTreinador.Show()
        Button7.Visible = False
        Button8.Visible = False
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        'Creditos'
        Créditos.Show()
    End Sub

    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click
        'Torneio'
        Torneio.Show()
        Button7.Visible = False
        Button8.Visible = False
    End Sub
    Sub CriarEquipaTorn()
        NomeEquipaTorn(1) = "Avelar"
        NomeEquipaTorn(2) = "Ancião"
        NomeEquipaTorn(3) = "Leiria"
        NomeEquipaTorn(4) = "Andorra"
        NomeEquipaTorn(5) = "Matosinhos"
        NomeEquipaTorn(6) = "Porto"
        NomeEquipaTorn(7) = "Lisboa"
        NomeEquipaTorn(8) = "Portugal"

        NomeJogadoresEquipaTorn(1) = "João"
        NomeJogadoresEquipaTorn(2) = "Joaquim"
        NomeJogadoresEquipaTorn(3) = "Julio"
        NomeJogadoresEquipaTorn(4) = "Jonior"
        NomeJogadoresEquipaTorn(5) = "Jacinto"
        NomeJogadoresEquipaTorn(6) = "Jaquelino"
        NomeJogadoresEquipaTorn(7) = "Jelson"
        NomeJogadoresEquipaTorn(8) = "Jonas"

        Randomize()
        MediaEquipaTorn(1) = Int(25 * Rnd())
        MediaEquipaTorn(2) = Int(10 * Rnd() + 30)
        MediaEquipaTorn(3) = Int(20 * Rnd() + 30)
        MediaEquipaTorn(4) = Int(20 * Rnd() + 50)
        MediaEquipaTorn(5) = Int(20 * Rnd() + 60)
        MediaEquipaTorn(6) = Int(20 * Rnd() + 70)
        MediaEquipaTorn(7) = Int(20 * Rnd() + 80)
        MediaEquipaTorn(8) = Int(10 * Rnd() + 90)

    End Sub

    Private Sub Button10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button10.Click
        'ajuda'
        MsgBox("Estas na parte centrar do jogo, aqui é onde ocorre toda a ligação entre tudo, clicando em qualquer um dos butões, serás direcionado para uma determinada parta da aplicação. Diverte-te!")
    End Sub

    Private Sub Button9_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button9.Click
        'Treino'
        Treino.Show()
    End Sub

    Private Sub Button11_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button11.Click
        'Cheats
        Randomize()
        Cheats = InputBox("Escreve o código", "Hack", "0000")
        If Cheats = "$PorFavor" Then
            'Receber pouco dinhiero
            DinheiroAcumolado += 10

        ElseIf Cheats = "DinheiroPorFavor" Then
            'Receber dinhiero
            DinheiroAcumolado += 100

        ElseIf Cheats = "MuitoDinheiroPorFavor" Then
            'Receber muito dinhiero
            DinheiroAcumolado += 1000

        ElseIf Cheats = "VitóriaPorFavor" Then
            'Vencer sempre, no Jogo Rápido
            VitoriaPorFavor = True

        ElseIf Cheats = "IniciadosSuplentesPorFavor" Then
            'Iniciados
            NJSuplente(1) = "Cheater"
            NJSuplente(2) = "Cheater"
            NJSuplente(3) = "Cheater"
            NJSuplente(4) = "Cheater"
            NJSuplente(5) = "Cheater"

            PRandomCheats = Int(50 * Rnd())

            PJSuplente(1) = PRandomCheats
            PJSuplente(2) = PRandomCheats
            PJSuplente(3) = PRandomCheats
            PJSuplente(4) = PRandomCheats
            PJSuplente(5) = PRandomCheats

        ElseIf Cheats = "CadetesSuplentesPorFavor" Then
            'Cadetes
            NJSuplente(1) = "Cheater"
            NJSuplente(2) = "Cheater"
            NJSuplente(3) = "Cheater"
            NJSuplente(4) = "Cheater"
            NJSuplente(5) = "Cheater"

            PRandomCheats = Int(30 * Rnd() + 50)

            PJSuplente(1) = PRandomCheats
            PJSuplente(2) = PRandomCheats
            PJSuplente(3) = PRandomCheats
            PJSuplente(4) = PRandomCheats
            PJSuplente(5) = PRandomCheats

        ElseIf Cheats = "ProficionaisSuplentesPorFavor" Then
            'Proficionais
            NJSuplente(1) = "Cheater"
            NJSuplente(2) = "Cheater"
            NJSuplente(3) = "Cheater"
            NJSuplente(4) = "Cheater"
            NJSuplente(5) = "Cheater"

            PRandomCheats = Int(20 * Rnd() + 80)

            PJSuplente(1) = PRandomCheats
            PJSuplente(2) = PRandomCheats
            PJSuplente(3) = PRandomCheats
            PJSuplente(4) = PRandomCheats
            PJSuplente(5) = PRandomCheats

        ElseIf Cheats = "SuperSuplentesPorFavor" Then
            'super
            NJSuplente(1) = "Cheater"
            NJSuplente(2) = "Cheater"
            NJSuplente(3) = "Cheater"
            NJSuplente(4) = "Cheater"
            NJSuplente(5) = "Cheater"

            PJSuplente(1) = 100
            PJSuplente(2) = 100
            PJSuplente(3) = 100
            PJSuplente(4) = 100
            PJSuplente(5) = 100

        ElseIf Cheats = "IniciadosTitularesPorFavor" Then
            'Iniciados
            NJogadorTitular(1) = "Cheater"
            NJogadorTitular(2) = "Cheater"
            NJogadorTitular(3) = "Cheater"
            NJogadorTitular(4) = "Cheater"
            NJogadorTitular(5) = "Cheater"

            PRandomCheats = Int(50 * Rnd())

            PJTitular(1) = PRandomCheats
            PJTitular(2) = PRandomCheats
            PJTitular(3) = PRandomCheats
            PJTitular(4) = PRandomCheats
            PJTitular(5) = PRandomCheats

        ElseIf Cheats = "CadetesTitularesPorFavor" Then
            'Cadetes
            NJogadorTitular(1) = "Cheater"
            NJogadorTitular(2) = "Cheater"
            NJogadorTitular(3) = "Cheater"
            NJogadorTitular(4) = "Cheater"
            NJogadorTitular(5) = "Cheater"

            PRandomCheats = Int(30 * Rnd() + 50)

            PJTitular(1) = PRandomCheats
            PJTitular(2) = PRandomCheats
            PJTitular(3) = PRandomCheats
            PJTitular(4) = PRandomCheats
            PJTitular(5) = PRandomCheats

        ElseIf Cheats = "ProficionaisTitularesPorFavor" Then
            'Proficionais
            NJogadorTitular(1) = "Cheater"
            NJogadorTitular(2) = "Cheater"
            NJogadorTitular(3) = "Cheater"
            NJogadorTitular(4) = "Cheater"
            NJogadorTitular(5) = "Cheater"

            PRandomCheats = Int(20 * Rnd() + 80)

            PJTitular(1) = PRandomCheats
            PJTitular(2) = PRandomCheats
            PJTitular(3) = PRandomCheats
            PJTitular(4) = PRandomCheats
            PJTitular(5) = PRandomCheats

        ElseIf Cheats = "SuperTitularesPorFavor" Then
            'super
            NJogadorTitular(1) = "Cheater"
            NJogadorTitular(2) = "Cheater"
            NJogadorTitular(3) = "Cheater"
            NJogadorTitular(4) = "Cheater"
            NJogadorTitular(5) = "Cheater"

            PJTitular(1) = 100
            PJTitular(2) = 100
            PJTitular(3) = 100
            PJTitular(4) = 100
            PJTitular(5) = 100
        Else
            'Fail
            MsgBox("Acabas-te de ser ponido por tentar burlar o jogo legalmente!")
            VitoriaPorFavor = False
            DinheiroAcumolado = 0

            NJSuplente(1) = "Cheater"
            NJSuplente(2) = "Cheater"
            NJSuplente(3) = "Cheater"
            NJSuplente(4) = "Cheater"
            NJSuplente(5) = "Cheater"

            PJSuplente(1) = 0
            PJSuplente(2) = 0
            PJSuplente(3) = 0
            PJSuplente(4) = 0
            PJSuplente(5) = 0


            NJogadorTitular(1) = "Cheater"
            NJogadorTitular(2) = "Cheater"
            NJogadorTitular(3) = "Cheater"
            NJogadorTitular(4) = "Cheater"
            NJogadorTitular(5) = "Cheater"

            PJTitular(1) = 0
            PJTitular(2) = 0
            PJTitular(3) = 0
            PJTitular(4) = 0
            PJTitular(5) = 0
        End If
    End Sub
End Class