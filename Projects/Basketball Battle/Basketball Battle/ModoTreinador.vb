﻿Public Class ModoTreinador
    Private Sub ModoTreinador_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Inicio do programa'
        If NSemanas = 11 Then
            Button2.Enabled = False
            MsgBox("Parabens acabaste esta época em modo treinador por isso recebes 1000$")
            DinheiroAcumolado += 1000
        End If

        'Apresentação das semanas'
        Label32.Text = NSemanas & "/11"

        'Nomes das Equipas adversárias'
        NomeEquipaMT(1) = NomeEquipa
        NomeEquipaMT(2) = "SulBasket"
        NomeEquipaMT(3) = "TeamFixe"
        NomeEquipaMT(4) = "NorteBasket"
        NomeEquipaMT(5) = "Xavier"
        NomeEquipaMT(6) = "5Team"
        'Nome das Equipas Adversárias'

        'Médias das equipas adversárias'
        MediaMT(1) = MediaUTL
        MediaMT(2) = 45
        MediaMT(3) = 85
        MediaMT(4) = 70
        MediaMT(5) = 63
        MediaMT(6) = 28
        'Médias das equipas adversárias'

        'Total de pontos das equipas adversárias'
        TCestosMT(1) += SCestosMT(1)
        TCestosMT(2) += SCestosMT(2)
        TCestosMT(3) += SCestosMT(3)
        TCestosMT(4) += SCestosMT(4)
        TCestosMT(5) += SCestosMT(5)
        TCestosMT(6) += SCestosMT(6)
        'Total de pontos das equipas adversárias'
        ApresentaraEquipas()
    End Sub
    Sub ApresentaraEquipas()
        'Mostrar a equipa do utilizador (1)'
        Label6.Text = NomeEquipaMT(1)
        Label7.Text = PontosMT(1)
        Label8.Text = TCestosMT(1)
        Label14.Text = MediaMT(1)
        'Mostrar a equipa do utilizador(1)'

        'Mostrar a equipa do CPU (2)'
        Label15.Text = NomeEquipaMT(2)
        Label19.Text = PontosMT(2)
        Label23.Text = TCestosMT(2)
        Label27.Text = MediaMT(2)
        'Mostrar a equipa do CPU (2)'

        'Mostrar a equipa do CPU (3)'
        Label16.Text = NomeEquipaMT(3)
        Label20.Text = PontosMT(3)
        Label24.Text = TCestosMT(3)
        Label28.Text = MediaMT(3)
        'Mostrar a equipa do CPU (3)'

        'Mostrar a equipa do CPU (4)'
        Label17.Text = NomeEquipaMT(4)
        Label21.Text = PontosMT(4)
        Label25.Text = TCestosMT(4)
        Label29.Text = MediaMT(4)
        'Mostrar a equipa do CPU (4)'

        'Mostrar a equipa do CPU (5)'
        Label18.Text = NomeEquipaMT(5)
        Label22.Text = PontosMT(5)
        Label26.Text = TCestosMT(5)
        Label30.Text = MediaMT(5)
        'Mostrar a equipa do CPU (5)'

        'Mostrar a equipa do CPU (6)'
        Label36.Text = NomeEquipaMT(6)
        Label35.Text = PontosMT(6)
        Label34.Text = TCestosMT(6)
        Label33.Text = MediaMT(6)
        'Mostrar a equipa do CPU (6)'
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        'Voltar'
        Me.Close()
        TelaInicial.Show()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        'jogar'
        JogoModoTreinador.Show()
        Me.Close()
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        'Recomeçar'
        TCestosMT(1) = 0
        TCestosMT(2) = 0
        TCestosMT(3) = 0
        TCestosMT(4) = 0
        TCestosMT(5) = 0
        TCestosMT(6) = 0

        PontosMT(1) = 0
        PontosMT(2) = 0
        PontosMT(3) = 0
        PontosMT(4) = 0
        PontosMT(5) = 0
        PontosMT(6) = 0

        NSemanas = 0
        Label32.Text = NSemanas & "/11"

        ApresentaraEquipas()

        MsgBox("Tudo começou de novo, Boa sorte!")
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        'Treino'
        Treino.Show()
        modotreinadorTF = True
    End Sub

    Private Sub Button10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button10.Click
        'Ajuda'
        MsgBox("Aqui pode ver como está a tua jornada, quantos pontos tens, quantos cestos marcas-te no total, a tua média e o nome da tua equipa. Tal como tudo isso mas das outras equipas tambem.")
    End Sub
End Class