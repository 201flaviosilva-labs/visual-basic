﻿Public Class Treino
    Dim Probabilidade, PontosRandom, NPontosRandom, diferençaMedias As String
    Dim NomeEquipaTreino(2) As String
    Dim MediaEquipaTreino(2), PontosEquipaTreino(2) As Double
    Sub pausa(ByVal tempopausa)
        'Tempo de Pausa'
        Dim tempoatual As Date
        Dim tempoparado As Date

        tempoatual = Date.Now()
        tempoparado = tempoatual.AddMilliseconds(tempopausa)

        Do While Date.Now < tempoparado
            Application.DoEvents()
        Loop
    End Sub

    Private Sub Teste_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Loading inicial'
        inicio()
        MediaEquipaTreino(2) = 50
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        PontosEquipaTreino(1) = 0
        PontosEquipaTreino(2) = 0
        Dim count As Byte
        'Criação aleatória de Número máximo de pontos na partida'
        Randomize()
        NPontosRandom = Int(80 * Rnd() + 45)

        'Iniciação do ciclo for (jogo)'
        For count = 1 To NPontosRandom
            pausa(75)

            'Verificação de quem tem a maior média na ajuda dos calculos'
            If MediaEquipaTreino(2) < MediaEquipaTreino(1) Then

                'Obtenção da difença de média e a probabilidade de marcar ponto'
                diferençaMedias = MediaEquipaTreino(1) - MediaEquipaTreino(2)
                Probabilidade = 50 + diferençaMedias

                If Probabilidade >= 95 Then
                    Probabilidade = 95
                End If

                'Criação e verificação de quem maca o ponto, soma dos pontos para as equipas'
                Randomize()
                PontosRandom = Int(100 * Rnd())
                If PontosRandom < Probabilidade Then
                    PontosEquipaTreino(1) += 1
                Else
                    PontosEquipaTreino(2) += 1
                End If
            Else
                'Obtenção da difença de média e a probabilidade de marcar ponto'
                diferençaMedias = MediaEquipaTreino(2) - MediaEquipaTreino(1)
                Probabilidade = 50 + diferençaMedias

                If Probabilidade >= 95 Then
                    Probabilidade = 95
                End If

                'Criação e verificação de quem maca o ponto, soma dos pontos para as equipas'
                Randomize()
                PontosRandom = Int(100 * Rnd())
                If PontosRandom < Probabilidade Then
                    PontosEquipaTreino(2) += 1
                Else
                    PontosEquipaTreino(1) += 1
                End If
            End If

            'Apresentação dos Pontos'
            Label10.Text = PontosEquipaTreino(1)
            Label11.Text = PontosEquipaTreino(2)

            If PontosEquipaTreino(1) > PontosEquipaTreino(2) Then
                'Vitória da Equipa 1'
                Label10.BackColor = Color.Green
                Label1.BackColor = Color.Green
                Label2.BackColor = Color.Red
                Label11.BackColor = Color.Red

            ElseIf PontosEquipaTreino(1) < PontosEquipaTreino(2) Then
                'Derrota da Equipa 1'
                Label10.BackColor = Color.Red
                Label1.BackColor = Color.Red
                Label2.BackColor = Color.Green
                Label11.BackColor = Color.Green

            Else
                'Empate'
                Label10.BackColor = Color.Orange
                Label1.BackColor = Color.Orange
                Label2.BackColor = Color.Orange
                Label11.BackColor = Color.Orange
            End If
        Next

        If PontosEquipaTreino(1) > PontosEquipaTreino(2) Then
            'Vitória da Equipa 1'
            MsgBox("Muito bem venceste!")

        ElseIf PontosEquipaTreino(1) < PontosEquipaTreino(2) Then
            'Derrota da Equipa 1'
            MsgBox("Infelismente perdes-te, tenta para a próxima!")

        Else
            'Empate'
            desempate()
        End If
    End Sub

    Sub desempate()
        Dim count As Byte

        'Criação aleatória de Número máximo de pontos na partida'
        Randomize()
        NPontosRandom = Int(30 * Rnd() + 1)

        'Iniciação do ciclo for (jogo)'
        For count = 1 To NPontosRandom
            pausa(100)

            'Verificação de quem tem a maior média na ajuda dos calculos'
            If MediaEquipaTreino(2) < MediaEquipaTreino(1) Then

                'Obtenção da difença de média e a probabilidade de marcar ponto'
                diferençaMedias = MediaEquipaTreino(1) - MediaEquipaTreino(2)
                Probabilidade = 50 + diferençaMedias

                If Probabilidade >= 95 Then
                    Probabilidade = 95
                End If

                'Criação e verificação de quem maca o ponto, soma dos pontos para as equipas'
                Randomize()
                PontosRandom = Int(100 * Rnd())
                If PontosRandom < Probabilidade Then
                    PontosEquipaTreino(1) += 1
                Else
                    PontosEquipaTreino(2) += 1
                End If
            Else
                'Obtenção da difença de média e a probabilidade de marcar ponto'
                diferençaMedias = MediaEquipaTreino(2) - MediaEquipaTreino(1)
                Probabilidade = 50 + diferençaMedias

                If Probabilidade >= 95 Then
                    Probabilidade = 95
                End If

                'Criação e verificação de quem maca o ponto, soma dos pontos para as equipas'
                Randomize()
                PontosRandom = Int(100 * Rnd())
                If PontosRandom < Probabilidade Then
                    PontosEquipaTreino(2) += 1
                Else
                    PontosEquipaTreino(1) += 1
                End If
            End If

            'Apresentação dos Pontos'
            Label10.Text = PontosEquipaTreino(1)
            Label11.Text = PontosEquipaTreino(2)

            If PontosEquipaTreino(1) > PontosEquipaTreino(2) Then
                'Vitória da Equipa 1'
                Label10.BackColor = Color.Green
                Label1.BackColor = Color.Green
                Label2.BackColor = Color.Red
                Label11.BackColor = Color.Red

            ElseIf PontosEquipaTreino(1) < PontosEquipaTreino(2) Then
                'Derrota da Equipa 1'
                Label10.BackColor = Color.Red
                Label1.BackColor = Color.Red
                Label2.BackColor = Color.Green
                Label11.BackColor = Color.Green

            Else
                'Empate'
                Label10.BackColor = Color.Orange
                Label1.BackColor = Color.Orange
                Label2.BackColor = Color.Orange
                Label11.BackColor = Color.Orange
            End If
        Next

        If PontosEquipaTreino(1) > PontosEquipaTreino(2) Then
            'Vitória da Equipa 1'
            MsgBox("Muito bem venceste!")

        ElseIf PontosEquipaTreino(1) < PontosEquipaTreino(2) Then
            'Derrota da Equipa 1'
            MsgBox("Infelismente perdes-te, tenta para a próxima!")

        Else
            'Empate'
            desempate()
        End If
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        'voltar
        If modotreinadorTF = True Then
            modotreinadorTF = False
            ModoTreinador.Show()
            Me.Close()
        ElseIf TorneioTF = True Then
            TorneioTF = False
            Torneio.Show()
            Me.Close()
        Else
            TelaInicial.Show()
            Me.Close()
        End If
    End Sub

    Private Sub Button10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button10.Click
        'Ajuda'
        MsgBox("Aqui tu podes decidir testar a tua equipa e ver como se irá dar uma média que tu determines para o adeversário.")
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        'Nova Média'
        Dim MadiaEquipaTreino2 As Byte
        MsgBox("Apenas é aceite média Inteiras (NÃO PODEM TER VIRGULAS ',')")
        MadiaEquipaTreino2 = InputBox("Diz que média queres para a equipa adversária!", "Nova Média", "50")
        If MadiaEquipaTreino2 < 1 Or MadiaEquipaTreino2 > 100 Then
            MsgBox("Número inválido!")
        Else
            MediaEquipaTreino(2) = MadiaEquipaTreino2
            inicio()
        End If
    End Sub

    Sub inicio()
        NomeEquipaTreino(1) = NomeEquipa
        MediaEquipaTreino(1) = MediaUTL

        NomeEquipaTreino(2) = "Basquet"
        '1 Equipa Escolhida'
        Label1.Text = NomeEquipaTreino(1)
        Label21.Text = MediaEquipaTreino(1)

        '2 Equipa Escolhida'
        Label2.Text = NomeEquipaTreino(2)
        Label22.Text = MediaEquipaTreino(2)

        If MediaEquipaTreino(1) > MediaEquipaTreino(2) Then
            Label21.BackColor = Color.Green
            Label22.BackColor = Color.Red
        ElseIf MediaEquipaTreino(1) < MediaEquipaTreino(2) Then
            Label21.BackColor = Color.Red
            Label22.BackColor = Color.Green
        End If
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        'Minha Equipa
        treinoTF = True
        MinhaEquipa.Show()
        Me.Close()
    End Sub
End Class