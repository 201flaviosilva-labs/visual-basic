﻿Public Class JogoTorneio
    Dim Media1 As Integer
    Dim PartesJogoLabel, pontos1, pontosUTL, NPontosRandom, PartesJogo, PartesJogoSoma As Byte
    Dim probabilidade, diferençaMedias, PontosRandom As Byte
    Dim NomeEquipa1 As String

    'Media1 0 Media da Equipa do CPU
    'PartesJogoLabel = Parte do jogo que aparece na label
    'Pontos1 = potnos do cpu
    'PontosUTL = Pontos do Utilizador na Partida
    'NpontosRandom = Número de Pontos Random na partida
    'PartesJogoSoma = Soma das partes do jogo
    ' Probabilidade = de pontos
    'DiferençaMedia = Diferença entre as médias para saber quem tem a mior probabilidade de vencer
    'PontosRandom = Ponto criado aleatório, para depois saber em qual equipa irá o ponto
    'NomeEquipa1 = Nome da Equipa do Utilizador

    Sub pausa(ByVal tempopausa)
        'Tempo de Pausa'
        Dim tempoatual As Date
        Dim tempoparado As Date

        tempoatual = Date.Now()
        tempoparado = tempoatual.AddMilliseconds(tempopausa)

        Do While Date.Now < tempoparado
            Application.DoEvents()
        Loop
    End Sub

    Sub desempate()

        Button2.Enabled = False
        Button3.Enabled = False

        pausa(1000)

        Dim count As Byte

        Randomize()
        NPontosRandom = Int(30 * Rnd())

        For count = 1 To NPontosRandom
            pausa(400)

            If Media1 < MediaUTL Then
                diferençaMedias = MediaUTL - Media1
                probabilidade = 50 + diferençaMedias

                If probabilidade >= 95 Then
                    probabilidade = 95
                End If

                Randomize()
                PontosRandom = Int(100 * Rnd())
                If PontosRandom < probabilidade Then
                    pontosUTL += 1
                Else
                    pontos1 += 1
                End If
            Else

                diferençaMedias = Media1 - MediaUTL
                probabilidade = 50 + diferençaMedias

                If probabilidade >= 95 Then
                    probabilidade = 95
                End If

                Randomize()
                PontosRandom = Int(100 * Rnd())
                If PontosRandom < probabilidade Then
                    pontos1 += 1
                Else
                    pontosUTL += 1
                End If
            End If

            Label29.Visible = True
            Label31.Visible = True
            Label32.Visible = True
            Label30.Visible = True
            Label33.Visible = True
            Label34.Visible = True

            Label29.Enabled = True
            Label31.Enabled = True
            Label32.Enabled = True
            Label30.Enabled = True
            Label34.Enabled = True

            Label31.Text = "Pontos " & NomeEquipa
            Label32.Text = "Pontos " & NomeEquipa1
            Label29.Text = pontosUTL
            Label30.Text = pontos1

            If pontosUTL > pontos1 Then
                'Vitória do Utilizador'
                Label29.BackColor = Color.Green
                Label31.BackColor = Color.Green
                Label32.BackColor = Color.Red
                Label30.BackColor = Color.Red
            ElseIf pontosUTL < pontos1 Then
                'Derrota do Utilizador'
                Label29.BackColor = Color.Red
                Label31.BackColor = Color.Red
                Label30.BackColor = Color.Green
                Label32.BackColor = Color.Green
            Else
                'Empate'
                Label29.BackColor = Color.Orange
                Label31.BackColor = Color.Orange
                Label30.BackColor = Color.Orange
                Label32.BackColor = Color.Orange
            End If
        Next

        If pontosUTL > pontos1 Then
            'vitória'
            Label34.Text = "Jogo Acabou"
            MsgBox("Muito bem vences-te, agora tenta jogar contra a próxima!")
            Button3.Enabled = True
        ElseIf pontosUTL < pontos1 Then
            'Derrota'
            Label34.Text = "Jogo Acabou"
            MsgBox("Perdes-te mas não faz mal, tu consegues para a próxima!")
            Button3.Enabled = True
        Else
            MsgBox("A tua equipa empatou contra a do Computador ountra vez, jogo renhido")
            desempate()
        End If
    End Sub

    Private Sub JogoTorneio_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim count As Byte
        'Apresentação do cpu
        For count = 1 To 8
            If JogosVencidos = count Then
                Label1.Text = NomeEquipaTorn(count)
                NomeEquipa1 = NomeEquipaTorn(count)
                Label23.Text = MediaEquipaTorn(count) & "/100"
                Media1 = MediaEquipaTorn(count)

                Label13.Text = Media1 & "/100"
                Label14.Text = Media1 & "/100"
                Label15.Text = Media1 & "/100"
                Label16.Text = Media1 & "/100"
                Label17.Text = Media1 & "/100"

                Label2.Text = NomeEquipa1
                Label4.Text = NomeEquipa1
                Label6.Text = NomeEquipa1
                Label5.Text = NomeEquipa1
                Label3.Text = NomeEquipa1
            End If
        Next

        'Apresentação da equipa do Utilizador'
        Label12.Text = NomeEquipa

        Label22.Text = MediaUTL & "/100"

        Label11.Text = NJogadorTitular(1)
        Label9.Text = NJogadorTitular(2)
        Label7.Text = NJogadorTitular(3)
        Label8.Text = NJogadorTitular(4)
        Label10.Text = NJogadorTitular(5)

        Label24.Text = PJTitular(1) & "/100"
        Label26.Text = PJTitular(2) & "/100"
        Label28.Text = PJTitular(3) & "/100"
        Label27.Text = PJTitular(4) & "/100"
        Label25.Text = PJTitular(5) & "/100"

        'Mostrar a Equipa com maior média'
        If MediaUTL > Media1 Then
            'vitória'
            Label22.BackColor = Color.Green
            Label23.BackColor = Color.Red
        ElseIf MediaUTL < Media1 Then
            'Derrota'
            Label22.BackColor = Color.Red
            Label23.BackColor = Color.Green
        Else
            'Empate'
            Label22.BackColor = Color.Orange
            Label23.BackColor = Color.Orange
        End If
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        'Jogar'
        'Bloquear Buttões'
        Button2.Enabled = False
        Button3.Enabled = False

        pontos1 = 0
        PontosUTL = 0
        PartesJogoLabel = 1

        'Variavel do ciclo "for"'
        Dim count As Byte
        'Variavel do ciclo "for"'

        'Criação aleatória de Número máximo de pontos na partida'
        Randomize()
        NPontosRandom = Int(80 * Rnd() + 45)
        PartesJogo = NPontosRandom / 4
        'Criação aleatória de Número máximo de pontos na partida'

        'Iniciação do ciclo for (jogo)'
        For count = 1 To NPontosRandom
            pausa(400)
            'Iniciação do ciclo for (jogo)'
            PartesJogoSoma += 1
            If PartesJogoSoma = PartesJogo Then
                PartesJogoSoma = 0
                PartesJogoLabel += 1
                Label34.Text = "Parte " & PartesJogoLabel
            End If

            'Verificação de quem tem a maior média na ajuda dos calculos'
            If Media1 < MediaUTL Then
                'Verificação de quem tem a maior média na ajuda dos calculos'

                'Obtenção da difença de média e a probabilidade de marcar ponto'
                diferençaMedias = MediaUTL - Media1
                probabilidade = 50 + diferençaMedias
                'Obtenção da difença de média e a probabilidade de marcar ponto'

                'Não permitir que a probabilidade seja maior que 100'
                If probabilidade >= 95 Then
                    probabilidade = 95
                End If
                'Não permitir que a probabilidade seja maior que 100'

                'Criação e verificação de quem maca o ponto, soma dos pontos para as equipas'
                Randomize()
                PontosRandom = Int(100 * Rnd())
                If PontosRandom < probabilidade Then
                    pontosUTL += 1
                Else
                    pontos1 += 1
                End If
                'Criação e verificação de quem maca o ponto, soma dos pontos para as equipas'
            Else
                'Obtenção da difença de média e a probabilidade de marcar ponto'
                diferençaMedias = Media1 - MediaUTL
                probabilidade = 50 + diferençaMedias
                'Obtenção da difença de média e a probabilidade de marcar ponto'

                'Não permitir que a probabilidade seja maior que 100'
                If probabilidade >= 95 Then
                    probabilidade = 95
                End If
                'Não permitir que a probabilidade seja maior que 100'

                'Criação e verificação de quem maca o ponto, soma dos pontos para as equipas'
                Randomize()
                PontosRandom = Int(100 * Rnd())
                If PontosRandom < probabilidade Then
                    pontos1 += 1
                Else
                    pontosUTL += 1
                End If
                'Criação e verificação de quem maca o ponto, soma dos pontos para as equipas'
            End If

            'Deixar Visible e deisponivel as labels dos pontos, e Marcação dos pontos e Indentificação da Equipa e seus respetivos pontos'
            Label29.Visible = True
            Label31.Visible = True
            Label32.Visible = True
            Label30.Visible = True
            Label33.Visible = True
            Label34.Visible = True

            Label29.Enabled = True
            Label31.Enabled = True
            Label32.Enabled = True
            Label30.Enabled = True
            Label34.Enabled = True

            Label31.Text = "Pontos " & NomeEquipa
            Label32.Text = "Pontos " & NomeEquipa1
            Label29.Text = PontosUTL
            Label30.Text = pontos1
            'Deixar Visible e deisponivel as labels dos pontos, e Marcação dos pontos e Indentificação da Equipa e seus respetivos pontos'

            'Colorir consoante a vitória e a derrota da equipa'
            If pontosUTL > pontos1 Then
                'Vitória do Utilizador'
                Label29.BackColor = Color.Green
                Label31.BackColor = Color.Green
                Label32.BackColor = Color.Red
                Label30.BackColor = Color.Red
            ElseIf pontosUTL < pontos1 Then
                'Derrota do Utilizador'
                Label29.BackColor = Color.Red
                Label31.BackColor = Color.Red
                Label30.BackColor = Color.Green
                Label32.BackColor = Color.Green
            Else
                'Empate'
                Label29.BackColor = Color.Orange
                Label31.BackColor = Color.Orange
                Label30.BackColor = Color.Orange
                Label32.BackColor = Color.Orange
            End If
            'Colorir consoante a vitória e a derrota da equipa'
        Next

        'Aumento do número de Vitórias/Derrotas e a soma do dinheiro'
        If pontosUTL > pontos1 Then
            'vitória'
            Label34.Text = "Jogo Acabou"
            JogosVencidos += 1
            MsgBox("Muito bem vences-te, agora tenta jogar contra a próxima!")
        ElseIf pontosUTL < pontos1 Then
            'Derrota'
            Label34.Text = "Jogo Acabou"
            MsgBox("Perdes-te mas não faz mal, tu consegues para a próxima!")
        Else
            desempate()
            Label34.Text = "Desempate"
        End If
        'Aumento do número de Vitórias/Derrotas e a soma do dinheiro'

        'Desbloquear Botões'
        Button3.Enabled = True

    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        'Voltar'
        Torneio.Show()
        Me.Close()
    End Sub

    Private Sub Button10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button10.Click
        'Ajuda'
        MsgBox("Aqui tens o teu jogo para poderes avançar no torneio.")
        MsgBox("Aqui é como no 'Jogo rápido' mas aqui as coisas vão ficando cada vez mais quentes, boa sorte")
    End Sub
End Class