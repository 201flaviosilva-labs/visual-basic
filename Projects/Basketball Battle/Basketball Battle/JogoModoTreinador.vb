﻿Public Class JogoModoTreinador
    Dim Probabilidade, PontosRandom, pontos2, Pontos1, NPontosRandom, média2, diferençaMedias As String
    Dim RandomEquipasMT, EquipasBloqueadasMT, NaoEliminarEstaVariavel As Byte
    Dim EquipaEscolhidaMT(6) As Boolean
    Dim NomeEquipaEscolhidaPorOrdemMT(6) As String
    Dim MediaEquipaEscolhidaPorOrdemMT(6), PontosEquipaEscolhidaPorOrdemMT(6) As Double

    'EquipasBloqueadasMT= Soma das equipa bloqueadas'
    'RandomEquipasMT= Equipa escolhida Aleatóriamente'
    'EquipaEscolhidaMT= Equipa escolhida Bloqueada ou não'
    'NomeEquipaEscolhidaPorOrdemMT= Nome da esquipa Por Ordem de seleção'
    'MediaEquipaEscolhidaPorOrdemMT= Média da esquipa Por Ordem de seleção'
    'PontosEquipaEscolhidaPorOrdemMT(6) = Pontos da equipa por Ordem De Seleção
    'NaoEliminarEstaVariavel = não serve para nada, mas não a elemines

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        'Jogar'
        pausa(100)

        PontosEquipaEscolhidaPorOrdemMT(1) = 0
        PontosEquipaEscolhidaPorOrdemMT(2) = 0

        PontosEquipaEscolhidaPorOrdemMT(3) = 0
        PontosEquipaEscolhidaPorOrdemMT(4) = 0

        PontosEquipaEscolhidaPorOrdemMT(5) = 0
        PontosEquipaEscolhidaPorOrdemMT(6) = 0

        'Bloquear Botões'
        Button1.Enabled = False
        Button2.Enabled = False
        Button9.Enabled = False

        jogo1()
        jogo2()
        jogo3()

        NSemanas += 1
    End Sub

    Sub pausa(ByVal tempopausa)
        'Tempo de Pausa'
        Dim tempoatual As Date
        Dim tempoparado As Date

        tempoatual = Date.Now()
        tempoparado = tempoatual.AddMilliseconds(tempopausa)

        Do While Date.Now < tempoparado
            Application.DoEvents()
        Loop
    End Sub

    Sub desempate1()
        pausa(1000)
        'Desemparar o jogo, caso de empate'
        Dim count As Byte

        Randomize()
        NPontosRandom = Int(30 * Rnd())

        'Iniciação do ciclo for (jogo)'
        For count = 1 To NPontosRandom
            pausa(300)

            'Verificação de quem tem a maior média na ajuda dos calculos'
            If MediaEquipaEscolhidaPorOrdemMT(2) < MediaEquipaEscolhidaPorOrdemMT(1) Then

                'Obtenção da difença de média e a probabilidade de marcar ponto'
                diferençaMedias = MediaEquipaEscolhidaPorOrdemMT(1) - MediaEquipaEscolhidaPorOrdemMT(2)
                Probabilidade = 50 + diferençaMedias

                If Probabilidade >= 95 Then
                    Probabilidade = 95
                End If

                'Criação e verificação de quem maca o ponto, soma dos pontos para as equipas'
                Randomize()
                PontosRandom = Int(100 * Rnd())
                If PontosRandom < Probabilidade Then
                    PontosEquipaEscolhidaPorOrdemMT(1) += 1
                Else
                    PontosEquipaEscolhidaPorOrdemMT(2) += 1
                End If
            Else
                'Obtenção da difença de média e a probabilidade de marcar ponto'
                diferençaMedias = MediaEquipaEscolhidaPorOrdemMT(2) - MediaEquipaEscolhidaPorOrdemMT(1)
                Probabilidade = 50 + diferençaMedias

                If Probabilidade >= 95 Then
                    Probabilidade = 95
                End If

                'Criação e verificação de quem maca o ponto, soma dos pontos para as equipas'
                Randomize()
                PontosRandom = Int(100 * Rnd())
                If PontosRandom < Probabilidade Then
                    PontosEquipaEscolhidaPorOrdemMT(2) += 1
                Else
                    PontosEquipaEscolhidaPorOrdemMT(1) += 1
                End If
            End If

            'Apresentação dos Pontos'
            Label10.Text = PontosEquipaEscolhidaPorOrdemMT(1)
            Label11.Text = PontosEquipaEscolhidaPorOrdemMT(2)

            'Colorir consoante a vitória e a derrota da equipa'
            If PontosEquipaEscolhidaPorOrdemMT(1) > PontosEquipaEscolhidaPorOrdemMT(2) Then
                'Vitória da Equipa 1'
                Label10.BackColor = Color.Green
                Label1.BackColor = Color.Green
                Label2.BackColor = Color.Red
                Label11.BackColor = Color.Red

            ElseIf PontosEquipaEscolhidaPorOrdemMT(1) < PontosEquipaEscolhidaPorOrdemMT(2) Then
                'Derrota da Equipa 1'
                Label10.BackColor = Color.Red
                Label1.BackColor = Color.Red
                Label2.BackColor = Color.Green
                Label11.BackColor = Color.Green
            Else
                'Empate'
                Label10.BackColor = Color.Orange
                Label1.BackColor = Color.Orange
                Label2.BackColor = Color.Orange
                Label11.BackColor = Color.Orange
            End If
        Next
        'Detetar o Vencedor'
        If PontosEquipaEscolhidaPorOrdemMT(1) = PontosEquipaEscolhidaPorOrdemMT(2) Then
            desempate1()
        End If
        If PontosEquipaEscolhidaPorOrdemMT(1) > PontosEquipaEscolhidaPorOrdemMT(2) Then
            'Aumentar pontos ao vencedor'
            For count = 1 To 6
                If NomeEquipaEscolhidaPorOrdemMT(1) = NomeEquipaMT(count) Then
                    PontosMT(count) += 2
                    SCestosMT(count) = PontosEquipaEscolhidaPorOrdemMT(1)
                    'Derrota'
                    For count2 = 1 To 6
                        If NomeEquipaEscolhidaPorOrdemMT(2) = NomeEquipaMT(count2) Then
                            SCestosMT(count2) = PontosEquipaEscolhidaPorOrdemMT(2)
                        End If
                    Next
                End If
            Next
        End If

        If PontosEquipaEscolhidaPorOrdemMT(1) < PontosEquipaEscolhidaPorOrdemMT(2) Then
            'Aumentar pontos ao vencedor'
            For count = 1 To 6
                If NomeEquipaEscolhidaPorOrdemMT(2) = NomeEquipaMT(count) Then
                    PontosMT(count) += 2
                    SCestosMT(count) = PontosEquipaEscolhidaPorOrdemMT(2)
                    'Derrota'
                    For count2 = 1 To 6
                        If NomeEquipaEscolhidaPorOrdemMT(1) = NomeEquipaMT(count2) Then
                            SCestosMT(count2) = PontosEquipaEscolhidaPorOrdemMT(1)
                        End If
                    Next
                End If
            Next
        End If
    End Sub

    Sub desempate2()
        pausa(1000)
        'Desemparar o jogo, caso de empate'
        Dim count As Byte

        Randomize()
        NPontosRandom = Int(30 * Rnd())

        'Iniciação do ciclo for (jogo)'
        For count = 1 To NPontosRandom
            pausa(300)

            'Verificação de quem tem a maior média na ajuda dos calculos'
            If MediaEquipaEscolhidaPorOrdemMT(4) < MediaEquipaEscolhidaPorOrdemMT(3) Then

                'Obtenção da difença de média e a probabilidade de marcar ponto'
                diferençaMedias = MediaEquipaEscolhidaPorOrdemMT(3) - MediaEquipaEscolhidaPorOrdemMT(4)
                Probabilidade = 50 + diferençaMedias

                If Probabilidade >= 95 Then
                    Probabilidade = 95
                End If

                'Criação e verificação de quem maca o ponto, soma dos pontos para as equipas'
                Randomize()
                PontosRandom = Int(100 * Rnd())
                If PontosRandom < Probabilidade Then
                    PontosEquipaEscolhidaPorOrdemMT(3) += 1
                Else
                    PontosEquipaEscolhidaPorOrdemMT(4) += 1
                End If
            Else
                'Obtenção da difença de média e a probabilidade de marcar ponto'
                diferençaMedias = MediaEquipaEscolhidaPorOrdemMT(4) - MediaEquipaEscolhidaPorOrdemMT(3)
                Probabilidade = 50 + diferençaMedias

                If Probabilidade >= 95 Then
                    Probabilidade = 95
                End If

                'Criação e verificação de quem maca o ponto, soma dos pontos para as equipas'
                Randomize()
                PontosRandom = Int(100 * Rnd())
                If PontosRandom < Probabilidade Then
                    PontosEquipaEscolhidaPorOrdemMT(4) += 1
                Else
                    PontosEquipaEscolhidaPorOrdemMT(3) += 1
                End If
            End If

            'Apresentação dos Pontos'
            Label12.Text = PontosEquipaEscolhidaPorOrdemMT(3)
            Label13.Text = PontosEquipaEscolhidaPorOrdemMT(4)

            'Colorir consoante a vitória e a derrota da equipa'
            If PontosEquipaEscolhidaPorOrdemMT(3) > PontosEquipaEscolhidaPorOrdemMT(4) Then
                'Vitória da Equipa 1'
                Label12.BackColor = Color.Green
                Label3.BackColor = Color.Green
                Label4.BackColor = Color.Red
                Label13.BackColor = Color.Red
            ElseIf PontosEquipaEscolhidaPorOrdemMT(3) < PontosEquipaEscolhidaPorOrdemMT(4) Then
                'Derrota da Equipa 1'
                Label12.BackColor = Color.Red
                Label3.BackColor = Color.Red
                Label4.BackColor = Color.Green
                Label13.BackColor = Color.Green
            Else
                'Empate'

                Label12.BackColor = Color.Orange
                Label3.BackColor = Color.Orange
                Label4.BackColor = Color.Orange
                Label13.BackColor = Color.Orange
            End If
        Next
        'Detetar o Vencedor'
        If PontosEquipaEscolhidaPorOrdemMT(3) = PontosEquipaEscolhidaPorOrdemMT(4) Then
            desempate2()
        End If
        If PontosEquipaEscolhidaPorOrdemMT(3) > PontosEquipaEscolhidaPorOrdemMT(4) Then
            'Aumentar pontos ao vencedor'
            For count = 1 To 6
                If NomeEquipaEscolhidaPorOrdemMT(3) = NomeEquipaMT(count) Then
                    PontosMT(count) += 2
                    SCestosMT(count) = PontosEquipaEscolhidaPorOrdemMT(3)
                    'Derrota'
                    For count2 = 1 To 6
                        If NomeEquipaEscolhidaPorOrdemMT(4) = NomeEquipaMT(count2) Then
                            SCestosMT(count2) = PontosEquipaEscolhidaPorOrdemMT(4)
                        End If
                    Next
                End If
            Next
        End If

        If PontosEquipaEscolhidaPorOrdemMT(3) < PontosEquipaEscolhidaPorOrdemMT(4) Then
            'Aumentar pontos ao vencedor'
            For count = 1 To 6
                If NomeEquipaEscolhidaPorOrdemMT(4) = NomeEquipaMT(count) Then
                    PontosMT(count) += 2
                    SCestosMT(count) = PontosEquipaEscolhidaPorOrdemMT(4)
                    'Derrota'
                    For count2 = 1 To 6
                        If NomeEquipaEscolhidaPorOrdemMT(3) = NomeEquipaMT(count2) Then
                            SCestosMT(count2) = PontosEquipaEscolhidaPorOrdemMT(3)
                        End If
                    Next
                End If
            Next
        End If
    End Sub

    Sub desempate3()
        pausa(1000)

        'Desemparar o jogo, caso de empate'
        Dim count As Byte
        Randomize()
        NPontosRandom = Int(30 * Rnd())

        'Iniciação do ciclo for (jogo)'
        For count = 1 To NPontosRandom
            pausa(300)

            'Verificação de quem tem a maior média na ajuda dos calculos'
            If MediaEquipaEscolhidaPorOrdemMT(6) < MediaEquipaEscolhidaPorOrdemMT(5) Then

                'Obtenção da difença de média e a probabilidade de marcar ponto'
                diferençaMedias = MediaEquipaEscolhidaPorOrdemMT(5) - MediaEquipaEscolhidaPorOrdemMT(6)
                Probabilidade = 50 + diferençaMedias

                If Probabilidade >= 95 Then
                    Probabilidade = 95
                End If

                'Criação e verificação de quem maca o ponto, soma dos pontos para as equipas'
                Randomize()
                PontosRandom = Int(100 * Rnd())
                If PontosRandom < Probabilidade Then
                    PontosEquipaEscolhidaPorOrdemMT(5) += 1
                Else
                    PontosEquipaEscolhidaPorOrdemMT(6) += 1
                End If
            Else
                'Obtenção da difença de média e a probabilidade de marcar ponto'
                diferençaMedias = MediaEquipaEscolhidaPorOrdemMT(6) - MediaEquipaEscolhidaPorOrdemMT(5)
                Probabilidade = 50 + diferençaMedias

                If Probabilidade >= 95 Then
                    Probabilidade = 95
                End If

                'Criação e verificação de quem maca o ponto, soma dos pontos para as equipas'
                Randomize()
                PontosRandom = Int(100 * Rnd())
                If PontosRandom < Probabilidade Then
                    PontosEquipaEscolhidaPorOrdemMT(6) += 1
                Else
                    PontosEquipaEscolhidaPorOrdemMT(5) += 1
                End If
            End If

            'Apresentação dos Pontos'
            Label14.Text = PontosEquipaEscolhidaPorOrdemMT(5)
            Label15.Text = PontosEquipaEscolhidaPorOrdemMT(6)

            'Colorir consoante a vitória e a derrota da equipa'
            If PontosEquipaEscolhidaPorOrdemMT(5) > PontosEquipaEscolhidaPorOrdemMT(6) Then
                'Vitória da Equipa Esquerda'
                Label14.BackColor = Color.Green
                Label5.BackColor = Color.Green
                Label6.BackColor = Color.Red
                Label15.BackColor = Color.Red

            ElseIf PontosEquipaEscolhidaPorOrdemMT(5) < PontosEquipaEscolhidaPorOrdemMT(6) Then
                'Derrota da Equipa Esquerda'
                Label14.BackColor = Color.Red
                Label5.BackColor = Color.Red
                Label6.BackColor = Color.Green
                Label15.BackColor = Color.Green

            Else
                'Empate'
                Label14.BackColor = Color.Orange
                Label5.BackColor = Color.Orange
                Label6.BackColor = Color.Orange
                Label15.BackColor = Color.Orange
            End If
        Next

        'Detetar o Vencedor'
        If PontosEquipaEscolhidaPorOrdemMT(5) = PontosEquipaEscolhidaPorOrdemMT(6) Then
            desempate3()
        End If

        If PontosEquipaEscolhidaPorOrdemMT(5) > PontosEquipaEscolhidaPorOrdemMT(6) Then
            'Disponibilisar "Voltar"'
            Button2.Enabled = True
            'Aumentar pontos ao vencedor'
            For count = 1 To 6
                If NomeEquipaEscolhidaPorOrdemMT(5) = NomeEquipaMT(count) Then
                    PontosMT(count) += 2
                    SCestosMT(count) = PontosEquipaEscolhidaPorOrdemMT(5)
                    'Derrota'
                    For count2 = 1 To 6
                        If NomeEquipaEscolhidaPorOrdemMT(6) = NomeEquipaMT(count2) Then
                            SCestosMT(count2) = PontosEquipaEscolhidaPorOrdemMT(6)
                        End If
                    Next
                End If
            Next
            Button2.Enabled = True
            Button9.Enabled = True
        End If

        If PontosEquipaEscolhidaPorOrdemMT(5) < PontosEquipaEscolhidaPorOrdemMT(6) Then
            'Disponibilisar "Voltar"'
            Button2.Enabled = True
            'Aumentar pontos ao vencedor'
            For count = 1 To 6
                If NomeEquipaEscolhidaPorOrdemMT(6) = NomeEquipaMT(count) Then
                    PontosMT(count) += 2
                    SCestosMT(count) = PontosEquipaEscolhidaPorOrdemMT(6)
                    'Derrota'
                    For count2 = 1 To 6
                        If NomeEquipaEscolhidaPorOrdemMT(5) = NomeEquipaMT(count2) Then
                            SCestosMT(count2) = PontosEquipaEscolhidaPorOrdemMT(5)
                        End If
                    Next
                End If
            Next
            Button2.Enabled = True
            Button9.Enabled = True
        End If
    End Sub

    Private Sub JogoModoTreinador_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Loading Inicial'
        Button2.Enabled = False
        RandomPartidas()
        'Partida 1'
        If MediaEquipaEscolhidaPorOrdemMT(1) > MediaEquipaEscolhidaPorOrdemMT(2) Then
            Label21.BackColor = Color.Green
            Label1.BackColor = Color.Green
            Label2.BackColor = Color.Red
            Label22.BackColor = Color.Red
        ElseIf MediaEquipaEscolhidaPorOrdemMT(1) < MediaEquipaEscolhidaPorOrdemMT(2) Then
            Label21.BackColor = Color.Red
            Label1.BackColor = Color.Red
            Label2.BackColor = Color.Green
            Label22.BackColor = Color.Green
        Else
            Label21.BackColor = Color.Orange
            Label22.BackColor = Color.Orange
        End If

        'Partida 2'
        If MediaEquipaEscolhidaPorOrdemMT(3) > MediaEquipaEscolhidaPorOrdemMT(4) Then
            Label23.BackColor = Color.Green
            Label3.BackColor = Color.Green
            Label4.BackColor = Color.Red
            Label24.BackColor = Color.Red
        ElseIf MediaEquipaEscolhidaPorOrdemMT(3) < MediaEquipaEscolhidaPorOrdemMT(4) Then
            Label23.BackColor = Color.Red
            Label3.BackColor = Color.Red
            Label4.BackColor = Color.Green
            Label24.BackColor = Color.Green
        Else
            Label23.BackColor = Color.Orange
            Label24.BackColor = Color.Orange
        End If

        'Partida 3'
        If MediaEquipaEscolhidaPorOrdemMT(5) > MediaEquipaEscolhidaPorOrdemMT(6) Then
            Label25.BackColor = Color.Green
            Label5.BackColor = Color.Green
            Label6.BackColor = Color.Red
            Label26.BackColor = Color.Red
        ElseIf MediaEquipaEscolhidaPorOrdemMT(5) < MediaEquipaEscolhidaPorOrdemMT(6) Then
            Label25.BackColor = Color.Red
            Label5.BackColor = Color.Red
            Label6.BackColor = Color.Green
            Label26.BackColor = Color.Green
        Else
            Label25.BackColor = Color.Orange
            Label26.BackColor = Color.Orange
        End If
    End Sub

    Sub RandomPartidas()
        Randomize()
        Dim count As Byte

        For count = 1 To 6
            EquipaEscolhidaMT(count) = False
        Next
        Do Until ((EquipaEscolhidaMT(1) = True) And (EquipaEscolhidaMT(2) = True) And (EquipaEscolhidaMT(3) = True) And (EquipaEscolhidaMT(4) = True) And (EquipaEscolhidaMT(5) = True) And (EquipaEscolhidaMT(6) = True))
            RandomEquipasMT = Int(6 * Rnd() + 1)
            If ((RandomEquipasMT = 1) And (EquipaEscolhidaMT(1) = False)) Then
                EquipasBloqueadasMT += 1
                EquipaEscolhidaMT(1) = True
                NomeEquipaEscolhidaPorOrdemMT(EquipasBloqueadasMT) = NomeEquipa
                MediaEquipaEscolhidaPorOrdemMT(EquipasBloqueadasMT) = MediaUTL

            ElseIf ((RandomEquipasMT = 2) And (EquipaEscolhidaMT(2) = False)) Then
                EquipasBloqueadasMT += 1
                EquipaEscolhidaMT(2) = True
                NomeEquipaEscolhidaPorOrdemMT(EquipasBloqueadasMT) = NomeEquipaMT(2)
                MediaEquipaEscolhidaPorOrdemMT(EquipasBloqueadasMT) = MediaMT(2)

            ElseIf ((RandomEquipasMT = 3) And (EquipaEscolhidaMT(3) = False)) Then
                EquipasBloqueadasMT += 1
                EquipaEscolhidaMT(3) = True
                NomeEquipaEscolhidaPorOrdemMT(EquipasBloqueadasMT) = NomeEquipaMT(3)
                MediaEquipaEscolhidaPorOrdemMT(EquipasBloqueadasMT) = MediaMT(3)

            ElseIf ((RandomEquipasMT = 4) And (EquipaEscolhidaMT(4) = False)) Then
                EquipasBloqueadasMT += 1
                EquipaEscolhidaMT(4) = True
                NomeEquipaEscolhidaPorOrdemMT(EquipasBloqueadasMT) = NomeEquipaMT(4)
                MediaEquipaEscolhidaPorOrdemMT(EquipasBloqueadasMT) = MediaMT(4)

            ElseIf ((RandomEquipasMT = 5) And (EquipaEscolhidaMT(5) = False)) Then
                EquipasBloqueadasMT += 1
                EquipaEscolhidaMT(5) = True
                NomeEquipaEscolhidaPorOrdemMT(EquipasBloqueadasMT) = NomeEquipaMT(5)
                MediaEquipaEscolhidaPorOrdemMT(EquipasBloqueadasMT) = MediaMT(5)

            ElseIf ((RandomEquipasMT = 6) And (EquipaEscolhidaMT(6) = False)) Then
                EquipasBloqueadasMT += 1
                EquipaEscolhidaMT(6) = True
                NomeEquipaEscolhidaPorOrdemMT(EquipasBloqueadasMT) = NomeEquipaMT(6)
                MediaEquipaEscolhidaPorOrdemMT(EquipasBloqueadasMT) = MediaMT(6)
            Else
                NaoEliminarEstaVariavel += 1
            End If

        Loop

        'Apresentação Equipas'
        '1 Equipa Escolhida'
        Label1.Text = NomeEquipaEscolhidaPorOrdemMT(1)
        Label21.Text = MediaEquipaEscolhidaPorOrdemMT(1)

        '2 Equipa Escolhida'
        Label2.Text = NomeEquipaEscolhidaPorOrdemMT(2)
        Label22.Text = MediaEquipaEscolhidaPorOrdemMT(2)

        '3 Equipa Escolhida'
        Label3.Text = NomeEquipaEscolhidaPorOrdemMT(3)
        Label23.Text = MediaEquipaEscolhidaPorOrdemMT(3)

        '4 Equipa Escolhida'
        Label4.Text = NomeEquipaEscolhidaPorOrdemMT(4)
        Label24.Text = MediaEquipaEscolhidaPorOrdemMT(4)

        '5 Equipa Escolhida'
        Label5.Text = NomeEquipaEscolhidaPorOrdemMT(5)
        Label25.Text = MediaEquipaEscolhidaPorOrdemMT(5)

        '6 Equipa Escolhida'
        Label6.Text = NomeEquipaEscolhidaPorOrdemMT(6)
        Label26.Text = MediaEquipaEscolhidaPorOrdemMT(6)
    End Sub

    Sub jogo1()
        'Variavel do ciclo "for"'
        Dim count As Byte

        'Criação aleatória de Número máximo de pontos na partida'
        Randomize()
        NPontosRandom = Int(80 * Rnd() + 45)

        'Iniciação do ciclo for (jogo)'
        For count = 1 To NPontosRandom
            pausa(250)

            'Verificação de quem tem a maior média na ajuda dos calculos'
            If MediaEquipaEscolhidaPorOrdemMT(2) < MediaEquipaEscolhidaPorOrdemMT(1) Then

                'Obtenção da difença de média e a probabilidade de marcar ponto'
                diferençaMedias = MediaEquipaEscolhidaPorOrdemMT(1) - MediaEquipaEscolhidaPorOrdemMT(2)
                Probabilidade = 50 + diferençaMedias

                If Probabilidade >= 95 Then
                    Probabilidade = 95
                End If

                'Criação e verificação de quem maca o ponto, soma dos pontos para as equipas'
                Randomize()
                PontosRandom = Int(100 * Rnd())
                If PontosRandom < Probabilidade Then
                    PontosEquipaEscolhidaPorOrdemMT(1) += 1
                Else
                    PontosEquipaEscolhidaPorOrdemMT(2) += 1
                End If
            Else
                'Obtenção da difença de média e a probabilidade de marcar ponto'
                diferençaMedias = MediaEquipaEscolhidaPorOrdemMT(2) - MediaEquipaEscolhidaPorOrdemMT(1)
                Probabilidade = 50 + diferençaMedias

                If Probabilidade >= 95 Then
                    Probabilidade = 95
                End If

                'Criação e verificação de quem maca o ponto, soma dos pontos para as equipas'
                Randomize()
                PontosRandom = Int(100 * Rnd())
                If PontosRandom < Probabilidade Then
                    PontosEquipaEscolhidaPorOrdemMT(2) += 1
                Else
                    PontosEquipaEscolhidaPorOrdemMT(1) += 1
                End If
            End If

            'Apresentação dos Pontos'
            Label10.Text = PontosEquipaEscolhidaPorOrdemMT(1)
            Label11.Text = PontosEquipaEscolhidaPorOrdemMT(2)

            If PontosEquipaEscolhidaPorOrdemMT(1) > PontosEquipaEscolhidaPorOrdemMT(2) Then
                'Vitória da Equipa 1'
                Label10.BackColor = Color.Green
                Label1.BackColor = Color.Green
                Label2.BackColor = Color.Red
                Label11.BackColor = Color.Red

            ElseIf PontosEquipaEscolhidaPorOrdemMT(1) < PontosEquipaEscolhidaPorOrdemMT(2) Then
                'Derrota da Equipa 1'
                Label10.BackColor = Color.Red
                Label1.BackColor = Color.Red
                Label2.BackColor = Color.Green
                Label11.BackColor = Color.Green

            Else
                'Empate'
                Label10.BackColor = Color.Orange
                Label1.BackColor = Color.Orange
                Label2.BackColor = Color.Orange
                Label11.BackColor = Color.Orange
            End If
        Next

        'Detetar o Vencedor'
        If PontosEquipaEscolhidaPorOrdemMT(1) = PontosEquipaEscolhidaPorOrdemMT(2) Then
            desempate1()
        End If
        If PontosEquipaEscolhidaPorOrdemMT(1) > PontosEquipaEscolhidaPorOrdemMT(2) Then
            'Aumentar pontos ao vencedor'
            For count = 1 To 6
                If NomeEquipaEscolhidaPorOrdemMT(1) = NomeEquipaMT(count) Then
                    PontosMT(count) += 2
                    SCestosMT(count) = PontosEquipaEscolhidaPorOrdemMT(1)
                    'Derrota'
                    For count2 = 1 To 6
                        If NomeEquipaEscolhidaPorOrdemMT(2) = NomeEquipaMT(count2) Then
                            SCestosMT(count2) = PontosEquipaEscolhidaPorOrdemMT(2)
                        End If
                    Next
                End If
            Next
        End If

        If PontosEquipaEscolhidaPorOrdemMT(1) < PontosEquipaEscolhidaPorOrdemMT(2) Then
            'Aumentar pontos ao vencedor'
            For count = 1 To 6
                If NomeEquipaEscolhidaPorOrdemMT(2) = NomeEquipaMT(count) Then
                    PontosMT(count) += 2
                    SCestosMT(count) = PontosEquipaEscolhidaPorOrdemMT(2)
                    'Derrota'
                    For count2 = 1 To 6
                        If NomeEquipaEscolhidaPorOrdemMT(1) = NomeEquipaMT(count2) Then
                            SCestosMT(count2) = PontosEquipaEscolhidaPorOrdemMT(1)
                        End If
                    Next
                End If
            Next
        End If
    End Sub

    Sub jogo2()
        pausa(1000)
        'Variavel do ciclo "for"'
        Dim count As Byte

        'Criação aleatória de Número máximo de pontos na partida'
        Randomize()
        NPontosRandom = Int(80 * Rnd() + 45)

        'Iniciação do ciclo for (jogo)'
        For count = 1 To NPontosRandom
            pausa(250)

            'Verificação de quem tem a maior média na ajuda dos calculos'
            If MediaEquipaEscolhidaPorOrdemMT(4) < MediaEquipaEscolhidaPorOrdemMT(3) Then

                'Obtenção da difença de média e a probabilidade de marcar ponto'
                diferençaMedias = MediaEquipaEscolhidaPorOrdemMT(3) - MediaEquipaEscolhidaPorOrdemMT(4)
                Probabilidade = 50 + diferençaMedias

                If Probabilidade >= 95 Then
                    Probabilidade = 95
                End If

                'Criação e verificação de quem maca o ponto, soma dos pontos para as equipas'
                Randomize()
                PontosRandom = Int(100 * Rnd())
                If PontosRandom < Probabilidade Then
                    PontosEquipaEscolhidaPorOrdemMT(3) += 1
                Else
                    PontosEquipaEscolhidaPorOrdemMT(4) += 1
                End If
            Else
                'Obtenção da difença de média e a probabilidade de marcar ponto'
                diferençaMedias = MediaEquipaEscolhidaPorOrdemMT(4) - MediaEquipaEscolhidaPorOrdemMT(3)
                Probabilidade = 50 + diferençaMedias

                If Probabilidade >= 95 Then
                    Probabilidade = 95
                End If

                'Criação e verificação de quem maca o ponto, soma dos pontos para as equipas'
                Randomize()
                PontosRandom = Int(100 * Rnd())
                If PontosRandom < Probabilidade Then
                    PontosEquipaEscolhidaPorOrdemMT(4) += 1
                Else
                    PontosEquipaEscolhidaPorOrdemMT(3) += 1
                End If
            End If

            'Apresentação dos Pontos'
            Label12.Text = PontosEquipaEscolhidaPorOrdemMT(3)
            Label13.Text = PontosEquipaEscolhidaPorOrdemMT(4)

            'Colorir consoante a vitória e a derrota da equipa'
            If PontosEquipaEscolhidaPorOrdemMT(3) > PontosEquipaEscolhidaPorOrdemMT(4) Then
                'Vitória da Equipa 1'
                Label12.BackColor = Color.Green
                Label3.BackColor = Color.Green
                Label4.BackColor = Color.Red
                Label13.BackColor = Color.Red

            ElseIf PontosEquipaEscolhidaPorOrdemMT(3) < PontosEquipaEscolhidaPorOrdemMT(4) Then
                'Derrota da Equipa 1'
                Label12.BackColor = Color.Red
                Label3.BackColor = Color.Red
                Label4.BackColor = Color.Green
                Label13.BackColor = Color.Green

            Else
                'Empate'

                Label12.BackColor = Color.Orange
                Label3.BackColor = Color.Orange
                Label4.BackColor = Color.Orange
                Label13.BackColor = Color.Orange
            End If
        Next
        'Detetar o Vencedor'
        If PontosEquipaEscolhidaPorOrdemMT(3) = PontosEquipaEscolhidaPorOrdemMT(4) Then
            desempate2()
        End If
        If PontosEquipaEscolhidaPorOrdemMT(3) > PontosEquipaEscolhidaPorOrdemMT(4) Then
            'Aumentar pontos ao vencedor'
            For count = 1 To 6
                If NomeEquipaEscolhidaPorOrdemMT(3) = NomeEquipaMT(count) Then
                    PontosMT(count) += 2
                    SCestosMT(count) = PontosEquipaEscolhidaPorOrdemMT(3)
                    'Derrota'
                    For count2 = 1 To 6
                        If NomeEquipaEscolhidaPorOrdemMT(4) = NomeEquipaMT(count2) Then
                            SCestosMT(count2) = PontosEquipaEscolhidaPorOrdemMT(4)
                        End If
                    Next
                End If
            Next
        End If

        If PontosEquipaEscolhidaPorOrdemMT(3) < PontosEquipaEscolhidaPorOrdemMT(4) Then
            'Aumentar pontos ao vencedor'
            For count = 1 To 6
                If NomeEquipaEscolhidaPorOrdemMT(4) = NomeEquipaMT(count) Then
                    PontosMT(count) += 2
                    SCestosMT(count) = PontosEquipaEscolhidaPorOrdemMT(4)
                    'Derrota'
                    For count2 = 1 To 6
                        If NomeEquipaEscolhidaPorOrdemMT(3) = NomeEquipaMT(count2) Then
                            SCestosMT(count2) = PontosEquipaEscolhidaPorOrdemMT(3)
                        End If
                    Next
                End If
            Next
        End If
    End Sub

    Sub jogo3()
        'Variavel do ciclo "for"'
        pausa(1000)
        Dim count As Byte

        'Criação aleatória de Número máximo de pontos na partida'
        Randomize()
        NPontosRandom = Int(80 * Rnd() + 45)

        'Iniciação do ciclo for (jogo)'
        For count = 1 To NPontosRandom
            pausa(250)

            'Verificação de quem tem a maior média na ajuda dos calculos'
            If MediaEquipaEscolhidaPorOrdemMT(6) < MediaEquipaEscolhidaPorOrdemMT(5) Then

                'Obtenção da difença de média e a probabilidade de marcar ponto'
                diferençaMedias = MediaEquipaEscolhidaPorOrdemMT(5) - MediaEquipaEscolhidaPorOrdemMT(6)
                Probabilidade = 50 + diferençaMedias

                If Probabilidade >= 95 Then
                    Probabilidade = 95
                End If

                'Criação e verificação de quem maca o ponto, soma dos pontos para as equipas'
                Randomize()
                PontosRandom = Int(100 * Rnd())
                If PontosRandom < Probabilidade Then
                    PontosEquipaEscolhidaPorOrdemMT(5) += 1
                Else
                    PontosEquipaEscolhidaPorOrdemMT(6) += 1
                End If
            Else
                'Obtenção da difença de média e a probabilidade de marcar ponto'
                diferençaMedias = MediaEquipaEscolhidaPorOrdemMT(6) - MediaEquipaEscolhidaPorOrdemMT(5)
                Probabilidade = 50 + diferençaMedias

                If Probabilidade >= 95 Then
                    Probabilidade = 95
                End If

                'Criação e verificação de quem maca o ponto, soma dos pontos para as equipas'
                Randomize()
                PontosRandom = Int(100 * Rnd())
                If PontosRandom < Probabilidade Then
                    PontosEquipaEscolhidaPorOrdemMT(6) += 1
                Else
                    PontosEquipaEscolhidaPorOrdemMT(5) += 1
                End If
            End If

            'Apresentação dos Pontos'
            Label14.Text = PontosEquipaEscolhidaPorOrdemMT(5)
            Label15.Text = PontosEquipaEscolhidaPorOrdemMT(6)

            'Colorir consoante a vitória e a derrota da equipa'
            If PontosEquipaEscolhidaPorOrdemMT(5) > PontosEquipaEscolhidaPorOrdemMT(6) Then
                'Vitória da Equipa Esquerda'
                Label14.BackColor = Color.Green
                Label5.BackColor = Color.Green
                Label6.BackColor = Color.Red
                Label15.BackColor = Color.Red

            ElseIf PontosEquipaEscolhidaPorOrdemMT(5) < PontosEquipaEscolhidaPorOrdemMT(6) Then
                'Derrota da Equipa Esquerda'
                Label14.BackColor = Color.Red
                Label5.BackColor = Color.Red
                Label6.BackColor = Color.Green
                Label15.BackColor = Color.Green

            Else
                'Empate'
                Label14.BackColor = Color.Orange
                Label5.BackColor = Color.Orange
                Label6.BackColor = Color.Orange
                Label15.BackColor = Color.Orange
            End If
        Next

        'Detetar o Vencedor'
        If PontosEquipaEscolhidaPorOrdemMT(5) = PontosEquipaEscolhidaPorOrdemMT(6) Then
            desempate3()
        End If

        If PontosEquipaEscolhidaPorOrdemMT(5) > PontosEquipaEscolhidaPorOrdemMT(6) Then
            'Disponibilisar "Voltar"'
            Button2.Enabled = True
            'Aumentar pontos ao vencedor'
            For count = 1 To 6
                If NomeEquipaEscolhidaPorOrdemMT(5) = NomeEquipaMT(count) Then
                    PontosMT(count) += 2
                    SCestosMT(count) = PontosEquipaEscolhidaPorOrdemMT(5)
                    'Derrota'
                    For count2 = 1 To 6
                        If NomeEquipaEscolhidaPorOrdemMT(6) = NomeEquipaMT(count2) Then
                            SCestosMT(count2) = PontosEquipaEscolhidaPorOrdemMT(6)
                        End If
                    Next
                End If
            Next
            Button2.Enabled = True
            Button9.Enabled = True
        End If

        If PontosEquipaEscolhidaPorOrdemMT(5) < PontosEquipaEscolhidaPorOrdemMT(6) Then
            'Disponibilisar "Voltar"'
            Button2.Enabled = True
            'Aumentar pontos ao vencedor'
            For count = 1 To 6
                If NomeEquipaEscolhidaPorOrdemMT(6) = NomeEquipaMT(count) Then
                    PontosMT(count) += 2
                    SCestosMT(count) = PontosEquipaEscolhidaPorOrdemMT(6)
                    'Derrota'
                    For count2 = 1 To 6
                        If NomeEquipaEscolhidaPorOrdemMT(5) = NomeEquipaMT(count2) Then
                            SCestosMT(count2) = PontosEquipaEscolhidaPorOrdemMT(5)
                        End If
                    Next
                End If
            Next
            Button2.Enabled = True
            Button3.Enabled = True
        End If
    End Sub

    'Ver Equipa e Sair'
    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        ModoTreinador.Show()
        Me.Close()
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        EquipaPosicaoMT = Label1.Text
        VerEquipaMT.Show()
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        EquipaPosicaoMT = Label2.Text
        VerEquipaMT.Show()
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        EquipaPosicaoMT = Label3.Text
        VerEquipaMT.Show()
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        EquipaPosicaoMT = Label4.Text
        VerEquipaMT.Show()
    End Sub

    Private Sub Button8_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click
        EquipaPosicaoMT = Label6.Text
        VerEquipaMT.Show()
    End Sub

    Private Sub Button7_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        EquipaPosicaoMT = Label5.Text
        VerEquipaMT.Show()
    End Sub

    Private Sub Button9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button9.Click
        'Minha Equipa'
        JogoModoTreinadorTF = True
        MinhaEquipa.Show()
        Me.Close()
    End Sub

    Private Sub Button10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button10.Click
        'Ajuda'
        MsgBox("Aqui tens todos os jogos que irão ocorrer nesta semana, se quiseres podes melhorar a tua equipa se vires que ela nãoe stá preparada, clicando em 'Minha Equipa'")
        MsgBox("Se caso a tua equipa já estiver pronta para tal, então clica em jogar e deixa que os teus jogadores vençam a partida!")
    End Sub
End Class