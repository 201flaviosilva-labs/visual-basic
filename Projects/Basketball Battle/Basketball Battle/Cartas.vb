﻿Public Class Cartas
    Dim JogadorPoder As Byte
    Dim NJContratado(360) As String

    Sub subclickes()
        'Aviso de Limite de suplentes e Bloquio de Butões de compras'
        clickes += 1
        Label20.Text = clickes & "/5"
        If clickes = 4 Then
            MsgBox("Não te esqueças de que só podes ter 5 suplentes!")
        ElseIf clickes >= 5 Then
            MsgBox("Atingiste o máximo de suplentes (5)!")
        End If

        If clickes >= 5 Then
            Button1.Enabled = False
            Button2.Enabled = False
            Button3.Enabled = False
            Button4.Enabled = False
        End If
        'Aviso de Limite de suplentes e Bloquio de Butões de compras'
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        'Voltar'
        MinhaEquipa.Show()
        Me.Close()
    End Sub

    Private Sub Cartas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Tela Inicial'
        clickes = 0
        Label20.Text = clickes & "/5"

        Label10.Text = DinheiroAcumolado

        'Criação de nomes de possiveis jogadores'
        NJContratado(1) = "Élio"
        NJContratado(2) = "Eliseu"
        NJContratado(3) = "António"
        NJContratado(4) = "Emídio"
        NJContratado(5) = "Énio"
        NJContratado(6) = "Alex"
        NJContratado(7) = "Enzo"
        NJContratado(8) = "Eric"
        NJContratado(9) = "Ester"
        NJContratado(10) = "Evaldo"
        NJContratado(11) = "Alvaro"
        NJContratado(12) = "Acílio"
        NJContratado(13) = "Adalberto"
        NJContratado(14) = "Adamantino"
        NJContratado(15) = "Adeodato"
        NJContratado(16) = "Adolfo Hitler"
        NJContratado(17) = "Afonsino"
        NJContratado(18) = "Afonso Henrriques"
        NJContratado(19) = "Albano"
        NJContratado(20) = "Alberto"
        NJContratado(21) = "Aldónio"
        NJContratado(22) = "Alexandro"
        NJContratado(23) = "Aléxio"
        NJContratado(24) = "Alípio"
        NJContratado(25) = "Alítio"
        NJContratado(26) = "Almiro"
        NJContratado(27) = "Alvarino"
        NJContratado(28) = "Amadeu"
        NJContratado(29) = "Amarildo"
        NJContratado(30) = "Américo"
        NJContratado(31) = "André"
        NJContratado(32) = "Anfílito"
        NJContratado(33) = "Anielo"
        NJContratado(34) = "Anísio"
        NJContratado(35) = "Anolido"
        NJContratado(36) = "Antero"
        NJContratado(37) = "Ápio"
        NJContratado(38) = "Apolinário"
        NJContratado(39) = "Aquiles"
        NJContratado(40) = "Arcádio"
        NJContratado(41) = "Arcélio"
        NJContratado(42) = "Aristóteles"
        NJContratado(43) = "Armandino"
        NJContratado(44) = "Arquimedes"
        NJContratado(45) = "Arsénio"
        NJContratado(46) = "Artur"
        NJContratado(47) = "Asélio"
        NJContratado(48) = "Atão"
        NJContratado(49) = "Áureo"
        NJContratado(50) = "Austrelino"
        NJContratado(51) = "Aventino"
        NJContratado(52) = "Azélio"
        NJContratado(53) = "Baldemar"
        NJContratado(54) = "Baldomero"
        NJContratado(55) = "Barão"
        NJContratado(56) = "Batista"
        NJContratado(57) = "Basílio"
        NJContratado(58) = "Bartolomeu Dias"
        NJContratado(59) = "Belchior"
        NJContratado(60) = "Benjamim"
        NJContratado(61) = "Bento"
        NJContratado(62) = "Bernadete"
        NJContratado(63) = "Bernardim"
        NJContratado(64) = "Bernardino"
        NJContratado(65) = "Bernardo"
        NJContratado(66) = "Boanerges"
        NJContratado(67) = "Bóris"
        NJContratado(68) = "Brás"
        NJContratado(69) = "Bráulio"
        NJContratado(70) = "Brian"
        NJContratado(71) = "Briolanjo"
        NJContratado(72) = "Agostinho"
        NJContratado(73) = "Agnelo"
        NJContratado(74) = "Bruno"
        NJContratado(75) = "Bruce Lee"
        NJContratado(76) = "Caetano"
        NJContratado(77) = "Cândido"
        NJContratado(78) = "Carlos"
        NJContratado(79) = "Carmério"
        NJContratado(80) = "Carmo"
        NJContratado(81) = "Cássio"
        NJContratado(82) = "Catarino"
        NJContratado(83) = "Cedrico"
        NJContratado(84) = "Célio"
        NJContratado(85) = "Césaro Verde"
        NJContratado(86) = "Cildo"
        NJContratado(87) = "Cirilo"
        NJContratado(88) = "Clarindo"
        NJContratado(89) = "Claudemiro"
        NJContratado(90) = "Claudiano"
        NJContratado(91) = "Clésio"
        NJContratado(92) = "Crispim"
        NJContratado(93) = "Cristiano"
        NJContratado(94) = "Cristóforo"
        NJContratado(95) = "Cristóvão Colombo"
        NJContratado(96) = "Cursino"
        NJContratado(97) = "Dácio"
        NJContratado(98) = "Damião"
        NJContratado(99) = "Daniel"
        NJContratado(100) = "Dárcio"
        NJContratado(101) = "Dante"
        NJContratado(102) = "Dário"
        NJContratado(103) = "David"
        NJContratado(104) = "Délcio"
        NJContratado(105) = "Délio"
        NJContratado(106) = "Delmiro"
        NJContratado(107) = "Deolindo"
        NJContratado(108) = "Dércio"
        NJContratado(109) = "Diego"
        NJContratado(110) = "Dinis"
        NJContratado(111) = "Diogo"
        NJContratado(112) = "Dionísio"
        NJContratado(113) = "Dírio"
        NJContratado(114) = "Domingos"
        NJContratado(115) = "Donzílio"
        NJContratado(116) = "Dóris"
        NJContratado(117) = "Duarte"
        NJContratado(118) = "Durvalino"
        NJContratado(119) = "Éder"
        NJContratado(120) = "Eduardo"
        NJContratado(121) = "Eduíno"
        NJContratado(122) = "Eleutério"
        NJContratado(123) = "Elgar"
        NJContratado(124) = "Evangelino"
        NJContratado(125) = "Ezequiel"
        NJContratado(126) = "Fabiano"
        NJContratado(127) = "Fábio"
        NJContratado(128) = "Fabíola"
        NJContratado(129) = "Fabrício"
        NJContratado(130) = "Faustino"
        NJContratado(131) = "Fausto"
        NJContratado(132) = "Félix"
        NJContratado(133) = "Ferdinando"
        NJContratado(134) = "Ferrer"
        NJContratado(135) = "Fidélio"
        NJContratado(136) = "Filipo"
        NJContratado(137) = "Filipe"
        NJContratado(138) = "Filoteu"
        NJContratado(139) = "Firmino"
        NJContratado(140) = "Florentino"
        NJContratado(141) = "Francisco"
        NJContratado(142) = "Fred"
        NJContratado(143) = "Frederico"
        NJContratado(144) = "Fred"
        NJContratado(145) = "Gabínio"
        NJContratado(146) = "Gabriel"
        NJContratado(147) = "Gabi"
        NJContratado(148) = "Galileu Galilei"
        NJContratado(149) = "Gaspar"
        NJContratado(150) = "Genésio"
        NJContratado(151) = "Georgino"
        NJContratado(152) = "Gerberto"
        NJContratado(153) = "Gervásio"
        NJContratado(154) = "Gil Vicente"
        NJContratado(155) = "Gildásio"
        NJContratado(156) = "Gildo"
        NJContratado(157) = "Giovani"
        NJContratado(158) = "Gomes"
        NJContratado(159) = "Gonçalo"
        NJContratado(160) = "Graciano"
        NJContratado(161) = "Guadalupe"
        NJContratado(162) = "Guido"
        NJContratado(163) = "Guilherme"
        NJContratado(164) = "Gustavo"
        NJContratado(165) = "Hamilton"
        NJContratado(166) = "Hélder"
        NJContratado(167) = "Hélvio"
        NJContratado(168) = "Hemitério"
        NJContratado(169) = "Henrique"
        NJContratado(170) = "Hérman"
        NJContratado(171) = "Hermano"
        NJContratado(172) = "Hermínio"
        NJContratado(173) = "Homero"
        NJContratado(174) = "Hugo"
        NJContratado(175) = "Hortênsio"
        NJContratado(176) = "Iago"
        NJContratado(177) = "Idálio"
        NJContratado(178) = "Idélso"
        NJContratado(179) = "Igor"
        NJContratado(180) = "Xico"
        NJContratado(181) = "Yuri"
        NJContratado(182) = "Zacarias"
        NJContratado(183) = "Zaido"
        NJContratado(184) = "Zaíro"
        NJContratado(185) = "Zaqueu"
        NJContratado(186) = "Zé"
        NJContratado(187) = "Zélio"
        NJContratado(188) = "Zoé"
        NJContratado(189) = "Zulmiro"
        NJContratado(190) = "Infante"
        NJContratado(191) = "Isaac Newton"
        NJContratado(192) = "Isac"
        NJContratado(193) = "Isauro"
        NJContratado(194) = "Ivan"
        NJContratado(195) = "Ivo"
        NJContratado(196) = "Jacó"
        NJContratado(197) = "Jacob"
        NJContratado(198) = "Jaime"
        NJContratado(199) = "Jair"
        NJContratado(200) = "James"
        NJContratado(201) = "Januário"
        NJContratado(202) = "Jaque"
        NJContratado(203) = "Jardel"
        NJContratado(204) = "Jerónimo"
        NJContratado(205) = "Jesualdo"
        NJContratado(206) = "Jesus"
        NJContratado(207) = "João"
        NJContratado(208) = "Joaquim"
        NJContratado(209) = "Jocelino"
        NJContratado(210) = "Joel"
        NJContratado(211) = "Jonas"
        NJContratado(212) = "Jordano"
        NJContratado(213) = "Joscelino"
        NJContratado(214) = "José"
        NJContratado(215) = "Josefo"
        NJContratado(216) = "Joselindo"
        NJContratado(217) = "Josué"
        NJContratado(218) = "Judas"
        NJContratado(219) = "Júlio César"
        NJContratado(220) = "Junior"
        NJContratado(221) = "Laurénio"
        NJContratado(222) = "Lauro"
        NJContratado(223) = "Lázaro"
        NJContratado(224) = "Lécio"
        NJContratado(225) = "Leoberto"
        NJContratado(226) = "Leonardo"
        NJContratado(227) = "Leonício"
        NJContratado(228) = "Liciniano"
        NJContratado(229) = "Lilian"
        NJContratado(230) = "Lisandro"
        NJContratado(231) = "Lorenzo"
        NJContratado(232) = "Lourenço"
        NJContratado(233) = "Lucas"
        NJContratado(234) = "Lucílio"
        NJContratado(235) = "Lúcio"
        NJContratado(236) = "Luís Vaz de Camões"
        NJContratado(237) = "Manel"
        NJContratado(238) = "Manuel"
        NJContratado(239) = "Marco"
        NJContratado(240) = "Márcio"
        NJContratado(241) = "Marcelo"
        NJContratado(242) = "Mariano"
        NJContratado(243) = "Mário"
        NJContratado(244) = "Martim"
        NJContratado(245) = "Maurício"
        NJContratado(246) = "Micael"
        NJContratado(247) = "Mercedes"
        NJContratado(249) = "Michele"
        NJContratado(250) = "Moisés"
        NJContratado(251) = "Nadine"
        NJContratado(252) = "Napoleão"
        NJContratado(253) = "Nazaré"
        NJContratado(254) = "Nelson Mandela"
        NJContratado(255) = "Nicolas"
        NJContratado(256) = "Nicolau"
        NJContratado(257) = "Noa"
        NJContratado(258) = "Norberto"
        NJContratado(259) = "Normando"
        NJContratado(260) = "Nuno"
        NJContratado(261) = "Octávio"
        NJContratado(262) = "Otávio"
        NJContratado(263) = "Olívio"
        NJContratado(264) = "Óscar"
        NJContratado(265) = "Orlando"
        NJContratado(266) = "Osvaldo"
        NJContratado(267) = "Palmiro"
        NJContratado(268) = "Pascoal"
        NJContratado(269) = "Patrício"
        NJContratado(270) = "Paulino"
        NJContratado(271) = "Paulo"
        NJContratado(272) = "Pedro"
        NJContratado(273) = "Pépio"
        NJContratado(274) = "Querubim"
        NJContratado(275) = "Quévin"
        NJContratado(276) = "Quim"
        NJContratado(277) = "Quitério"
        NJContratado(278) = "Rafael"
        NJContratado(279) = "Ramão"
        NJContratado(280) = "Ramiro"
        NJContratado(281) = "Raul"
        NJContratado(282) = "Reginaldo"
        NJContratado(283) = "Remo"
        NJContratado(284) = "Renato"
        NJContratado(285) = "Ricardo"
        NJContratado(286) = "Roberto"
        NJContratado(287) = "Rodolfo"
        NJContratado(288) = "Rodrigo"
        NJContratado(289) = "Rogério"
        NJContratado(290) = "Romão"
        NJContratado(291) = "Rómulo"
        NJContratado(292) = "Ronaldo"
        NJContratado(293) = "Rúben"
        NJContratado(294) = "Ruca"
        NJContratado(295) = "Salazar"
        NJContratado(296) = "Salomão"
        NJContratado(297) = "Salomé"
        NJContratado(298) = "Salvador"
        NJContratado(299) = "Salvador de Jesus"
        NJContratado(300) = "Sancho"
        NJContratado(301) = "Samuel"
        NJContratado(302) = "Sancler"
        NJContratado(303) = "Sancler"
        NJContratado(304) = "Sancler"
        NJContratado(305) = "Santelmo"
        NJContratado(306) = "Saúl"
        NJContratado(307) = "Sebastião"
        NJContratado(308) = "Selmo"
        NJContratado(309) = "Sénio"
        NJContratado(310) = "Seomara"
        NJContratado(311) = "Sérgio"
        NJContratado(312) = "Simão"
        NJContratado(313) = "Simauro"
        NJContratado(314) = "Simone"
        NJContratado(315) = "Sinésio"
        NJContratado(316) = "Sisínio"
        NJContratado(317) = "Sócrates"
        NJContratado(318) = "Susano"
        NJContratado(319) = "Taciano"
        NJContratado(320) = "Tálio"
        NJContratado(321) = "Tamár"
        NJContratado(322) = "Tarcísio"
        NJContratado(323) = "Telmo"
        NJContratado(324) = "Teodemiro"
        NJContratado(325) = "Teodoro"
        NJContratado(326) = "Teófilo"
        NJContratado(327) = "Teseu"
        NJContratado(328) = "Tiago"
        NJContratado(329) = "Tibério"
        NJContratado(330) = "Tierri"
        NJContratado(331) = "Timóteo"
        NJContratado(332) = "Tito"
        NJContratado(333) = "Tobias"
        NJContratado(334) = "Tomás"
        NJContratado(335) = "Tomé"
        NJContratado(336) = "Toni"
        NJContratado(337) = "Trindade"
        NJContratado(338) = "Tony Carreira"
        NJContratado(339) = "Ubaldino"
        NJContratado(340) = "Ulisses"
        NJContratado(341) = "Ursácio"
        NJContratado(342) = "Úrsulo"
        NJContratado(343) = "Vasco da Gama"
        NJContratado(344) = "Veríssimo"
        NJContratado(345) = "Vicente"
        NJContratado(346) = "Victor"
        NJContratado(347) = "Vidal"
        NJContratado(348) = "Vílmar"
        NJContratado(349) = "Virgílio"
        NJContratado(350) = "Virgínio"
        NJContratado(351) = "Viriato"
        NJContratado(352) = "Vítor"
        NJContratado(353) = "Vitório"
        NJContratado(354) = "Vladimiro"
        NJContratado(355) = "Wilson"
        NJContratado(356) = "Xavier"
        NJContratado(357) = "Xénio"
        NJContratado(358) = "Xenócrates"
        NJContratado(359) = "Xerxes"
        NJContratado(360) = "Vinício"


        'Criação de nomes de possiveis jogadores'
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        'Comprar Iniciado'
        subclickes()

        'Compra de jogadores'
        If DinheiroAcumolado <= 199 Then
            MsgBox("Não tens dinheiro suficiente!Porque não tentas ganhar dinheiro ganhando jogos?")
        ElseIf DinheiroAcumolado >= 200 Then
            DinheiroAcumolado -= 200
            Randomize()
            'Escolha de poderes, nomes e fotografia do jogador e revelação das labels'
            JogadorPoder = Int(51 * Rnd())
            Label11.Visible = True
            Label11.Enabled = True
            Label11.Text = JogadorPoder & "/100"

            Randomize()
            NJContratadoRandom = Int(360 * Rnd() + 1)
            Label15.Text = NJContratado(NJContratadoRandom)
            Label15.Visible = True
            Label15.Enabled = True
            'Escolha de poderes, nomes e fotografia do jogador e revelação das labels'

            'Indetificação do jogador comprado'
            NJSuplente(clickes) = NJContratado(NJContratadoRandom)
            PJSuplente(clickes) = JogadorPoder
            'Indetificação do jogador comprado'
        End If
        'Compra de jogadores'

        Label10.Text = DinheiroAcumolado
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        'Comprar Cadetes'
        subclickes()
        'Compra de jogadores'
        If DinheiroAcumolado <= 399 Then
            MsgBox("Não tens dinheiro suficiente! Porque não tentas ganhar dinheiro ganhando jogos?")
        ElseIf DinheiroAcumolado >= 400 Then
            'Escolha de poderes, nomes e fotografia do jogador e revelação das labels'
            DinheiroAcumolado -= 400
            Randomize()
            JogadorPoder = Int(31 * Rnd() + 50)
            Label12.Visible = True
            Label12.Enabled = True
            Label12.Text = JogadorPoder & "/100"

            Randomize()
            NJContratadoRandom = Int(360 * Rnd() + 1)
            Label16.Text = NJContratado(NJContratadoRandom)
            Label16.Visible = True
            Label16.Enabled = True
            'Escolha de poderes, nomes e fotografia do jogador e revelação das labels'

            'Indetificação do jogador comprado'
            NJSuplente(clickes) = NJContratado(NJContratadoRandom)
            PJSuplente(clickes) = JogadorPoder
            'Indetificação do jogador comprado'
        End If
        'Compra de jogadores'

        Label10.Text = DinheiroAcumolado
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        'Comprar Proficional'
        subclickes()

        'Compra de jogadores'
        If DinheiroAcumolado <= 599 Then
            MsgBox("Não tens dinheiro suficiente! Porque não tenstas ganhar dinheiro ganhando jogos?")
        ElseIf DinheiroAcumolado >= 600 Then
            'Escolha de poderes, nomes e fotografia do jogador e revelação das labels'
            DinheiroAcumolado -= 600
            Randomize()
            JogadorPoder = Int(21 * Rnd() + 80)
            Label13.Visible = True
            Label13.Enabled = True
            Label13.Text = JogadorPoder & "/100"

            Randomize()
            NJContratadoRandom = Int(360 * Rnd() + 1)
            Label17.Text = NJContratado(NJContratadoRandom)
            Label17.Visible = True
            Label17.Enabled = True
            'Escolha de poderes, nomes e fotografia do jogador e revelação das labels'

            'Indetificação do jogador comprado'
            NJSuplente(clickes) = NJContratado(NJContratadoRandom)
            PJSuplente(clickes) = JogadorPoder
            'Indetificação do jogador comprado'
        End If
        'Compra de jogadores'

        Label10.Text = DinheiroAcumolado
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        'Comprar Tudo a Monte'
        subclickes()

        'Compra de jogadores'
        If DinheiroAcumolado <= 399 Then
            MsgBox("Não tens dinheiro suficiente! Porque não tenstas ganhar dinheiro ganhando jogos?")
        ElseIf DinheiroAcumolado >= 400 Then
            'Escolha de poderes, nomes e fotografia do jogador e revelação das labels'
            DinheiroAcumolado -= 400
            Randomize()
            JogadorPoder = Int(101 * Rnd())
            Label14.Visible = True
            Label14.Enabled = True
            Label14.Text = JogadorPoder & "/100"

            Randomize()
            NJContratadoRandom = Int(360 * Rnd() + 1)
            Label18.Text = NJContratado(NJContratadoRandom)
            Label18.Visible = True
            Label18.Enabled = True
            'Escolha de poderes, nomes e fotografia do jogador e revelação das labels'

            'Indetificação do jogador comprado'
            NJSuplente(clickes) = NJContratado(NJContratadoRandom)
            PJSuplente(clickes) = JogadorPoder
            'Indetificação do jogador comprado'
        End If
        'Compra de jogadores'

        Label10.Text = DinheiroAcumolado
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        MsgBox("Aqui podes contratar os teus jogadores.")
        MsgBox("Clica no botão que diz: 'Comprar' e vais ver a magia. Não te esquecas só podes comprar 5 jogadores de cada vez.")
        MsgBox("Depois de comprares os jogadores que quiseres, colica em voltar e eles irão aparecer no lugar dos suplentes.")
    End Sub
End Class