﻿Public Class VerEquipaMT

    Private Sub Jogo1Equerda_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If EquipaPosicaoMT = NomeEquipaMT(1) Then
            Label1.Text = NomeEquipaMT(1)
            Label23.Text = MediaMT(1)

            Label2.Text = NJogadorTitular(1)
            Label4.Text = NJogadorTitular(2)
            Label6.Text = NJogadorTitular(3)
            Label5.Text = NJogadorTitular(4)
            Label3.Text = NJogadorTitular(5)

            Label13.Text = PJTitular(1) & "/100"
            Label14.Text = PJTitular(2) & "/100"
            Label15.Text = PJTitular(3) & "/100"
            Label16.Text = PJTitular(4) & "/100"
            Label17.Text = PJTitular(5) & "/100"

        ElseIf EquipaPosicaoMT = NomeEquipaMT(2) Then
            Label1.Text = NomeEquipaMT(2)
            Label23.Text = MediaMT(2)

            Label2.Text = "Dinis"
            Label4.Text = "David"
            Label6.Text = "Anielo"
            Label5.Text = "Énio"
            Label3.Text = "Úsulo"

            Label13.Text = "55/100"
            Label14.Text = "55/100"
            Label15.Text = "40/100"
            Label16.Text = "35/100"
            Label17.Text = "40/100"

        ElseIf EquipaPosicaoMT = NomeEquipaMT(3) Then
            Label1.Text = NomeEquipaMT(3)
            Label23.Text = MediaMT(3)

            Label2.Text = "Homero"
            Label4.Text = "Zaqueu"
            Label6.Text = "Sisínio"
            Label5.Text = "Arquemedes"
            Label3.Text = "Éder"

            Label13.Text = "80/100"
            Label14.Text = "90/100"
            Label15.Text = "95/100"
            Label16.Text = "85/100"
            Label17.Text = "75/100"

        ElseIf EquipaPosicaoMT = NomeEquipaMT(4) Then
            Label1.Text = NomeEquipaMT(4)
            Label23.Text = MediaMT(4)

            Label2.Text = "Filoteu"
            Label4.Text = "Ezequiel"
            Label6.Text = "Duarte"
            Label5.Text = "Atão"
            Label3.Text = "Albano"

            Label13.Text = "80/100"
            Label14.Text = "70/100"
            Label15.Text = "50/100"
            Label16.Text = "80/100"
            Label17.Text = "70/100"

        ElseIf EquipaPosicaoMT = NomeEquipaMT(5) Then
            Label1.Text = NomeEquipaMT(5)
            Label23.Text = MediaMT(5)

            Label2.Text = "Xavier"
            Label4.Text = "Xavier"
            Label6.Text = "Xavier"
            Label5.Text = "Xavier"
            Label3.Text = "Xavier"

            Label13.Text = "63/100"
            Label14.Text = "63/100"
            Label15.Text = "63/100"
            Label16.Text = "63/100"
            Label17.Text = "63/100"

        Else
            Label1.Text = NomeEquipaMT(6)
            Label23.Text = MediaMT(6)

            Label2.Text = "Ferrer"
            Label4.Text = "Olívio"
            Label6.Text = "Elgar"
            Label5.Text = "Liciniano"
            Label3.Text = "Fred"

            Label13.Text = "40/100"
            Label14.Text = "20/100"
            Label15.Text = "25/100"
            Label16.Text = "45/100"
            Label17.Text = "10/100"

        End If
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        'Voltar'
        JogoModoTreinador.Show()
        Me.Close()
    End Sub

    Private Sub Button10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button10.Click
        'ajuda'
        MsgBox("Aqui podes ver a equipa que tu selecionas-te ver")
    End Sub
End Class