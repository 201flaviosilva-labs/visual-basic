﻿Public Class Jogo
    Dim diferençaMedias, probabilidade As Double
    Dim NJogadorRandom, NEquipaRandom, NVitóriasCPU, NDerrotasCPU, médiaCPU, PoderCPU(5) As Integer
    Dim sinal, PontosRandom, pontosCPU, PontosUTL, NPontosRandom As Double
    Dim num1 As Integer = 360
    Dim NJogadorCPU(num1), NEquipaCPU(num1), NEquipaFinalCPU, NJogadorFinalCPU(5) As String

    'NjogadorRandom = Número do Jogador escolhido aleatóriamnete pelo pc para o CPU'
    'NEquipaRandom = Nome da Equipa escolhido aleatóriamnete pelo pc para o CPU'
    'NVitóriasCPU = Número de Vitórias escolhido aleatóriamnete pelo pc para o CPU'
    'NDerrotasCPU = Número de Derrotas escolhido aleatóriamnete pelo pc para o CPU'
    'médiaCPU = Média da Equipa escolhido aleatóriamnete pelo pc para o CPU'
    'diferençaMedias = Difeneça de médias da equipa do cpu e do Utilizador'
    'probabilidadeCPU = Probabilidade calculada do cpu vencer a partida'
    'probabilidadeUTL = Probabilidade calculada do Utilizador vencer a partida'
    'PontosRandom = Pontos aleatório para uma das equipas'
    'pontosCPU = Número de pontos do cpu'
    'pontosUTL = Número de pontos do Utilizador'
    'PoderCPU = Poder dos jogadores do cpu'
    'Sinal = Sinal emitido para dizer que já foi escolhido a equipa'
    'num1 = Variavel de numero de nomes'
    'NJogadorCPU = Nome do Jogadores disponiveis para o cpu'
    'NEquipaCPU = Nome de equipa disponiveis para o cpu'
    'NEquipaFinalCPU = Nnome do nome da equipa que ficou escolhido'
    'NJogadorFinalCPU = Nome do jogador que ficou escolhido'

    Sub pausa(ByVal tempopausa)
        'Tempo de Pausa'
        Dim tempoatual As Date
        Dim tempoparado As Date

        tempoatual = Date.Now()
        tempoparado = tempoatual.AddMilliseconds(tempopausa)

        Do While Date.Now < tempoparado
            Application.DoEvents()
        Loop
    End Sub

    Sub desempate()
        pausa(1000)

        'Desemparar o jogo, caso de empate'
        Dim count As Byte

        Randomize()
        NPontosRandom = Int(30 * Rnd())

        For count = 1 To NPontosRandom
            pausa(300)

            If médiaCPU < MediaUTL Then

                diferençaMedias = MediaUTL - médiaCPU
                probabilidade = 50 + diferençaMedias

                If probabilidade >= 95 Then
                    probabilidade = 95
                End If

                Randomize()
                PontosRandom = Int(100 * Rnd())
                If PontosRandom < probabilidade Then
                    PontosUTL += 1
                Else
                    pontosCPU += 1
                End If

            Else
                diferençaMedias = médiaCPU - MediaUTL
                probabilidade = 50 + diferençaMedias

                If probabilidade >= 95 Then
                    probabilidade = 95
                End If

                Randomize()
                PontosRandom = Int(100 * Rnd())
                If PontosRandom < probabilidade Then
                    pontosCPU += 1
                Else
                    PontosUTL += 1
                End If
            End If

            Label29.Text = PontosUTL
            Label30.Text = pontosCPU

            If PontosUTL > pontosCPU Then
                'vitória'
                Label29.BackColor = Color.Green
                Label31.BackColor = Color.Green
                Label32.BackColor = Color.Red
                Label30.BackColor = Color.Red
            ElseIf PontosUTL < pontosCPU Then
                'Derrota'
                Label29.BackColor = Color.Red
                Label31.BackColor = Color.Red
                Label32.BackColor = Color.Green
                Label30.BackColor = Color.Green
            Else
                'Empate'
                Label29.BackColor = Color.Orange
                Label31.BackColor = Color.Orange
                Label32.BackColor = Color.Orange
                Label30.BackColor = Color.Orange
            End If
        Next

        If PontosUTL > pontosCPU Then
            'vitória'
            NVitórias += 1
            DinheiroAcumolado += 25
            MsgBox("Muito bem! Ganhaste esta partida e ainda ganhaste 25$, agora tens: " & DinheiroAcumolado & "$")
        ElseIf PontosUTL < pontosCPU Then
            'Derrota'
            NDerrotas += 1
            DinheiroAcumolado += 10
            MsgBox("Perdeste esta partida, mas está tudo bem, ganhaste 10$, agora tens: " & DinheiroAcumolado & "$")
        Else
            'Empate'
            MsgBox("A tua equipa empatou contra a do Computador")
            desempate()
        End If

        Button1.Enabled = True
        Button3.Enabled = True
        'Atualizar vitórias e derrotas'
        Label20.Text = "N. Derrotas: " & NDerrotas
        Label21.Text = "N. Vitórias: " & NVitórias
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        'Criar Adversário'
        Button2.Enabled = True
        Button1.Enabled = False

        'Sinal de Escolha de Equipa'
        sinal = 1
        'Sinal de escolha de equipa'

        NomeJogadorRandomCPU()

        NomeEquipaRandomCPU()

        'Hora dos Random'
        Randomize()
        'Escolha Aleatória de Habilidades("poder") dos jogadores do CPU'

        PoderCPU(1) = Int(100 * Rnd() + 1)
        Label13.Text = PoderCPU(1) & "/100"

        PoderCPU(2) = Int(100 * Rnd() + 1)
        Label14.Text = PoderCPU(2) & "/100"

        PoderCPU(3) = Int(100 * Rnd() + 1)
        Label15.Text = PoderCPU(3) & "/100"

        PoderCPU(4) = Int(100 * Rnd() + 1)
        Label16.Text = PoderCPU(4) & "/100"

        PoderCPU(5) = Int(100 * Rnd() + 1)
        Label17.Text = PoderCPU(5) & "/100"
        'Escolha Aleatória de Habilidades("poder") dos jogadores do CPU'

        'Escolha Aleatória do nome de Jogadores do CPU'
        NJogadorRandom = Int(360 * Rnd() + 1)
        NJogadorFinalCPU(1) = NJogadorCPU(NJogadorRandom)
        Label2.Text = NJogadorFinalCPU(1)

        NJogadorRandom = Int(360 * Rnd() + 1)
        NJogadorFinalCPU(2) = NJogadorCPU(NJogadorRandom)
        Label3.Text = NJogadorFinalCPU(2)

        NJogadorRandom = Int(360 * Rnd() + 1)
        NJogadorFinalCPU(3) = NJogadorCPU(NJogadorRandom)
        Label4.Text = NJogadorFinalCPU(3)

        NJogadorRandom = Int(360 * Rnd() + 1)
        NJogadorFinalCPU(4) = NJogadorCPU(NJogadorRandom)
        Label5.Text = NJogadorFinalCPU(4)

        NJogadorRandom = Int(360 * Rnd() + 1)
        NJogadorFinalCPU(5) = NJogadorCPU(NJogadorRandom)
        Label6.Text = NJogadorFinalCPU(5)
        'Escolha Aleatória do nome de Jogadores do CPU'

        'Escolha Aleatória do nome de Equipa do CPU'
        NEquipaRandom = Int(180 * Rnd() + 1)
        NEquipaFinalCPU = NEquipaCPU(NEquipaRandom)
        Label1.Text = NEquipaFinalCPU
        'Escolha Aleatória do nome de Equipa do CPU'

        'Escolha Aleatória das vitórias e das derrotas da Equipa do CPU'
        NVitóriasCPU = Int(10000 * Rnd())
        Label18.Text = "N. Vitórias: " & NVitóriasCPU

        NDerrotasCPU = Int(10000 * Rnd())
        Label19.Text = "N. Derrotas: " & NDerrotasCPU
        'Escolha Aleatória das vitórias e das derrotas da Equipa do CPU'

        'Média da Equipa'
        médiaCPU = (PoderCPU(1) + PoderCPU(2) + PoderCPU(3) + PoderCPU(4) + PoderCPU(5)) / 5
        Label23.Text = médiaCPU & "/100"
        'Média da Equipa'

        'Mostrar a Equipa com maior média'
        If MediaUTL > médiaCPU Then
            'vitória'
            Label22.BackColor = Color.Green
            Label23.BackColor = Color.Red
        ElseIf MediaUTL < médiaCPU Then
            'Derrota'
            Label22.BackColor = Color.Red
            Label23.BackColor = Color.Green
        Else
            'Empate'
            Label22.BackColor = Color.Orange
            Label23.BackColor = Color.Orange
        End If
        'Mostrar a Equipa com maior média'
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        'Jogar'
        Button1.Enabled = False
        Button2.Enabled = False
        Button3.Enabled = False

        pontosCPU = 0
        PontosUTL = 0

        If sinal = 0 Then
            MsgBox("Ainda não criou uma equipa adeversária")
        End If
        'Variavel do ciclo "for"'
        Dim count As Byte

        'Criação aleatória de Número máximo de pontos na partida'
        Randomize()
        NPontosRandom = Int(80 * Rnd() + 45)

        'Aparecer Situação do jogo'
        Label34.Text = "Jogo Iniciado"

        'Iniciação do ciclo for (jogo)'
        For count = 1 To NPontosRandom
            pausa(250)


            'Verificação de quem tem a maior média na ajuda dos calculos'
            If médiaCPU < MediaUTL Then

                'Obtenção da difença de média e a probabilidade de marcar ponto'
                diferençaMedias = MediaUTL - médiaCPU
                Probabilidade = 50 + diferençaMedias

                'Não permitir que a probabilidade seja maior que 100'
                If probabilidade >= 95 Then
                    probabilidade = 95
                End If
                'Não permitir que a probabilidade seja maior que 100'

                'Criação e verificação de quem maca o ponto, soma dos pontos para as equipas'
                PontosRandom = Int(100 * Rnd())
                If PontosRandom < probabilidade Then
                    PontosUTL += 1
                Else
                    pontosCPU += 1
                End If
            Else
                'Obtenção da difença de média e a probabilidade de marcar ponto'
                diferençaMedias = médiaCPU - MediaUTL
                probabilidade = 50 + diferençaMedias

                'Não permitir que a probabilidade seja maior que 100'
                If probabilidade >= 95 Then
                    probabilidade = 95
                End If

                'Criação e verificação de quem maca o ponto, soma dos pontos para as equipas'
                PontosRandom = Int(100 * Rnd())
                If PontosRandom < probabilidade Then
                    pontosCPU += 1
                Else
                    PontosUTL += 1
                End If
            End If

            'Deixar Visible e deisponivel as labels dos pontos, e Marcação dos pontos e Indentificação da Equipa e seus respetivos pontos'
            Label29.Visible = True
            Label31.Visible = True
            Label32.Visible = True
            Label30.Visible = True
            Label34.Visible = True

            Label29.Enabled = True
            Label31.Enabled = True
            Label32.Enabled = True
            Label30.Enabled = True
            Label34.Enabled = True

            Label31.Text = "Pontos " & NomeEquipa
            Label32.Text = "Pontos " & NEquipaFinalCPU
            Label29.Text = PontosUTL
            Label30.Text = pontosCPU

            'Colorir consoante a vitória e a derrota da equipa'
            If PontosUTL > pontosCPU Then
                'Vitória do Utilizador'
                Label29.BackColor = Color.Green
                Label31.BackColor = Color.Green
                Label32.BackColor = Color.Red
                Label30.BackColor = Color.Red
            ElseIf PontosUTL < pontosCPU Then
                'Derrota do Utilizador'
                Label29.BackColor = Color.Red
                Label31.BackColor = Color.Red
                Label30.BackColor = Color.Green
                Label32.BackColor = Color.Green
            Else
                'Empate'
                Label29.BackColor = Color.Orange
                Label31.BackColor = Color.Orange
                Label30.BackColor = Color.Orange
                Label32.BackColor = Color.Orange
            End If
        Next

        'Cheat de Vitória
        If VitoriaPorFavor = True Then
            PontosUTL = 130
        End If

        'Aumento do número de Vitórias/Derrotas e a soma do dinheiro'
        If PontosUTL > pontosCPU Then
            'vitória'
            NVitórias += 1
            DinheiroAcumolado += 25
            MsgBox("Muito bem! Ganhaste esta partida e ainda ganhaste 25$")
            Label34.Text = "Jogo Acabou"
        ElseIf PontosUTL < pontosCPU Then
            'Derrota'
            NDerrotas += 1
            DinheiroAcumolado += 10
            MsgBox("Perdeste esta partida, mas está tudo bem, ganhaste 10$")
            Label34.Text = "Jogo Acabou"
        Else
            MsgBox("A tua equipa empatou contra a do Computador")
            Label34.Text = "Jogo Empatado"
            desempate()
        End If

        'Atualizar vitórias e derrotas'
        Label20.Text = "N. Derrotas: " & NDerrotas
        Label21.Text = "N. Vitórias: " & NVitórias

        'Desbloquear Botões'
        pausa(300)
        Button1.Enabled = True
        Button3.Enabled = True
    End Sub

    Private Sub AnteVisao_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Plano Inicial'
        pausa(500)
        'Apresentação da Equipa do Utilizador'
        Label20.Text = "N. Derrotas: " & NDerrotas
        Label21.Text = "N. Vitórias: " & NVitórias

        Label12.Text = NomeEquipa

        Label22.Text = MediaUTL & "/100"

        Label11.Text = NJogadorTitular(1)
        Label9.Text = NJogadorTitular(2)
        Label7.Text = NJogadorTitular(3)
        Label8.Text = NJogadorTitular(4)
        Label10.Text = NJogadorTitular(5)

        Label24.Text = PJTitular(1) & "/100"
        Label26.Text = PJTitular(2) & "/100"
        Label28.Text = PJTitular(3) & "/100"
        Label27.Text = PJTitular(4) & "/100"
        Label25.Text = PJTitular(5) & "/100"
        'Apresentação da Equipa do Utilizador'

        'Boqueio do botão jogar'
        Button2.Enabled = False
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        TelaInicial.Show()
        Me.Close()
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        MsgBox("Aqui é onde podes por a tua equipa em prova. Como podes ver a tua equipa já está pronta para jogar.")
        MsgBox("Se andas á procura de um adeversário bom para te defrontar, clica em 'Criar Adversário'")
        MsgBox("Depois de teres criado o teu advérsário clica em jogar e espera até ver se és tu ou o computador que irá ganhar.")
        MsgBox("Podes jogar quantas veses quiseres, mas nunca com a mesma equipa!")
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        'Ver dinheiro'
        MsgBox("Total de Dinhiero Acumolado: " & DinheiroAcumolado & "$")
    End Sub

    Sub NomeEquipaRandomCPU()
        'Nomes disponiveis de Equipas do CPU'
        NEquipaCPU(1) = "Aveiro"
        NEquipaCPU(2) = "Beja"
        NEquipaCPU(3) = "Braga"
        NEquipaCPU(4) = "Bragança"
        NEquipaCPU(5) = "C. Branco"
        NEquipaCPU(6) = "Coimbra"
        NEquipaCPU(7) = "Évora"
        NEquipaCPU(8) = "Faro"
        NEquipaCPU(9) = "Guarda"
        NEquipaCPU(10) = "Leiria"
        NEquipaCPU(11) = "Lisboa"
        NEquipaCPU(12) = "Portoalegre"
        NEquipaCPU(13) = "Porto"
        NEquipaCPU(14) = "Santarém"
        NEquipaCPU(15) = "Stubal"
        NEquipaCPU(16) = "V. Castelo"
        NEquipaCPU(17) = "V. Real"
        NEquipaCPU(18) = "Viseu"
        NEquipaCPU(19) = "A. Heroísmo"
        NEquipaCPU(20) = "Funchal"
        NEquipaCPU(21) = "Horta"
        NEquipaCPU(22) = "Lamego"
        NEquipaCPU(23) = "P. Delgada"
        NEquipaCPU(24) = "Goa"
        NEquipaCPU(25) = "Abrantes"
        NEquipaCPU(26) = "Águeda"
        NEquipaCPU(27) = "Alandoroal"
        NEquipaCPU(28) = "Albofeira"
        NEquipaCPU(29) = "Alcácer Sal"
        NEquipaCPU(30) = "Alcanena"
        NEquipaCPU(31) = "Alcobaça"
        NEquipaCPU(32) = "Alcochete"
        NEquipaCPU(33) = "Alijó"
        NEquipaCPU(34) = "Almada"
        NEquipaCPU(35) = "Almeirim"
        NEquipaCPU(36) = "Alpiarça"
        NEquipaCPU(37) = "Alter Chão"
        NEquipaCPU(38) = "Alvaiázere"
        NEquipaCPU(39) = "Amadora"
        NEquipaCPU(40) = "A. Valdevez"
        NEquipaCPU(41) = "Armamar"
        NEquipaCPU(42) = "Arouca"
        NEquipaCPU(43) = "Aveiro"
        NEquipaCPU(44) = "Avis"
        NEquipaCPU(45) = "Barcelos"
        NEquipaCPU(46) = "Baião"
        NEquipaCPU(47) = "Barreiro"
        NEquipaCPU(48) = "Batalha"
        NEquipaCPU(49) = "Beja"
        NEquipaCPU(50) = "Belmonte"
        NEquipaCPU(51) = "Braga"
        NEquipaCPU(52) = "Bragança"
        NEquipaCPU(53) = "Cal. Rainha"
        NEquipaCPU(54) = "Cam. Maior"
        NEquipaCPU(55) = "Cascais"
        NEquipaCPU(56) = "Cas. Pera"
        NEquipaCPU(57) = "Cas. Paiva"
        NEquipaCPU(58) = "Cas. Verde"
        NEquipaCPU(59) = "Chamusca"
        NEquipaCPU(60) = "Chaves"
        NEquipaCPU(61) = "Cinfães"
        NEquipaCPU(62) = "Con. Nova"
        NEquipaCPU(63) = "Covilhã"
        NEquipaCPU(64) = "Elvas"
        NEquipaCPU(65) = "Entroncamento"
        NEquipaCPU(66) = "Espinho"
        NEquipaCPU(67) = "Esposende"
        NEquipaCPU(68) = "Estarreja"
        NEquipaCPU(69) = "Évora"
        NEquipaCPU(70) = "Faro"
        NEquipaCPU(71) = "Felgueiras"
        NEquipaCPU(72) = "Fig. Foz"
        NEquipaCPU(73) = "Funchal"
        NEquipaCPU(74) = "Fundão"
        NEquipaCPU(75) = "Góis"
        NEquipaCPU(76) = "Golegã"
        NEquipaCPU(77) = "Gondomar"
        NEquipaCPU(78) = "Gouveia"
        NEquipaCPU(79) = "Guarda"
        NEquipaCPU(80) = "Guimarães"
        NEquipaCPU(81) = "Horta"
        NEquipaCPU(82) = "Ílhavo"
        NEquipaCPU(83) = "Lagoa"
        NEquipaCPU(84) = "Lagos"
        NEquipaCPU(85) = "Lamego"
        NEquipaCPU(86) = "Leiria"
        NEquipaCPU(87) = "Loulé"
        NEquipaCPU(88) = "Lousã"
        NEquipaCPU(89) = "Lousada"
        NEquipaCPU(91) = "Mac. Cavaleiros"
        NEquipaCPU(92) = "Mafra"
        NEquipaCPU(93) = "Maia"
        NEquipaCPU(94) = "Matosinhos"
        NEquipaCPU(95) = "Mira"
        NEquipaCPU(96) = "Melgaço"
        NEquipaCPU(97) = "Mir. Corvo"
        NEquipaCPU(98) = "Mirandela"
        NEquipaCPU(99) = "Monção"
        NEquipaCPU(100) = "Monchique"
        NEquipaCPU(101) = "Montalegre"
        NEquipaCPU(102) = "Mon. Novo"
        NEquipaCPU(103) = "Montijo"
        NEquipaCPU(104) = "Mora"
        NEquipaCPU(105) = "Mortágua"
        NEquipaCPU(106) = "Murça"
        NEquipaCPU(107) = "Nazaré"
        NEquipaCPU(108) = "Nelas"
        NEquipaCPU(109) = "Nordeste"
        NEquipaCPU(110) = "Óbidos"
        NEquipaCPU(111) = "Odivelas"
        NEquipaCPU(112) = "Oeiras"
        NEquipaCPU(113) = "Oeiras"
        NEquipaCPU(114) = "Oli. Hospital"
        NEquipaCPU(115) = "Ourém"
        NEquipaCPU(116) = "Ourique"
        NEquipaCPU(117) = "Ovar"
        NEquipaCPU(118) = "P. Ferreira"
        NEquipaCPU(119) = "Palmela"
        NEquipaCPU(120) = "Pampilhosa Ser."
        NEquipaCPU(121) = "Paredes"
        NEquipaCPU(122) = "Par. Coura"
        NEquipaCPU(123) = "Pedrógão Gra."
        NEquipaCPU(124) = "Penacova"
        NEquipaCPU(125) = "Penafiel"
        NEquipaCPU(126) = "	Penamacor"
        NEquipaCPU(127) = "Penedono"
        NEquipaCPU(128) = "Penela"
        NEquipaCPU(129) = "Pombal"
        NEquipaCPU(130) = "Pon. Delgada"
        NEquipaCPU(131) = "Pon. Lima"
        NEquipaCPU(132) = "Portalegre"
        NEquipaCPU(133) = "Portimão"
        NEquipaCPU(134) = "Por. Mós"
        NEquipaCPU(135) = "Por. Moniz"
        NEquipaCPU(136) = "Por. Santo"
        NEquipaCPU(137) = "Póv. Lanhoso"
        NEquipaCPU(138) = "Póv. Varzim"
        NEquipaCPU(139) = "Reg. Monsaraz"
        NEquipaCPU(140) = "Sabugal"
        NEquipaCPU(141) = "San. Cruz"
        NEquipaCPU(142) = "Santarém"
        NEquipaCPU(143) = "San. Cacém"
        NEquipaCPU(144) = "San. Tirso"
        NEquipaCPU(145) = "S. Joã. Madeira"
        NEquipaCPU(146) = "S. Ped. Sul"
        NEquipaCPU(147) = "S. Vicente"
        NEquipaCPU(148) = "Sardoal"
        NEquipaCPU(149) = "Seixal"
        NEquipaCPU(150) = "Serpa"
        NEquipaCPU(151) = "Sertã"
        NEquipaCPU(152) = "Setúbal"
        NEquipaCPU(153) = "Sintra"
        NEquipaCPU(154) = "Soure"
        NEquipaCPU(155) = "Tavira"
        NEquipaCPU(156) = "Tomar"
        NEquipaCPU(157) = "Tondela"
        NEquipaCPU(158) = "Tor. Moncorvo"
        NEquipaCPU(159) = "Tor. Novas"
        NEquipaCPU(160) = "Tor. Vedras"
        NEquipaCPU(161) = "Trofa"
        NEquipaCPU(162) = "Valongo"
        NEquipaCPU(163) = "Valpaços"
        NEquipaCPU(164) = "Via. Castelo"
        NEquipaCPU(165) = "Vie. Minho"
        NEquipaCPU(166) = "Vila Conde"
        NEquipaCPU(167) = "Vila Bispo"
        NEquipaCPU(168) = "Vila Flor"
        NEquipaCPU(169) = "V. Fra. Xira"
        NEquipaCPU(170) = "Famalicão"
        NEquipaCPU(171) = "Gaia"
        NEquipaCPU(172) = "Vila Real"
        NEquipaCPU(173) = "V. R. S. António"
        NEquipaCPU(174) = "Vila Viçosa"
        NEquipaCPU(175) = "Vila Verde"
        NEquipaCPU(176) = "Viseu"
        NEquipaCPU(177) = "Vizela"
        NEquipaCPU(178) = "Vouzela"
        NEquipaCPU(179) = "Via. Alentejo"
        NEquipaCPU(180) = "Vend. Novas"
        'Nomes disponiveis de jogadores do CPU'
    End Sub
    Sub NomeJogadorRandomCPU()
        'Nomes disponiveis de jogadores do CPU'
        NJogadorCPU(1) = "zé"
        NJogadorCPU(1) = "Élio"
        NJogadorCPU(2) = "Eliseu"
        NJogadorCPU(3) = "António"
        NJogadorCPU(4) = "Emídio"
        NJogadorCPU(5) = "Énio"
        NJogadorCPU(6) = "Alex"
        NJogadorCPU(7) = "Enzo"
        NJogadorCPU(8) = "Eric"
        NJogadorCPU(9) = "Ester"
        NJogadorCPU(10) = "Evaldo"
        NJogadorCPU(11) = "Alvaro"
        NJogadorCPU(12) = "Acílio"
        NJogadorCPU(13) = "Adalberto"
        NJogadorCPU(14) = "Adamantino"
        NJogadorCPU(15) = "Adeodato"
        NJogadorCPU(16) = "Adolfo"
        NJogadorCPU(17) = "Afonsino"
        NJogadorCPU(18) = "Afonso"
        NJogadorCPU(19) = "Albano"
        NJogadorCPU(20) = "Alberto"
        NJogadorCPU(21) = "Aldónio"
        NJogadorCPU(22) = "Alexandro"
        NJogadorCPU(23) = "Aléxio"
        NJogadorCPU(24) = "Alípio"
        NJogadorCPU(25) = "Alítio"
        NJogadorCPU(26) = "Almiro"
        NJogadorCPU(27) = "Alvarino"
        NJogadorCPU(28) = "Amadeu"
        NJogadorCPU(29) = "Amarildo"
        NJogadorCPU(30) = "Américo"
        NJogadorCPU(31) = "André"
        NJogadorCPU(32) = "Anfílito"
        NJogadorCPU(33) = "Anielo"
        NJogadorCPU(34) = "Anísio"
        NJogadorCPU(35) = "Anolido"
        NJogadorCPU(36) = "Antero"
        NJogadorCPU(37) = "Ápio"
        NJogadorCPU(38) = "Apolinário"
        NJogadorCPU(39) = "Aquiles"
        NJogadorCPU(40) = "Arcádio"
        NJogadorCPU(41) = "Arcélio"
        NJogadorCPU(42) = "Aristóteles"
        NJogadorCPU(43) = "Armandino"
        NJogadorCPU(44) = "Arquimedes"
        NJogadorCPU(45) = "Arsénio"
        NJogadorCPU(46) = "Artur"
        NJogadorCPU(47) = "Asélio"
        NJogadorCPU(48) = "Atão"
        NJogadorCPU(49) = "Áureo"
        NJogadorCPU(50) = "Austrelino"
        NJogadorCPU(51) = "Aventino"
        NJogadorCPU(52) = "Azélio"
        NJogadorCPU(53) = "Baldemar"
        NJogadorCPU(54) = "Baldomero"
        NJogadorCPU(55) = "Barão"
        NJogadorCPU(56) = "Batista"
        NJogadorCPU(57) = "Basílio"
        NJogadorCPU(58) = "Bartolomeu Dias"
        NJogadorCPU(59) = "Belchior"
        NJogadorCPU(60) = "Benjamim"
        NJogadorCPU(61) = "Bento"
        NJogadorCPU(62) = "Bernadete"
        NJogadorCPU(63) = "Bernardim"
        NJogadorCPU(64) = "Bernardino"
        NJogadorCPU(65) = "Bernardo"
        NJogadorCPU(66) = "Boanerges"
        NJogadorCPU(67) = "Bóris"
        NJogadorCPU(68) = "Brás"
        NJogadorCPU(69) = "Bráulio"
        NJogadorCPU(70) = "Brian"
        NJogadorCPU(71) = "Briolanjo"
        NJogadorCPU(72) = "Agostinho"
        NJogadorCPU(73) = "Agnelo"
        NJogadorCPU(74) = "Bruno"
        NJogadorCPU(75) = "Bruce"
        NJogadorCPU(76) = "Caetano"
        NJogadorCPU(77) = "Cândido"
        NJogadorCPU(78) = "Carlos"
        NJogadorCPU(79) = "Carmério"
        NJogadorCPU(80) = "Carmo"
        NJogadorCPU(81) = "Cássio"
        NJogadorCPU(82) = "Catarino"
        NJogadorCPU(83) = "Cedrico"
        NJogadorCPU(84) = "Célio"
        NJogadorCPU(85) = "Césaro"
        NJogadorCPU(86) = "Cildo"
        NJogadorCPU(87) = "Cirilo"
        NJogadorCPU(88) = "Clarindo"
        NJogadorCPU(89) = "Claudemiro"
        NJogadorCPU(90) = "Claudiano"
        NJogadorCPU(91) = "Clésio"
        NJogadorCPU(92) = "Crispim"
        NJogadorCPU(93) = "Cristiano"
        NJogadorCPU(94) = "Cristóforo"
        NJogadorCPU(95) = "Cristóvão"
        NJogadorCPU(96) = "Cursino"
        NJogadorCPU(97) = "Dácio"
        NJogadorCPU(98) = "Damião"
        NJogadorCPU(99) = "Daniel"
        NJogadorCPU(100) = "Dárcio"
        NJogadorCPU(101) = "Dante"
        NJogadorCPU(102) = "Dário"
        NJogadorCPU(103) = "David"
        NJogadorCPU(104) = "Délcio"
        NJogadorCPU(105) = "Délio"
        NJogadorCPU(106) = "Delmiro"
        NJogadorCPU(107) = "Deolindo"
        NJogadorCPU(108) = "Dércio"
        NJogadorCPU(109) = "Diego"
        NJogadorCPU(110) = "Dinis"
        NJogadorCPU(111) = "Diogo"
        NJogadorCPU(112) = "Dionísio"
        NJogadorCPU(113) = "Dírio"
        NJogadorCPU(114) = "Domingos"
        NJogadorCPU(115) = "Donzílio"
        NJogadorCPU(116) = "Dóris"
        NJogadorCPU(117) = "Duarte"
        NJogadorCPU(118) = "Durvalino"
        NJogadorCPU(119) = "Éder"
        NJogadorCPU(120) = "Eduardo"
        NJogadorCPU(121) = "Eduíno"
        NJogadorCPU(122) = "Eleutério"
        NJogadorCPU(123) = "Elgar"
        NJogadorCPU(124) = "Evangelino"
        NJogadorCPU(125) = "Ezequiel"
        NJogadorCPU(126) = "Fabiano"
        NJogadorCPU(127) = "Fábio"
        NJogadorCPU(128) = "Fabíola"
        NJogadorCPU(129) = "Fabrício"
        NJogadorCPU(130) = "Faustino"
        NJogadorCPU(131) = "Fausto"
        NJogadorCPU(132) = "Félix"
        NJogadorCPU(133) = "Ferdinando"
        NJogadorCPU(134) = "Ferrer"
        NJogadorCPU(135) = "Fidélio"
        NJogadorCPU(136) = "Filipo"
        NJogadorCPU(137) = "Filipe"
        NJogadorCPU(138) = "Filoteu"
        NJogadorCPU(139) = "Firmino"
        NJogadorCPU(140) = "Florentino"
        NJogadorCPU(141) = "Francisco"
        NJogadorCPU(142) = "Fred"
        NJogadorCPU(143) = "Frederico"
        NJogadorCPU(144) = "Fred"
        NJogadorCPU(145) = "Gabínio"
        NJogadorCPU(146) = "Gabriel"
        NJogadorCPU(147) = "Gabi"
        NJogadorCPU(148) = "Galileu"
        NJogadorCPU(149) = "Gaspar"
        NJogadorCPU(150) = "Genésio"
        NJogadorCPU(151) = "Georgino"
        NJogadorCPU(152) = "Gerberto"
        NJogadorCPU(153) = "Gervásio"
        NJogadorCPU(154) = "Gil"
        NJogadorCPU(155) = "Gildásio"
        NJogadorCPU(156) = "Gildo"
        NJogadorCPU(157) = "Giovani"
        NJogadorCPU(158) = "Gomes"
        NJogadorCPU(159) = "Gonçalo"
        NJogadorCPU(160) = "Graciano"
        NJogadorCPU(161) = "Guadalupe"
        NJogadorCPU(162) = "Guido"
        NJogadorCPU(163) = "Guilherme"
        NJogadorCPU(164) = "Gustavo"
        NJogadorCPU(165) = "Hamilton"
        NJogadorCPU(166) = "Hélder"
        NJogadorCPU(167) = "Hélvio"
        NJogadorCPU(168) = "Hemitério"
        NJogadorCPU(169) = "Henrique"
        NJogadorCPU(170) = "Hérman"
        NJogadorCPU(171) = "Hermano"
        NJogadorCPU(172) = "Hermínio"
        NJogadorCPU(173) = "Homero"
        NJogadorCPU(174) = "Hugo"
        NJogadorCPU(175) = "Hortênsio"
        NJogadorCPU(176) = "Iago"
        NJogadorCPU(177) = "Idálio"
        NJogadorCPU(178) = "Idélso"
        NJogadorCPU(179) = "Igor"
        NJogadorCPU(180) = "Xico"
        NJogadorCPU(181) = "Yuri"
        NJogadorCPU(182) = "Zacarias"
        NJogadorCPU(183) = "Zaido"
        NJogadorCPU(184) = "Zaíro"
        NJogadorCPU(185) = "Zaqueu"
        NJogadorCPU(186) = "Zé"
        NJogadorCPU(187) = "Zélio"
        NJogadorCPU(188) = "Zoé"
        NJogadorCPU(189) = "Zulmiro"
        NJogadorCPU(190) = "Infante"
        NJogadorCPU(191) = "Isaac"
        NJogadorCPU(192) = "Isac"
        NJogadorCPU(193) = "Isauro"
        NJogadorCPU(194) = "Ivan"
        NJogadorCPU(195) = "Ivo"
        NJogadorCPU(196) = "Jacó"
        NJogadorCPU(197) = "Jacob"
        NJogadorCPU(198) = "Jaime"
        NJogadorCPU(199) = "Jair"
        NJogadorCPU(200) = "James"
        NJogadorCPU(201) = "Januário"
        NJogadorCPU(202) = "Jaque"
        NJogadorCPU(203) = "Jardel"
        NJogadorCPU(204) = "Jerónimo"
        NJogadorCPU(205) = "Jesualdo"
        NJogadorCPU(206) = "Jesus"
        NJogadorCPU(207) = "João"
        NJogadorCPU(208) = "Joaquim"
        NJogadorCPU(209) = "Jocelino"
        NJogadorCPU(210) = "Joel"
        NJogadorCPU(211) = "Jonas"
        NJogadorCPU(212) = "Jordano"
        NJogadorCPU(213) = "Joscelino"
        NJogadorCPU(214) = "José"
        NJogadorCPU(215) = "Josefo"
        NJogadorCPU(216) = "Joselindo"
        NJogadorCPU(217) = "Josué"
        NJogadorCPU(218) = "Judas"
        NJogadorCPU(219) = "Júlio"
        NJogadorCPU(220) = "Junior"
        NJogadorCPU(221) = "Laurénio"
        NJogadorCPU(222) = "Lauro"
        NJogadorCPU(223) = "Lázaro"
        NJogadorCPU(224) = "Lécio"
        NJogadorCPU(225) = "Leoberto"
        NJogadorCPU(226) = "Leonardo"
        NJogadorCPU(227) = "Leonício"
        NJogadorCPU(228) = "Liciniano"
        NJogadorCPU(229) = "Lilian"
        NJogadorCPU(230) = "Lisandro"
        NJogadorCPU(231) = "Lorenzo"
        NJogadorCPU(232) = "Lourenço"
        NJogadorCPU(233) = "Lucas"
        NJogadorCPU(234) = "Lucílio"
        NJogadorCPU(235) = "Lúcio"
        NJogadorCPU(236) = "Luís"
        NJogadorCPU(237) = "Manel"
        NJogadorCPU(238) = "Manuel"
        NJogadorCPU(239) = "Marco"
        NJogadorCPU(240) = "Márcio"
        NJogadorCPU(241) = "Marcelo"
        NJogadorCPU(242) = "Mariano"
        NJogadorCPU(243) = "Mário"
        NJogadorCPU(244) = "Martim"
        NJogadorCPU(245) = "Maurício"
        NJogadorCPU(246) = "Micael"
        NJogadorCPU(247) = "Mercedes"
        NJogadorCPU(249) = "Michele"
        NJogadorCPU(250) = "Moisés"
        NJogadorCPU(251) = "Nadine"
        NJogadorCPU(252) = "Napoleão"
        NJogadorCPU(254) = "Nelson"
        NJogadorCPU(255) = "Nicolas"
        NJogadorCPU(256) = "Nicolau"
        NJogadorCPU(257) = "Noa"
        NJogadorCPU(258) = "Norberto"
        NJogadorCPU(259) = "Normando"
        NJogadorCPU(260) = "Nuno"
        NJogadorCPU(261) = "Octávio"
        NJogadorCPU(262) = "Otávio"
        NJogadorCPU(263) = "Olívio"
        NJogadorCPU(264) = "Óscar"
        NJogadorCPU(265) = "Orlando"
        NJogadorCPU(266) = "Osvaldo"
        NJogadorCPU(267) = "Palmiro"
        NJogadorCPU(268) = "Pascoal"
        NJogadorCPU(269) = "Patrício"
        NJogadorCPU(270) = "Paulino"
        NJogadorCPU(271) = "Paulo"
        NJogadorCPU(272) = "Pedro"
        NJogadorCPU(273) = "Pépio"
        NJogadorCPU(274) = "Querubim"
        NJogadorCPU(275) = "Quévin"
        NJogadorCPU(276) = "Quim"
        NJogadorCPU(277) = "Quitério"
        NJogadorCPU(278) = "Rafael"
        NJogadorCPU(279) = "Ramão"
        NJogadorCPU(280) = "Ramiro"
        NJogadorCPU(281) = "Raul"
        NJogadorCPU(282) = "Reginaldo"
        NJogadorCPU(283) = "Remo"
        NJogadorCPU(284) = "Renato"
        NJogadorCPU(285) = "Ricardo"
        NJogadorCPU(286) = "Roberto"
        NJogadorCPU(287) = "Rodolfo"
        NJogadorCPU(288) = "Rodrigo"
        NJogadorCPU(289) = "Rogério"
        NJogadorCPU(290) = "Romão"
        NJogadorCPU(291) = "Rómulo"
        NJogadorCPU(292) = "Ronaldo"
        NJogadorCPU(293) = "Rúben"
        NJogadorCPU(294) = "Ruca"
        NJogadorCPU(295) = "Salazar"
        NJogadorCPU(296) = "Salomão"
        NJogadorCPU(297) = "Salomé"
        NJogadorCPU(298) = "Salvador"
        NJogadorCPU(299) = "Salvadore"
        NJogadorCPU(300) = "Sancho"
        NJogadorCPU(301) = "Samuel"
        NJogadorCPU(302) = "Sancler"
        NJogadorCPU(303) = "Sancler"
        NJogadorCPU(304) = "Sancler"
        NJogadorCPU(305) = "Santelmo"
        NJogadorCPU(306) = "Saúl"
        NJogadorCPU(307) = "Sebastião"
        NJogadorCPU(308) = "Selmo"
        NJogadorCPU(309) = "Sénio"
        NJogadorCPU(310) = "Seomara"
        NJogadorCPU(311) = "Sérgio"
        NJogadorCPU(312) = "Simão"
        NJogadorCPU(313) = "Simauro"
        NJogadorCPU(314) = "Simone"
        NJogadorCPU(315) = "Sinésio"
        NJogadorCPU(316) = "Sisínio"
        NJogadorCPU(317) = "Sócrates"
        NJogadorCPU(318) = "Susano"
        NJogadorCPU(319) = "Taciano"
        NJogadorCPU(320) = "Tálio"
        NJogadorCPU(321) = "Tamár"
        NJogadorCPU(322) = "Tarcísio"
        NJogadorCPU(323) = "Telmo"
        NJogadorCPU(324) = "Teodemiro"
        NJogadorCPU(325) = "Teodoro"
        NJogadorCPU(326) = "Teófilo"
        NJogadorCPU(327) = "Teseu"
        NJogadorCPU(328) = "Tiago"
        NJogadorCPU(329) = "Tibério"
        NJogadorCPU(330) = "Tierri"
        NJogadorCPU(331) = "Timóteo"
        NJogadorCPU(332) = "Tito"
        NJogadorCPU(333) = "Tobias"
        NJogadorCPU(334) = "Tomás"
        NJogadorCPU(335) = "Tomé"
        NJogadorCPU(336) = "Toni"
        NJogadorCPU(337) = "Trindade"
        NJogadorCPU(338) = "Tony"
        NJogadorCPU(339) = "Ubaldino"
        NJogadorCPU(340) = "Ulisses"
        NJogadorCPU(341) = "Ursácio"
        NJogadorCPU(342) = "Úrsulo"
        NJogadorCPU(343) = "Vasco"
        NJogadorCPU(344) = "Veríssimo"
        NJogadorCPU(345) = "Vicente"
        NJogadorCPU(346) = "Victor"
        NJogadorCPU(347) = "Vidal"
        NJogadorCPU(348) = "Vílmar"
        NJogadorCPU(349) = "Virgílio"
        NJogadorCPU(350) = "Virgínio"
        NJogadorCPU(351) = "Viriato"
        NJogadorCPU(352) = "Vítor"
        NJogadorCPU(353) = "Vitório"
        NJogadorCPU(354) = "Vladimiro"
        NJogadorCPU(355) = "Wilson"
        NJogadorCPU(356) = "Xavier"
        NJogadorCPU(357) = "Xénio"
        NJogadorCPU(358) = "Xenócrates"
        NJogadorCPU(359) = "Xerxes"
        NJogadorCPU(360) = "Vinício"
        'Nomes disponiveis de jogadores do CPU'
    End Sub
End Class
