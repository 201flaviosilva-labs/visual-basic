﻿Public Class Torneio

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        'Voltar'
        TelaInicial.Show()
        Me.Close()
    End Sub

    Private Sub Torneio_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Loading Inicial
        Recompenca = 1
        NivtoriasTorneio()
        ApresentaçãoEquipas()
    End Sub

    Sub NivtoriasTorneio()
        If JogosVencidos = 2 Then
            Label32.Text = "Vitória"
            Label32.BackColor = Color.Green

        ElseIf JogosVencidos = 3 Then
            Label32.Text = "Vitória"
            Label32.BackColor = Color.Green

            Label31.Text = "Vitória"
            Label31.BackColor = Color.Green

        ElseIf JogosVencidos = 4 Then
            Label32.Text = "Vitória"
            Label32.BackColor = Color.Green

            Label31.Text = "Vitória"
            Label31.BackColor = Color.Green

            Label26.Text = "Vitória"
            Label26.BackColor = Color.Green

        ElseIf JogosVencidos = 5 Then
            Label32.Text = "Vitória"
            Label32.BackColor = Color.Green

            Label31.Text = "Vitória"
            Label31.BackColor = Color.Green

            Label26.Text = "Vitória"
            Label26.BackColor = Color.Green

            Label25.Text = "Vitória"
            Label25.BackColor = Color.Green

            If Recompenca = 1 Then
                MsgBox("Muito Bem vais a meio do Torneio. Ganhas 200$ para insentivo de mais vitórias!")
                DinheiroAcumolado += 200
                Recompenca += 1
            End If

        ElseIf JogosVencidos = 6 Then
            Label32.Text = "Vitória"
            Label32.BackColor = Color.Green

            Label31.Text = "Vitória"
            Label31.BackColor = Color.Green

            Label26.Text = "Vitória"
            Label26.BackColor = Color.Green

            Label25.Text = "Vitória"
            Label25.BackColor = Color.Green

            Label24.Text = "Vitória"
            Label24.BackColor = Color.Green

        ElseIf JogosVencidos = 7 Then
            Label32.Text = "Vitória"
            Label32.BackColor = Color.Green

            Label31.Text = "Vitória"
            Label31.BackColor = Color.Green

            Label26.Text = "Vitória"
            Label26.BackColor = Color.Green

            Label25.Text = "Vitória"
            Label25.BackColor = Color.Green

            Label24.Text = "Vitória"
            Label24.BackColor = Color.Green

            Label23.Text = "Vitória"
            Label23.BackColor = Color.Green

        ElseIf JogosVencidos = 8 Then
            Label32.Text = "Vitória"
            Label32.BackColor = Color.Green

            Label31.Text = "Vitória"
            Label31.BackColor = Color.Green

            Label26.Text = "Vitória"
            Label26.BackColor = Color.Green

            Label25.Text = "Vitória"
            Label25.BackColor = Color.Green

            Label24.Text = "Vitória"
            Label24.BackColor = Color.Green

            Label23.Text = "Vitória"
            Label23.BackColor = Color.Green

            Label22.Text = "Vitória"
            Label22.BackColor = Color.Green

            If Recompenca = 2 Then
                MsgBox("Muito Bem só falta mais um jogo para o fim do Torneio. Ganhas 500$ para a ultimo jogo!")
                DinheiroAcumolado += 500
                Recompenca += 1
            End If

        ElseIf JogosVencidos = 9 Then
            Label32.Text = "Vitória"
            Label32.BackColor = Color.Green

            Label31.Text = "Vitória"
            Label31.BackColor = Color.Green

            Label26.Text = "Vitória"
            Label26.BackColor = Color.Green

            Label25.Text = "Vitória"
            Label25.BackColor = Color.Green

            Label24.Text = "Vitória"
            Label24.BackColor = Color.Green

            Label23.Text = "Vitória"
            Label23.BackColor = Color.Green

            Label22.Text = "Vitória"
            Label22.BackColor = Color.Green

            Label21.Text = "Vitória"
            Label21.BackColor = Color.Green

            If Recompenca = 3 Then
                MsgBox("Muito Bem venceste o Torneio. Ganhas mais 1000$. Tenta Mais uma vez!")
                DinheiroAcumolado += 1000
                Recompenca += 1
            End If
        Else
            Label32.Text = ""
            Label32.BackColor = Color.Red

            Label31.Text = ""
            Label31.BackColor = Color.Red

            Label26.Text = ""
            Label26.BackColor = Color.Red

            Label25.Text = ""
            Label25.BackColor = Color.Red

            Label24.Text = ""
            Label24.BackColor = Color.Red

            Label23.Text = ""
            Label23.BackColor = Color.Red

            Label22.Text = ""
            Label22.BackColor = Color.Red

            Label21.Text = ""
            Label21.BackColor = Color.Red
        End If
    End Sub

    Sub ApresentaçãoEquipas()
        Label6.Text = NomeEquipaTorn(1)
        Label15.Text = NomeEquipaTorn(2)
        Label16.Text = NomeEquipaTorn(3)
        Label17.Text = NomeEquipaTorn(4)
        Label18.Text = NomeEquipaTorn(5)
        Label36.Text = NomeEquipaTorn(6)
        Label4.Text = NomeEquipaTorn(7)
        Label19.Text = NomeEquipaTorn(8)
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        'Jogar'
        JogoTorneio.Show()
        Me.Close()
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        'Recomeçar'
        Recompenca = 1
        JogosVencidos = 1
        NivtoriasTorneio()
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        'Minha Equipa'
        TorneioTF = True
        MinhaEquipa.Show()
        Me.Close()
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        'Treino'
        TorneioTF = True
        Treino.Show()
        Me.Close()
    End Sub

    Private Sub Button10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button10.Click
        'Ajuda'
        MsgBox("Aqui é o torneio, onde podes ir enfrentando equipas cada vez mais fortes, se conseguires chagera ao final e venceres, terás um presente, força nisso.")
    End Sub
End Class