﻿Public Class Créditos
    Dim tempo As Byte
    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        tempo = tempo + 1
        If tempo = 1 Then
            Label1.BackColor = Color.White
            Label1.ForeColor = Color.Black
            Button1.BackColor = Color.White
            Button1.ForeColor = Color.Black

            Label2.BackColor = Color.White
            Label2.ForeColor = Color.Black
            Button2.BackColor = Color.White
            Button2.ForeColor = Color.Black

        ElseIf tempo = 2 Then
            Label1.BackColor = Color.Black
            Label1.ForeColor = Color.White
            Button1.BackColor = Color.Black
            Button1.ForeColor = Color.White

            Label2.BackColor = Color.Black
            Label2.ForeColor = Color.White
            Button2.BackColor = Color.Black
            Button2.ForeColor = Color.White
            tempo = 0
        End If
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        TelaInicial.Show()
        Me.Close()
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        'Ajuda'
        MsgBox("Aqui podes ver o criador deste jogo e tens a opção de desligar o tutorial, tal como clicando em 'outros' podes ver as fontes que o criador usou para melhorar o jogo.")
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        ' Outros '
        MsgBox("Neste Butão podes ver sites que eu utilizei para melhorar este jogo, eles também merecem os créditos!")
        MsgBox("https://www.portugal-a-programar.pt/")
        MsgBox("https://pt.wikipedia.org/wiki/Lista_de_nomes_portugueses")
        MsgBox("https://pt.wikipedia.org/wiki/Lista_de_munic%C3%ADpios_de_Portugal")
        MsgBox("https://pt.wikipedia.org/wiki/Distritos_de_Portugal")
    End Sub
End Class