﻿Public Class Academia

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Woodbury.Show()
        Me.Close()
    End Sub

    Private Sub Academia_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Label12.Text = Forca & "/100"
        Label10.Text = Percepcao & "/100"
        Label19.Text = Resistencia & "/100"
        Label21.Text = carisma & "/100"
        Label2.Text = Inteligencia & "/100"
        Label7.Text = Agilidade & "/100"
        Label9.Text = Sorte & "/100"
        Label4.Text = Dinheiro & "$"
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        'Força
        Forca += 1
        If Forca >= 100 Then
            Forca = 100
            Button1.Enabled = False
        ElseIf Dinheiro < 100 Then
            MsgBox("Não tens dinheiro suficiente")
            Forca -= 1
        Else
            Dinheiro -= 100
        End If
        Label12.Text = Forca & "/100"
        Label4.Text = Dinheiro & "$"
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        'Percepção
        Percepcao += 1
        If Percepcao >= 100 Then
            Percepcao = 100
            Button4.Enabled = False
        ElseIf Dinheiro < 100 Then
            MsgBox("Não tens dinheiro suficiente")
            Percepcao -= 1
        Else
            Dinheiro -= 100
        End If
        Label10.Text = Percepcao & "/100"
        Label4.Text = Dinheiro & "$"
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        'Resistência
        Resistencia += 1
        If Resistencia >= 100 Then
            Resistencia = 100
            Button6.Enabled = False
        ElseIf Dinheiro < 100 Then
            MsgBox("Não tens dinheiro suficiente")
            Resistencia -= 1
        Else
            Dinheiro -= 100
        End If
        Label19.Text = Resistencia & "/100"
        Label4.Text = Dinheiro & "$"
    End Sub

    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click
        'Carisma
        Carisma += 1
        If Carisma >= 100 Then
            Carisma = 100
            Button8.Enabled = False
        ElseIf Dinheiro < 100 Then
            MsgBox("Não tens dinheiro suficiente")
            Carisma -= 1
        Else
            Dinheiro -= 100
        End If
        Label21.Text = Carisma & "/100"
        Label4.Text = Dinheiro & "$"
    End Sub

    Private Sub Button10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button10.Click
        'Inteligência
        Inteligencia += 1
        If Inteligencia >= 100 Then
            Inteligencia = 100
            Button10.Enabled = False
        ElseIf Dinheiro < 100 Then
            MsgBox("Não tens dinheiro suficiente")
            Inteligencia -= 1
        Else
            Dinheiro -= 100
        End If
        Label2.Text = Inteligencia & "/100"
        Label4.Text = Dinheiro & "$"
    End Sub

    Private Sub Button12_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button12.Click
        'Agilidade
        Agilidade += 1
        If Agilidade >= 100 Then
            Agilidade = 100
            Button12.Enabled = False
        ElseIf Dinheiro < 100 Then
            MsgBox("Não tens dinheiro suficiente")
            Agilidade -= 1
        Else
            Dinheiro -= 100
        End If
        Label7.Text = Agilidade & "/100"
        Label4.Text = Dinheiro & "$"
    End Sub

    Private Sub Button14_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button14.Click
        'Sorte
        Sorte += 1
        If Sorte >= 100 Then
            Sorte = 100
            Button14.Enabled = False
        ElseIf Dinheiro < 100 Then
            MsgBox("Não tens dinheiro suficiente")
            Sorte -= 1
        Else
            Dinheiro -= 100
        End If
        Label9.Text = Sorte & "/100"
        Label4.Text = Dinheiro & "$"
    End Sub
End Class