﻿Public Class F_Personagem
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Woodbury.Show()
        Me.Close()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Inventário.Show()
        Me.Close()
    End Sub

    Private Sub Label24_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label24.Click
        NomePersonagem = InputBox("Diz um nome para a tua personagem:", "Informações", "Nome")
    End Sub

    Private Sub F_Personagem_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Label14.Text = Nivel & "/50"  'Quanto mais melhor, Melhor o Nivel mais possibilidades
        Label13.Text = Dinheiro & "$" 'Quanto mais melhor, Mais pode comprar
        Label8.Text = Vida & "/100" 'Quanto mais melhor, Melhor Saúde tem
        Label9.Text = Radiacao & "/100" 'Quanto mais melhor, menos Radiação tem
        Label16.Text = Protecao 'Quanto mais melhor, menos Proteção está
        Label11.Text = Fome & "/100" 'Quanto mais meelhor, menos Fome tem
        Label12.Text = Sede & "/100" 'Quanto mais melhor, menos menos Sede tem
        Label10.Text = Cansaco & "/100" 'Quanto mais melhor, menos Cansaço está
        Label19.Text = Sanidade & "/100" 'Quanto mais melhor, mais Sanidade tem
        Label21.Text = Felicidade & "/100" 'Quanto mais melhor, mais Feliciade tem

        'Skills do Fallout (Skills Fisicas)'
        Label34.Text = Forca & "/100" 'Quanto mais melhor, Mais Força tem
        Label35.Text = Percepcao & "/100" 'Quanto mais melhor, Mais Percepção tem
        Label32.Text = Resistencia & "/100" 'Quanto mais melhor, Mais Resistência tem
        Label30.Text = Carisma & "/100" 'Quanto mais melhor, Mais Carisma tem
        Label28.Text = Inteligencia & "/100" 'Quanto mais melhor, Mais Inteligência tem
        Label26.Text = Agilidade & "/100" 'Quanto mais melhor, Mais Agilidade tem
        Label22.Text = Sorte & "/100" 'Quanto mais melhor, Mais Sorte tem
    End Sub
End Class