﻿Public Class Form1
    Dim pontos As Integer = 0
    Dim Vidas As Integer = 0

    Private Sub Form1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress

        'Esquerda ------------------------
        'Topo
        If e.KeyChar = "W" Or e.KeyChar = "w" Then
            If Pic_Cpu_Esquerda_Topo.Visible = True Then
                Certo()
            Else
                Errado()
            End If
        End If

        'Centro
        If e.KeyChar = "A" Or e.KeyChar = "a" Then
            If Pic_Cpu_Esquerda_Topo.Visible = True Then
                Certo()
            Else
                Errado()
            End If
        End If

        'Baixo
        If e.KeyChar = "Z" Or e.KeyChar = "z" Then
            If Pic_Cpu_Esquerda_Topo.Visible = True Then
                Certo()
            Else
                Errado()
            End If
        End If


        'Direita --------------------
        'Topo
        If e.KeyChar = "I" Or e.KeyChar = "i" Then
            If Pic_Cpu_Esquerda_Topo.Visible = True Then
                Certo()
            Else
                Errado()
            End If
        End If

        'Centro
        If e.KeyChar = "K" Or e.KeyChar = "k" Then
            If Pic_Cpu_Esquerda_Topo.Visible = True Then
                Certo()
            Else
                Errado()
            End If
        End If

        'Baixo
        If e.KeyChar = "M" Or e.KeyChar = "m" Then
            If Pic_Cpu_Esquerda_Topo.Visible = True Then
                Certo()
            Else
                Errado()
            End If
        End If

        RandomPicAparecer()


        '-----------------------------
        'Sair
        If e.KeyChar = "C" Or e.KeyChar = "c" Then
            Me.Close()
        End If
    End Sub

    Private Sub Pic_Cpu_Esquerda_Topo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Pic_Cpu_Esquerda_Topo.Click
        MsgBox("Ok")
    End Sub

    Private Sub Pic_Cpu_Esquerda_Centro_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Pic_Cpu_Esquerda_Centro.Click
        MsgBox("Ok")
    End Sub

    Private Sub Pic_Cpu_Esquerda_Baixo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Pic_Cpu_Esquerda_Baixo.Click
        MsgBox("Ok")
    End Sub

    Private Sub Pic_Cpu_Diretira_Topo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Pic_Cpu_Direitira_Topo.Click
        MsgBox("Ok")
    End Sub

    Private Sub Pic_Cpu_Diretira_Centro_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Pic_Cpu_Direitira_Centro.Click
        MsgBox("Ok")
    End Sub

    Private Sub Pic_Cpu_Diretira_Baixo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Pic_Cpu_Direitira_Baixo.Click
        MsgBox("Ok")
    End Sub

    Sub Certo()
        pontos += 1
        Label16.Text = "Pontos: " & pontos
    End Sub

    Sub Errado()
        Vidas += 1
        If Vidas = 1 Then
            Vida_1.Visible = False
        End If
        If Vidas = 2 Then
            Vida_2.Visible = False
        End If
        If Vidas = 3 Then
            Vida_3.Visible = False
        End If
    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        RandomPicAparecer()
    End Sub

    Sub RandomPicAparecer()

        Pic_Cpu_Esquerda_Topo.Visible = False
        Pic_Cpu_Esquerda_Centro.Visible = False
        Pic_Cpu_Esquerda_Baixo.Visible = False
        Pic_Cpu_Direitira_Topo.Visible = False
        Pic_Cpu_Direitira_Centro.Visible = False
        Pic_Cpu_Direitira_Baixo.Visible = False

        Dim AleatoriaPic As Integer
        Randomize()
        AleatoriaPic = Int(6 * Rnd() + 1) 'Cria um número inteiro de 0 + 1 = 1 a 6

        If AleatoriaPic = 1 Then
            Pic_Cpu_Esquerda_Topo.Visible = True
        ElseIf AleatoriaPic = 2 Then
            Pic_Cpu_Esquerda_Centro.Visible = True
        ElseIf AleatoriaPic = 3 Then
            Pic_Cpu_Esquerda_Baixo.Visible = True
        ElseIf AleatoriaPic = 4 Then
            Pic_Cpu_Direitira_Topo.Visible = True
        ElseIf AleatoriaPic = 5 Then
            Pic_Cpu_Direitira_Centro.Visible = True
        Else
            Pic_Cpu_Direitira_Baixo.Visible = True
        End If
    End Sub
End Class
