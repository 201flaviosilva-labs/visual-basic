﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Vida_1 = New System.Windows.Forms.Label()
        Me.Vida_2 = New System.Windows.Forms.Label()
        Me.Vida_3 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.Pic_Cpu_Esquerda_Baixo = New System.Windows.Forms.PictureBox()
        Me.Pic_Cpu_Esquerda_Centro = New System.Windows.Forms.PictureBox()
        Me.Pic_Cpu_Esquerda_Topo = New System.Windows.Forms.PictureBox()
        Me.Pic_Cpu_Direitira_Baixo = New System.Windows.Forms.PictureBox()
        Me.Pic_Cpu_Direitira_Centro = New System.Windows.Forms.PictureBox()
        Me.Pic_Cpu_Direitira_Topo = New System.Windows.Forms.PictureBox()
        Me.Pic_Jogador = New System.Windows.Forms.PictureBox()
        CType(Me.Pic_Cpu_Esquerda_Baixo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Pic_Cpu_Esquerda_Centro, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Pic_Cpu_Esquerda_Topo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Pic_Cpu_Direitira_Baixo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Pic_Cpu_Direitira_Centro, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Pic_Cpu_Direitira_Topo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Pic_Jogador, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label1.Font = New System.Drawing.Font("Consolas", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(55, 37)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(30, 30)
        Me.Label1.TabIndex = 7
        Me.Label1.Text = "W"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label2
        '
        Me.Label2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label2.Font = New System.Drawing.Font("Consolas", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(27, 130)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(30, 30)
        Me.Label2.TabIndex = 8
        Me.Label2.Text = "A"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label3
        '
        Me.Label3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label3.Font = New System.Drawing.Font("Consolas", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(55, 214)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(30, 30)
        Me.Label3.TabIndex = 9
        Me.Label3.Text = "Z"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label4
        '
        Me.Label4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label4.Font = New System.Drawing.Font("Consolas", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(400, 37)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(30, 30)
        Me.Label4.TabIndex = 10
        Me.Label4.Text = "I"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label5
        '
        Me.Label5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label5.Font = New System.Drawing.Font("Consolas", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(434, 130)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(30, 30)
        Me.Label5.TabIndex = 11
        Me.Label5.Text = "K"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label6
        '
        Me.Label6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label6.Font = New System.Drawing.Font("Consolas", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(400, 214)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(30, 30)
        Me.Label6.TabIndex = 12
        Me.Label6.Text = "M"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Vida_1
        '
        Me.Vida_1.BackColor = System.Drawing.Color.Red
        Me.Vida_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Vida_1.Font = New System.Drawing.Font("Consolas", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Vida_1.Location = New System.Drawing.Point(190, 240)
        Me.Vida_1.Name = "Vida_1"
        Me.Vida_1.Size = New System.Drawing.Size(30, 30)
        Me.Vida_1.TabIndex = 13
        Me.Vida_1.Text = "X"
        Me.Vida_1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.Vida_1.Visible = False
        '
        'Vida_2
        '
        Me.Vida_2.BackColor = System.Drawing.Color.Red
        Me.Vida_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Vida_2.Font = New System.Drawing.Font("Consolas", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Vida_2.Location = New System.Drawing.Point(227, 240)
        Me.Vida_2.Name = "Vida_2"
        Me.Vida_2.Size = New System.Drawing.Size(30, 30)
        Me.Vida_2.TabIndex = 14
        Me.Vida_2.Text = "X"
        Me.Vida_2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.Vida_2.Visible = False
        '
        'Vida_3
        '
        Me.Vida_3.BackColor = System.Drawing.Color.Red
        Me.Vida_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Vida_3.Font = New System.Drawing.Font("Consolas", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Vida_3.Location = New System.Drawing.Point(263, 240)
        Me.Vida_3.Name = "Vida_3"
        Me.Vida_3.Size = New System.Drawing.Size(30, 30)
        Me.Vida_3.TabIndex = 15
        Me.Vida_3.Text = "X"
        Me.Vida_3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.Vida_3.Visible = False
        '
        'Label10
        '
        Me.Label10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label10.Font = New System.Drawing.Font("Consolas", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(304, 71)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(90, 23)
        Me.Label10.TabIndex = 16
        Me.Label10.Text = "1000"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.Label10.Visible = False
        '
        'Label11
        '
        Me.Label11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label11.Font = New System.Drawing.Font("Consolas", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(331, 159)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(90, 23)
        Me.Label11.TabIndex = 17
        Me.Label11.Text = "1000"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.Label11.Visible = False
        '
        'Label12
        '
        Me.Label12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label12.Font = New System.Drawing.Font("Consolas", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(304, 247)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(90, 23)
        Me.Label12.TabIndex = 18
        Me.Label12.Text = "1000"
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.Label12.Visible = False
        '
        'Label13
        '
        Me.Label13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label13.Font = New System.Drawing.Font("Consolas", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(91, 247)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(90, 23)
        Me.Label13.TabIndex = 19
        Me.Label13.Text = "1000"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.Label13.Visible = False
        '
        'Label14
        '
        Me.Label14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label14.Font = New System.Drawing.Font("Consolas", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Location = New System.Drawing.Point(72, 159)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(90, 23)
        Me.Label14.TabIndex = 20
        Me.Label14.Text = "1000"
        Me.Label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.Label14.Visible = False
        '
        'Label15
        '
        Me.Label15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label15.Font = New System.Drawing.Font("Consolas", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(91, 71)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(90, 23)
        Me.Label15.TabIndex = 21
        Me.Label15.Text = "1000"
        Me.Label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.Label15.Visible = False
        '
        'Label16
        '
        Me.Label16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label16.Font = New System.Drawing.Font("Consolas", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.Location = New System.Drawing.Point(187, 37)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(111, 30)
        Me.Label16.TabIndex = 22
        Me.Label16.Text = "Pontos: 0"
        Me.Label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Timer1
        '
        Me.Timer1.Enabled = True
        Me.Timer1.Interval = 1
        '
        'Pic_Cpu_Esquerda_Baixo
        '
        Me.Pic_Cpu_Esquerda_Baixo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Pic_Cpu_Esquerda_Baixo.Image = CType(resources.GetObject("Pic_Cpu_Esquerda_Baixo.Image"), System.Drawing.Image)
        Me.Pic_Cpu_Esquerda_Baixo.Location = New System.Drawing.Point(91, 188)
        Me.Pic_Cpu_Esquerda_Baixo.Name = "Pic_Cpu_Esquerda_Baixo"
        Me.Pic_Cpu_Esquerda_Baixo.Size = New System.Drawing.Size(90, 80)
        Me.Pic_Cpu_Esquerda_Baixo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Pic_Cpu_Esquerda_Baixo.TabIndex = 6
        Me.Pic_Cpu_Esquerda_Baixo.TabStop = False
        Me.Pic_Cpu_Esquerda_Baixo.Visible = False
        '
        'Pic_Cpu_Esquerda_Centro
        '
        Me.Pic_Cpu_Esquerda_Centro.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Pic_Cpu_Esquerda_Centro.Image = CType(resources.GetObject("Pic_Cpu_Esquerda_Centro.Image"), System.Drawing.Image)
        Me.Pic_Cpu_Esquerda_Centro.Location = New System.Drawing.Point(72, 100)
        Me.Pic_Cpu_Esquerda_Centro.Name = "Pic_Cpu_Esquerda_Centro"
        Me.Pic_Cpu_Esquerda_Centro.Size = New System.Drawing.Size(90, 80)
        Me.Pic_Cpu_Esquerda_Centro.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Pic_Cpu_Esquerda_Centro.TabIndex = 5
        Me.Pic_Cpu_Esquerda_Centro.TabStop = False
        Me.Pic_Cpu_Esquerda_Centro.Visible = False
        '
        'Pic_Cpu_Esquerda_Topo
        '
        Me.Pic_Cpu_Esquerda_Topo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Pic_Cpu_Esquerda_Topo.Image = CType(resources.GetObject("Pic_Cpu_Esquerda_Topo.Image"), System.Drawing.Image)
        Me.Pic_Cpu_Esquerda_Topo.Location = New System.Drawing.Point(91, 12)
        Me.Pic_Cpu_Esquerda_Topo.Name = "Pic_Cpu_Esquerda_Topo"
        Me.Pic_Cpu_Esquerda_Topo.Size = New System.Drawing.Size(90, 80)
        Me.Pic_Cpu_Esquerda_Topo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Pic_Cpu_Esquerda_Topo.TabIndex = 4
        Me.Pic_Cpu_Esquerda_Topo.TabStop = False
        Me.Pic_Cpu_Esquerda_Topo.Visible = False
        '
        'Pic_Cpu_Direitira_Baixo
        '
        Me.Pic_Cpu_Direitira_Baixo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Pic_Cpu_Direitira_Baixo.Image = CType(resources.GetObject("Pic_Cpu_Direitira_Baixo.Image"), System.Drawing.Image)
        Me.Pic_Cpu_Direitira_Baixo.Location = New System.Drawing.Point(304, 188)
        Me.Pic_Cpu_Direitira_Baixo.Name = "Pic_Cpu_Direitira_Baixo"
        Me.Pic_Cpu_Direitira_Baixo.Size = New System.Drawing.Size(90, 80)
        Me.Pic_Cpu_Direitira_Baixo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Pic_Cpu_Direitira_Baixo.TabIndex = 3
        Me.Pic_Cpu_Direitira_Baixo.TabStop = False
        Me.Pic_Cpu_Direitira_Baixo.Visible = False
        '
        'Pic_Cpu_Direitira_Centro
        '
        Me.Pic_Cpu_Direitira_Centro.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Pic_Cpu_Direitira_Centro.Image = CType(resources.GetObject("Pic_Cpu_Direitira_Centro.Image"), System.Drawing.Image)
        Me.Pic_Cpu_Direitira_Centro.Location = New System.Drawing.Point(331, 100)
        Me.Pic_Cpu_Direitira_Centro.Name = "Pic_Cpu_Direitira_Centro"
        Me.Pic_Cpu_Direitira_Centro.Size = New System.Drawing.Size(90, 80)
        Me.Pic_Cpu_Direitira_Centro.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Pic_Cpu_Direitira_Centro.TabIndex = 2
        Me.Pic_Cpu_Direitira_Centro.TabStop = False
        Me.Pic_Cpu_Direitira_Centro.Visible = False
        '
        'Pic_Cpu_Direitira_Topo
        '
        Me.Pic_Cpu_Direitira_Topo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Pic_Cpu_Direitira_Topo.Image = Global.Street_Fight.My.Resources.Resources.Pronto_a_Disparar
        Me.Pic_Cpu_Direitira_Topo.Location = New System.Drawing.Point(304, 12)
        Me.Pic_Cpu_Direitira_Topo.Name = "Pic_Cpu_Direitira_Topo"
        Me.Pic_Cpu_Direitira_Topo.Size = New System.Drawing.Size(90, 80)
        Me.Pic_Cpu_Direitira_Topo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Pic_Cpu_Direitira_Topo.TabIndex = 1
        Me.Pic_Cpu_Direitira_Topo.TabStop = False
        Me.Pic_Cpu_Direitira_Topo.Visible = False
        '
        'Pic_Jogador
        '
        Me.Pic_Jogador.Location = New System.Drawing.Point(187, 83)
        Me.Pic_Jogador.Name = "Pic_Jogador"
        Me.Pic_Jogador.Size = New System.Drawing.Size(111, 125)
        Me.Pic_Jogador.TabIndex = 0
        Me.Pic_Jogador.TabStop = False
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(489, 301)
        Me.Controls.Add(Me.Label16)
        Me.Controls.Add(Me.Label15)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.Vida_3)
        Me.Controls.Add(Me.Vida_2)
        Me.Controls.Add(Me.Vida_1)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Pic_Cpu_Esquerda_Baixo)
        Me.Controls.Add(Me.Pic_Cpu_Esquerda_Centro)
        Me.Controls.Add(Me.Pic_Cpu_Esquerda_Topo)
        Me.Controls.Add(Me.Pic_Cpu_Direitira_Baixo)
        Me.Controls.Add(Me.Pic_Cpu_Direitira_Centro)
        Me.Controls.Add(Me.Pic_Cpu_Direitira_Topo)
        Me.Controls.Add(Me.Pic_Jogador)
        Me.Name = "Form1"
        Me.Text = "Streat Fight"
        CType(Me.Pic_Cpu_Esquerda_Baixo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Pic_Cpu_Esquerda_Centro, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Pic_Cpu_Esquerda_Topo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Pic_Cpu_Direitira_Baixo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Pic_Cpu_Direitira_Centro, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Pic_Cpu_Direitira_Topo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Pic_Jogador, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Pic_Jogador As System.Windows.Forms.PictureBox
    Friend WithEvents Pic_Cpu_Direitira_Topo As System.Windows.Forms.PictureBox
    Friend WithEvents Pic_Cpu_Direitira_Centro As System.Windows.Forms.PictureBox
    Friend WithEvents Pic_Cpu_Direitira_Baixo As System.Windows.Forms.PictureBox
    Friend WithEvents Pic_Cpu_Esquerda_Baixo As System.Windows.Forms.PictureBox
    Friend WithEvents Pic_Cpu_Esquerda_Centro As System.Windows.Forms.PictureBox
    Friend WithEvents Pic_Cpu_Esquerda_Topo As System.Windows.Forms.PictureBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Vida_1 As System.Windows.Forms.Label
    Friend WithEvents Vida_2 As System.Windows.Forms.Label
    Friend WithEvents Vida_3 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Timer1 As System.Windows.Forms.Timer

End Class
