﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me.Alien12 = New System.Windows.Forms.PictureBox()
        Me.Alien11 = New System.Windows.Forms.PictureBox()
        Me.Alien10 = New System.Windows.Forms.PictureBox()
        Me.Alien9 = New System.Windows.Forms.PictureBox()
        Me.Alien8 = New System.Windows.Forms.PictureBox()
        Me.Alien7 = New System.Windows.Forms.PictureBox()
        Me.Alien6 = New System.Windows.Forms.PictureBox()
        Me.Alien5 = New System.Windows.Forms.PictureBox()
        Me.Alien4 = New System.Windows.Forms.PictureBox()
        Me.Alien3 = New System.Windows.Forms.PictureBox()
        Me.Alien2 = New System.Windows.Forms.PictureBox()
        Me.Alien1 = New System.Windows.Forms.PictureBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Tiro5 = New System.Windows.Forms.Label()
        Me.Tiro4 = New System.Windows.Forms.Label()
        Me.Tiro3 = New System.Windows.Forms.Label()
        Me.Tiro2 = New System.Windows.Forms.Label()
        Me.Tiro1 = New System.Windows.Forms.Label()
        Me.InicialTiro = New System.Windows.Forms.Label()
        Me.SPNave = New System.Windows.Forms.PictureBox()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.Button1 = New System.Windows.Forms.Button()
        CType(Me.Alien12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Alien11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Alien10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Alien9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Alien8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Alien7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Alien6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Alien5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Alien4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Alien3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Alien2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Alien1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SPNave, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Alien12
        '
        Me.Alien12.Image = CType(resources.GetObject("Alien12.Image"), System.Drawing.Image)
        Me.Alien12.Location = New System.Drawing.Point(448, 74)
        Me.Alien12.Name = "Alien12"
        Me.Alien12.Size = New System.Drawing.Size(63, 55)
        Me.Alien12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Alien12.TabIndex = 40
        Me.Alien12.TabStop = False
        '
        'Alien11
        '
        Me.Alien11.Image = CType(resources.GetObject("Alien11.Image"), System.Drawing.Image)
        Me.Alien11.Location = New System.Drawing.Point(369, 74)
        Me.Alien11.Name = "Alien11"
        Me.Alien11.Size = New System.Drawing.Size(63, 55)
        Me.Alien11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Alien11.TabIndex = 39
        Me.Alien11.TabStop = False
        '
        'Alien10
        '
        Me.Alien10.Image = CType(resources.GetObject("Alien10.Image"), System.Drawing.Image)
        Me.Alien10.Location = New System.Drawing.Point(294, 74)
        Me.Alien10.Name = "Alien10"
        Me.Alien10.Size = New System.Drawing.Size(63, 55)
        Me.Alien10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Alien10.TabIndex = 38
        Me.Alien10.TabStop = False
        '
        'Alien9
        '
        Me.Alien9.Image = CType(resources.GetObject("Alien9.Image"), System.Drawing.Image)
        Me.Alien9.Location = New System.Drawing.Point(219, 74)
        Me.Alien9.Name = "Alien9"
        Me.Alien9.Size = New System.Drawing.Size(63, 55)
        Me.Alien9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Alien9.TabIndex = 37
        Me.Alien9.TabStop = False
        '
        'Alien8
        '
        Me.Alien8.Image = CType(resources.GetObject("Alien8.Image"), System.Drawing.Image)
        Me.Alien8.Location = New System.Drawing.Point(146, 74)
        Me.Alien8.Name = "Alien8"
        Me.Alien8.Size = New System.Drawing.Size(63, 55)
        Me.Alien8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Alien8.TabIndex = 36
        Me.Alien8.TabStop = False
        '
        'Alien7
        '
        Me.Alien7.Image = CType(resources.GetObject("Alien7.Image"), System.Drawing.Image)
        Me.Alien7.Location = New System.Drawing.Point(67, 74)
        Me.Alien7.Name = "Alien7"
        Me.Alien7.Size = New System.Drawing.Size(63, 55)
        Me.Alien7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Alien7.TabIndex = 35
        Me.Alien7.TabStop = False
        '
        'Alien6
        '
        Me.Alien6.Image = CType(resources.GetObject("Alien6.Image"), System.Drawing.Image)
        Me.Alien6.Location = New System.Drawing.Point(448, 12)
        Me.Alien6.Name = "Alien6"
        Me.Alien6.Size = New System.Drawing.Size(63, 55)
        Me.Alien6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Alien6.TabIndex = 34
        Me.Alien6.TabStop = False
        '
        'Alien5
        '
        Me.Alien5.Image = CType(resources.GetObject("Alien5.Image"), System.Drawing.Image)
        Me.Alien5.Location = New System.Drawing.Point(369, 12)
        Me.Alien5.Name = "Alien5"
        Me.Alien5.Size = New System.Drawing.Size(63, 55)
        Me.Alien5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Alien5.TabIndex = 33
        Me.Alien5.TabStop = False
        '
        'Alien4
        '
        Me.Alien4.Image = CType(resources.GetObject("Alien4.Image"), System.Drawing.Image)
        Me.Alien4.Location = New System.Drawing.Point(294, 12)
        Me.Alien4.Name = "Alien4"
        Me.Alien4.Size = New System.Drawing.Size(63, 55)
        Me.Alien4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Alien4.TabIndex = 32
        Me.Alien4.TabStop = False
        '
        'Alien3
        '
        Me.Alien3.Image = CType(resources.GetObject("Alien3.Image"), System.Drawing.Image)
        Me.Alien3.Location = New System.Drawing.Point(219, 12)
        Me.Alien3.Name = "Alien3"
        Me.Alien3.Size = New System.Drawing.Size(63, 55)
        Me.Alien3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Alien3.TabIndex = 31
        Me.Alien3.TabStop = False
        '
        'Alien2
        '
        Me.Alien2.Image = CType(resources.GetObject("Alien2.Image"), System.Drawing.Image)
        Me.Alien2.Location = New System.Drawing.Point(144, 12)
        Me.Alien2.Name = "Alien2"
        Me.Alien2.Size = New System.Drawing.Size(63, 55)
        Me.Alien2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Alien2.TabIndex = 30
        Me.Alien2.TabStop = False
        '
        'Alien1
        '
        Me.Alien1.Image = CType(resources.GetObject("Alien1.Image"), System.Drawing.Image)
        Me.Alien1.Location = New System.Drawing.Point(67, 12)
        Me.Alien1.Name = "Alien1"
        Me.Alien1.Size = New System.Drawing.Size(63, 55)
        Me.Alien1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Alien1.TabIndex = 29
        Me.Alien1.TabStop = False
        '
        'Label3
        '
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Font = New System.Drawing.Font("Consolas", 20.0!, System.Drawing.FontStyle.Bold)
        Me.Label3.ForeColor = System.Drawing.Color.White
        Me.Label3.Location = New System.Drawing.Point(567, 47)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(180, 35)
        Me.Label3.TabIndex = 42
        Me.Label3.Text = "Recorde: 1"
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Consolas", 20.0!, System.Drawing.FontStyle.Bold)
        Me.Label2.ForeColor = System.Drawing.Color.White
        Me.Label2.Location = New System.Drawing.Point(567, 12)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(180, 35)
        Me.Label2.TabIndex = 41
        Me.Label2.Text = "Nivel: 1"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Consolas", 48.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(223, 204)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(312, 75)
        Me.Label1.TabIndex = 43
        Me.Label1.Text = "Derrota!"
        Me.Label1.Visible = False
        '
        'Tiro5
        '
        Me.Tiro5.Image = CType(resources.GetObject("Tiro5.Image"), System.Drawing.Image)
        Me.Tiro5.Location = New System.Drawing.Point(370, 374)
        Me.Tiro5.Name = "Tiro5"
        Me.Tiro5.Size = New System.Drawing.Size(5, 20)
        Me.Tiro5.TabIndex = 50
        Me.Tiro5.Visible = False
        '
        'Tiro4
        '
        Me.Tiro4.Image = CType(resources.GetObject("Tiro4.Image"), System.Drawing.Image)
        Me.Tiro4.Location = New System.Drawing.Point(370, 374)
        Me.Tiro4.Name = "Tiro4"
        Me.Tiro4.Size = New System.Drawing.Size(5, 20)
        Me.Tiro4.TabIndex = 49
        Me.Tiro4.Visible = False
        '
        'Tiro3
        '
        Me.Tiro3.Image = CType(resources.GetObject("Tiro3.Image"), System.Drawing.Image)
        Me.Tiro3.Location = New System.Drawing.Point(370, 374)
        Me.Tiro3.Name = "Tiro3"
        Me.Tiro3.Size = New System.Drawing.Size(5, 20)
        Me.Tiro3.TabIndex = 48
        Me.Tiro3.Visible = False
        '
        'Tiro2
        '
        Me.Tiro2.Image = CType(resources.GetObject("Tiro2.Image"), System.Drawing.Image)
        Me.Tiro2.Location = New System.Drawing.Point(370, 374)
        Me.Tiro2.Name = "Tiro2"
        Me.Tiro2.Size = New System.Drawing.Size(5, 20)
        Me.Tiro2.TabIndex = 47
        Me.Tiro2.Visible = False
        '
        'Tiro1
        '
        Me.Tiro1.Image = CType(resources.GetObject("Tiro1.Image"), System.Drawing.Image)
        Me.Tiro1.Location = New System.Drawing.Point(370, 374)
        Me.Tiro1.Name = "Tiro1"
        Me.Tiro1.Size = New System.Drawing.Size(5, 20)
        Me.Tiro1.TabIndex = 46
        Me.Tiro1.Visible = False
        '
        'InicialTiro
        '
        Me.InicialTiro.Image = CType(resources.GetObject("InicialTiro.Image"), System.Drawing.Image)
        Me.InicialTiro.Location = New System.Drawing.Point(370, 374)
        Me.InicialTiro.Name = "InicialTiro"
        Me.InicialTiro.Size = New System.Drawing.Size(5, 20)
        Me.InicialTiro.TabIndex = 45
        Me.InicialTiro.Visible = False
        '
        'SPNave
        '
        Me.SPNave.BackgroundImage = Global.SpaceInvaders.My.Resources.Resources.SpaceInvadersShip
        Me.SPNave.Image = Global.SpaceInvaders.My.Resources.Resources.SpaceInvadersShip
        Me.SPNave.Location = New System.Drawing.Point(328, 374)
        Me.SPNave.Name = "SPNave"
        Me.SPNave.Size = New System.Drawing.Size(87, 97)
        Me.SPNave.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.SPNave.TabIndex = 44
        Me.SPNave.TabStop = False
        '
        'Timer1
        '
        Me.Timer1.Enabled = True
        Me.Timer1.Interval = 10
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(1, 2)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(69, 45)
        Me.Button1.TabIndex = 51
        Me.Button1.Text = "Sair"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = Global.SpaceInvaders.My.Resources.Resources.estrelas
        Me.ClientSize = New System.Drawing.Size(759, 483)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Tiro5)
        Me.Controls.Add(Me.Tiro4)
        Me.Controls.Add(Me.Tiro3)
        Me.Controls.Add(Me.Tiro2)
        Me.Controls.Add(Me.Tiro1)
        Me.Controls.Add(Me.InicialTiro)
        Me.Controls.Add(Me.SPNave)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Alien12)
        Me.Controls.Add(Me.Alien11)
        Me.Controls.Add(Me.Alien10)
        Me.Controls.Add(Me.Alien9)
        Me.Controls.Add(Me.Alien8)
        Me.Controls.Add(Me.Alien7)
        Me.Controls.Add(Me.Alien6)
        Me.Controls.Add(Me.Alien5)
        Me.Controls.Add(Me.Alien4)
        Me.Controls.Add(Me.Alien3)
        Me.Controls.Add(Me.Alien2)
        Me.Controls.Add(Me.Alien1)
        Me.Name = "Form1"
        Me.Text = "Form1"
        CType(Me.Alien12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Alien11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Alien10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Alien9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Alien8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Alien7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Alien6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Alien5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Alien4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Alien3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Alien2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Alien1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SPNave, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Alien12 As System.Windows.Forms.PictureBox
    Friend WithEvents Alien11 As System.Windows.Forms.PictureBox
    Friend WithEvents Alien10 As System.Windows.Forms.PictureBox
    Friend WithEvents Alien9 As System.Windows.Forms.PictureBox
    Friend WithEvents Alien8 As System.Windows.Forms.PictureBox
    Friend WithEvents Alien7 As System.Windows.Forms.PictureBox
    Friend WithEvents Alien6 As System.Windows.Forms.PictureBox
    Friend WithEvents Alien5 As System.Windows.Forms.PictureBox
    Friend WithEvents Alien4 As System.Windows.Forms.PictureBox
    Friend WithEvents Alien3 As System.Windows.Forms.PictureBox
    Friend WithEvents Alien2 As System.Windows.Forms.PictureBox
    Friend WithEvents Alien1 As System.Windows.Forms.PictureBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Tiro5 As System.Windows.Forms.Label
    Friend WithEvents Tiro4 As System.Windows.Forms.Label
    Friend WithEvents Tiro3 As System.Windows.Forms.Label
    Friend WithEvents Tiro2 As System.Windows.Forms.Label
    Friend WithEvents Tiro1 As System.Windows.Forms.Label
    Friend WithEvents InicialTiro As System.Windows.Forms.Label
    Friend WithEvents SPNave As System.Windows.Forms.PictureBox
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents Button1 As System.Windows.Forms.Button

End Class
