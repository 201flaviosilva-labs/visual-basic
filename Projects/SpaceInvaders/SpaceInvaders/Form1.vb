﻿Public Class Form1
    Dim alien(11) As PictureBox
    Dim localisacoes(11) As System.Drawing.Point

    Dim esquer As Boolean
    Dim direi As Boolean
    Dim mover As Integer = 3

    Dim fogo As Boolean
    Dim fogo1 As Boolean
    Dim fogo2 As Boolean
    Dim fogo3 As Boolean
    Dim fogo4 As Boolean
    Dim fogo5 As Boolean

    Dim alvo As Integer
    Dim vencedor As Integer
    Dim nivel As Integer = 1
    Dim recorde As Integer

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        cartearray()
    End Sub

    Private Sub Esquerda(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        If e.KeyValue = Keys.A Then
            esquer = True
        End If

        If e.KeyValue = Keys.D Then
            direi = True
        End If

        If e.KeyValue = Keys.L Then
            fogo = True
        End If
    End Sub

    Private Sub Direita(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        If e.KeyValue = Keys.A Then
            esquer = False
        End If

        If e.KeyValue = Keys.D Then
            direi = False
        End If

        If e.KeyValue = Keys.L Then
            fogo = False
        End If
    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        If esquer = True Then
            SPNave.Left = SPNave.Left - 3
            InicialTiro.Left = InicialTiro.Left - 3
            If fogo1 = False Then
                Tiro1.Left = Tiro1.Left - 3
            End If
            If fogo2 = False Then
                Tiro2.Left = Tiro2.Left - 3
            End If
            If fogo3 = False Then
                Tiro3.Left = Tiro3.Left - 3
            End If
            If fogo4 = False Then
                Tiro4.Left = Tiro4.Left - 3
            End If
            If fogo5 = False Then
                Tiro5.Left = Tiro5.Left - 3
            End If

            If SPNave.Left < 0 Then
                SPNave.Left = SPNave.Left + 3
                InicialTiro.Left = InicialTiro.Left + 3
                If fogo1 = False Then
                    Tiro1.Left = Tiro1.Left + 3
                End If
                If fogo2 = False Then
                    Tiro2.Left = Tiro2.Left + 3
                End If
                If fogo3 = False Then
                    Tiro3.Left = Tiro3.Left + 3
                End If
                If fogo4 = False Then
                    Tiro4.Left = Tiro4.Left + 3
                End If
                If fogo5 = False Then
                    Tiro5.Left = Tiro5.Left + 3
                End If
            End If
        End If

        If direi = True Then
            SPNave.Left = SPNave.Left + 3
            InicialTiro.Left = InicialTiro.Left + 3
            If fogo1 = False Then
                Tiro1.Left = Tiro1.Left + 3
            End If
            If fogo2 = False Then
                Tiro2.Left = Tiro2.Left + 3
            End If
            If fogo3 = False Then
                Tiro3.Left = Tiro3.Left + 3
            End If
            If fogo4 = False Then
                Tiro4.Left = Tiro4.Left + 3
            End If
            If fogo5 = False Then
                Tiro5.Left = Tiro5.Left + 3
            End If

            If SPNave.Left > Me.Width - SPNave.Width Then
                SPNave.Left = SPNave.Left - 3
                InicialTiro.Left = InicialTiro.Left - 3
                If fogo1 = False Then
                    Tiro1.Left = Tiro1.Left - 3
                End If
                If fogo2 = False Then
                    Tiro2.Left = Tiro2.Left - 3
                End If
                If fogo3 = False Then
                    Tiro3.Left = Tiro3.Left - 3
                End If
                If fogo4 = False Then
                    Tiro4.Left = Tiro4.Left - 3
                End If
                If fogo5 = False Then
                    Tiro5.Left = Tiro5.Left - 3
                End If
            End If
        End If

        movimento()
        If fogo = True Then
            disparotiro()
        End If
        movimentotiro()
    End Sub

    Sub cartearray()
        alien(0) = Alien1
        alien(1) = Alien2
        alien(2) = Alien3
        alien(3) = Alien4
        alien(4) = Alien5
        alien(5) = Alien6
        alien(6) = Alien7
        alien(7) = Alien8
        alien(8) = Alien9
        alien(9) = Alien10
        alien(10) = Alien11
        alien(11) = Alien12
        For i = 0 To 11
            localisacoes(i) = alien(i).Location
        Next
    End Sub

    Sub movimento()
        For i = 0 To 11
            alien(i).Left = alien(i).Left + mover
            If alien(i).Bounds.IntersectsWith(SPNave.Bounds) Then
                Derrota()

            End If
        Next

        If Alien6.Left > Me.Width - Alien6.Width Then
            mover = mover * -1
            For i = 0 To 11
                alien(i).Top = alien(i).Top + 25
            Next
        End If

        If Alien1.Left < 0 Then
            mover = mover * -1
            For i = 0 To 11
                alien(i).Top = alien(i).Top + 25
            Next
        End If
    End Sub

    Private Sub disparotiro()
        fogo = False
        If fogo1 = False Then
            fogo1 = True
            Tiro1.Show()
            Exit Sub
        End If
        If fogo2 = False Then
            fogo2 = True
            Tiro2.Show()
            Exit Sub
        End If
        If fogo3 = False Then
            fogo3 = True
            Tiro3.Show()
            Exit Sub
        End If
        If fogo4 = False Then
            fogo4 = True
            Tiro4.Show()
            Exit Sub
        End If
        If fogo5 = False Then
            fogo5 = True
            Tiro5.Show()
            Exit Sub
        End If
    End Sub

    Private Sub movimentotiro()
        If fogo1 = True Then
            Tiro1.Top = Tiro1.Top - 10
            For i = 0 To 11
                If Tiro1.Bounds.IntersectsWith(alien(i).Bounds) Then
                    alvo = i
                    tiro1hit()
                End If
            Next
            If Tiro1.Top < 0 Then
                Tiro1.Hide()
                fogo1 = False
                Tiro1.Location = InicialTiro.Location
            End If
        End If
        If fogo2 = True Then
            Tiro2.Top = Tiro2.Top - 10
            For i = 0 To 11
                If Tiro2.Bounds.IntersectsWith(alien(i).Bounds) Then
                    alvo = i
                    tiro2hit()
                End If
            Next

            If Tiro2.Top < 0 Then
                Tiro2.Hide()
                fogo2 = False
                Tiro2.Location = InicialTiro.Location
            End If
        End If
        If fogo3 = True Then
            Tiro3.Top = Tiro3.Top - 10
            For i = 0 To 11
                If Tiro3.Bounds.IntersectsWith(alien(i).Bounds) Then
                    alvo = i
                    tiro3hit()
                End If
            Next
            If Tiro3.Top < 0 Then
                Tiro3.Hide()
                fogo3 = False
                Tiro3.Location = InicialTiro.Location
            End If
        End If
        If fogo4 = True Then
            Tiro4.Top = Tiro4.Top - 10
            For i = 0 To 11
                If Tiro4.Bounds.IntersectsWith(alien(i).Bounds) Then
                    alvo = i
                    tiro4hit()
                End If
            Next
            If Tiro4.Top < 0 Then
                Tiro4.Hide()
                fogo4 = False
                Tiro4.Location = InicialTiro.Location
            End If
        End If
        If fogo5 = True Then
            Tiro5.Top = Tiro5.Top - 10
            For i = 0 To 11
                If Tiro5.Bounds.IntersectsWith(alien(i).Bounds) Then
                    alvo = i
                    tiro5hit()
                End If
            Next
            If Tiro5.Top < 0 Then
                Tiro5.Hide()
                fogo5 = False
                Tiro5.Location = InicialTiro.Location
            End If
        End If
    End Sub

    Private Sub tiro1hit()
        fogo1 = False
        Tiro1.Hide()
        Tiro1.Location = InicialTiro.Location
        alien(alvo).Top = alien(alvo).Top + 1000
        vencedor = vencedor + 1
        If vencedor = 12 Then
            FaseCompleta()
        End If
        recorde = recorde + 1
        Label3.Text = "recorde: " & recorde
    End Sub
    Private Sub tiro2hit()
        fogo2 = False
        Tiro2.Hide()
        Tiro2.Location = InicialTiro.Location
        alien(alvo).Top = alien(alvo).Top + 1000
        vencedor = vencedor + 1
        If vencedor = 12 Then
            FaseCompleta()
        End If
        recorde = recorde + 1
        Label3.Text = "recorde: " & recorde
    End Sub
    Private Sub tiro3hit()
        fogo3 = False
        Tiro3.Hide()
        Tiro3.Location = InicialTiro.Location
        alien(alvo).Top = alien(alvo).Top + 1000
        vencedor = vencedor + 1
        If vencedor = 12 Then
            FaseCompleta()
        End If
        recorde = recorde + 1
        Label3.Text = "recorde: " & recorde
    End Sub
    Private Sub tiro4hit()
        fogo4 = False
        Tiro4.Hide()
        Tiro4.Location = InicialTiro.Location
        alien(alvo).Top = alien(alvo).Top + 1000
        vencedor = vencedor + 1
        If vencedor = 12 Then
            FaseCompleta()
        End If
        recorde = recorde + 1
        Label3.Text = "recorde: " & recorde
    End Sub

    Private Sub tiro5hit()
        fogo5 = False
        Tiro5.Hide()
        Tiro5.Location = InicialTiro.Location
        alien(alvo).Top = alien(alvo).Top + 1000
        vencedor = vencedor + 1
        If vencedor = 12 Then
            FaseCompleta()
        End If
        recorde = recorde + 1
        Label3.Text = "recorde: " & recorde
    End Sub

    Private Sub Derrota()
        Timer1.Stop()
        SPNave.Image = My.Resources.SIExplusão
        Label1.Visible = True
    End Sub


    Private Sub FaseCompleta()
        vencedor = 0
        nivel = nivel + 1
        Label2.Text = "Nivel: " & nivel
        For i = 0 To 11
            alien(i).Location = localisacoes(i)
        Next
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        End
    End Sub
End Class
