﻿Public Class _3Numeros
    Dim A1, A2, A3, n1, n2, n3 As Byte
    Sub bloqueios()
        If RadioButton1.Checked = True Or RadioButton2.Checked = True Or RadioButton3.Checked = True Or RadioButton4.Checked = True Or RadioButton5.Checked = True Or RadioButton6.Checked = True Then
            Button1.Enabled = True
            Button3.Enabled = True
            Button4.Enabled = True
            Button5.Enabled = True
            Button12.Enabled = False

            RadioButton1.Enabled = False
            RadioButton2.Enabled = False
            RadioButton3.Enabled = False
            RadioButton4.Enabled = False
            RadioButton5.Enabled = False
            RadioButton6.Enabled = False

            MsgBox("Foi retirado da sua conta o dinheiro por cada aposta!")
        Else
            MsgBox("Ainda não decidiu o valor da aposta")
        End If
    End Sub

    Sub receber()
        If RadioButton1.Checked = True Then
            Conta = Conta + 0.5
        ElseIf RadioButton2.Checked = True Then
            Conta = Conta + 1
        ElseIf RadioButton3.Checked = True Then
            Conta = Conta + 2.5
        ElseIf RadioButton4.Checked = True Then
            Conta = Conta + 10
        ElseIf RadioButton5.Checked = True Then
            Conta = Conta + 10
        ElseIf RadioButton6.Checked = True Then
            Conta = Conta + 25
        End If
    End Sub

    Private Sub Button12_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button12.Click
        If RadioButton1.Checked = True Then
            If Conta < 1 Then
                MsgBox("Não tens dinheiro soficiente para apostar")
                RadioButton1.Checked = False
            Else
                Conta = Conta - 1
                bloqueios()
            End If
        ElseIf RadioButton2.Checked = True Then
            If Conta < 2 Then
                MsgBox("Não tens dinheiro soficiente para apostar")
                RadioButton2.Checked = False
            Else
                Conta = Conta - 2
                bloqueios()
            End If
        ElseIf RadioButton3.Checked = True Then
            If Conta < 5 Then
                MsgBox("Não tens dinheiro soficiente para apostar")
                RadioButton3.Checked = False
            Else
                Conta = Conta - 5
                bloqueios()
            End If
        ElseIf RadioButton4.Checked = True Then
            If Conta < 10 Then
                MsgBox("Não tens dinheiro soficiente para apostar")
                RadioButton4.Checked = False
            Else
                Conta = Conta - 10
                bloqueios()
            End If
        ElseIf RadioButton5.Checked = True Then
            If Conta < 20 Then
                MsgBox("Não tens dinheiro soficiente para apostar")
                RadioButton5.Checked = False
            Else
                Conta = Conta - 20
                bloqueios()
            End If
        ElseIf RadioButton6.Checked = True Then
            If Conta < 50 Then
                MsgBox("Não tens dinheiro soficiente para apostar")
                RadioButton6.Checked = False
            Else
                Conta = Conta - 50
                bloqueios()
            End If
        End If

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        A1 = InputBox("Diz a tua aposta de 1 a 10:", "Aposta 1", "1")
        Label5.Text = A1
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        A2 = InputBox("Diz a tua aposta de 1 a 10:", "Aposta 2", "1")
        Label6.Text = A2
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        A3 = InputBox("Diz a tua aposta de 1 a 10:", "Aposta 3", "1")
        Label7.Text = A3
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Inicial.Show()
        Me.Close()
    End Sub

    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click
        Randomize()
        n1 = (10 * Rnd() + 1)
        n2 = (10 * Rnd() + 1)
        n3 = (10 * Rnd() + 1)
        Label15.Text = n1
        Label14.Text = n2
        Label13.Text = n3

        Button8.Enabled = False
        Button11.Enabled = True

    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Button1.Enabled = False
        Button3.Enabled = False
        Button4.Enabled = False
        Button5.Enabled = False
        Button8.Enabled = True

    End Sub

    Private Sub Button11_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button11.Click
        If n1 = A1 Then
            MsgBox("Muito bem fizeste uma boa aposta no Primeiro número! Por isso ganhas metade do valor apostaste.")
            receber()
        Else
            MsgBox("Falhaste no Primeiro número!")
        End If

        If n2 = A2 Then
            MsgBox("Muito bem fizeste uma boa aposta no Segundo número! Por isso ganhas metade do valor apostaste.")
            receber()
        Else
            MsgBox("Falhaste no Segundo número!")
        End If

        If n3 = A3 Then
            MsgBox("Muito bem fizeste uma boa aposta no Treceiro número! Por isso ganhas metade do valor apostaste.")
            receber()
        Else
            MsgBox("Falhaste no Treceiro número!")
        End If
    End Sub

    Private Sub Button10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button10.Click
        MsgBox("Tu tens: " & Conta & "$ na sua conta")
    End Sub

    Private Sub Button9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button9.Click
        RadioButton1.Enabled = True
        RadioButton2.Enabled = True
        RadioButton3.Enabled = True
        RadioButton4.Enabled = True
        RadioButton5.Enabled = True
        RadioButton6.Enabled = True

        Button1.Enabled = False
        Button3.Enabled = False
        Button4.Enabled = False
        Button5.Enabled = False
        Button8.Enabled = False
        Button11.Enabled = False
        Button12.Enabled = True

        A1 = A2 = A3 = n1 = n2 = n3 = 0

        Label13.Text = "Resultado"
        Label14.Text = "Resultado"
        Label15.Text = "Resultado"

        Label5.Text = "Aposta"
        Label6.Text = "Aposta"
        Label7.Text = "Aposta"
    End Sub

    Private Sub _3Numeros_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        A1 = A2 = A3 = n1 = n2 = n3 = 0
    End Sub
End Class