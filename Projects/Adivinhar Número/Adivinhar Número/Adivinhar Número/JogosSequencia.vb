﻿Public Class JogosSequencia
    Dim Ntentaivas, NMaximoTentativasSequencia, Nivel As Byte
    Dim NRandom, tentativa As Integer
    Dim Vitoria As Boolean = False
    Private Sub JogosSequencia_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Loading Inicial'
            MsgBox("Aqui tu tens oito niveis por completar, e o teu objetivo é obvio, completar todos os niveis. Cada vez é mais dificil.")
        iniciar()
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        'Sair'
        TelaInicial.Show()
        Me.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        'Nivel Seguinte'
        'Se tiver vencido'
        If Vitoria = True Then
            'Aumentar o nivel e aprentar valores
            Nivel += 1
            Ntentaivas = 0
            'libertar butão
            Button2.Enabled = True
            If Nivel = 1 Then
                NMaximo = 6
                NMaximoTentativas = 2
                Randomize()
                NRandom = Int(NMaximo * Rnd())
                MsgBox("Estás no nivel: " & Nivel & " por isso tens " & NMaximoTentativas & " e o número escondido é de 0 a " & NMaximo - 1)
            ElseIf Nivel = 2 Then
                NMaximo = 11
                NMaximoTentativas = 3
                Randomize()
                NRandom = Int(NMaximo * Rnd())
                MsgBox("Estás no nivel: " & Nivel & " por isso tens " & NMaximoTentativas & " e o número escondido é de 0 a " & NMaximo - 1)
            ElseIf Nivel = 3 Then
                NMaximo = 51
                NMaximoTentativas = 5
                Randomize()
                NRandom = Int(NMaximo * Rnd())
                MsgBox("Estás no nivel: " & Nivel & " por isso tens " & NMaximoTentativas & " e o número escondido é de 0 a " & NMaximo - 1)
            ElseIf Nivel = 4 Then
                NMaximo = 101
                NMaximoTentativas = 7
                Randomize()
                NRandom = Int(NMaximo * Rnd())
                MsgBox("Estás no nivel: " & Nivel & " por isso tens " & NMaximoTentativas & " e o número escondido é de 0 a " & NMaximo - 1)
            ElseIf Nivel = 5 Then
                NMaximo = 251
                NMaximoTentativas = 9
                Randomize()
                NRandom = Int(NMaximo * Rnd())
                MsgBox("Estás no nivel: " & Nivel & " por isso tens " & NMaximoTentativas & " e o número escondido é de 0 a " & NMaximo - 1)
            ElseIf Nivel = 6 Then
                NMaximo = 501
                NMaximoTentativas = 12
                Randomize()
                NRandom = Int(NMaximo * Rnd())
                MsgBox("Estás no nivel: " & Nivel & " por isso tens " & NMaximoTentativas & " e o número escondido é de 0 a " & NMaximo - 1)
            ElseIf Nivel = 7 Then
                NMaximo = 751
                NMaximoTentativas = 14
                Randomize()
                NRandom = Int(NMaximo * Rnd())
                MsgBox("Estás no nivel: " & Nivel & " por isso tens " & NMaximoTentativas & " e o número escondido é de 0 a " & NMaximo - 1)
            ElseIf Nivel = 8 Then
                NMaximo = 1001
                NMaximoTentativas = 15
                Randomize()
                NRandom = Int(NMaximo * Rnd())
                MsgBox("Estás no nivel: " & Nivel & " por isso tens " & NMaximoTentativas & " e o número escondido é de 0 a " & NMaximo - 1)
            End If
            'Apresentar valores
            Label8.Text = "0 - " & NMaximoTentativas - 1
            Label9.Text = Nivel
            Label3.Text = Ntentaivas & "/" & NMaximoTentativas
        End If
        'Fechar vitória do nivel e libertação de butão
        Vitoria = False
        Button1.Enabled = False
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        'Validar'
        'Ler aposta do jogardor
        tentativa = TextBox1.Text
        'Validação de número
        If ((tentativa < 0) Or (tentativa > NMaximo - 1)) Then
            MsgBox("Número invalido!")
        Else
            'Aumento de tentativas
            Ntentaivas += 1

            'Declaração de acertos/errados
            If tentativa = NRandom Then
                MsgBox("Muito bem acertas-te no número")
                Label4.Text = "Acertas-te"
                'Declaração vitória
                Vitoria = True
                Button1.Enabled = True
                Button2.Enabled = False
                'Se acabar a sequancia de 8 então será libertado o nivel lendário
                If Nivel = 8 Then
                    MsgBox("Muito bem completas-te esta sequencia de 8 niveis. E com isso desbloqueas-te o nivel lendário. Vai até ás opções!")
                    Button1.Enabled = False
                    Lendario = True
                End If
                'Errado
            ElseIf tentativa < NRandom Then
                Label4.Text = "PEQUENO"
                'Limitação de tentativas
                If Ntentaivas >= NMaximoTentativas Then
                    MsgBox("Não tens mais tentativas disponiveis! Perdes-te o Nivel!")
                    Button2.Enabled = False
                End If
            Else
                Label4.Text = "GRANDE"
                'Limitação de tentativas
                If Ntentaivas >= NMaximoTentativas Then
                    MsgBox("Não tens mais tentativas disponiveis! Perdes-te o Nivel!")
                    Button2.Enabled = False
                End If
            End If

            'Apresentação dos Valores
            Label6.Text = tentativa
            Label3.Text = Ntentaivas & "/" & NMaximoTentativas
        End If
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        'Reiniciar'
        iniciar()
    End Sub
    Sub iniciar()
        'Declarar o nivel 1
        NMaximo = 6
        NMaximoTentativas = 2
        Nivel = 1
        Ntentaivas = 0

        'Criação de número aleatório
        Randomize()
        NRandom = Int(NMaximo * Rnd())
        'Apresentar valores
        Label3.Text = Ntentaivas & "/" & NMaximoTentativas
        Label9.Text = Nivel
        'Bloque no butão do nivel seguinte
        Button1.Enabled = False
    End Sub
End Class