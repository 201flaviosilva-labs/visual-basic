﻿Public Class Opções
    Dim soma As Byte = 1
    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        'Voltar'
        TelaInicial.Show()
        Me.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        'Validar'
        'Ler o até que número máximo estará escondido
        If RadioButton1.Checked Then
            NMaximo = 6
        ElseIf RadioButton2.Checked Then
            NMaximo = 11
        ElseIf RadioButton3.Checked Then
            NMaximo = 51
        ElseIf RadioButton4.Checked Then
            NMaximo = 101
        ElseIf RadioButton5.Checked Then
            NMaximo = 251
        ElseIf RadioButton6.Checked Then
            NMaximo = 501
        ElseIf RadioButton7.Checked Then
            NMaximo = 751
        ElseIf RadioButton8.Checked Then
            NMaximo = 1001
        Else
            NMaximo = 6
        End If

        'Máximo de tentativas
        NMaximoTentativas = NumericUpDown1.Value

        'Dar informação das escolhas
        MsgBox("O máximo de tentativas escolhido foi de: " & NMaximoTentativas & " O máximo de escondido escolhido foi de: " & NMaximo - 1)
        Me.Close()
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        'Céditos'
        creditos.show()
        Me.Close()
    End Sub

    Private Sub RadioButton9_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton9.CheckedChanged
        'Libertar o lendário
        If Lendario = True Then
            RadioButton9.Visible = True
            NMaximo = 10001
        Else
            RadioButton9.Visible = False
        End If
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        'Ajudas'
        soma += 1
        'Mod = "%" = Resto de uma conta, exemplo (3/2=1) ou (8/4=0)'
        If soma Mod 2 = 0 Then
            Ajudas = True
            Label2.BackColor = Color.Green
        Else
            Ajudas = False
            Label2.BackColor = Color.Red
        End If
    End Sub
End Class