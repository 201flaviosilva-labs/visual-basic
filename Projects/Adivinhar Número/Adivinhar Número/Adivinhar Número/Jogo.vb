﻿Public Class Jogo
    Dim Ntentaivas As Byte
    Dim NRandom, tentativa, Diferenca, SerieV As Integer
    Dim Vitoria As Boolean

    'Ntentativas = Número de Tentativas'
    'NRandom = Número gerado aleatóriamente
    'Tentativa = Aposta do utilazdor
    'Diferença = diferençã entre a respoata do utilizador e o número gerado
    'SerieV = Sequencia de vitórias seguidas
    'Vitoria = Sinalisação de vitória na partida

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        'Voltar'
        TelaInicial.Show()
        Me.Close()
    End Sub

    Private Sub Jogo_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Tela Inicial'
        'Toturial'
        MsgBox("Como já deves ter escolhido nas opções o Número Máximo de Tentativas e o Número máximo escondido, agora é hora de aplicar as tuas artes de dedução. Tenta fazer o maior número de sequencias possivel.")
        'Motrar O Número de Tentativas Máximo'
        Label3.Text = Ntentaivas & "/" & NMaximoTentativas

        'Criação de Número Aleatório
        Randomize()
        NRandom = Int(NMaximo * Rnd())
        'Apresentação de valores'
        Label11.Text = "0/" & NMaximo - 1

        'Seajudas estiver ativo o butão ajuda está ativo
        If Ajudas = True Then
            Button3.Enabled = True
        Else
            Button3.Enabled = False
        End If
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        'Validar'
        'Ler "Aposta" do Útilizador'
        tentativa = TextBox1.Text
        'Funções Lógicas de invalides
        If ((tentativa < 0) Or (tentativa > NMaximo - 1)) Then
            MsgBox("Número invalido!")
        Else
            'Aumento de tentativas
            Ntentaivas += 1

            'Vitória'
            If tentativa = NRandom Then
                MsgBox("Muito bem acertas-te no número")
                Label4.Text = "Acertas-te"
                'Bloque de Botões de jogo
                Button2.Enabled = False
                Button3.Enabled = False
                Button5.Enabled = False
                'Aumento de series de Vitórias
                SerieV += 1
                'Asinalar a Vitória
                Vitoria = True
                'Derrotas'
            ElseIf tentativa < NRandom Then
                Label4.Text = "PEQUENO"
            Else
                Label4.Text = "GRANDE"
            End If

            'Colorir a label de ajuda
            If Ajudas = True Then
                CorAproximacao()
            End If

            'Apresentação de valores
            Label6.Text = tentativa
            Label3.Text = Ntentaivas & "/" & NMaximoTentativas

            'Finalizações de máximo de tentativas
            If Ntentaivas >= NMaximoTentativas Then
                MsgBox("Não tens mais tentativas disponiveis!")
                Button2.Enabled = False
                Button3.Enabled = False
                SerieV = 0
            End If
        End If

        'Apresentação de valores
        Label9.Text = SerieV
        TextBox1.Text = "0"
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        'Novo Número'
        'Criação de um novo número
        Randomize()
        NRandom = Int(NMaximo * Rnd())
        'Apresentação de valores
        Label3.Text = Ntentaivas & "/" & NMaximoTentativas
        Label4.Text = "0000"
        Label4.BackColor = Color.White
        Label6.Text = "0000"

        MsgBox("Um novo número entre 0 e " & NMaximo - 1 & " foi criado")
        'Libertar butoes
        Button2.Enabled = True
        Button5.Enabled = True
        If Ajudas = True Then
            Button3.Enabled = True
        Else
            Button3.Enabled = False
        End If

        ''Apresentação de valores
        tentativa = 0
        Ntentaivas = 0
        'O caso de vitórias ser falso'
        If Vitoria = False Then
            SerieV = 0
            Label9.Text = SerieV
        End If
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        'Desistir'
        'Inspiração
        MsgBox("Nunca mais voltes a dizistir!!!!")
        MsgBox("O teu número escondido é: " & NRandom)
        'Apresentação de valores
        Label11.Text = NRandom & "/" & NMaximo - 1
        SerieV = 0
        Label9.Text = SerieV
        'Bloque de Botoes
        Button2.Enabled = False
        Button3.Enabled = False
        Button5.Enabled = False
    End Sub

    Sub CorAproximacao()
        Diferenca = 0

        'Colorir de ajuda'
            Diferenca = 0
            If tentativa > NRandom Then
                Diferenca = tentativa - NRandom
                'Indicar uma ajuda'
            If Diferenca <= 5 Then
                Label4.BackColor = Color.Lime
            ElseIf Diferenca <= 10 Then
                Label4.BackColor = Color.Aquamarine

            ElseIf Diferenca <= 15 Then
                Label4.BackColor = Color.OliveDrab

            ElseIf Diferenca <= 25 Then
                Label4.BackColor = Color.Green

            ElseIf Diferenca <= 50 Then
                Label4.BackColor = Color.DarkGreen

            ElseIf Diferenca <= 75 Then
                Label4.BackColor = Color.Salmon

            ElseIf Diferenca <= 100 Then
                Label4.BackColor = Color.Red

            ElseIf Diferenca <= 250 Then
                Label4.BackColor = Color.Firebrick

            Else
                Label4.BackColor = Color.DarkRed
            End If

            ElseIf tentativa < NRandom Then
            Diferenca = NRandom - tentativa

                'Indicar uma ajuda'
            If Diferenca <= 5 Then
                Label4.BackColor = Color.Lime

            ElseIf Diferenca <= 10 Then
                Label4.BackColor = Color.Aquamarine

            ElseIf Diferenca <= 15 Then
                Label4.BackColor = Color.OliveDrab

            ElseIf Diferenca <= 25 Then
                Label4.BackColor = Color.Green

            ElseIf Diferenca <= 50 Then
                Label4.BackColor = Color.DarkGreen

            ElseIf Diferenca <= 75 Then
                Label4.BackColor = Color.Salmon

            ElseIf Diferenca <= 100 Then
                Label4.BackColor = Color.Red

            ElseIf Diferenca <= 250 Then
                Label4.BackColor = Color.Firebrick

            Else
                Label4.BackColor = Color.DarkRed
            End If
        Else
            Label4.BackColor = Color.Gold
        End If
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        'Ajuda'
        Diferenca = 0

        If tentativa > NRandom Then
            Diferenca = tentativa - NRandom

            'Indicar uma ajuda'
            If Diferenca <= 5 Then
                MsgBox("Tens de descer -5")
            ElseIf Diferenca <= 10 Then
                MsgBox("Tens de descer -10")

            ElseIf Diferenca <= 15 Then
                MsgBox("Tens de descer -15")

            ElseIf Diferenca <= 25 Then
                MsgBox("Tens de descer -25")

            ElseIf Diferenca <= 50 Then
                MsgBox("Tens de descer -50")

            ElseIf Diferenca <= 75 Then
                MsgBox("Tens de descer -75")

            ElseIf Diferenca <= 100 Then
                MsgBox("Tens de descer -100")

            ElseIf Diferenca <= 250 Then
                MsgBox("Tens de descer -250")

            Else
                MsgBox("Tens de descer - 500")
            End If

        ElseIf tentativa < NRandom Then
            Diferenca = NRandom - tentativa

            'Indicar uma ajuda'
            If Diferenca <= 5 Then
                MsgBox("Tens de subir +5")

            ElseIf Diferenca <= 10 Then
                MsgBox("Tens de subir +10")

            ElseIf Diferenca <= 15 Then
                MsgBox("Tens de subir +15")

            ElseIf Diferenca <= 25 Then
                MsgBox("Tens de subir +25")

            ElseIf Diferenca <= 50 Then
                MsgBox("Tens de subir +50")

            ElseIf Diferenca <= 75 Then
                MsgBox("Tens de subir +75")

            ElseIf Diferenca <= 100 Then
                MsgBox("Tens de subir +100")

            ElseIf Diferenca <= 250 Then
                MsgBox("Tens de subir +250")

            Else
                MsgBox("Tens de subir +500")
            End If
        End If
    End Sub
End Class