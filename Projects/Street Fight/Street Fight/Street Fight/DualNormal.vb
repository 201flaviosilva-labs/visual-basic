﻿Public Class DualNormal
    Dim Esquerda As Boolean = False
    Dim Direita As Boolean = False
    Dim CPUEsquerda1 As Boolean = False
    Dim CPUEsquerda2 As Boolean = False
    Dim CPUEsquerda3 As Boolean = False
    Dim CPUDireita1 As Boolean = False
    Dim CPUDireita2 As Boolean = False
    Dim CPUDireita3 As Boolean = False
    Dim EsquedaAtaque As Byte = 0
    Dim DireitaAtaque As Byte = 0
    Dim Pontos As Integer = 0
    Dim NovoCPU As Byte = 0
    Dim tempo As Byte = 0
    Dim Vidas As Byte = 0
    Dim InicioJogo As Boolean = True

    Sub Ataque()
        'Ataque'
        tempo = 0
        Randomize()
        If Esquerda = True Then
            EsquedaAtaque = Int(2 * Rnd() + 1)
            If EsquedaAtaque = 1 Then
                PictureBox8.Visible = True
            Else
                PictureBox9.Visible = True
            End If


            If CPUEsquerda1 = True Then
                PictureBox4.Visible = False
                Pontos += 1
            Else
                Falha()
            End If

        Else
            DireitaAtaque = Int(2 * Rnd() + 1)
            If EsquedaAtaque = 1 Then
                PictureBox6.Visible = True
            Else
                PictureBox7.Visible = True
            End If


            If CPUDireita1 = True Then
                PictureBox2.Visible = False
                Pontos += 1
            Else
                Falha()
            End If
        End If
        Esquerda = False
        Direita = False
        Label4.Text = Pontos
        NovaImagem()
    End Sub

    Sub VisibilidadeFalse()
        'Esconder Imagens
        PictureBox1.Visible = False
        PictureBox6.Visible = False
        PictureBox7.Visible = False
        PictureBox8.Visible = False
        PictureBox9.Visible = False
        PictureBox10.Visible = False
        PictureBox11.Visible = False

        Timer1.Enabled = True
    End Sub

    Sub Falha()
        'Falha 
        MsgBox("Falhas-te")

        If CPUEsquerda1 = True Then
            PictureBox11.Visible = True
        End If

        If CPUDireita1 = True Then
            PictureBox10.Visible = True
        End If

        Pontos = 0
        Label4.Text = Pontos

        Vidas += 1
        Label9.Text = Vidas & "/3"

        Timer1.Enabled = False

        If Vidas = 3 Then
            Button1.Enabled = False
            Button2.Enabled = False
            Button4.Enabled = False
            Button5.Enabled = False
        End If
    End Sub

    Sub NovaImagem()
        If CPUEsquerda1 = True Then
            CPUEsquerda1 = False

            PictureBox4.Visible = False
        End If

        If CPUDireita1 = True Then
            CPUDireita1 = False

            PictureBox2.Visible = False
        End If

        If CPUDireita2 = True Then
            CPUDireita2 = False
            CPUDireita1 = True

            PictureBox3.Visible = False
            PictureBox2.Visible = True
        End If

        If CPUEsquerda2 = True Then
            CPUEsquerda2 = False
            CPUEsquerda1 = True

            PictureBox5.Visible = False
            PictureBox4.Visible = True
        End If

        If CPUDireita3 = True Then
            CPUDireita3 = False
            CPUDireita2 = True

            PictureBox13.Visible = False
            PictureBox3.Visible = True
        End If

        If CPUEsquerda3 = True Then
            CPUEsquerda3 = False
            CPUEsquerda2 = True

            PictureBox12.Visible = False
            PictureBox5.Visible = True
        End If


        If InicioJogo = False Then
            Randomize()
            NovoCPU = Int(2 * Rnd() + 1)
            If NovoCPU = 1 Then
                PictureBox12.Visible = True
                CPUEsquerda3 = True
            Else
                PictureBox13.Visible = True
                CPUDireita3 = True
            End If
        End If
        InicioJogo = False
    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        'Timer
        tempo += 1
        Label2.Text = tempo & "/5"
        If tempo = 5 Then
            MsgBox("Tempo Acabou")
            Timer1.Enabled = False
            Falha()
        End If
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        'Reiniciar
        tempo = 0
        Pontos = 0
        Vidas = 0
        Button1.Enabled = True
        Button2.Enabled = True
        Button4.Enabled = True
        Button5.Enabled = True
        Timer1.Enabled = True
        Label2.Text = tempo & "/5"
        Label4.Text = Pontos
        Label9.Text = Vidas & "/5"
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        'Butão para ataque á direita
        Direita = True
        VisibilidadeFalse()
        Ataque()
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        'Butão para ataque á Esquerda
        Esquerda = True
        VisibilidadeFalse()
        Ataque()
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        'Iniciar
        Button1.Enabled = True
        Button2.Enabled = True
        Button3.Enabled = True
        Button4.Enabled = True
        Button5.Enabled = True
        Timer1.Enabled = True

        Button6.Visible = False
    End Sub

    Private Sub Button1_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        'Butão para ataque á Esquerda
        Esquerda = True
        VisibilidadeFalse()
        Ataque()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        'Butão para ataque á direita
        Direita = True
        VisibilidadeFalse()
        Ataque()
    End Sub

    Private Sub DualNormal_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Loading Inicial
        PictureBox5.Visible = True
        PictureBox2.Visible = True

        CPUDireita2 = True
        CPUEsquerda1 = True
        CPUEsquerda3 = True

        NovaImagem()
    End Sub
End Class