﻿Public Class Inicial

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Combate.Show()
    End Sub

    Private Sub Inicial_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Loading Inicial

        ''Exemplo ---------------------------------------
        ' NomeCpu(1) = "Exemplo"
        ' VidaCpu(1) = 1 000 Max.
        'EscudoCpu(1) = 1 000 Max.
        ' ForcaCpu(1) = 100 Max.
        ' ResistenciaCpu(1) = 100 Max.
        ' AgilidadeCpu(1) = 100 Max.
        'InteligenciaCpu(1) = 100 Max.
        'PercepcaoCpu(1) = 100 Max.


        '-----------------------------------------------------------------------------------------------------------
        '-------------------------------------------- DC Comics ----------------------------------------------------
        '-----------------------------------------------------------------------------------------------------------
        Dim y As Integer = 0
        For x As Integer = 1 To 50
            Randomize()
            y = Int(1000 * Rnd())
            VidaCpu(x) = 1000
            EscudoCpu(x) = y

        Next

        'Mulher Maravilha ---------------------------------------
        NomeCpu(1) = "Mulher Maravilha"

        ForcaCpu(1) = 65
        ResistenciaCpu(1) = 60
        AgilidadeCpu(1) = 75
        InteligenciaCpu(1) = 60
        PercepcaoCpu(1) = 80
        VelocidadeCpu(1) = 65


        'Super-Homem ---------------------------------------
        NomeCpu(2) = "Super-Homem"

        ForcaCpu(2) = 100
        ResistenciaCpu(2) = 75
        AgilidadeCpu(2) = 60
        InteligenciaCpu(2) = 60
        PercepcaoCpu(2) = 70
        VelocidadeCpu(2) = 85


        'Super-Girl ---------------------------------------
        NomeCpu(3) = "Super-Girl"

        ForcaCpu(3) = 100
        ResistenciaCpu(3) = 70
        AgilidadeCpu(3) = 70
        InteligenciaCpu(3) = 50
        PercepcaoCpu(3) = 80
        VelocidadeCpu(3) = 90


        'Batman ---------------------------------------
        NomeCpu(4) = "Batman"

        ForcaCpu(4) = 60
        ResistenciaCpu(4) = 60
        AgilidadeCpu(4) = 90
        InteligenciaCpu(4) = 90
        PercepcaoCpu(4) = 80
        VelocidadeCpu(4) = 75


        'Flash ---------------------------------------
        NomeCpu(5) = "Flash"

        ForcaCpu(5) = 50
        ResistenciaCpu(5) = 90
        AgilidadeCpu(5) = 100
        InteligenciaCpu(5) = 50
        PercepcaoCpu(5) = 75
        VelocidadeCpu(5) = 90


        'Lanterna Verde ---------------------------------------
        NomeCpu(6) = "Lanterna Verde"

        ForcaCpu(6) = 60
        ResistenciaCpu(6) = 50
        AgilidadeCpu(6) = 55
        InteligenciaCpu(6) = 40
        PercepcaoCpu(6) = 50
        VelocidadeCpu(6) = 50


        'Aqua-Men ---------------------------------------
        NomeCpu(7) = "Aqua-Men"

        ForcaCpu(7) = 0
        ResistenciaCpu(7) = 0
        AgilidadeCpu(7) = 0
        InteligenciaCpu(7) = 0
        PercepcaoCpu(7) = 0
        VelocidadeCpu(7) = 0


        'Ciborgue  ---------------------------------------
        NomeCpu(8) = "Ciborgue "

        ForcaCpu(8) = 0
        ResistenciaCpu(8) = 0
        AgilidadeCpu(8) = 0
        InteligenciaCpu(8) = 0
        PercepcaoCpu(8) = 0
        VelocidadeCpu(8) = 0


        'Senhor Destino ---------------------------------------
        NomeCpu(9) = "Senhor Destino"

        ForcaCpu(9) = 45
        ResistenciaCpu(9) = 40
        AgilidadeCpu(9) = 40
        InteligenciaCpu(9) = 55
        PercepcaoCpu(9) = 60
        VelocidadeCpu(9) = 0


        'Joker ---------------------------------------
        NomeCpu(10) = "Joker"

        ForcaCpu(10) = 30
        ResistenciaCpu(10) = 30
        AgilidadeCpu(10) = 40
        InteligenciaCpu(10) = 90
        PercepcaoCpu(10) = 50
        VelocidadeCpu(10) = 40


        'Harley Quinn ---------------------------------------
        NomeCpu(11) = "Harley Quinn"

        ForcaCpu(11) = 50
        ResistenciaCpu(11) = 30
        AgilidadeCpu(11) = 50
        InteligenciaCpu(11) = 30
        PercepcaoCpu(11) = 45
        '-------------------------------------------------------------------

        'CatWoman ---------------------------------------
        NomeCpu(12) = "CatWoman"

        ForcaCpu(12) = 50
        ResistenciaCpu(12) = 75
        AgilidadeCpu(12) = 85
        InteligenciaCpu(12) = 70
        PercepcaoCpu(12) = 80


        'Batgirl ---------------------------------------
        NomeCpu(13) = "Batgirl"

        ForcaCpu(13) = 0
        ResistenciaCpu(13) = 0
        AgilidadeCpu(13) = 0
        InteligenciaCpu(13) = 0
        PercepcaoCpu(13) = 0


        'Arqueiro Verde ---------------------------------------
        NomeCpu(14) = "Arqueiro Verde"

        ForcaCpu(14) = 0
        ResistenciaCpu(14) = 0
        AgilidadeCpu(14) = 0
        InteligenciaCpu(14) = 0
        PercepcaoCpu(14) = 0


        'Hellboy ---------------------------------------
        NomeCpu(15) = "Hellboy"

        ForcaCpu(15) = 0
        ResistenciaCpu(15) = 0
        AgilidadeCpu(15) = 0
        InteligenciaCpu(15) = 0
        PercepcaoCpu(15) = 0


        'Shazam ---------------------------------------
        NomeCpu(16) = "Shazam"

        ForcaCpu(16) = 0
        ResistenciaCpu(16) = 0
        AgilidadeCpu(16) = 0
        InteligenciaCpu(16) = 0
        PercepcaoCpu(16) = 0


        'Katana ---------------------------------------
        NomeCpu(17) = "Katana"

        ForcaCpu(17) = 0
        ResistenciaCpu(17) = 0
        AgilidadeCpu(17) = 0
        InteligenciaCpu(17) = 0
        PercepcaoCpu(17) = 0


        'Caçador de Marte ---------------------------------------
        NomeCpu(18) = "Caçador de Marte"

        ForcaCpu(18) = 70
        ResistenciaCpu(18) = 60
        AgilidadeCpu(18) = 60
        InteligenciaCpu(18) = 80
        PercepcaoCpu(18) = 90


        'Asa Noturna ---------------------------------------
        NomeCpu(19) = "Asa Noturna"

        ForcaCpu(19) = 0
        ResistenciaCpu(19) = 0
        AgilidadeCpu(19) = 0
        InteligenciaCpu(19) = 0
        PercepcaoCpu(19) = 0


        'Deathstroke ---------------------------------------
        NomeCpu(20) = "Deathstroke"

        ForcaCpu(20) = 0
        ResistenciaCpu(20) = 0
        AgilidadeCpu(20) = 0
        InteligenciaCpu(20) = 0
        PercepcaoCpu(20) = 0

        'Robin ---------------------------------------
        NomeCpu(21) = "Robin"

        ForcaCpu(21) = 0
        ResistenciaCpu(21) = 0
        AgilidadeCpu(21) = 0
        InteligenciaCpu(21) = 0
        PercepcaoCpu(21) = 0


        'Canário Negro ---------------------------------------
        NomeCpu(22) = "Canário Negro"

        ForcaCpu(22) = 0
        ResistenciaCpu(22) = 0
        AgilidadeCpu(22) = 0
        InteligenciaCpu(22) = 0
        PercepcaoCpu(22) = 0

        'Starfire ---------------------------------------
        NomeCpu(23) = "Starfire"

        ForcaCpu(23) = 0
        ResistenciaCpu(23) = 0
        AgilidadeCpu(23) = 0
        InteligenciaCpu(23) = 0
        PercepcaoCpu(2) = 0

        'Ravena ---------------------------------------
        NomeCpu(24) = "Ravena"

        ForcaCpu(24) = 0
        ResistenciaCpu(24) = 0
        AgilidadeCpu(24) = 0
        InteligenciaCpu(24) = 0
        PercepcaoCpu(24) = 0

        'Raio Negro ---------------------------------------
        NomeCpu(25) = "Raio Negro"

        ForcaCpu(25) = 55
        ResistenciaCpu(25) = 50
        AgilidadeCpu(25) = 65
        InteligenciaCpu(25) = 50
        PercepcaoCpu(25) = 70

        'Mera ---------------------------------------
        NomeCpu(26) = "Mera"

        ForcaCpu(26) = 0
        ResistenciaCpu(26) = 0
        AgilidadeCpu(26) = 0
        InteligenciaCpu(26) = 0
        PercepcaoCpu(26) = 0

        '-----------------------------------------------------------------------------------------------------------
        '---------------------------------------------- Marvel -----------------------------------------------------
        '-----------------------------------------------------------------------------------------------------------

        'Deadpool ---------------------------------------
        NomeCpu(27) = "Deadpool"

        ForcaCpu(27) = 70
        ResistenciaCpu(27) = 100
        AgilidadeCpu(27) = 95
        InteligenciaCpu(27) = 0
        PercepcaoCpu(27) = 70

        'Cavaleiro da Lua ---------------------------------------
        NomeCpu(28) = "Cavaleiro da Lua"

        ForcaCpu(28) = 0
        ResistenciaCpu(28) = 0
        AgilidadeCpu(28) = 0
        InteligenciaCpu(28) = 0
        PercepcaoCpu(28) = 0

        'Homem de Ferro ---------------------------------------
        NomeCpu(29) = "Homem de Ferro"

        ForcaCpu(29) = 75
        ResistenciaCpu(29) = 70
        AgilidadeCpu(29) = 65
        InteligenciaCpu(29) = 90
        PercepcaoCpu(29) = 80

        'Thor ---------------------------------------
        NomeCpu(30) = "Thor"

        ForcaCpu(30) = 90
        ResistenciaCpu(30) = 75
        AgilidadeCpu(30) = 80
        InteligenciaCpu(30) = 70
        PercepcaoCpu(30) = 60

        'Hulk ---------------------------------------
        NomeCpu(31) = "Hulk"

        ForcaCpu(31) = 100
        ResistenciaCpu(31) = 60
        AgilidadeCpu(31) = 50
        InteligenciaCpu(31) = 30
        PercepcaoCpu(31) = 40

        'Homem-Aranha ---------------------------------------
        NomeCpu(32) = "Homem-Aranha"

        ForcaCpu(32) = 65
        ResistenciaCpu(32) = 70
        AgilidadeCpu(32) = 95
        InteligenciaCpu(32) = 95
        PercepcaoCpu(32) = 100

        'Capitão América ---------------------------------------
        NomeCpu(33) = "Capitão América"

        ForcaCpu(33) = 0
        ResistenciaCpu(33) = 0
        AgilidadeCpu(33) = 0
        InteligenciaCpu(33) = 0
        PercepcaoCpu(33) = 0

        'Capitã Marvel ---------------------------------------
        NomeCpu(34) = "Capitã Marvel"

        ForcaCpu(34) = 100
        ResistenciaCpu(34) = 95
        AgilidadeCpu(34) = 85
        InteligenciaCpu(34) = 70
        PercepcaoCpu(34) = 75

        'Viúva Negra ---------------------------------------
        NomeCpu(35) = "Viúva Negra"

        ForcaCpu(35) = 35
        ResistenciaCpu(35) = 50
        AgilidadeCpu(35) = 90
        InteligenciaCpu(35) = 85
        PercepcaoCpu(35) = 65

        'Black Panther ---------------------------------------
        NomeCpu(36) = "Black Panther"

        ForcaCpu(36) = 0
        ResistenciaCpu(36) = 0
        AgilidadeCpu(36) = 0
        InteligenciaCpu(36) = 0
        PercepcaoCpu(36) = 0

        'Homem-Formiga ---------------------------------------
        NomeCpu(37) = "Homem-Formiga"

        ForcaCpu(37) = 0
        ResistenciaCpu(37) = 0
        AgilidadeCpu(37) = 0
        InteligenciaCpu(37) = 0
        PercepcaoCpu(37) = 0

        'Wolverine  ---------------------------------------
        NomeCpu(38) = "Wolverine"

        ForcaCpu(38) = 0
        ResistenciaCpu(38) = 0
        AgilidadeCpu(38) = 0
        InteligenciaCpu(38) = 0
        PercepcaoCpu(38) = 0

        'Nick Fury ---------------------------------------
        NomeCpu(39) = "Nick Fury"

        ForcaCpu(39) = 0
        ResistenciaCpu(39) = 0
        AgilidadeCpu(39) = 0
        InteligenciaCpu(39) = 0
        PercepcaoCpu(39) = 0

        'Stan Lee ---------------------------------------
        NomeCpu(40) = "Stan Lee"

        EscudoCpu(40) = 100
        ForcaCpu(40) = 100
        ResistenciaCpu(40) = 100
        AgilidadeCpu(40) = 100
        InteligenciaCpu(40) = 100
        PercepcaoCpu(40) = 100


        'Tocha Humana ---------------------------------------
        NomeCpu(41) = "Tocha Humana"

        ForcaCpu(41) = 0
        ResistenciaCpu(41) = 0
        AgilidadeCpu(41) = 0
        InteligenciaCpu(41) = 0
        PercepcaoCpu(41) = 0

        'Mulher Invisível ---------------------------------------
        NomeCpu(42) = "Mulher Invisível"

        ForcaCpu(42) = 0
        ResistenciaCpu(42) = 0
        AgilidadeCpu(42) = 0
        InteligenciaCpu(42) = 0
        PercepcaoCpu(42) = 0

        'Senhor Fantástico ---------------------------------------
        NomeCpu(43) = "Senhor Fantástico"

        ForcaCpu(43) = 0
        ResistenciaCpu(43) = 0
        AgilidadeCpu(43) = 0
        InteligenciaCpu(43) = 0
        PercepcaoCpu(43) = 0

        'Doutor Estranho ---------------------------------------
        NomeCpu(44) = "Doutor Estranho"

        ForcaCpu(44) = 0
        ResistenciaCpu(44) = 0
        AgilidadeCpu(44) = 0
        InteligenciaCpu(44) = 0
        PercepcaoCpu(44) = 0

        'Coisa ---------------------------------------
        NomeCpu(45) = "Coisa"

        ForcaCpu(45) = 0
        ResistenciaCpu(45) = 0
        AgilidadeCpu(45) = 0
        InteligenciaCpu(45) = 0
        PercepcaoCpu(45) = 0


        'Punho de Ferro ---------------------------------------
        NomeCpu(46) = "Punho de Ferro"

        ForcaCpu(46) = 0
        ResistenciaCpu(46) = 0
        AgilidadeCpu(46) = 0
        InteligenciaCpu(46) = 0
        PercepcaoCpu(46) = 0

        'Jessica Jones ---------------------------------------
        NomeCpu(47) = "Jessica Jones"

        ForcaCpu(47) = 0
        ResistenciaCpu(47) = 0
        AgilidadeCpu(47) = 0
        InteligenciaCpu(47) = 0
        PercepcaoCpu(47) = 0

        'Demolidor ---------------------------------------
        NomeCpu(48) = "Demolidor"

        ForcaCpu(48) = 0
        ResistenciaCpu(48) = 0
        AgilidadeCpu(48) = 0
        InteligenciaCpu(48) = 0
        PercepcaoCpu(48) = 0

        '-----------------------------------------------------------------------------------------------------------
        '---------------------------------------------- Outros -----------------------------------------------------
        '-----------------------------------------------------------------------------------------------------------


        'Aguia Vermelha ---------------------------------------
        NomeCpu(49) = "Aguia Vermelha"

        ForcaCpu(49) = 0
        ResistenciaCpu(49) = 0
        AgilidadeCpu(49) = 0
        InteligenciaCpu(49) = 0
        PercepcaoCpu(49) = 0

        'Son Goku ---------------------------------------
        NomeCpu(50) = "Son Goku"

        ForcaCpu(50) = 0
        ResistenciaCpu(50) = 0
        AgilidadeCpu(50) = 0
        InteligenciaCpu(50) = 0
        PercepcaoCpu(50) = 0
    End Sub
End Class