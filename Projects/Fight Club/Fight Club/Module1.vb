﻿Module Module1
    'Cup - Computador
    Public NomeCpu(50) As String
    Public ImagemCpu(50) As PictureBox
    Public VidaCpu(50) As Integer
    Public EscudoCpu(50) As Integer

    Public ForcaCpu(50) As Integer
    Public ResistenciaCpu(50) As Integer
    Public AgilidadeCpu(50) As Integer
    Public InteligenciaCpu(50) As Integer
    Public PercepcaoCpu(50) As Integer
    Public VelocidadeCpu(50) As Integer

    ' Jogador
    Public NomeJogador As String
    Public VidaJogador As Integer
    Public EscudoJogador As Integer
    Public ForcaJogador As Integer
    Public ResistenciaJogador As Integer
    Public AgilidadeJogador As Integer
    Public InteligenciaJogador As Integer
    Public PercepcaoJogador As Integer
    Public VelocidadeJogador As Integer
    Public DinheiroJogador As Integer
End Module
